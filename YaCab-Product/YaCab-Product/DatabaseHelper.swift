//
//  DatabaseHelper.swift
//  DatabaseDemo
//
//  Created by Mohammed Al Khalidi on 2/12/15.
//  Copyright (c) 2015 Mohammed Al Khalidi. All rights reserved.
//

import CoreData
import UIKit
class DatabaseHelper {
    func deleteCredentials(){
            let appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            let deleteContext : NSManagedObjectContext  = appDel.managedObjectContext!

            let fetchDataRequest = NSFetchRequest(entityName: "User")
            fetchDataRequest.returnsObjectsAsFaults=false
            let results : NSArray = try! deleteContext.executeFetchRequest(fetchDataRequest)
        
            if (results.count == 0) {
                return;
            }
            for entity in results {
                
                deleteContext.deleteObject(entity as! NSManagedObject)
            }
            do {
                try deleteContext.save()
            } catch _ {
            }
            deleteContext.reset()
        }
    func isSignedIn() -> Bool{
        let appDel = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        let fetchDataRequest = NSFetchRequest(entityName: "User")
        fetchDataRequest.returnsObjectsAsFaults=false
        let results : NSArray = try! context.executeFetchRequest(fetchDataRequest)
        //print(results)
        if(results.count>0){
            return true
        }
        else
        {
            return false
        }
    }
    
    func SaveCredentials(id : UInt,login : String,password : String) -> Bool{
        if login.isEmpty {
            return false
        }
        let appDel:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        let fetchDataRequest = NSFetchRequest(entityName: "User")
        fetchDataRequest.returnsObjectsAsFaults=false
        let results : NSArray = try! context.executeFetchRequest(fetchDataRequest)
        if(results.count==0){
        let newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) 
            newUser.setValue(id  , forKey: "id")
            newUser.setValue(login, forKey : "login")
            newUser.setValue(password, forKey: "password")
            do {
                try context.save()
            } catch _ {
            }
            return true
        }
        else {
            //print("Already Registered")
            do {
                try context.save()
            } catch _ {
            }
            return false
        }
    }
    func FetchUser() -> QBUUser
    {
        let appDel = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        let fetchDataRequest = NSFetchRequest()
        
        
        let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: context)
        fetchDataRequest.entity = entity
        fetchDataRequest.returnsObjectsAsFaults=false
        fetchDataRequest.fetchLimit = 1
        let results : NSArray = try! context.executeFetchRequest(fetchDataRequest)
        let user : QBUUser = QBUUser ()
        user.ID = 0
        user.login = ""
        user.password = ""
        if(results.count>0){
            for res in results{
                //print(res.description)
                user.ID = res.valueForKey("id")! as! UInt
                user.login = res.valueForKey("login") as! String
                user.password = res.valueForKey("password")! as! String
            }
            
            return user
        }
        else
        {
            return user
        }
    }
}

