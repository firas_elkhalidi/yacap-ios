//
//  MyRouteViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/17/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class MyRouteViewController: UIViewController,CurrentThemeDelegate, MKMapViewDelegate{
    //@IBOutlet weak var closeButton: UIButton!
    //@IBAction func closeButtonPressed(sender: UIButton) {
    //    self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
    //    })
    //}
    //@IBOutlet weak var mapViewTopConstraint: NSLayoutConstraint!
    /**
    method that detects when the edit button is pressed and takes the user to the MyRoutesViewController.
    - Parameter sender: holds the sender of the function.
    */
    @IBAction func editButtonPressed(sender: UIButton) {
        goToMyRoutesViewController()
    }
    ///variable that has the state of the routeMapView, whether its expanded, or retracted.
    var isExpanded = false
    ///instance of the map view that shows the route.
    @IBOutlet weak var routeMapView: MKMapView!
    ///instance of the edit button.
    @IBOutlet weak var editButton: UIButton!
    ///label that holds the name label
    @IBOutlet weak var locationNameLabel: UILabel!
    var presented : Bool = false
    ///hamburger style button on the top left in the navigation bar/
    @IBOutlet weak var menuButton: UIBarButtonItem!
    ///label that holds the name of the destination.
    @IBOutlet weak var destinationLocationNameLabel: UILabel!
    ///label that holds the name of the source.
    @IBOutlet weak var sourceLocationNameLabel: UILabel!
    ///holds the id of the current route of the user.
    var currentRouteID : String?
    ///holds the current route of the user as an object.
    var currentRoute : QBCOCustomObject?
    ///holds the currentPolyline of the user that is calculated using a Google API call.
    var currentPolyline : MKPolyline?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "My Route"
        self.navigationController?.navigationBar.translucent = false
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            editButton.hidden = true
        }
        if presented{
            let doneButton : UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("dismissSelf"))
            self.navigationItem.rightBarButtonItem = doneButton
            self.navigationItem.leftBarButtonItem = nil
            editButton.hidden = true
            //closeButton.hidden = false
        }
        routeMapView.delegate = self
        currentThemeDelegate = self
        let mapTapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleMapSize"))
        routeMapView.addGestureRecognizer(mapTapGestureRecognizer)
        setTheme()
        
    }
    ///method that dismisses the view controller.
    func dismissSelf(){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if isExpanded{
            handleMapSize()
        }
    }
    ///method that is called when the routeMapView is tapped using the mapTapGestureRecognizer.
    func handleMapSize(){
        isExpanded = !isExpanded
        if isExpanded{
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.routeMapView.frame = CGRect(origin: self.routeMapView.frame.origin, size: CGSize(width: self.routeMapView.frame.width, height: self.routeMapView.frame.height + 200))
                for scview in self.view.subviews{
                    if let scrollView = scview as? TPKeyboardAvoidingScrollView{
                        for sview in scrollView.subviews{
                            if let _ = sview as? MKMapView{
                                
                            }
                            else{
                                sview.frame = CGRect(x: sview.frame.origin.x, y: sview.frame.origin.y + 200, width: sview.frame.width, height: sview.frame.height)
                            }
                        }
                    }
                }
                }, completion: { (Bool) -> Void in
                    if self.routeMapView.annotations.count > 0{
                        self.routeMapView.showAnnotations(self.routeMapView.annotations, animated:true)
                        if self.currentPolyline != nil{
                            zoomToPolyLine(self.routeMapView, polyline: self.currentPolyline!, animated: true)
                        }
                    }
            })
            
        }
        else if !isExpanded{
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.routeMapView.frame = CGRect(origin: self.routeMapView.frame.origin, size: CGSize(width: self.routeMapView.frame.width, height: self.routeMapView.frame.height - 200))
                for scview in self.view.subviews{
                    if let scrollView = scview as? TPKeyboardAvoidingScrollView{
                        for sview in scrollView.subviews{
                            if let _ = sview as? MKMapView{
                                
                            }
                            else{
                                sview.frame = CGRect(x: sview.frame.origin.x, y: sview.frame.origin.y - 200, width: sview.frame.width, height: sview.frame.height)
                            }
                        }
                    }
                }
            }, completion: { (Bool) -> Void in
                if self.routeMapView.annotations.count > 0{
                    self.routeMapView.showAnnotations(self.routeMapView.annotations, animated:true)
                    if self.currentPolyline != nil{
                        zoomToPolyLine(self.routeMapView, polyline: self.currentPolyline!, animated: true)
                    }
                    
                }
            })
        }

    }
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer : MKPolylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = UIColor.blueColor()
        polylineRenderer.lineWidth = 3
        polylineRenderer.fillColor = UIColor.blueColor()
        return polylineRenderer
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        let view: MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        { // 2
            dequeuedView.annotation = annotation
            // Add a detail disclosure button to the callout.
            if annotation.title! == "Start"{
                var image : UIImage = UIImage(named: "Location.png")!
                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/1, height : image.size.height/1))
                dequeuedView.image = image
                dequeuedView.centerOffset = CGPointMake(0, -dequeuedView.image!.size.height / 2);
            }
            else if annotation.title! == "Finish"{
                var image : UIImage = UIImage(named: "Destination.png")!
                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/1, height : image.size.height/1))
                dequeuedView.image = image
                //dequeuedView.centerOffset = CGPointMake(0, -dequeuedView.image.size.height / 2);
            }
           
            
            return dequeuedView
            
            
        } else {
            // 3
            view.annotation = annotation
            view.canShowCallout = false
            if annotation.title! == "Start"{
                var image : UIImage = UIImage(named: "Location.png")!
                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/1, height : image.size.height/1))
                view.image = image
                view.centerOffset = CGPointMake(0, -view.image!.size.height / 2);
            }
            else if annotation.title! == "Finish"{
                var image : UIImage = UIImage(named: "Destination.png")!
                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/1, height : image.size.height/1))
                view.image = image
                //view.centerOffset = CGPointMake(0, -view.image.size.height / 2);
            }
            return view
        }
        //return view
    }
    ///method used to get the route of user using the currentRouteID.
    func getData(){
        if currentRouteID != nil{
            QBRequest.objectWithClassName("Route", ID: currentRouteID, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
                self.destinationLocationNameLabel.text = route.fields.valueForKey("destinationLocationName") as? String
                self.sourceLocationNameLabel.text = route.fields.valueForKey("sourceLocationName") as? String
                if (route.fields.valueForKey("name") as? String) != nil{
                    self.locationNameLabel.text = route.fields.valueForKey("name") as? String
                }
                else{
                    self.locationNameLabel.text = ""
                }
                
                self.currentRoute = QBCOCustomObject()
                self.currentRoute = route
                
                let sourceLat = route.fields.valueForKey("sourceLocationLatitude") as! Double

                let sourceLon = route.fields.valueForKey("sourceLocationLongitude") as! Double

                let destinationLat = route.fields.valueForKey("destinationLocationLatitude") as! Double

                let destinationLon = route.fields.valueForKey("destinationLocationLongitude") as! Double
                
                let urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + sourceLat.description + "," + sourceLon.description + "&destination=" + destinationLat.description + "," + destinationLon.description + "&sensor=false"
                //print("routeurl:" + urlString)
                //self.drawRouteHelper()
                GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                    if status{
                        if let routes: AnyObject = dictionary["routes"]{
                            if routes.count > 0{
                                if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                    if let polylineString : String = polylineDictionary["points"] as? String{
                                         self.currentPolyline = MKPolyline(encodedString: polylineString)
                                        //print("polyline: ")
                                        //print(self.currentPolyline!.coordinate.latitude.description + " " + self.currentPolyline!.coordinate.longitude.description)
                                        let sourceAnnotation : MKPointAnnotation = MKPointAnnotation()
                                        sourceAnnotation.coordinate.latitude = sourceLat
                                        sourceAnnotation.coordinate.longitude = sourceLon
                                        sourceAnnotation.title = "Start"
                                        let destinationAnnotation : MKPointAnnotation = MKPointAnnotation()
                                        destinationAnnotation.coordinate.latitude = destinationLat
                                        destinationAnnotation.coordinate.longitude = destinationLon
                                        destinationAnnotation.title = "Finish"
                                        self.routeMapView.addAnnotations([sourceAnnotation,destinationAnnotation])
                                        
                                        self.routeMapView.addOverlay(self.currentPolyline!, level: MKOverlayLevel.AboveLabels)
                                        self.routeMapView.showAnnotations(self.routeMapView.annotations, animated: true)
                                        zoomToPolyLine(self.routeMapView, polyline: self.currentPolyline!, animated: true)
                                        
                                    }
                                }
                            }
                        }
                        
                        
                    }
                    else if !status{
                        //print("routeDictionaryNotFound!")
                    }
                })

                
                
                
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
            
        else if currentRouteID == nil{
            //QBRequest.userWithLogin(DatabaseHelper().FetchUser().login, successBlock: { (response : QBResponse!,user :  QBUUser!) -> Void in
                //var goToMyRoutesButton : UIBarButtonItem = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("goToMyRoutesViewController"))
                if self.navigationItem.rightBarButtonItem == nil{
                    self.editButton.hidden = false
                }
                let userCustomData = getCustomDataDictionaryFromUser(currentUser)
            
                if let routeID = userCustomData?.valueForKey("activeRouteID") as? String{
                    if !routeID.isEmpty{
                        QBRequest.objectWithClassName("Route", ID: routeID, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
                            self.destinationLocationNameLabel.text = route.fields.valueForKey("destinationLocationName") as? String
                            self.sourceLocationNameLabel.text = route.fields.valueForKey("sourceLocationName") as? String
                            self.currentRoute = QBCOCustomObject()
                            self.currentRoute = route
                            if (route.fields.valueForKey("name") as? String) != nil{
                                self.locationNameLabel.text = route.fields.valueForKey("name") as? String
                            }
                            else{
                                self.locationNameLabel.text = ""
                            }
                            
                            

                            let sourceLat = route.fields.valueForKey("sourceLocationLatitude") as! Double
                            
                            let sourceLon = route.fields.valueForKey("sourceLocationLongitude") as! Double
                            
                            let destinationLat = route.fields.valueForKey("destinationLocationLatitude") as! Double
                            
                            let destinationLon = route.fields.valueForKey("destinationLocationLongitude") as! Double
                            
                            let urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + sourceLat.description + "," + sourceLon.description + "&destination=" + destinationLat.description + "," + destinationLon.description + "&sensor=false"
                            //print("routeurl:" + urlString)
                            //self.drawRouteHelper()
                            GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                                if status{
                                    if let routes: AnyObject = dictionary["routes"]{
                                        if routes.count > 0{
                                            if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                                if let polylineString : String = polylineDictionary["points"] as? String{
                                                    let polyline : MKPolyline = MKPolyline(encodedString: polylineString)
                                                    //print("polyline: ")
                                                    //print(polyline.coordinate.latitude.description + " " + polyline.coordinate.longitude.description)
                                                    let sourceAnnotation : MKPointAnnotation = MKPointAnnotation()
                                                    sourceAnnotation.coordinate.latitude = sourceLat
                                                    sourceAnnotation.coordinate.longitude = sourceLon
                                                    sourceAnnotation.title = "Start"
                                                    let destinationAnnotation : MKPointAnnotation = MKPointAnnotation()
                                                    destinationAnnotation.coordinate.latitude = destinationLat
                                                    destinationAnnotation.coordinate.longitude = destinationLon
                                                    destinationAnnotation.title = "Finish"
                                                    self.routeMapView.addAnnotations([sourceAnnotation,destinationAnnotation])
                                                    
                                                    self.routeMapView.addOverlay(polyline, level: MKOverlayLevel.AboveLabels)
                                                    self.routeMapView.showAnnotations(self.routeMapView.annotations, animated:true)
                                                    if self.currentPolyline != nil{
                                                        zoomToPolyLine(self.routeMapView, polyline: self.currentPolyline!, animated: true)
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                }
                                else if !status{
                                    //print("routeDictionaryNotFound!")
                                }
                            })

                            
                            
                            
                            
                            }, errorBlock: { (response : QBResponse!) -> Void in
                        })
                    }
                    
                }
                else {
                    userCustomData?.setValue("", forKey: "activeRouteID")
                    let error = NSErrorPointer()
                    do {
                        let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(userCustomData!, options: NSJSONWritingOptions.PrettyPrinted)
                        //print("JSON:", terminator: "")
                        let theJSONText = NSString(data: newUserCustomDataJSON,
                            encoding: NSASCIIStringEncoding)
                        //print(theJSONText)
                        currentUser.customData = String(theJSONText!)
                        let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                        userParameters.customData = String(theJSONText!)
                        QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                            }, errorBlock: { (response : QBResponse!) -> Void in
                        })
                    } catch let error1 as NSError {
                        error.memory = error1
                    }
                }
                //}) { (response : QBResponse!) -> Void in
                    
            //}
        }
    }
    ///method used to set the theme of the view controller.
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            self.editButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            self.editButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
        }
        
    }
    ///delegate method of the CurrentThemeDelegate that is used to update the theme of the view
    func themeDidChange() {
        setTheme()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///method that performs the segue that takes the user to the MyRoutesViewController
    func goToMyRoutesViewController(){
        isExpanded = false
        self.performSegueWithIdentifier("goToMyRoutesViewController", sender: self)
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRouteViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as![NSObject : AnyObject])
        getData()
    }
    ///method that detects the exit segue from the MyRoutesViewController to update to the new values
    @IBAction func exitSegueFromMyRoutesViewController(segue : UIStoryboardSegue){
        self.routeMapView.removeAnnotations(self.routeMapView.annotations)
        self.routeMapView.removeOverlays(self.routeMapView.overlays)
        getData()
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
