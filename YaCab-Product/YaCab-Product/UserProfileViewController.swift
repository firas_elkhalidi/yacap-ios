//
//  UserProfileViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/31/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController, CurrentThemeDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    var editButton : UIBarButtonItem = UIBarButtonItem()
    ///label that is above the route of the user
    @IBOutlet weak var routeLabel: UILabel!
    ///label that is above the details of the user
    @IBOutlet weak var aboutLabel: UILabel!
    ///grey seperator view
    @IBOutlet weak var seperatorView: UIView!
    ///username constraint between the top of the username text label and the car image view
    @IBOutlet weak var usernameVerticalConstraint: NSLayoutConstraint!
    ///star rating of the user
    @IBOutlet weak var starRating: CosmosView!
    ///image view indicating the gender of the user
    @IBOutlet weak var genderImageView: UIImageView!
    ///image view indicating whether the user has air conditioning or not
    @IBOutlet weak var acImageView: UIImageView!
    ///image view indicating whether the user is a smoker or not
    @IBOutlet weak var smokingImageView: UIImageView!
    ///separator view for the route area
    @IBOutlet weak var routeLabelLineView: UIView!
    ///separator view for the about area
    @IBOutlet weak var aboutLabelLineView: UIView!
    ///label for the destination of the route of the user
    @IBOutlet weak var destinationLabel: UILabel!
    ///label for the source of the route of the user
    @IBOutlet weak var sourceLabel: UILabel!
    ///scroll view that is below all the other views
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    ///car image view of the user
    @IBOutlet weak var carImageView: UIImageView!
    ///user image view of the user
    @IBOutlet weak var userImageView: UIImageView!
    ///user rating value label
    @IBOutlet weak var ratingLabel: UILabel!
    ///instance of the start negotiations button
    @IBOutlet weak var startNegotiationsButton: UIButton!
    ///label of the username
    @IBOutlet weak var usernameLabel: UILabel!
    ///car or address image view of the user
    @IBOutlet weak var carOrAddressImageView: UIImageView!
    /**
    method that detects when the start negotiations button is pressed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func startNegotiationsButtonPressed(sender: UIButton) {

        var numberOfSeats: Int?
        if let n = getCustomDataDictionaryFromUser(self.selectedUser!)?.valueForKey("numberOfSeats") as? Int{
            numberOfSeats = n
            //print("numberOfSeats:" + numberOfSeats!.description)
            
            
        }
        else{
            numberOfSeats = -1
        }
        
        getNumberOfCarpoolersForUserWithID(selectedUser!.ID, completion: { (numberOfCarpools) -> Void in
            if (getCustomDataDictionaryFromUser(self.selectedUser!)?.valueForKey("isDriver") as! Bool) && numberOfSeats != -1 && numberOfCarpools >= numberOfSeats{
                ////println("numberOfCarpools:" + numberOfCarpools.description + " vs. numberOfSeats:" + numberOfSeats!.description)
                
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.DRIVERFULLVEHICLE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction) -> Void in
                    //self.navigationController?.popViewControllerAnimated(true)
                })
                alert.addAction(alertOK)
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
            else if !(getCustomDataDictionaryFromUser(self.selectedUser!)?.valueForKey("isDriver") as! Bool) && numberOfCarpools > 0{
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERALREADYINCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                alert.addAction(alertOK)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else{
                QBRequest.userWithID(currentUser.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                    currentUser = user
                    if !(getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool){
                        if numberOfCurrentCarpoolers > 0{
                            let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERPASSENGERALREADYHASCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                            let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                            alert.addAction(alertOK)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else if !(getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool) {
                            let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERPASSENGERCARPOOLWITHPASSENGER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                            let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                            alert.addAction(alertOK)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else{
                            self.performSegueWithIdentifier("segueToNegotiationsViewControllerFromUserProfileViewController", sender: self)
                        }
                    }
                    else if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool) {
                        
                        
                        if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                            let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                            let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                            alert.addAction(alertOK)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                            
                        else if (((getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("activeRouteID") as! String).isEmpty)){
                            let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERHASNOROUTE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                            let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                //self.navigationController?.popViewControllerAnimated(true)
                            })
                            alert.addAction(alertOK)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                            
                        else if numberOfCurrentCarpoolers > 0{
                            if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else if numberOfCurrentCarpoolers >= Int((getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String)){
                                //print("")
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERNOTENOUGHSEATS.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else if (getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool) {
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else{
                                self.performSegueWithIdentifier("segueToNegotiationsViewControllerFromUserProfileViewController", sender: self)
                            }
                        }
                        else if (getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool) {
                            let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                            let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                            alert.addAction(alertOK)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else{
                            self.performSegueWithIdentifier("segueToNegotiationsViewControllerFromUserProfileViewController", sender: self)
                        }
                        
                    }
                    
                    }, errorBlock: { (response : QBResponse!) -> Void in
                })
            }
        })
        
    }
    
    ///car description label
    @IBOutlet weak var carDescriptionLabel: UILabel!
    ///hamburger button
    var menuButton: UIBarButtonItem!
    ///a boolean value indicating whether the view controller has a back button or not
    var hasBackButton : Bool = false
    ///user that is displayed in this view controller
    var selectedUser : QBUUser?
    func scrollViewDidScroll(sender: UIScrollView) {
            if (sender.contentOffset.x != 0) {
                var offset : CGPoint = sender.contentOffset;
                offset.x = 0;
                sender.contentOffset = offset;
            }
    }
    ///method called by the gesture recognizer when the user presses on the profile image
    func didPressOnProfileImage(gesture : UITapGestureRecognizer){
        if userImageView.image != nil{
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let imageViewerViewController = storyboard.instantiateViewControllerWithIdentifier("ImageViewerViewController") as! ImageViewerViewController
            imageViewerViewController.image = userImageView.image!
            self.presentViewController(imageViewerViewController, animated: true, completion: nil)
        }
    }
    ///method called by the gesture recognizer when the user presses on the car image
    func didPressOnCarImage(gesture : UITapGestureRecognizer){
        if carImageView.image != nil{
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let imageViewerViewController = storyboard.instantiateViewControllerWithIdentifier("ImageViewerViewController") as! ImageViewerViewController
            imageViewerViewController.image = carImageView.image!
            self.presentViewController(imageViewerViewController, animated: true, completion: nil)
        }
    }

    override func viewDidLoad() {
        ratingLabel.numberOfLines = 1
        ratingLabel.adjustsFontSizeToFitWidth = true
        ratingLabel.text = ""
        ratingLabel.minimumScaleFactor = 0.8/ratingLabel.font.pointSize;
        userImageView.userInteractionEnabled = true
        let carGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("didPressOnCarImage:"))
        let userGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("didPressOnProfileImage:"))
        userGestureRecognizer.delegate = self
        carGestureRecognizer.delegate = self
        userImageView.addGestureRecognizer(userGestureRecognizer)
        carImageView.addGestureRecognizer(carGestureRecognizer)
        carImageView.userInteractionEnabled = true
        userImageView.userInteractionEnabled = true
        starRating.settings.updateOnTouch = false
        starRating.settings.fillMode = StarFillMode.Precise
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentSizeToFit()
        if scrollView.frame.height < UIScreen.mainScreen().bounds.height{
            scrollView.frame = CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.width, UIScreen.mainScreen().bounds.height)
        }
        userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 6;
        userImageView.clipsToBounds = true;
        carImageView.contentMode = UIViewContentMode.ScaleAspectFill
        userImageView.contentMode = UIViewContentMode.ScaleAspectFill
        if !hasBackButton{
            menuButton = UIBarButtonItem(image: UIImage(named : "menu.png"), landscapeImagePhone: UIImage(named : "menu.png"), style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItem = menuButton
        }
        super.viewDidLoad()
        scrollView.delegate = self
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 6;
        //carImageView.image = UIImage(named: "CarPlaceholder.jpg")
        self.starRating.hidden = true
        self.seperatorView.hidden = true
        self.ratingLabel.hidden = true
        self.usernameVerticalConstraint.constant = 8
        if selectedUser == nil{
            self.editButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("editProfile"))
            self.navigationItem.rightBarButtonItem = self.editButton
        }
        setTheme()
        getProfile()
    }
    ///delegate method of the CurrentThemeDelegate
    func themeDidChange() {
        print("changedTheme")
        setTheme()
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        sourceLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        destinationLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        usernameLabel.textColor = UIColor.grayColor()
        aboutLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        routeLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        routeLabelLineView.backgroundColor = ApplicationThemeColor().LightGreyTextColor
        aboutLabelLineView.backgroundColor = ApplicationThemeColor().LightGreyTextColor
        if currentTheme == Theme.PassengerTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            startNegotiationsButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            startNegotiationsButton.setTitle("START NEGOTIATIONS", forState: UIControlState.Normal)
            startNegotiationsButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            startNegotiationsButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            startNegotiationsButton.setTitle("START NEGOTIATIONS", forState: UIControlState.Normal)
            startNegotiationsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
        }
        getProfile()
    }
    ///method that gets the profile of the user from the server
    func getProfile(){
        carOrAddressImageView.image = nil
        if selectedUser == nil{
            selectedUser = currentUser
            startNegotiationsButton.hidden = true
        }
        QBRequest.userWithID(selectedUser!.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            self.selectedUser = user
            //currentUser = user
            
            
            let extendedRequest : NSMutableDictionary = NSMutableDictionary()
            extendedRequest.setValue(self.selectedUser!.ID, forKey: "ratedUserID")
            

            
            if self.selectedUser != nil{
                QBRequest.objectWithClassName("Route", ID: getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("activeRouteID") as? String, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
                    self.sourceLabel.text = route.fields.valueForKey("sourceLocationName") as? String
                    self.destinationLabel.text = route.fields.valueForKey("destinationLocationName") as? String
                    }, errorBlock: { (reponse : QBResponse!) -> Void in
                        self.destinationLabel.text = "N/A"
                        self.sourceLabel.text = "N/A"
                })
            }
            

            //print("newselecteduser: ")
            //print(self.selectedUser)
            self.usernameLabel.text = user.login.uppercaseString
            if user.customData != nil{
                let str : String = user.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                    if let gender: String = json["genderType"] as? String{
                        if gender == "m"{
                            let image : UIImage = colorImage(UIImage(named: "male")!, color: ApplicationThemeColor().LightGreyTextColor)
                            self.genderImageView.image = image
                        }
                        else if gender == "f"{
                            let image : UIImage = colorImage(UIImage(named: "female")!, color: ApplicationThemeColor().LightGreyTextColor)
                            self.genderImageView.image = image
                        }
                    }

                    if let isDriver = json["isDriver"] as? Bool{
                        if !isDriver{
                            self.userImageView.hidden = true
                            self.starRating.hidden = true
                            self.seperatorView.hidden = true
                            self.ratingLabel.hidden = true
                            self.usernameVerticalConstraint.constant = 8
                            if let address = json["address"] as? String{
                                self.carDescriptionLabel.text = address
                                self.carOrAddressImageView.image = UIImage(named: "address")
                            }
                            self.acImageView.hidden = true
                            
                        }
                        else if isDriver{
                            if let hasAC: Bool = json["hasAC"] as? Bool{
                                self.acImageView.hidden = false
                                if hasAC{
                                    let image : UIImage = colorImage(UIImage(named: "AC")!, color: ApplicationThemeColor().LightGreyTextColor)
                                    self.acImageView.image = image
                                }
                                else{
                                    let image : UIImage = colorImage(UIImage(named: "NoAC")!, color: ApplicationThemeColor().LightGreyTextColor)
                                    self.acImageView.image = image
                                }
                            }
                            self.userImageView.hidden = false
                            self.starRating.hidden = false
                            self.seperatorView.hidden = false
                            self.ratingLabel.hidden = false
                            if let carModel = json["carModel"] as? String{
                                self.carOrAddressImageView.image = UIImage(named: "car")
                                self.carDescriptionLabel.text = carModel
                            }
                            UIView.animateWithDuration(0.3, animations: { () -> Void in
                                self.usernameVerticalConstraint.constant = 56
                            })
                            if let numberOfSeatsInCar = json["numberOfSeats"] as? String{
                                
                                getNumberOfCarpoolersForUserWithID(self.selectedUser!.ID, completion: { (numberOfCarpools) -> Void in
                                    let availableSeats = Int(numberOfSeatsInCar)! - numberOfCarpools
                                    self.ratingLabel.text = availableSeats.description + "/" + numberOfSeatsInCar + " available seats"
                                })
                            }
                            QBRequest.objectsWithClassName("UserRatingRecord", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, records : [AnyObject]!,  page : QBResponsePage!) -> Void in
                                if records != nil{
                                    if records.count > 0{
                                        if let record = records[0] as? QBCOCustomObject{
                                            var count = 0
                                            count = count + (record.fields.valueForKey("starRating1") as! Int)
                                            count = count + (record.fields.valueForKey("starRating2") as! Int)
                                            count = count + (record.fields.valueForKey("starRating3") as! Int)
                                            count = count + (record.fields.valueForKey("starRating4") as! Int)
                                            count = count + (record.fields.valueForKey("starRating5") as! Int)
                                            count = count + (record.fields.valueForKey("starRating1_5") as! Int)
                                            count = count + (record.fields.valueForKey("starRating2_5") as! Int)
                                            count = count + (record.fields.valueForKey("starRating3_5") as! Int)
                                            count = count + (record.fields.valueForKey("starRating4_5") as! Int)
                                            
                                            //self.ratingLabel.text = count.description + " ratings"
                                            self.starRating.rating = record.fields.valueForKey("rating") as! Double
                                        }
                                    }
                                }
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    
                            })
                        }
                    }
                    if let isSmoker = json["isSmoker"] as? Bool{
                        if !isSmoker{
                            let image : UIImage = colorImage(UIImage(named: "notsmoker")!, color: ApplicationThemeColor().LightGreyTextColor)
                            self.smokingImageView.image = image
                        }
                        else if isSmoker{
                            let image : UIImage = colorImage(UIImage(named: "smoker")!, color: ApplicationThemeColor().LightGreyTextColor)
                            self.smokingImageView.image = image
                        }
                    }

                }
            }
            if (getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool){
                if _imageCache!.objectForKey(self.selectedUser!.ID.description) != nil{
                    self.userImageView.image = UIImage(data: _imageCache!.objectForKey(self.selectedUser!.ID.description) as! NSData)!
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: self.selectedUser!.ID.description)
                    self.userImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.userImageView.image = UIImage(named: "placeholder.jpg")
                })
                
                if _imageCache!.objectForKey(self.selectedUser!.ID.description + "_car") != nil{
                    self.carImageView.image = UIImage(data: _imageCache!.objectForKey(self.selectedUser!.ID.description + "_car") as! NSData)!
                }
                if let carBlobID = getCustomDataDictionaryFromUser(self.selectedUser!)?.valueForKey("carBlobID") as? UInt{
                    QBRequest.downloadFileWithID(carBlobID, successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                        _imageCache!.setObject(data, forKey: self.selectedUser!.ID.description + "_car")
                        
                        if (getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool){
                            self.carImageView.image = UIImage(data: data)
                        }
                        }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                            
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            if (getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool){
                                self.carImageView.image = UIImage(named: "CarPlaceholder")
                            }
                    })
                }
            }
            else{
                if _imageCache!.objectForKey(self.selectedUser!.ID.description) != nil{
                    self.userImageView.image = UIImage(data: _imageCache!.objectForKey(self.selectedUser!.ID.description) as! NSData)!
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    
                    _imageCache!.setObject(data, forKey: self.selectedUser!.ID.description)
                    if !(getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool){
                        self.carImageView.image = UIImage(data: data)
                    }
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        if !(getCustomDataDictionaryFromUser(self.selectedUser!)!.valueForKey("isDriver") as! Bool){
                            self.carImageView.image = UIImage(named: "placeholder.jpg")
                        }
                })
                self.userImageView.hidden = true
            }

            }) { (response : QBResponse!) -> Void in
                
        }

    }
    ///method that is run when the edit profile button is pressed
    func editProfile(){
        if selectedUser != nil{
            self.performSegueWithIdentifier("goToEditProfileSegue", sender: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        getProfile()
        currentViewController = self
        currentThemeDelegate = self
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "UserProfileViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "embedUserProfileTableViewController"{
//            if let userProfileTableViewController : UserProfileTableViewController = segue.destinationViewController as? UserProfileTableViewController{
//                userProfileTableViewController.fromLabelText = fromString
//                userProfileTableViewController.toLabelText = toString
//                
//            }
//
//        }
        if segue.identifier == "goToEditProfileSegue"{
            let editProfileViewController = segue.destinationViewController as! EditProfileViewController
            editProfileViewController.selectedUser = self.selectedUser!
        }
        else if segue.identifier == "segueToNegotiationsViewControllerFromUserProfileViewController"{
            let negotiationsViewController = segue.destinationViewController as! NegotiationsViewController
            negotiationsViewController.opponentUser = selectedUser!
        }
    }
    

}
