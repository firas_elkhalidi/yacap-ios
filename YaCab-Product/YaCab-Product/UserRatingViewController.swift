//
//  UserRatingViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 9/29/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit
protocol UserRatingViewControllerDelegate {
    func willDismissUserRating()
}
class UserRatingViewController: UIViewController, QBChatDelegate{
    ///instance of the friend request button that will send a friend request to the user being rated
    @IBOutlet weak var friendRequestButton: UIBarButtonItem!
    ///instance of the user profile image view that holds the image view of the user being rated
    @IBOutlet weak var userProfileImageView: UIImageView!
    ///instance of the exit button that will dismiss the user rating view controller
    @IBOutlet weak var exitButton: UIBarButtonItem!
    ///instance of a label containing the introductory text of the view controller
    @IBOutlet weak var introLabel: UILabel!
    ///instance of a label that is the header of the star rating
    @IBOutlet weak var ratingLabel: UILabel!
    ///instance of a label that is the header of the comment text area
    @IBOutlet weak var commentLabel: UILabel!
    ///instance of the comment text view that the user can type in as a comment on the carpool
    @IBOutlet weak var commentTextView: UITextView!
    ///instance of the button used to submit the rating of the user
    @IBOutlet weak var submitButton: UIButton!
    /**
    method that detects when the submit button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func submitButtonPressed(sender: UIButton) {
        if opponentUser != nil{
            submitButton.enabled = false
            let newUserRating : QBCOCustomObject = QBCOCustomObject()
            newUserRating.className = "UserRating"
            newUserRating.fields.setValue(opponentUser!.ID, forKey: "ratedUserID")
            newUserRating.fields.setValue(starRatingView.rating, forKey: "rating")
            newUserRating.fields.setValue(commentTextView.text, forKey: "comment")
            QBRequest.createObject(newUserRating, successBlock: { (response :QBResponse!, createdRating : QBCOCustomObject!) -> Void in
                let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                extendedRequest.setObject(self.opponentUser!.ID, forKey: "ratedUserID")
                QBRequest.objectsWithClassName("UserRatingRecord", extendedRequest: extendedRequest, successBlock: { (reponse : QBResponse!, records : [AnyObject]!, page : QBResponsePage!) -> Void in
                    if records != nil{
                        if records.count > 0{
                            if let record = records[0] as? QBCOCustomObject{
                                var updateRecord = self.addRatingToRecord(record)
                                updateRecord = self.recalculateRating(updateRecord)
                                updateRecord.className = "UserRatingRecord"
                                QBRequest.updateObject(updateRecord, successBlock: { (response : QBResponse!, updatedRecord : QBCOCustomObject!) -> Void in
                                    self.delegate?.willDismissUserRating()
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                    }, errorBlock: { (reponse : QBResponse!) -> Void in
                                        self.delegate?.willDismissUserRating()
                                        self.dismissViewControllerAnimated(true, completion: nil)
                                })
                            }
                        }
                        else{
                            self.createNewRatingRecord()
                        }
                    }
                    else{
                        self.createNewRatingRecord()
                    }
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.createNewRatingRecord()
                })

                }, errorBlock: { (response : QBResponse!) -> Void in
                    self.delegate?.willDismissUserRating()
                    self.dismissViewControllerAnimated(true, completion: nil)
            })
        }
    }
    ///method that creates a new rating record of the user in the database
    func createNewRatingRecord(){
        var newRecord : QBCOCustomObject = QBCOCustomObject()
        newRecord.className = "UserRatingRecord"
        newRecord.fields.setObject(self.opponentUser!.ID, forKey: "ratedUserID")
        newRecord.fields.setObject(0, forKey: "starRating1")
        newRecord.fields.setObject(0, forKey: "starRating1_5")
        newRecord.fields.setObject(0, forKey: "starRating2")
        newRecord.fields.setObject(0, forKey: "starRating2_5")
        newRecord.fields.setObject(0, forKey: "starRating3")
        newRecord.fields.setObject(0, forKey: "starRating3_5")
        newRecord.fields.setObject(0, forKey: "starRating4")
        newRecord.fields.setObject(0, forKey: "starRating4_5")
        newRecord.fields.setObject(0, forKey: "starRating5")
        newRecord.fields.setObject(0, forKey: "rating")
        QBRequest.createObject(newRecord, successBlock: { (response : QBResponse!, record : QBCOCustomObject!) -> Void in
            
            newRecord = self.addRatingToRecord(record)
            newRecord = self.recalculateRating(record)
            newRecord.className = "UserRatingRecord"
            QBRequest.updateObject(newRecord, successBlock: { (reponse : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in
                self.delegate?.willDismissUserRating()
                self.dismissViewControllerAnimated(true, completion: nil)
                }, errorBlock: { (response : QBResponse!) -> Void in
                    self.delegate?.willDismissUserRating()
                    self.dismissViewControllerAnimated(true, completion: nil)
            })
            }) { (response : QBResponse!) -> Void in
                self.delegate?.willDismissUserRating()
                self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    /**
    method that adds a new record to the rating record of the user
    - Parameter record: the record that is to be updated with the new rating
    - Returns: the updated record of the user
    */
    func addRatingToRecord(record : QBCOCustomObject) -> QBCOCustomObject{
        if starRatingView.rating == 1{
            if let starRating = record.fields.valueForKey("starRating1") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating1")
            }
            else{
                record.fields.setObject(1, forKey: "starRating1")
            }
        }
        else if starRatingView.rating == 1.5{
            if let starRating = record.fields.valueForKey("starRating1_5") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating1_5")
            }
            else{
                record.fields.setObject(1, forKey: "starRating1_5")
            }
        }
        else if starRatingView.rating == 2{
            if let starRating = record.fields.valueForKey("starRating2") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating2")
            }
            else{
                record.fields.setObject(1, forKey: "starRating2")
            }
        }
        else if starRatingView.rating == 2.5{
            if let starRating = record.fields.valueForKey("starRating2_5") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating2_5")
            }
            else{
                record.fields.setObject(1, forKey: "starRating2_5")
            }
        }
        else if starRatingView.rating == 3{
            if let starRating = record.fields.valueForKey("starRating3") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating3")
            }
            else{
                record.fields.setObject(1, forKey: "starRating3")
            }
        }
        else if starRatingView.rating == 3.5{
            if let starRating = record.fields.valueForKey("starRating3_5") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating3_5")
            }
            else{
                record.fields.setObject(1, forKey: "starRating3_5")
            }
        }
        else if starRatingView.rating == 4{
            if let starRating = record.fields.valueForKey("starRating4") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating4")
            }
            else{
                record.fields.setObject(1, forKey: "starRating4")
            }
        }
        else if starRatingView.rating == 4.5{
            if let starRating = record.fields.valueForKey("starRating4_5") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating4_5")
            }
            else{
                record.fields.setObject(1, forKey: "starRating4_5")
            }
        }
        else if starRatingView.rating == 5{
            if let starRating = record.fields.valueForKey("starRating5") as? Int{
                record.fields.setObject(starRating + 1, forKey: "starRating5")
            }
            else{
                record.fields.setObject(1, forKey: "starRating5")
            }
        }
        return record
    }
    /**
    method that adds recalculates the rating of the user according to the recently added rating
    - Parameter record: the record that is to be updated with the new rating
    - Returns: the updated record of the user
    */
    func recalculateRating(record : QBCOCustomObject) -> QBCOCustomObject{
        let star1 = record.fields.valueForKey("starRating1") as! Float
        let star1_5 = record.fields.valueForKey("starRating1_5") as! Float
        let star2 = record.fields.valueForKey("starRating2") as! Float
        let star2_5 = record.fields.valueForKey("starRating2_5") as! Float
        let star3 = record.fields.valueForKey("starRating3") as! Float
        let star3_5 = record.fields.valueForKey("starRating3_5") as! Float
        let star4 = record.fields.valueForKey("starRating4") as! Float
        let star4_5 = record.fields.valueForKey("starRating4_5") as! Float
        let star5 = record.fields.valueForKey("starRating5") as! Float
        var above = (star1 * 1) + (star2 * 2) + (star3 * 3) + (star4 * 4) + (star5 * 5)
        above = above + (star1_5 * 1.5) + (star2_5 * 2.5) + (star3_5 * 3.5) + (star4_5 * 4.5)
        let below = star1 + star2 + star3 + star4 + star5 + star4_5 + star3_5 + star2_5 + star1_5
        if below != 0{
            let rating : Float = Float(above)/Float(below)
            record.fields.setObject(rating, forKey: "rating")
        }
        return record

    }
    ///line view
    @IBOutlet weak var lineView1: UIView!
    ///line view
    @IBOutlet weak var lineView2: UIView!
    ///line view
    @IBOutlet weak var lineView3: UIView!
    ///line view
    @IBOutlet weak var lineView4: UIView!
    ///instance of a CosmosView that is used to rate the user
    @IBOutlet weak var starRatingView: CosmosView!
    ///instance of the scroll view that contains all the other elements of the screen
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    ///holds the user that is to be rated
    var opponentUser : QBUUser?
    ///delegate of the user rating view controller that conforms to the UserRatingViewControllerDelegate protocol
    var delegate : UserRatingViewControllerDelegate?
    ///method that sets the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            submitButton.setTitle("SUBMIT", forState: UIControlState.Normal)
            submitButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            submitButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            introLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            ratingLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            commentLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            commentTextView.layer.cornerRadius = 5
            commentTextView.layer.borderWidth = 1
            starRatingView.backgroundColor = ApplicationThemeColor().GreyThemeColor
            starRatingView.rating = 3
            starRatingView.colorFilled = ApplicationThemeColor().YellowThemeColor
            starRatingView.colorEmpty = ApplicationThemeColor().GreyThemeColor
            scrollView.backgroundColor = ApplicationThemeColor().GreyThemeColor
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().DarkGreyTextColor
            
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            submitButton.setTitle("SUBMIT", forState: UIControlState.Normal)
            submitButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
            submitButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            introLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            ratingLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            commentLabel.textColor = ApplicationThemeColor().LightGreyTextColor
            commentTextView.layer.cornerRadius = 5
            commentTextView.layer.borderWidth = 1
            starRatingView.backgroundColor = ApplicationThemeColor().GreyThemeColor
            starRatingView.rating = 3
            starRatingView.colorFilled = ApplicationThemeColor().GreenThemeColor
            starRatingView.colorEmpty = ApplicationThemeColor().GreyThemeColor
            scrollView.backgroundColor = ApplicationThemeColor().GreyThemeColor
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().WhiteThemeColor
            
        }
    }
    /**
    method that detects when the friend request button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func handleFriendRequest(sender: UIBarButtonItem) {
        let alert : UIAlertController = UIAlertController(title: "Friend Request", message: "Do you want to send a friend request", preferredStyle: UIAlertControllerStyle.Alert)
        let alertYes : UIAlertAction = UIAlertAction(title: "Send Request", style: UIAlertActionStyle.Default) { (action : UIAlertAction) -> Void in
            self.sendContactRequest(self.opponentUser!.ID)
        }
        let alertNo : UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(alertYes)
        alert.addAction(alertNo)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    /**
    method that detects when the exit button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func handleExitButton(sender: UIBarButtonItem) {
        if delegate != nil{
            delegate?.willDismissUserRating()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    /**
    method that is used to send a contact request to the user with the send id as a parameter
    - Parameter userID: the userID that is to receive the friend request
    */
    func sendContactRequest(userID : UInt){
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setObject(userID, forKey: "receiverID")
        let customObject : QBCOCustomObject = QBCOCustomObject()
        customObject.className = "ContactRequest"
        customObject.fields = fields
        let permissions : QBCOPermissions = QBCOPermissions()
        permissions.deleteAccess = QBCOPermissionsAccessOpen
        permissions.updateAccess = QBCOPermissionsAccessOpen
        customObject.permissions = permissions
        self.navigationItem.rightBarButtonItem?.enabled = false
        QBRequest.createObject(customObject, successBlock: { (response : QBResponse!, customObject : QBCOCustomObject!) -> Void in
            
            QBChat.instance().addUserToContactListRequest(self.opponentUser!.ID, sentBlock: { (error : NSError!) -> Void in
                if error == nil{
                    self.navigationItem.rightBarButtonItem?.enabled = false
                }
                else{
                    self.navigationItem.rightBarButtonItem?.enabled = true
                }
                
            })
            }, errorBlock: { (response : QBResponse!) -> Void in
                //print("failedcreatedObject:")
                //print(response.description)
        })
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        starRatingView.settings.fillMode = StarFillMode.Half
        QBChat.instance().addDelegate(self)
        self.navigationController?.navigationBar.translucent = false
        for item in QBChat.instance().contactList.contacts{
            if (item as! QBContactListItem).userID == opponentUser!.ID{
                self.navigationItem.rightBarButtonItem?.enabled = false
            }
        }
        for item in QBChat.instance().contactList.pendingApproval{
            if opponentUser!.ID == item.userID{
                self.navigationItem.rightBarButtonItem?.enabled = false
            }
        }
        if opponentUser == nil{
            self.delegate?.willDismissUserRating()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else{
            setTheme()
            QBRequest.downloadFileWithID(UInt((opponentUser?.blobID)!), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                self.userProfileImageView.image = UIImage(data: data)
                }, statusBlock: { (response : QBRequest!, status : QBRequestStatus!) -> Void in
                    
                }, errorBlock: { (response : QBResponse!) -> Void in
                    self.userProfileImageView.image = UIImage(named: "placeholder.jpg")
            })
            introLabel.text = "How was your ride with \"" + (opponentUser?.login)! + "\"?"
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "UserRatingViewControllerDelegate")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
