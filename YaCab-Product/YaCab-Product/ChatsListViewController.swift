//
//  ChatsListViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/24/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class ChatsListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CurrentThemeDelegate,QBChatDelegate,CurrentNumberOfCarpoolersDelegate {
    ///label that is used to indicate the table view is loading the new chats in from the server
    var placeholder : UILabel = UILabel()
    ///delegate method of the CurrentNumberOfCarpoolersDelegate indicating that the current number of carpoolers did change
    func currentNumberOfCarpoolersDidChange() {
        getData()
    }
    ///array that holds the chats of the users
    var chats : [QBCOCustomObject] = []
    ///holds the user of the selected row
    var selectedRowUser : QBUUser?
    ///array that holds the user's ids that are typing
    var typingUsersIDS : NSMutableArray = []
    ///refresh control placed above the table view that lets the user refresh the list of chats
    var refreshControl = UIRefreshControl()
    ///instance of the table view that holds the chats
    @IBOutlet weak var tableView: UITableView!
    ///hamburger button that opens the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    ///delegate of the ChatsListViewController that conforms to the CurrentNumberOfCarpoolersDelegate protocol
    var delegate : ChatsListViewControllerDelegate?
    
    func chatDidReceiveUserIsTypingFromUserWithID(userID: UInt) {
        typingUsersIDS.addObject(userID)
        for var i = 0; i < chats.count; i++
        {
            if chats[i].userID == DatabaseHelper().FetchUser().ID{
                if (chats[i].fields.valueForKey("opponentID") as? UInt) == userID{
                    let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                    cell.lastMessageLabel.text = "typing..."
                }
            }
            else{
                if (chats[i].userID) == userID{
                    let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                    cell.lastMessageLabel.text = "typing..."
                }
            }
        }
    }
    func chatDidReceiveUserStopTypingFromUserWithID(userID: UInt) {
        for var i = 0; i < chats.count; i++
        {
            if chats[i].userID == DatabaseHelper().FetchUser().ID{
                if (chats[i].fields.valueForKey("opponentID") as? UInt) == userID{
                    let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                    cell.lastMessageLabel.text = chats[i].fields.valueForKey("lastMessageNotIsNeg") as? String
                }
            }
            else{
                if (chats[i].userID) == userID{
                    let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                    cell.lastMessageLabel.text = chats[i].fields.valueForKey("lastMessageNotIsNeg") as? String
                }
            }
        }
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placeholder.hidden = chats.count > 0
        return chats.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! ChatListTableViewCell
        let now : NSDate = NSDate()
        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
        extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
        extendedRequest["sort_desc"] = "date_sent"
        extendedRequest["limit"] = 50;

        if chats[indexPath.row].userID == DatabaseHelper().FetchUser().ID{
            if _userCache!.objectForKey((chats[indexPath.row].fields.valueForKey("opponentID") as! UInt).description) != nil{
                let user = _userCache!.objectForKey((chats[indexPath.row].fields.valueForKey("opponentID") as! UInt).description) as! QBUUser
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            }
            
            QBRequest.userWithID(chats[indexPath.row].fields.valueForKey("opponentID") as! UInt, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                _userCache!.setObject(user, forKey: user.ID.description)
                cell.usernameLabel.text = user.login
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                       cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        else {
            if _userCache!.objectForKey(chats[indexPath.row].userID.description) != nil{
                let user = _userCache!.objectForKey(chats[indexPath.row].userID.description) as! QBUUser
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            }
            QBRequest.userWithID(chats[indexPath.row].userID, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                _userCache!.setObject(user, forKey: user.ID.description)
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        cell.lastMessageLabel.text = chats[indexPath.row].fields.valueForKey("lastMessageNotIsNeg") as? String
        return cell
    }
    /**
    sorter method that sorts the chats according to the last message received
    - Parameters:
    - this: holds the first request object
    - that: holds the second request object
    - Returns: a boolean that indicates whether (this) is a greater date than (that)
    */
    func sorterForChats(this:QBCOCustomObject, that:QBCOCustomObject) -> Bool {
        return this.updatedAt.isGreaterThanDate(that.updatedAt)
    }
    func chatDidReceiveMessage(message: QBChatMessage!) {
        if let isNeg = message.customParameters["isNeg"] as? String{
            //print(message.customParameters["isNeg"] as? Bool)
            var refreshData = true
            if isNeg == "0" || isNeg == "false" {
                for var i = 0; i < chats.count; i++
                {
                    if chats[i].userID == DatabaseHelper().FetchUser().ID{
                        if (chats[i].fields.valueForKey("opponentID") as? UInt) == message.senderID{
                            chats[i].fields.setValue(message.text, forKey: "lastMessageNotIsNeg")
                            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                            cell.lastMessageLabel.text = message.text
                            chats[i].updatedAt = NSDate()
                            chats.sortInPlace(sorterForChats)
                            tableView.moveRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                            refreshData = false
                        }
                    }
                    else{
                        if (chats[i].userID) == message.senderID{
                            chats[i].fields.setValue(message.text, forKey: "lastMessageNotIsNeg")
                            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! ChatListTableViewCell
                            cell.lastMessageLabel.text = message.text
                            chats[i].updatedAt = NSDate()
                            chats.sortInPlace(sorterForChats)
                            tableView.moveRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                            refreshData = false
                        }
                    }
                }
                
            }
            else if isNeg == "1" || isNeg == "true"{
                if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                    for var i = 0; i < chats.count; i++
                    {
                        if chats[i].userID == DatabaseHelper().FetchUser().ID{
                            if (chats[i].fields.valueForKey("opponentID") as? UInt) == message.senderID{
                                chats.removeAtIndex(i)
                                tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: i, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                                refreshData = false
                            }
                        }
                        else{
                            if (chats[i].userID) == message.senderID{
                                chats[i].fields.setValue(message.text, forKey: "lastMessageIsNeg")
                                chats.removeAtIndex(i)
                                tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: i, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                                refreshData = false
                            }
                        }
                    }
                }
            }
            if refreshData {
                //tableView.reloadData()
                //getData()
            }
        }
    }
    override func viewWillDisappear(animated: Bool) {
        currentNumberOfCarpoolersInChatListDelegate = nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        currentNumberOfCarpoolersInChatListDelegate = self
        refreshControl.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        QBChat.instance().addDelegate(self)
        // register new tableviewcell
        let nib = UINib(nibName: "ChatListTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        if self.revealViewController() != nil{
            revealViewController().rightRevealToggleAnimated(true)
        }
        //currentThemeDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        setTheme()
        let placeholder : UILabel = UILabel()
        placeholder.font = UIFont.italicSystemFontOfSize(14)
        placeholder.numberOfLines = 1; // Use as many lines as needed.
        placeholder.text = "loading"
        placeholder.textAlignment = NSTextAlignment.Center
        placeholder.textColor = UIColor.lightGrayColor()
        
        placeholder.hidden = true
        self.tableView.addSubview(placeholder)
        
        self.placeholder = placeholder
        //getData()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.placeholder.frame = self.tableView.frame;
    }
    override func viewDidAppear(animated: Bool) {
        //currentViewController = self
        self.view.userInteractionEnabled = false
//        if pushOpponentUser != nil{
//            self.performSegueWithIdentifier("goToChatFromChatsListViewController", sender: self)
//        }
        self.placeholder.frame = self.tableView.frame;
        getData()
    }
    ///delegate method the CurrentThemeDelegate protocol that indicates that the theme of the application did change
    func themeDidChange() {
        setTheme()
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        self.title = "Chats"
        self.navigationController?.navigationBar.translucent = false
        if currentTheme == Theme.DriverTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().YellowThemeColor
            refreshControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
            menuButton.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().GreenThemeColor
            refreshControl.tintColor = ApplicationThemeColor().WhiteThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
            menuButton.tintColor = ApplicationThemeColor().GreenThemeColor
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //_ : UInt?
        self.view.userInteractionEnabled = false
        if chats[indexPath.row].userID == DatabaseHelper().FetchUser().ID{
            QBRequest.userWithID(chats[indexPath.row].fields.valueForKey("opponentID") as! UInt, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                self.selectedRowUser = user
                
                if currentViewController is RideRequestsListOrChatListViewController{
                    self.view.userInteractionEnabled = true
                    
                    if self.delegate != nil{
                        self.delegate?.goToThisChat(self.selectedRowUser!)
                    }
                    
                    //self.performSegueWithIdentifier("goToChatFromChatsListViewController", sender: self)
                }
                
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        else {
            QBRequest.userWithID(chats[indexPath.row].userID, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                self.selectedRowUser = user
                if currentViewController is RideRequestsListOrChatListViewController{
                    self.view.userInteractionEnabled = true
                    if self.delegate != nil{
                        self.delegate?.goToThisChat(self.selectedRowUser!)
                    }
                    //self.performSegueWithIdentifier("goToChatFromChatsListViewController", sender: self)
                }
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 71
    }
    ///method that gets the chats and places them in the table view
    func getData(){
        placeholder.text = "loading"
        chats = []
        tableView.reloadData()
        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
        extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "user_id[or]")
        extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "opponentID[or]")
        extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState")
        extendedRequest.setValue(true, forKey: "isActive")
        QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
            
            for carpool in carpools{
                //print("")
                if let c = carpool as? QBCOCustomObject{
                    var append = true
                    for chat in self.chats{
                        if c.ID == chat.ID{
                            append = false
                        }
                    }
                    if append{
                       self.chats.append(c)
                    }
                    
                }
            }
            if numberOfCurrentCarpoolers != self.chats.count{
                numberOfCurrentCarpoolers = self.chats.count
            }
            
            //print("chatsavailable:", self.chats)
            self.chats.sortInPlace(self.sorterForChats)
            self.tableView.reloadData()
            self.placeholder.text = "You have no chats"
            self.refreshControl.endRefreshing()
            self.view.userInteractionEnabled = true
            
            }) { (response : QBResponse!) -> Void in
                self.view.userInteractionEnabled = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToChatFromChatsListViewController"{
            //print("chatnavigation")

            let chatViewController : ChatViewController = segue.destinationViewController as! ChatViewController
            
            if pushOpponentUser != nil{
                //print(pushOpponentUser)
                //chatViewController.opponentUser = pushOpponentUser!
                //pushOpponentUser = nil
            }
            else if selectedRowUser != nil{
                chatViewController.opponentUser = selectedRowUser!
            }
        }
    }


}
