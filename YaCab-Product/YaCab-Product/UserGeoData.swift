//
//  UserGeoData.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
///class that contains the geodata of a user
class UserGeoData {
    ///contains the quickblox geodata object of a user
    internal var geoData : QBLGeoData = QBLGeoData()
    ///contains the user custom data dictionary
    internal var userJSONDictionary : NSDictionary = NSDictionary()
    init(geoData : QBLGeoData){
        self.geoData = geoData
        if geoData.user.customData != nil{
            let str : String = geoData.user.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
            if let json : NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                userJSONDictionary = json
            }
        }

    }
}