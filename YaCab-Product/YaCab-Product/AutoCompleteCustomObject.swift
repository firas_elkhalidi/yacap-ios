//
//  AutoCompleteCustomObject.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/7/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import Foundation
class AutoCompleteCustomObject: NSObject, MLPAutoCompletionObject {
    ///the object used as the autocompletion objects for the organizations drop down list
    var organization : QBCOCustomObject?
    init(organization : QBCOCustomObject){
        self.organization = organization
    }
    @objc func autocompleteString() -> String! {
        if organization != nil{
            return organization!.fields.valueForKey("name") as! String
        }
        return ""
    }
}