//
//  AboutUsViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 11/19/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController, CurrentThemeDelegate {
    ///instance of an image view that holds the logo the application
    @IBOutlet weak var logoImageView: UIImageView!
    ///hamburger button that opens the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if !QBChat.instance().isLoggedIn(){
            QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        }
        setTheme()
        // Do any additional setup after loading the view.
    }
    ///instance of the feedback button
    @IBOutlet weak var feedbackButton: UIButton!
    ///instance of the twitter account button
    @IBOutlet weak var twitterButton: UIButton!
    ///instance of the facebook page button
    @IBOutlet weak var facebookButton: UIButton!
    ///instance of the intagram account button
    @IBOutlet weak var instagramButton: UIButton!
    ///instance of the terms and conditions button that opens up the terms and conditions
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    ///instance of the linked in account button
    @IBOutlet weak var linkedinButton: UIButton!
    /**
    method that detects when the linked in button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func linkedinButtonPressed(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.linkedin.com/profile/view?id=ADEAABpN3WMBdlQJTJTdTFhLg9A_9S05LNW5iug&authType=NAME_SEARCH&authToken=w_gT&locale=en_US&srchid=3216137931448089482422&srchindex=1&srchtotal=52&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A3216137931448089482422%2CVSRPtargetId%3A441310563%2CVSRPcmpt%3Aprimary%2CVSRPnm%3Atrue%2CauthType%3ANAME_SEARCH")!)
    }
    /**
    method that detects when the terms and conditions button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func termsAndConditionsButtonPressed(sender: UIButton) {
        let navigationController : UINavigationController = UINavigationController()
        let termsAndConditionsViewController : TermsAndConditionsViewController = storyboard?.instantiateViewControllerWithIdentifier("TermsAndConditionsViewController") as! TermsAndConditionsViewController
        termsAndConditionsViewController.presentedFromSignUp = false
        navigationController.pushViewController(termsAndConditionsViewController, animated: false)
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad{
            navigationController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        }
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    /**
    method that detects when the facebook button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func facebookButtonPressed(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/YaCap-1456735694632897/?fref=ts")!)
    }
    /**
    method that detects when the twitter button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func twitterButtonPressed(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/yacapco")!)
    }
    /**
    method that detects when the instagram button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func instagramButtonPressed(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.instagram.com/yacap.co/")!)
    }
    /**
    method that detects when the feedback button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func feedbackButtonPressed(sender: UIButton) {
//        let navigationController : UINavigationController = UINavigationController()
//        let feedbackViewController : FeedbackViewController = storyboard?.instantiateViewControllerWithIdentifier("FeedbackViewController") as! FeedbackViewController
//        navigationController.pushViewController(feedbackViewController, animated: false)
//        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad{
//            navigationController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
//        }
//        self.presentViewController(navigationController, animated: true, completion: nil)
        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.yacap.net/feedback.html")!)
    }
    ///delegate method of the CurrentThemeDelegate that is used to indicate that the theme changed
    func themeDidChange() {
        setTheme()
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        currentThemeDelegate = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "AboutUsViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as? [NSObject : AnyObject])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    ///method that is used to set the theme of the view controller
    func setTheme(){
        logoImageView.image = logoImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        if currentTheme == Theme.PassengerTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            logoImageView.tintColor = ApplicationThemeColor().GreenThemeColor
            feedbackButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
            termsAndConditionsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            logoImageView.tintColor = ApplicationThemeColor().YellowThemeColor
             feedbackButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
            termsAndConditionsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
