//
//  Carpooler.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/26/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
///method that contains a carpooler
class Carpooler {
    ///boolean that indicates whether the carpooler is visible or not
    internal var isVisible : Bool = false
    ///the user that is the carpooler
    internal var user : QBUUser = QBUUser()
    init(user : QBUUser, isVisible : Bool) {
        self.user = user
        self.isVisible = isVisible
    }
}