//
//  DropDownTableViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/4/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
enum DropDownTableMode : String {
    case Smoker = "Smoker", Gender = "Gender", AC = "AC"
}
enum SmokerStates : String {
    case IsSmoker = "IsSmoker", NotSmoker = "NotSmoker" ,DontCareSmoker = "DontCareSmoker"
}
enum GenderStates : String {
    case Male = "m", Female = "f", DontCareGender = "DontCareGender"
}
enum ACStates : String{
    case AC = "AC", NoAC = "NoAC", DontCareAC = "DontCareAC"
}
class DropDownTableViewController: UITableViewController, CurrentThemeDelegate{
    var mode : String?
    var popOverDelegate : UIPopOverDataTransferDelegate?
    var currentCellColor : UIColor?
    var currentCellImageColor : UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        // register new tableviewcell
        let nib = UINib(nibName: "DropDownListCustomTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        tableView.scrollEnabled = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func themeDidChange() {
        setTheme()
    }
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            tableView.backgroundColor = ApplicationThemeColor().YellowThemeColor
            currentCellColor = ApplicationThemeColor().DarkGreyTextColor
            tableView.separatorColor = ApplicationThemeColor().YellowThemeColor
            currentCellImageColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            tableView.backgroundColor = ApplicationThemeColor().GreenThemeColor
            currentCellColor = ApplicationThemeColor().GreyThemeColor
            tableView.separatorColor = ApplicationThemeColor().GreenThemeColor
            currentCellImageColor = ApplicationThemeColor().GreenThemeColor
        }
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        if mode != nil{
            return 1
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if mode == DropDownTableMode.Smoker.rawValue{
            return 3
        }
        else if mode == DropDownTableMode.Gender.rawValue{
            return 3
        }
        else if mode == DropDownTableMode.AC.rawValue{
            return 3
        }
        return 0
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! DropDownListCustomTableViewCell
        
        if mode == DropDownTableMode.Smoker.rawValue{
            if indexPath.row == 0{
                cell.cellImageView?.image = UIImage(named: "smoker.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
                
            }
            else if indexPath.row == 1{
                cell.cellImageView?.image = UIImage(named: "notsmoker.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
            else if indexPath.row == 2{
                cell.cellImageView?.image = UIImage(named: "Any.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
        }
        else if mode == DropDownTableMode.Gender.rawValue{
            if indexPath.row == 0{
                cell.cellImageView?.image = UIImage(named: "male.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
            else if indexPath.row == 1{
                cell.cellImageView?.image = UIImage(named: "female.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
            else if indexPath.row == 2{
                cell.cellImageView?.image = UIImage(named: "Any.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
        }
        else if mode == DropDownTableMode.AC.rawValue{
            if indexPath.row == 0{
                cell.cellImageView?.image = UIImage(named: "AC.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
            else if indexPath.row == 1{
                cell.cellImageView?.image = UIImage(named: "NoAC.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
            else if indexPath.row == 2{
                cell.cellImageView?.image = UIImage(named: "Any.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.cellImageView.clipsToBounds = true
            }
        }
        cell.contentView.contentMode = UIViewContentMode.ScaleAspectFit
        cell.cellImageView.tintColor = currentCellImageColor
        cell.backgroundColor = currentCellColor
        cell.textLabel?.text = ""
        return cell
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if popOverDelegate != nil{
            if mode == DropDownTableMode.Smoker.rawValue{
                if indexPath.row == 0{
                    popOverDelegate?.returnData(SmokerStates.IsSmoker.rawValue)
                }
                else if indexPath.row == 1{
                    popOverDelegate?.returnData(SmokerStates.NotSmoker.rawValue)
                }
                else if indexPath.row == 2{
                    popOverDelegate?.returnData(SmokerStates.DontCareSmoker.rawValue)
                }
            }
            else if mode == DropDownTableMode.Gender.rawValue{
                if indexPath.row == 0{
                    popOverDelegate?.returnData(GenderStates.Male.rawValue)
                }
                else if indexPath.row == 1{
                    popOverDelegate?.returnData(GenderStates.Female.rawValue)
                }
                else if indexPath.row == 2{
                    popOverDelegate?.returnData(GenderStates.DontCareGender.rawValue)
                }
            }
            else if mode == DropDownTableMode.AC.rawValue{
                if indexPath.row == 0{
                    popOverDelegate?.returnData(ACStates.AC.rawValue)
                }
                else if indexPath.row == 1{
                    popOverDelegate?.returnData(ACStates.NoAC.rawValue)
                }
                else if indexPath.row == 2 {
                    popOverDelegate?.returnData(ACStates.DontCareAC.rawValue)
                }
            }
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
