//
//  UIPopOverDataTransferDelegate
//  popO
//
//  Created by Firas Al Khatib Al Khalidi on 8/3/15.
//  Copyright (c) 2015 Firas Al Khatib Al Khalidi. All rights reserved.
//

import Foundation
protocol UIPopOverDataTransferDelegate{
    func returnData(data : AnyObject)
}