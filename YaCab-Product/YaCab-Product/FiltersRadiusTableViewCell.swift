//
//  FiltersRadiusTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/29/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class FiltersRadiusTableViewCell: UITableViewCell {
    @IBOutlet weak var radiusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        slider.minimumValue = 0
        slider.maximumValue = 10
        slider.continuous = true
        slider.value = Float(Int(currentRadius)/10)
        radiusLabel.text = Int(currentRadius).description + " KM"
        tableViewFilterRadius = currentRadius
        // Initialization code
    }
    @IBOutlet weak var slider: UISlider!
    @IBAction func sliderValueChanged(sender: UISlider) {
        sender.value = (round(sender.value))
        radiusLabel.text = Int((round(sender.value)*10)).description + " KM"
        tableViewFilterRadius = Double(round(sender.value)) * 10
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
