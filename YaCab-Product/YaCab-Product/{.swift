//
//  MapViewController.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//
func imageResize (#image:UIImage, sizeChange:CGSize)-> UIImage{
    
    let hasAlpha = true
    let scale: CGFloat = 0.0 // Use scale factor of main screen
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
    
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    return scaledImage
}

import UIKit
import MapKit
import Darwin
var currentRadius : Double = Double()
var genderFilter : String = GenderStates.DontCareGender.rawValue
var smokerFilter : String = SmokerStates.DontCareSmoker.rawValue
var ACFilter : String = ACStates.DontCareAC.rawValue
class MapViewController: UIViewController,MKMapViewDelegate,WYPopoverControllerDelegate,CurrentThemeDelegate,UIPopOverDataTransferDelegate {
    var addedUserQueue : [QBLGeoData] = []
    var firsttime = true
    var visibleUsers : [QBLGeoData] = []
    var availableUsers : [UserGeoData] = []
    var draggableLocation : CLLocationCoordinate2D = CLLocationCoordinate2D()
    var timer = NSTimer()
    var viewIsVisible = false
    var dontDeselectAnnotation = false
    var selectedAnnotationID : String = String()
    var selectedAnnotationImage : UIImage?
    var selectedRouteID : String?
    var selectedRoutePolyline : MKPolyline?
    var popoverController : WYPopoverController?
    var popOverTableViewController : DropDownTableViewController?
    @IBOutlet weak var listViewButton: UIButton!
    @IBOutlet var map: MKMapView!
    var boxView : UIView = UIView()
    @IBOutlet weak var isSmokerButton: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var acButton: UIButton!
    @IBAction func genderButtonPressed(sender: UIButton) {
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        popOverTableViewController = storyboard.instantiateViewControllerWithIdentifier("DropDownTableViewController") as? DropDownTableViewController
        popOverTableViewController!.popOverDelegate = self
        popOverTableViewController?.modalInPopover = false
        popOverTableViewController!.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.tableView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.mode = DropDownTableMode.Gender.rawValue
        popoverController = WYPopoverController(contentViewController: popOverTableViewController)
        
        popoverController?.delegate = self
        
        popoverController?.popoverContentSize = CGSize(width: 40, height: 120)
        popoverController?.theme = WYPopoverTheme.themeForIOS7()
        
        popoverController?.presentPopoverFromRect(sender.frame, inView: self.view, permittedArrowDirections: WYPopoverArrowDirection.Any, animated: true, options: WYPopoverAnimationOptions.Fade)
    }
    @IBAction func smokerButtonPressed(sender: UIButton) {
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        popOverTableViewController = storyboard.instantiateViewControllerWithIdentifier("DropDownTableViewController") as? DropDownTableViewController
        popOverTableViewController!.popOverDelegate = self
        popOverTableViewController?.modalInPopover = false
        popOverTableViewController!.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.tableView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.mode = DropDownTableMode.Smoker.rawValue
        popoverController = WYPopoverController(contentViewController: popOverTableViewController)
        popoverController?.delegate = self
        popoverController?.popoverContentSize = CGSize(width: 40, height: 120)
        popoverController?.theme = WYPopoverTheme.themeForIOS7()
        popoverController?.presentPopoverFromRect(sender.frame, inView: self.view, permittedArrowDirections: WYPopoverArrowDirection.Any, animated: true, options: WYPopoverAnimationOptions.Fade)
    }
    @IBAction func acButtonPressed(sender: UIButton) {
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        popOverTableViewController = storyboard.instantiateViewControllerWithIdentifier("DropDownTableViewController") as? DropDownTableViewController
        popOverTableViewController!.popOverDelegate = self
        popOverTableViewController?.modalInPopover = false
        popOverTableViewController!.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.tableView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        popOverTableViewController!.mode = DropDownTableMode.AC.rawValue
        popoverController = WYPopoverController(contentViewController: popOverTableViewController)
        popoverController?.delegate = self
        popoverController?.popoverContentSize = CGSize(width: 40, height: 120)
        popoverController?.theme = WYPopoverTheme.themeForIOS7()
        popoverController?.presentPopoverFromRect(sender.frame, inView: self.view, permittedArrowDirections: WYPopoverArrowDirection.Any, animated: true, options: WYPopoverAnimationOptions.Fade)
    }
    func popoverControllerShouldDismissPopover(popoverController: WYPopoverController!) -> Bool {
        return true
    }
    func setupFilterButtons(){
        if smokerFilter == SmokerStates.IsSmoker.rawValue{
            isSmokerButton.setTitle("", forState: UIControlState.Normal)
            isSmokerButton.setImage(UIImage(named: "smoker.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            isSmokerButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            isSmokerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            isSmokerButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            isSmokerButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if smokerFilter == SmokerStates.NotSmoker.rawValue{
            isSmokerButton.setTitle("", forState: UIControlState.Normal)
            isSmokerButton.setImage(UIImage(named: "notsmoker.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            isSmokerButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            isSmokerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            isSmokerButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            isSmokerButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if smokerFilter == SmokerStates.DontCareSmoker.rawValue{
            isSmokerButton.setTitle("", forState: UIControlState.Normal)
            isSmokerButton.setImage(UIImage(named: "smoker.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            isSmokerButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            isSmokerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            isSmokerButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            isSmokerButton.imageView?.tintColor = ApplicationThemeColor().DarkGreyTextColor
        }
        if genderFilter == GenderStates.Male.rawValue{
            genderButton.setTitle("", forState: UIControlState.Normal)
            genderButton.setImage(UIImage(named: "male.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            genderButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            genderButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            genderButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if genderFilter == GenderStates.Female.rawValue{
            genderButton.setTitle("", forState: UIControlState.Normal)
            genderButton.setImage(UIImage(named: "female.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            genderButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            genderButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            genderButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if genderFilter == GenderStates.DontCareGender.rawValue{
            genderButton.setTitle("", forState: UIControlState.Normal)
            genderButton.setImage(UIImage(named: "male.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            genderButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            genderButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            genderButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            genderButton.imageView?.tintColor = ApplicationThemeColor().DarkGreyTextColor
        }
        if ACFilter == ACStates.AC.rawValue{
            acButton.setTitle("", forState: UIControlState.Normal)
            acButton.setImage(UIImage(named: "AC.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            acButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            acButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            acButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            acButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if ACFilter == ACStates.NoAC.rawValue{
            acButton.setTitle("", forState: UIControlState.Normal)
            acButton.setImage(UIImage(named: "NoAC.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            acButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            acButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            acButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            acButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if ACFilter == ACStates.DontCareAC.rawValue{
            acButton.setTitle("", forState: UIControlState.Normal)
            acButton.setImage(UIImage(named: "AC.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
            acButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
            acButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
            acButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
            acButton.imageView?.tintColor = ApplicationThemeColor().DarkGreyTextColor
        }
    }
    func returnData(data: AnyObject) {
        
        if data is String{
            if data as! String == SmokerStates.IsSmoker.rawValue || data as! String == SmokerStates.NotSmoker.rawValue || data as! String == SmokerStates.DontCareSmoker.rawValue{
                smokerFilter = data as! String
                setupFilterButtons()
            }
                
            else if data as! String == GenderStates.Male.rawValue || data as! String == GenderStates.Female.rawValue || data as! String == GenderStates.DontCareGender.rawValue{
                genderFilter = data as! String
                setupFilterButtons()
            }
            else if data as! String == ACStates.AC.rawValue || data as! String == ACStates.NoAC.rawValue || data as! String == ACStates.DontCareAC.rawValue{
                ACFilter = data as! String
                setupFilterButtons()
            }
        }
        popoverController?.dismissPopoverAnimated(true)
        
    }
    func popoverControllerDidDismissPopover(popoverController: WYPopoverController!) {
        self.popoverController?.delegate = nil
        self.popoverController = nil
        self.popOverTableViewController = nil
        
    }
    
    @IBOutlet var menuButton: UIBarButtonItem!
    func mapView(mapView: MKMapView!, didDeselectAnnotationView view: MKAnnotationView!)
    {
        if !dontDeselectAnnotation {
            selectedAnnotationID = ""
            selectedAnnotationImage = nil
            selectedRouteID = nil
            selectedRoutePolyline = nil
        }
    }
    func themeDidChange() {
        setTheme()
    }
    func setTheme(){
        self.title = "Map"
        genderButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        isSmokerButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        if currentTheme == Theme.PassengerTheme.rawValue{
            
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
            self.navigationController?.navigationBar.translucent = false
            //genderButton.imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
            //isSmokerButton.imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
            self.view.backgroundColor = ApplicationThemeColor().GreenThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            //listViewButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            //listViewButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
            self.navigationController?.navigationBar.translucent = false
            //genderButton.imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
            //isSmokerButton.imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
            self.view.backgroundColor = ApplicationThemeColor().YellowThemeColor
        }
        setupFilterButtons()
        
    }
    override func viewDidLoad() {
        //
        
        
        super.viewDidLoad()
        if !QBChat.instance().isLoggedIn(){
            QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        }
        map.delegate = self
        
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        setupFilterButtons()
        setTheme()
        
    }
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        draggableLocation = view.annotation.coordinate
        let alert : UIAlertController = UIAlertController(title: "Updated Location", message: draggableLocation.latitude.description + " " + draggableLocation.longitude.description  , preferredStyle: UIAlertControllerStyle.Alert)
        let action : UIAlertAction =  UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(action)
        if oldState == MKAnnotationViewDragState.Dragging && newState == MKAnnotationViewDragState.Ending{
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        currentThemeDelegate = self
        
        
        
        selectedGeoData = QBLGeoData()
        currentViewController = self
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView.backgroundColor = UIColor.whiteColor()
        boxView.alpha = 1.0
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        var textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Map"
        self.view.userInteractionEnabled = false
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        self.view.userInteractionEnabled = false
        viewIsVisible = true
        populateMap()
    }
    override func viewDidDisappear(animated: Bool) {
        viewIsVisible = false
        timer.invalidate()
    }
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        selectedAnnotationID = view.annotation.title!
        if selectedAnnotationImage != nil{
            var imageView = UIImageView(image: self.selectedAnnotationImage)
            
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 8;
            imageView.clipsToBounds = true;
            view.leftCalloutAccessoryView = imageView
            
        }
        else {
            for userLocation in self.visibleUsers{
                if selectedAnnotationID == userLocation.user.login{
                    var statusBlock : QBRequestStatusUpdateBlock = { (request : QBRequest!, status : QBRequestStatus!) in
                        NSLog("PercentageComplete:" + status.percentOfCompletion.description);
                    }
                    QBRequest.TDownloadFileWithBlobID(UInt(userLocation.user.blobID), successBlock: { (response : QBResponse!,imageData : NSData!) -> Void in
                        println("Something")
                        self.selectedAnnotationImage = UIImage(data: imageData)!
                        var imageView = UIImageView(image: self.selectedAnnotationImage)
                        imageView.frame = CGRectMake(0,0,31,31);
                        imageView.layer.cornerRadius = imageView.frame.size.width / 8;
                        imageView.clipsToBounds = true;
                        view.leftCalloutAccessoryView = imageView
                        }, statusBlock: statusBlock) { (response : QBResponse!) -> Void in
                    }
                    var customUserData : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user)!
                    if let activeRouteID = customUserData.valueForKey("activeRouteID") as? String{
                        drawRoute(activeRouteID)
                    }
                    
                }
            }
        }
        
    }
    func updateRadius(){
        //        double miles = 5.0;
        //        double scalingFactor = ABS( (cos(2 * M_PI * newLocation.coordinate.latitude / 360.0) ));
        //
        //        MKCoordinateSpan span;
        //
        //        span.latitudeDelta = miles/69.0;
        //        span.longitudeDelta = miles/(scalingFactor * 69.0);
        //
        //        MKCoordinateRegion region;
        //        region.span = span;
        //        region.center = newLocation.coordinate;
        //
        //        [mapView setRegion:region animated:YES];
        //var scalingFactor : Double = abs((cos(2 * M_PI * map.region.center.latitude/360.0)))
        //self.currentRadius = (map.region.span.longitudeDelta + map.region.span.latitudeDelta)
        
    }
    func populateMap(){
        if viewIsVisible{
            var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("requestMapUpdate"), userInfo: nil, repeats: false)
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        if overlay is MKCircle{
            var circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.strokeColor = UIColor.lightGrayColor()
            circleRenderer.lineWidth = 4
            circleRenderer.fillColor = UIColor.lightGrayColor()
            circleRenderer.alpha = 0.4
            return circleRenderer
        }
        
        return nil
    }
    
    func filterIntoVisibleUsers(){
        for unfilteredUserLocation in self.availableUsers{
            if(unfilteredUserLocation.geoData.user.ID == DatabaseHelper().FetchUser().ID){
                self.visibleUsers.append(unfilteredUserLocation.geoData)
            }
            else{
                if let isSmokerBool : Bool = unfilteredUserLocation.userJSONDictionary["isSmoker"] as? Bool{
                    var isSmokerString : String = String()
                    if isSmokerBool{
                        isSmokerString = SmokerStates.IsSmoker.rawValue
                    }
                    else{
                        isSmokerString = SmokerStates.NotSmoker.rawValue
                    }
                    if isSmokerString == smokerFilter || smokerFilter
                        == SmokerStates.DontCareSmoker.rawValue{
                            if let hasAC : Bool = unfilteredUserLocation.userJSONDictionary["hasAC"] as? Bool{
                                var hasACString : String = String()
                                if hasAC{
                                    hasACString = ACStates.AC.rawValue
                                }
                                else {
                                    hasACString = ACStates.NoAC.rawValue
                                }
                                if hasACString == ACFilter || ACFilter == ACStates.DontCareAC.rawValue{
                                    if let gender = unfilteredUserLocation.userJSONDictionary["genderType"] as? String{
                                        if gender == genderFilter || genderFilter == GenderStates.DontCareGender.rawValue{
                                            
                                            self.visibleUsers.append(unfilteredUserLocation.geoData)
                                        }
                                    }
                                }
                            }
                    }
                }
                
            }
            
        }
        
        for userLocation in self.visibleUsers{
            var userAnnotation : MKPointAnnotation = MKPointAnnotation()
            if(self.firsttime){
                var latDelta : CLLocationDegrees = 0.15
                var lonDelta : CLLocationDegrees = 0.15
                var span : MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
                var location : CLLocationCoordinate2D = CLLocationCoordinate2DMake(userLocation.latitude,userLocation.longitude)
                var Region : MKCoordinateRegion = MKCoordinateRegionMake(location, span)
                self.map.setRegion(Region, animated: false)
                self.firsttime = false
            }
            userAnnotation.coordinate.latitude = userLocation.latitude
            userAnnotation.coordinate.longitude = userLocation.longitude
            userAnnotation.title = userLocation.user.login
            
            println("addingannotation:" + userLocation.user.login)
            addedUserQueue.append(userLocation)
            self.map.addAnnotation(userAnnotation)
            
            
        }
    }
    func requestMapUpdate(){
        if currentUserLocation != nil{
            
            availableUsers = []
            println("requestmap")
            var userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            //userFilter.minCreatedAt = NSDate(timeInterval: -3600, sinceDate: NSDate())
            //userFilter.maxCreatedAt = NSDate()
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            userFilter.lastOnly = true
            var responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: 1, perPage: 70)
            
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in
                for geoData in userLocations{
                    if let element = geoData as? QBLGeoData{
                        self.availableUsers.append(UserGeoData(geoData: element))
                    }
                }
                
                self.dontDeselectAnnotation = true
                self.map.removeAnnotations(self.map.annotations)
                self.dontDeselectAnnotation = false
                
                self.visibleUsers = []
                self.filterIntoVisibleUsers()
                
                
                self.boxView.removeFromSuperview()
                
                
                
                self.view.userInteractionEnabled = true
                for ann in self.map.annotations{
                    if ann.title == self.selectedAnnotationID{
                        let selectedAnn = ann as! MKAnnotation
                        self.map.selectAnnotation(selectedAnn, animated: false)
                    }
                }
                self.populateMap()
                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    self.boxView.removeFromSuperview()
                    self.view.userInteractionEnabled = true
            }
        }
    }
    func drawRoute(routeID : String){
        var routeToBeDrawn : Route?
        QBRequest.objectWithClassName("Route", ID: routeID, successBlock: { (response : QBResponse!,
            route : QBCOCustomObject!) -> Void in
            
            routeToBeDrawn = Route()
            routeToBeDrawn?.sourceLocation = CLLocation(latitude: route.fields.valueForKey("sourceLocationLatitude") as! Double, longitude: route.fields.valueForKey("sourceLocationLongitude") as! Double)
            routeToBeDrawn?.destinationLocation = CLLocation(latitude: route.fields.valueForKey("destinationLocationLatitude") as! Double, longitude: route.fields.valueForKey("destinationLocationLongitude") as! Double)
            
            
            
            var urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + routeToBeDrawn!.sourceLocation.coordinate.latitude.description + "," + routeToBeDrawn!.sourceLocation.coordinate.longitude.description
            urlString = urlString + "&destination=" + routeToBeDrawn!.destinationLocation.coordinate.latitude.description + "," + routeToBeDrawn!.destinationLocation.coordinate.longitude.description + "&sensor=false"
            println("routeurl:" + urlString)
            GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                if status{
                    if let routes: AnyObject = dictionary["routes"]{
                        if routes.count > 0{
                            if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                if let polylineString : String = polylineDictionary["points"] as? String{
                                    var polyline : MKPolyline = MKPolyline(encodedString: polylineString)
                                    self.selectedRoutePolyline = polyline
                                }
                            }
                        }
                    }
                    
                    
                }
                else if !status{
                    println("routeDictionaryNotFound!")
                    
                }
            })
            }) { (response : QBResponse!) -> Void in
                
        }
        
        
    }
    @IBOutlet weak var userListViewButton: UIBarButtonItem!
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        let identifier = "pin"
        var view: MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        { // 2
            println()
            dequeuedView.annotation = annotation
            // Add a detail disclosure button to the callout.
            var rightButton : UIButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIButton
            dequeuedView.rightCalloutAccessoryView = rightButton
            
            // Add an image to the left callout.
            var imageView : UIImageView = UIImageView(image: UIImage(named: "placeholder.jpg"))
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 6;
            imageView.clipsToBounds = true;
            dequeuedView.leftCalloutAccessoryView = imageView;
            for userLocation in visibleUsers{
                if userLocation.user.login == annotation.title{
                    if let json : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user){
                        if let isDriver = json["isDriver"] as? Bool{
                            if isDriver {
                                var image : UIImage = UIImage(named: "DriverAnnotation.png")!
                                image = imageResize(image: image, CGSize(width: image.size.width/2, height : image.size.height/2))
                                dequeuedView.image = image
                            }
                            else if !isDriver{
                                var image : UIImage = UIImage(named: "PassengerAnnotation.png")!
                                image = imageResize(image: image, CGSize(width: image.size.width/2, height : image.size.height/2))
                                dequeuedView.image = image
                                
                            }
                        }
                    }
                }
            }
            
            var annTitle : String = annotation.title!
            
            annTitle = annTitle.lowercaseString
            println("userlogin: " + DatabaseHelper().FetchUser().ID.description)
            if annTitle == DatabaseHelper().FetchUser().login.lowercaseString{
                var circleOverlay : MKCircle = MKCircle(centerCoordinate: annotation.coordinate, radius: currentRadius * 1000)
                
                mapView.removeOverlays(mapView.overlays)
                if selectedRoutePolyline != nil{
                    mapView.addOverlay(selectedRoutePolyline)
                }
                mapView.addOverlay(circleOverlay)
                
            }
            
            return dequeuedView
            
            
        } else {
            // 3
            println("hello2")
            view.annotation = annotation
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            // Add a detail disclosure button to the callout.
            var rightButton : UIButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIButton
            view.rightCalloutAccessoryView = rightButton
            
            // Add an image to the left callout.
            var imageView : UIImageView = UIImageView(image: UIImage(named: "placeholder.jpg"))
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 6;
            imageView.clipsToBounds = true;
            view.leftCalloutAccessoryView = imageView;
            for userLocation in visibleUsers{
                if userLocation.user.login == annotation.title{
                    if let json : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user){
                        if let isDriver = json["isDriver"] as? Bool{
                            if isDriver {
                                var image : UIImage = UIImage(named: "DriverAnnotation.png")!
                                image = imageResize(image: image, CGSize(width: image.size.width/2, height : image.size.height/2))
                                view.image = image
                                
                            }
                            else if !isDriver{
                                var image : UIImage = UIImage(named: "PassengerAnnotation.png")!
                                image = imageResize(image: image, CGSize(width: image.size.width/2, height : image.size.height/2))
                                view.image = image
                                
                            }
                        }
                    }
                }
            }
            return view
        }
        //return view
    }
    
    
    
    
    
    
    
    
    
    
    var selectedGeoData : QBLGeoData = QBLGeoData()
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        selectedGeoData.user = QBUUser()
        selectedGeoData.user.login = ""
        for element in visibleUsers{
            if let annTitle = view.annotation.title{
                if element.user.login.lowercaseString == annTitle.lowercaseString{
                    selectedGeoData = element
                    currentUserLocation = element
                }
            }
            
        }
        if control == view.rightCalloutAccessoryView && !selectedGeoData.user.login.isEmpty {
            self.performSegueWithIdentifier("goToProfileFromMapSegue", sender: self)
        }
    }
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        for ann in mapView.annotations
        mapView.deselectAnnotation(<#annotation: MKAnnotation!#>, animated: <#Bool#>)
        var region = mapView.region
        var orientation = UIApplication.sharedApplication().statusBarOrientation
        
        if orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown{
            var edgePoint = mapView.center
            edgePoint.x = 0.06 * edgePoint.x
            var edgeCoord : CLLocationCoordinate2D = mapView.convertPoint(edgePoint, toCoordinateFromView: mapView)
            var edgeLocation = CLLocation(latitude: edgeCoord.latitude , longitude: edgeCoord.longitude)
            var centerCoord : CLLocationCoordinate2D = mapView.centerCoordinate
            var centerLocation : CLLocation = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
            if edgeLocation.distanceFromLocation(centerLocation)<10000.0{
                currentRadius = edgeLocation.distanceFromLocation(centerLocation)/1000.0
            }
            else
            {
                currentRadius = 10
            }
        }
        else{
            var edgePoint = mapView.center
            edgePoint.y = 0.06 * edgePoint.y
            var edgeCoord : CLLocationCoordinate2D = mapView.convertPoint(edgePoint, toCoordinateFromView: mapView)
            var edgeLocation = CLLocation(latitude: edgeCoord.latitude , longitude: edgeCoord.longitude)
            var centerCoord : CLLocationCoordinate2D = mapView.centerCoordinate
            var centerLocation : CLLocation = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
            if edgeLocation.distanceFromLocation(centerLocation)<10000.0{
                currentRadius = edgeLocation.distanceFromLocation(centerLocation)/1000.0
            }
            else
            {
                currentRadius = 10
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToProfileFromMapSegue"{
            let profileViewController : UserProfileViewController = segue.destinationViewController as! UserProfileViewController
            profileViewController.selectedUser = selectedGeoData.user
            profileViewController.hasBackButton = true
        }
    }
    
    
}
