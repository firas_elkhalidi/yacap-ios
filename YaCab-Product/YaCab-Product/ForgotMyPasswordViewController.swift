//
//  ForgotMyPasswordViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/5/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class ForgotMyPasswordViewController: UIViewController {
    ///background image of the view
    var backgroundImage : UIImage = UIImage()
    ///instance of the background image view of the view
    @IBOutlet weak var backgroundImageView: UIImageView!
    ///instance of a text field that will hold the email of the user
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.contentSize.height = UIScreen.mainScreen().bounds.height - 100
        //backgroundImageView.image = backgroundImage
        // Do any additional setup after loading the view.
        setTheme()
    }
    override func viewDidAppear(animated: Bool) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "ForgotMyPasswordViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    ///instance that holds all the views inside of it
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    ///instance of the close button that will close the forgot my password view controller
    @IBOutlet weak var closeButton: UIButton!
    /**
    method that detects when the close button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func closeButtonPressed(sender: UIButton) {
        
        self.view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    ///label that is used to tell the user what to do
    @IBOutlet weak var label: UILabel!
    ///method that is used to set the theme of the view controller
    func setTheme(){
        emailTextField.keyboardType = UIKeyboardType.EmailAddress
        //self.view.backgroundColor = UIColor.clearColor()
        sendEmailButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        sendEmailButton.setTitle("SEND EMAIL", forState: UIControlState.Normal)
        sendEmailButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        emailTextField.backgroundColor = UIColor.whiteColor()
        //closeButton.tintColor = UIColor.whiteColor()
        let image = UIImage(named: "downArrow")!
        colorImage(image, color: UIColor.whiteColor())
        closeButton.setImage(image, forState: UIControlState.Normal)
        label.textColor = ApplicationThemeColor().GreyTextColor
        scrollView.backgroundColor = UIColor.clearColor()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    ///instance of the button used to send an email to the user
    @IBOutlet weak var sendEmailButton: UIButton!
    /**
    method that detects when the send email button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func sendEmailButtonPressed(sender: UIButton) {
        self.view.endEditing(true)

        QBRequest.resetUserPasswordWithEmail(emailTextField.text, successBlock: { (response : QBResponse!) -> Void in
            let alert : UIAlertController = UIAlertController(title: "Alert", message: "Email to change the password has been sent to the email", preferredStyle: UIAlertControllerStyle.Alert)
            let actionOK : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            alert.addAction(actionOK)
            self.presentViewController(alert, animated: true, completion: nil)
            }) { (response : QBResponse!) -> Void in
                let alert : UIAlertController = UIAlertController(title: "Alert", message: "Something went wrong please check that it is a valid email or check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                    
                })
                
                alert.addAction(actionOK)
                self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
