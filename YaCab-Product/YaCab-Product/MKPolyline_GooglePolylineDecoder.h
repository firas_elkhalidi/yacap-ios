//
//  MKPolyline_GooglePolylineDecoder.h
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/10/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKPolyline ()
+ (MKPolyline*)polylineWithEncodedString:(NSString *)encodedString;
@end
