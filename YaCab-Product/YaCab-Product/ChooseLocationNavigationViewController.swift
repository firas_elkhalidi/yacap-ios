//
//  ChooseLocationNavigationViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/8/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class ChooseLocationNavigationViewController: UINavigationController {
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
