//
//  Route.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/7/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
import MapKit
class Route {
    ///holds the id of the route. Same as the one from QuickBlox
    var ID : String = String()
    ///holds the user that created that route
    var userID : UInt = UInt()
    ///holds the parent of the object. Same as the one from QuickBlox
    var parentID : UInt = UInt()
    ///holds the source location of the route
    var sourceLocation : CLLocation = CLLocation()
    ///holds the source location's name of the route
    var sourceLocationName : String = String()
    ///holds the destination location of the route
    var destinationLocation : CLLocation = CLLocation()
    ///holds the destination location name of the route
    var destinationLocationName : String = String()
    ///boolean that indicates whether the object was deleted by the user or not
    var isDeleted : Bool = Bool()
    ///holds the date that it was created at
    var createdAt : NSDate = NSDate()

    init(ID : String, userID : UInt, parentID : UInt, sourceLocation : CLLocation, sourceLocationName : String, destinationLocation : CLLocation, destinationLocationName : String, isDeleted : Bool, createdAt : NSDate){
        self.ID = ID
        self.userID = userID
        self.sourceLocation = sourceLocation
        self.sourceLocationName = sourceLocationName
        self.destinationLocation = destinationLocation
        self.destinationLocationName = destinationLocationName
        self.isDeleted = isDeleted
        self.createdAt = createdAt
    }
    init(){
        
    }
}