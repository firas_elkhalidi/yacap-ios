//
//  ProfileViewController.swift
//  YaCab-Product
//
//  Created by Mohammed Al Khalidi on 6/16/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    var selectedUser : QBUUser?
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var extraDataTextView: UITextView!

    override func viewDidAppear(animated: Bool) {
        currentViewController = self
    }
    

    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if selectedUser == nil {
            selectedUser = currentUser
        }
        QBRequest.userWithID(selectedUser!.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            self.loginLabel.text = self.selectedUser!.login
            if self.selectedUser!.ID == DatabaseHelper().FetchUser().ID{
                let editButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("editProfile"))
                self.navigationItem.rightBarButtonItem = editButton
            }
            if self.selectedUser!.customData != nil{
                let str : String = self.selectedUser!.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                    if let genderType: String = json["gender"] as? String{
                        //print(json["gender"])
                        self.extraDataTextView.text = self.extraDataTextView.text + "Gender :  " + genderType
                    }
                    if let isDriver = json["isDriver"] as? Bool{
                        if !isDriver{
                            self.extraDataTextView.text = self.extraDataTextView.text + " is not a driver "
                        }
                        else if isDriver{
                            self.extraDataTextView.text = self.extraDataTextView.text + "is a driver "
                        }
                    }
                    if let isSmoker = json["isSmoker"] as? Bool{
                        if !isSmoker{
                            self.extraDataTextView.text = self.extraDataTextView.text + " is not a smoker "
                        }
                        else if isSmoker{
                            self.extraDataTextView.text = self.extraDataTextView.text + "is a smoker "
                        }
                    }
                }
            }
            }) { (response : QBResponse!) -> Void in
            
        }

        // Do any additional setup after loading the view.
    }
    func editProfile(){
        self.performSegueWithIdentifier("goToEditProfileSegue", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func unwindToProfileViewSegue(sender : UIStoryboardSegue){
        if let profileViewController : ProfileViewController = sender.sourceViewController as? ProfileViewController{
            selectedUser = profileViewController.selectedUser
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToEditProfileSegue"{
            let editProfileViewController = segue.destinationViewController as! EditProfileViewController
            editProfileViewController.selectedUser = self.selectedUser!
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
