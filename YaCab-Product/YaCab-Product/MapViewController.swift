//
//  MapViewController.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//
/**
method that resizes an image according to the sizeChange parameter.
- Parameters:
    - image: the image to be scaled.
    - sizeChange: the size change.
*/
func imageResize (image image:UIImage, sizeChange:CGSize)-> UIImage{
    
    let hasAlpha = true
    let scale: CGFloat = 0.0 // Use scale factor of main screen
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
    
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    return scaledImage
}

import UIKit
import MapKit
import Darwin
///current radius of visibility on the map
var currentRadius : Double = 10
///state of the gender filter
var genderFilter : String = GenderStates.DontCareGender.rawValue
///state of the smoker filter
var smokerFilter : String = SmokerStates.DontCareSmoker.rawValue
///state of the ac filter
var ACFilter : String = ACStates.DontCareAC.rawValue
class MapViewController: UIViewController,MKMapViewDelegate,CurrentThemeDelegate, FiltersViewControllerDelegate {
    /**
    detects when the lockButton is pressed.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func lockButtonPressed(sender: UIButton) {
        lockMapToUserLocation = !lockMapToUserLocation
        if(!lockMapToUserLocation && currentUserLocation != nil){
            self.map.userTrackingMode = MKUserTrackingMode.None
        }
        else if lockMapToUserLocation && currentUserLocation != nil && currentUserHeading != nil{
            self.map.userTrackingMode = MKUserTrackingMode.FollowWithHeading
        }
        setupLockMapButton()
    }
    ///sets up the lock map button according to its current state
    func setupLockMapButton(){
        if lockMapToUserLocation{
            lockButton.setTitle("UNLOCKED", forState: UIControlState.Normal)
        }
        else{
            lockButton.setTitle("LOCKED", forState: UIControlState.Normal)
        }
    }
    ///instance of the lock button used to lock and unlock the map according to the user's heading.
    @IBOutlet weak var lockButton: UIButton!
    
    ///instance of the search filters button used to open up the filterTableViewController.
    @IBOutlet weak var searchFiltersButton: UIButton!
    ///boolean value used to convey whether this view controller was opened for the firsttime
    var firsttime = true
    ///array of visible users on the map
    var visibleUsers : [QBLGeoData] = []
    ///array of available users before they got filtered
    var availableUsers : [UserGeoData] = []
    ///timer that updates the locations of the users on the map
    var updateMapTimer = NSTimer()
    ///boolean that conveys whether the view is currently visible or not
    var viewIsVisible = false
    ///boolean that conveys whether the map should remove the selection of an annotation after it is updated or not
    var dontDeselectAnnotation = false
    ///holds the value of the selected annotation id on the map
    var selectedAnnotationID : String = String()
    ///holds the image of the selected annotation on the map
    var selectedAnnotationImage : UIImage?
    ///holds the id of the selected annotation's route id
    var selectedRouteID : String?
    ///holds the polyline of the selected annnotation's route
    var selectedRoutePolyline : MKPolyline?
    ///holds the user's route polyline
    var myPolyline : MKPolyline?
    ///instance of the FiltersTableViewController that allows the user to change the filters and the radius of his/her search
    var filtersTableViewController : FiltersTableViewController?
    ///boolean that indicates whether the map should be locked to the user's location and heading
    var lockMapToUserLocation = false
    ///holds the value of the current radius color
    var currentCircleColor = ApplicationThemeColor().GreenThemeColor
    ///holds the value of the user's current polyline color
    var myCurrentLineColor = ApplicationThemeColor().GreenThemeColor
    ///holds the value of the selected annotation's current polyline color
    var currentSelectedRouteLineColor = ApplicationThemeColor().GreenThemeColor
    ///holds the circle overlay that represents the radius of the search on the map
    var circleOverlay : MKCircle = MKCircle(centerCoordinate: CLLocationCoordinate2D(), radius: currentRadius * 1000)
    ///instance of the map that is found in the view controller
    @IBOutlet var map: MKMapView!
    ///instance of a view that is presented to show a loading indicator
    var boxView : UIView = UIView()
    ///delegate method of the filtersTableViewController that is used to inform the view controller that the filters' values were changed
    func didChangeFilters() {
        ACFilter = filtersTableViewController!.acFilterLocal!
        genderFilter = filtersTableViewController!.genderFilterLocal!
        smokerFilter = filtersTableViewController!.smokerFilterLocal!
        filtersTableViewController = nil
        currentRadius = tableViewFilterRadius
        requestMapUpdateOnce()
    }
    
    /**
    method that detects the filterButton was pressed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func filterButtonPressd(sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let filtersNavigationController = storyboard.instantiateViewControllerWithIdentifier("FiltersNavigationController") as! FiltersNavigationController
        filtersTableViewController = filtersNavigationController.topViewController as? FiltersTableViewController
        filtersTableViewController!.smokerFilterLocal = smokerFilter
        filtersTableViewController!.genderFilterLocal = genderFilter
        filtersTableViewController!.acFilterLocal = ACFilter
        filtersTableViewController?.delegate = self
        self.presentViewController(filtersNavigationController, animated: true) { () -> Void in
            
        }
    }

    ///hamburger button
    @IBOutlet var menuButton: UIBarButtonItem!
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView)
    {
        if !dontDeselectAnnotation {
            selectedAnnotationID = ""
            selectedAnnotationImage = nil
            selectedRouteID = nil
            selectedRoutePolyline = nil
        }
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("clearSelection"), userInfo: nil, repeats: false)
    }
    ///method that deselects the selected annotation on the map
    func clearSelection(){
        if map.annotations.count > 0{
            var clearSelection : Bool = true
            for ann in map.annotations{
                if let annotation = ann as? MKPointAnnotation{
                    if annotation.title == selectedAnnotationID{
                        clearSelection = false
                    }
                }
            }
            if clearSelection{
                selectedAnnotationID = ""
                selectedAnnotationImage = nil
                selectedRouteID = nil
                selectedRoutePolyline = nil
            }
        }
    }
    ///delegate method from the CurrentThemeDelegate that informs the view controller that the theme has changed
    func themeDidChange() {
        setTheme()
        if let routeID = getCustomDataDictionaryFromUser(currentUser)?.valueForKey("activeRouteID") as? String{
            if !routeID.isEmpty{
                self.drawCurrentUserRoute(routeID)
            }
        }
    }
    ///method that sets the theme of the view controller
    func setTheme(){

        self.title = "Map"
        //genderButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        //isSmokerButton.imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
        if currentTheme == Theme.PassengerTheme.rawValue{
            currentCircleColor = ApplicationThemeColor().GreenThemeColor
            self.lockButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
            self.navigationController?.navigationBar.translucent = false
            //genderButton.imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
            //isSmokerButton.imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
            self.view.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.searchFiltersButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.searchFiltersButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            currentCircleColor = ApplicationThemeColor().YellowThemeColor
            self.lockButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
            //listViewButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            //listViewButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
            self.navigationController?.navigationBar.translucent = false
            //genderButton.imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
            //isSmokerButton.imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
            self.view.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.searchFiltersButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.searchFiltersButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
        }
    }
    override func viewDidLoad() {
        currentThemeDelegate = self
        if waitForCurrentViewController{
            waitForCurrentViewController = false
            let appdel = UIApplication.sharedApplication().delegate as! AppDelegate
            appdel.handlePushNotificationFromBackground(pushOpponentUserInfo!)
        }
        setupLockMapButton()
        
        
        super.viewDidLoad()
        map.delegate = self
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        //setupFilterButtons()
        setTheme()
        //NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("updateCircleLocation"), userInfo: nil, repeats: true)
    }
    ///method that gets the user's route and draws it
    func getUserRoute(){
        QBRequest.userWithID(DatabaseHelper().FetchUser().ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            currentUser = user
            if let routeID = getCustomDataDictionaryFromUser(user)?.valueForKey("activeRouteID") as? String{
                if !routeID.isEmpty{
                    self.drawCurrentUserRoute(routeID)
                    
                }
            }
            }) { (response : QBResponse!) -> Void in
                
        }
    }
    override func viewDidAppear(animated: Bool) {
        
        
        //selectedGeoData = QBLGeoData()
        currentViewController = self
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView.backgroundColor = UIColor.whiteColor()
        boxView.alpha = 1.0
        boxView.layer.cornerRadius = 10
        //map.userTrackingMode = MKUserTrackingMode.FollowWithHeading
        //firsttime = true
        //map.userTrackingMode = MKUserTrackingMode.None
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Map"
        //self.view.userInteractionEnabled = false
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        viewIsVisible = true
        requestMapUpdateOnce()
        populateMap()
        updateCircleLocation()
        getUserRoute()
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MapViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
    }
    override func viewDidDisappear(animated: Bool) {
        viewIsVisible = false
        //firsttime = true
        //updateMapTimer.invalidate()
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        selectedAnnotationID = view.annotation!.title!!
        if selectedAnnotationImage != nil{
            let imageView = UIImageView(image: self.selectedAnnotationImage)
            
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 8;
            imageView.clipsToBounds = true;
            view.leftCalloutAccessoryView = imageView
            
        }
        else {
            for userLocation in self.visibleUsers{
                if selectedAnnotationID == userLocation.user.login{
                    if _imageCache!.objectForKey(userLocation.user.ID.description) != nil{
                        self.selectedAnnotationImage = UIImage(data:_imageCache!.objectForKey(userLocation.user.ID.description) as! NSData)!
                        let imageView = UIImageView(image: self.selectedAnnotationImage)
                        imageView.frame = CGRectMake(0,0,31,31);
                        imageView.layer.cornerRadius = imageView.frame.size.width / 8;
                        imageView.clipsToBounds = true;
                        view.leftCalloutAccessoryView = imageView
                    }
                    QBRequest.downloadFileWithID(UInt(userLocation.user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                        _imageCache!.setObject(data, forKey: userLocation.user.ID.description)
                        if self.selectedAnnotationImage != nil{
                            if self.selectedAnnotationImage != UIImage(data: data){
                                self.selectedAnnotationImage = UIImage(data: data)!
                                let imageView = UIImageView(image: self.selectedAnnotationImage)
                                imageView.frame = CGRectMake(0,0,31,31);
                                imageView.layer.cornerRadius = imageView.frame.size.width / 8;
                                imageView.clipsToBounds = true;
                                view.leftCalloutAccessoryView = imageView
                            }
                        }
                        else{
                            self.selectedAnnotationImage = UIImage(data: data)!
                            let imageView = UIImageView(image: self.selectedAnnotationImage)
                            imageView.frame = CGRectMake(0,0,31,31);
                            imageView.layer.cornerRadius = imageView.frame.size.width / 8;
                            imageView.clipsToBounds = true;
                            view.leftCalloutAccessoryView = imageView
                        }

                        }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                            
                        }, errorBlock: { (response : QBResponse!) -> Void in
                    })
                    let customUserData : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user)!
                    if let activeRouteID = customUserData.valueForKey("activeRouteID") as? String{
                        if customUserData.valueForKey("isDriver") as! Bool{
                            currentSelectedRouteLineColor = UIColor.redColor()
                        }
                        else{
                            currentSelectedRouteLineColor = ApplicationThemeColor().GreenThemeColor
                        }
                        drawRoute(activeRouteID)
                    }
                    else {
                        selectedRoutePolyline = nil
                    }
                    
                }
            }
        }
        
    }
    ///method that starts the updates of the map users
    func populateMap(){
        if viewIsVisible{
            updateMapTimer = NSTimer.scheduledTimerWithTimeInterval(20, target: self, selector: Selector("requestMapUpdate"), userInfo: nil, repeats: false)
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
            if overlay === myPolyline{
                polylineRenderer.strokeColor = myCurrentLineColor
            }
            else if overlay === selectedRoutePolyline{
                polylineRenderer.strokeColor = currentSelectedRouteLineColor
            }
            
            
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        if overlay is MKCircle{
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.strokeColor = currentCircleColor
            circleRenderer.lineWidth = 1
            circleRenderer.fillColor = currentCircleColor
            circleRenderer.alpha = 0.3
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
    ///method that filters the unfiltered user locations array into the visible users array
    func filterIntoVisibleUsers(){
        for carpooler in currentCarpoolers{
            carpooler.isVisible = false
        }
        for unfilteredUserLocation in self.availableUsers{
            
            if(unfilteredUserLocation.geoData.user.ID == DatabaseHelper().FetchUser().ID){
                self.visibleUsers.append(unfilteredUserLocation.geoData)
            }
            else{
                var skipFilter = false
                for carpooler in currentCarpoolers{
                    if carpooler.user.login == unfilteredUserLocation.geoData.user.login{
                        self.visibleUsers.append(unfilteredUserLocation.geoData)
                        skipFilter = true
                    }
                }
                if !skipFilter{
                    
                    if let isSmokerBool : Bool = unfilteredUserLocation.userJSONDictionary["isSmoker"] as? Bool{
                        var isSmokerString : String = String()
                        if isSmokerBool{
                            isSmokerString = SmokerStates.IsSmoker.rawValue
                        }
                        else{
                            isSmokerString = SmokerStates.NotSmoker.rawValue
                        }
                        if isSmokerString == smokerFilter || smokerFilter
                            == SmokerStates.DontCareSmoker.rawValue{
                                if let hasAC : Bool = unfilteredUserLocation.userJSONDictionary["hasAC"] as? Bool{
                                    var hasACString : String = String()
                                    if hasAC{
                                        hasACString = ACStates.AC.rawValue
                                    }
                                    else {
                                        hasACString = ACStates.NoAC.rawValue
                                    }
                                    if hasACString == ACFilter || ACFilter == ACStates.DontCareAC.rawValue || !(unfilteredUserLocation.userJSONDictionary["isDriver"] as! Bool){
                                        if let gender = unfilteredUserLocation.userJSONDictionary["genderType"] as? String{
                                            if gender == genderFilter || genderFilter == GenderStates.DontCareGender.rawValue{
                                                self.visibleUsers.append(unfilteredUserLocation.geoData)
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
                
            }
            
        }
        if(self.firsttime && currentUserLocation != nil){
            self.firsttime = false
            self.map.centerCoordinate.latitude = currentUserLocation!.latitude
            self.map.centerCoordinate.longitude = currentUserLocation!.longitude
            self.map.camera.altitude = 1500
            self.map.userTrackingMode = MKUserTrackingMode.None
            
        }
        for userLocation in self.visibleUsers{
            let userAnnotation : MKPointAnnotation = MKPointAnnotation()
            userAnnotation.coordinate.latitude = userLocation.latitude
            userAnnotation.coordinate.longitude = userLocation.longitude
            userAnnotation.title = userLocation.user.login
            self.map.addAnnotation(userAnnotation)
        }
//        if(!lockMapToUserLocation && currentUserLocation != nil){
//            self.map.userTrackingMode = MKUserTrackingMode.None
//        }
//        else if lockMapToUserLocation && currentUserLocation != nil && currentUserHeading != nil{
//            self.map.userTrackingMode = MKUserTrackingMode.FollowWithHeading
//        }
    }
    
    ///method that requests the map update one
    func requestMapUpdateOnce(){
        if currentUserLocation != nil{
            
            availableUsers = []
            //print("requestmap")
            let userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            //userFilter.minCreatedAt = NSDate(timeInterval: -3600, sinceDate: NSDate())
            //userFilter.maxCreatedAt = NSDate()
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            userFilter.lastOnly = true
            let responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: 1, perPage: 70)
            
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in
                
                var userIDs : [UInt] = []
                for carpooler in currentCarpoolers{
                    if !carpooler.isVisible{
                        userIDs.append(carpooler.user.ID)
                    }
                }
                let filter : QBLGeoDataFilter = QBLGeoDataFilter()
                filter.userIDs = userIDs
                filter.lastOnly = true
                QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: { (response : QBResponse!, geodata :[AnyObject]!, responsePage : QBGeneralResponsePage!) -> Void in
                    //print("numberofnonvisiblecarpoolers:" + geodata.count.description)
                    var insertedUserIDs : [UInt] = []
                    for g in geodata{
                        if let userGeoData = g as? QBLGeoData{
                            self.availableUsers.append(UserGeoData(geoData: userGeoData))
                            insertedUserIDs.append(userGeoData.user.ID)
                        }
                    }
                    
                    for geoData in userLocations{
                        if let element = geoData as? QBLGeoData{
                            if !insertedUserIDs.contains(element.user.ID){
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                    }
                    self.dontDeselectAnnotation = true
                    self.map.removeAnnotations(self.map.annotations)
                    self.dontDeselectAnnotation = false
                    
                    self.visibleUsers = []
                    self.filterIntoVisibleUsers()
                    
                    
                    self.boxView.removeFromSuperview()
                    
                    
                    
                    //self.view.userInteractionEnabled = true
                    for ann in self.map.annotations{
                        if ann.title!! == self.selectedAnnotationID{
                            let selectedAnn = ann
                            self.map.selectAnnotation(selectedAnn, animated: false)
                        }
                    }
                    //self.populateMap()
                    
                    
                    
                    }) { (response : QBResponse!) -> Void in
                        for geoData in userLocations{
                            if let element = geoData as? QBLGeoData{
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                        self.dontDeselectAnnotation = true
                        self.map.removeAnnotations(self.map.annotations)
                        self.dontDeselectAnnotation = false
                        
                        self.visibleUsers = []
                        self.filterIntoVisibleUsers()
                        
                        
                        self.boxView.removeFromSuperview()
                        
                        
                        
                        //self.view.userInteractionEnabled = true
                        for ann in self.map.annotations{
                            if ann.title!! == self.selectedAnnotationID{
                                let selectedAnn = ann
                                self.map.selectAnnotation(selectedAnn, animated: false)
                            }
                        }
                        self.populateMap()
                }
                
                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    self.boxView.removeFromSuperview()
                    //self.view.userInteractionEnabled = true
            }
        }
    }
    ///method that requests the map update and then calls the populate method again.
    func requestMapUpdate(){
        
        //NSLog("requestedMapUpdate")
        
        if currentUserLocation != nil{
            
            availableUsers = []
            //print("requestmap")
            let userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            //userFilter.minCreatedAt = NSDate(timeInterval: -3600, sinceDate: NSDate())
            //userFilter.maxCreatedAt = NSDate()
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            userFilter.lastOnly = true
            let responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: 1, perPage: 70)
            
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in

                var userIDs : [UInt] = []
                for carpooler in currentCarpoolers{
                    if !carpooler.isVisible{
                        userIDs.append(carpooler.user.ID)
                    }
                }
                let filter : QBLGeoDataFilter = QBLGeoDataFilter()
                filter.userIDs = userIDs
                QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: { (response : QBResponse!, geodata :[AnyObject]!, responsePage : QBGeneralResponsePage!) -> Void in
                    //print("numberofnonvisiblecarpoolers:" + geodata.count.description)
                    var insertedUserIDs : [UInt] = []
                    for g in geodata{
                        if let userGeoData = g as? QBLGeoData{
                            self.availableUsers.append(UserGeoData(geoData: userGeoData))
                            insertedUserIDs.append(userGeoData.user.ID)
                        }
                    }
                    
                    for geoData in userLocations{
                        if let element = geoData as? QBLGeoData{
                            if !insertedUserIDs.contains(element.user.ID){
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                    }
                    self.dontDeselectAnnotation = true
                    self.map.removeAnnotations(self.map.annotations)
                    self.dontDeselectAnnotation = false
                    
                    self.visibleUsers = []
                    self.filterIntoVisibleUsers()
                    
                    
                    self.boxView.removeFromSuperview()
                    
                    
                    
                    //self.view.userInteractionEnabled = true
                    for ann in self.map.annotations{
                        if ann.title!! == self.selectedAnnotationID{
                            let selectedAnn = ann 
                            self.map.selectAnnotation(selectedAnn, animated: false)
                        }
                    }
                    self.populateMap()
                    
                    
                    
                    }) { (response : QBResponse!) -> Void in
                        for geoData in userLocations{
                            if let element = geoData as? QBLGeoData{
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                        self.dontDeselectAnnotation = true
                        self.map.removeAnnotations(self.map.annotations)
                        self.dontDeselectAnnotation = false
                        
                        self.visibleUsers = []
                        self.filterIntoVisibleUsers()
                        
                        
                        self.boxView.removeFromSuperview()
                        
                        
                        
                        //self.view.userInteractionEnabled = true
                        for ann in self.map.annotations{
                            if ann.title!! == self.selectedAnnotationID{
                                let selectedAnn = ann 
                                self.map.selectAnnotation(selectedAnn, animated: false)
                            }
                        }
                        self.populateMap()
                }

                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    self.boxView.removeFromSuperview()
                    //self.view.userInteractionEnabled = true
            }
        }
    }
    /**
    draws the current user route according to the routeID.
    - Parameter routeID: route id that is to be drawn.
    */
    func drawCurrentUserRoute(routeID : String){
        var routeToBeDrawn : Route?
        QBRequest.objectWithClassName("Route", ID: routeID, successBlock: { (response : QBResponse!,
            route : QBCOCustomObject!) -> Void in
            
            routeToBeDrawn = Route()
            routeToBeDrawn?.sourceLocation = CLLocation(latitude: route.fields.valueForKey("sourceLocationLatitude") as! Double, longitude: route.fields.valueForKey("sourceLocationLongitude") as! Double)
            routeToBeDrawn?.destinationLocation = CLLocation(latitude: route.fields.valueForKey("destinationLocationLatitude") as! Double, longitude: route.fields.valueForKey("destinationLocationLongitude") as! Double)

            var urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + routeToBeDrawn!.sourceLocation.coordinate.latitude.description + "," + routeToBeDrawn!.sourceLocation.coordinate.longitude.description
            urlString = urlString + "&destination=" + routeToBeDrawn!.destinationLocation.coordinate.latitude.description + "," + routeToBeDrawn!.destinationLocation.coordinate.longitude.description + "&sensor=false"
            //print("routeurl:" + urlString)
            GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                if status{
                    if let routes: AnyObject = dictionary["routes"]{
                        if routes.count > 0{
                            if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                if let polylineString : String = polylineDictionary["points"] as? String{
                                    let polyline : MKPolyline = MKPolyline(encodedString: polylineString)
                                    self.myPolyline = polyline
                                    var currentUserDictionary : NSDictionary?
                                    currentUserDictionary = getCustomDataDictionaryFromUser(currentUser)!
                                    if  let isDriver = currentUserDictionary!.valueForKey("isDriver") as? Bool{
                                        if isDriver{
                                            self.myCurrentLineColor = UIColor.redColor()
                                        }
                                        else{
                                            self.myCurrentLineColor = ApplicationThemeColor().GreenThemeColor
                                        }
                                        self.map.addOverlay(self.myPolyline!)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                else if !status{
                    //print("routeDictionaryNotFound!")
                    
                }
            })
            }) { (response : QBResponse!) -> Void in
                
        }
        
        
    }
    /**
    draws a route on the map according to the route ID given.
    - Parameter routeID: route id that is to be drawn.
    */
    func drawRoute(routeID : String){
        var routeToBeDrawn : Route?
        QBRequest.objectWithClassName("Route", ID: routeID, successBlock: { (response : QBResponse!,
            route : QBCOCustomObject!) -> Void in
            
            routeToBeDrawn = Route()
            routeToBeDrawn?.sourceLocation = CLLocation(latitude: route.fields.valueForKey("sourceLocationLatitude") as! Double, longitude: route.fields.valueForKey("sourceLocationLongitude") as! Double)
            routeToBeDrawn?.destinationLocation = CLLocation(latitude: route.fields.valueForKey("destinationLocationLatitude") as! Double, longitude: route.fields.valueForKey("destinationLocationLongitude") as! Double)
            
            
            
            var urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + routeToBeDrawn!.sourceLocation.coordinate.latitude.description + "," + routeToBeDrawn!.sourceLocation.coordinate.longitude.description
            urlString = urlString + "&destination=" + routeToBeDrawn!.destinationLocation.coordinate.latitude.description + "," + routeToBeDrawn!.destinationLocation.coordinate.longitude.description + "&sensor=false"
            //print("routeurl:" + urlString)
            
            GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                if status{
                    if let routes: AnyObject = dictionary["routes"]{
                        if routes.count > 0{
                            if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                if let polylineString : String = polylineDictionary["points"] as? String{
                                    let polyline : MKPolyline = MKPolyline(encodedString: polylineString)
                                    self.selectedRoutePolyline = polyline
                                    self.selectedRouteID = routeID
                                }
                            }
                        }
                    }
                    
                    
                }
                else if !status{
                    //print("routeDictionaryNotFound!")
                    
                }
            })
            }) { (response : QBResponse!) -> Void in
                
        }
        
        
    }
    ///instance of the user list view button that takes the user to the VisibleUsersViewController
    @IBOutlet weak var userListViewButton: UIBarButtonItem!
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        let view: MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        { // 2
            dequeuedView.annotation = annotation
            // Add a detail disclosure button to the callout.
            let rightButton : UIButton = UIButton(type: UIButtonType.DetailDisclosure)
            dequeuedView.rightCalloutAccessoryView = rightButton
            
            // Add an image to the left callout.
            let imageView : UIImageView = UIImageView(image: UIImage(named: "placeholder.jpg"))
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 6;
            imageView.clipsToBounds = true;
            dequeuedView.leftCalloutAccessoryView = imageView;
            for userLocation in visibleUsers{
                if userLocation.user.login == annotation.title!!{
                    if let json : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user){
                        if let isDriver = json["isDriver"] as? Bool{
                            if isDriver {
                                var image : UIImage = UIImage(named: "DriverAnnotation.png")!
                                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/2.5, height : image.size.height/2.5))
                                dequeuedView.image = image
                                dequeuedView.centerOffset = CGPointMake(0, -dequeuedView.image!.size.height / 2);
                            }
                            else if !isDriver{
                                var image : UIImage = UIImage(named: "PassengerAnnotation.png")!
                                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/2.5, height : image.size.height/2.5))
                                dequeuedView.image = image
                                dequeuedView.centerOffset = CGPointMake(0, -dequeuedView.image!.size.height / 2);
                                
                            }
                        }
                    }
                    for carpooler in currentCarpoolers{
                        if carpooler.user.login == userLocation.user.login{
                            
                            UIView.animateWithDuration(0.5, animations: { () -> Void in
                                dequeuedView.alpha = 0.5
                                }, completion: { (Bool) -> Void in
                                    
                                    
                                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                                        dequeuedView.alpha = 1.0
                                        }, completion: { (Bool) -> Void in
                                            
                                    })
                                    
                                    
                            })
                        }
                    }
                }

            }
            
            var annTitle : String = annotation.title!!
            
            annTitle = annTitle.lowercaseString
            //print("userlogin: " + DatabaseHelper().FetchUser().ID.description)
            if annTitle == DatabaseHelper().FetchUser().login.lowercaseString{
                circleOverlay = MKCircle(centerCoordinate: annotation.coordinate, radius: currentRadius * 1000)
                mapView.removeOverlays(mapView.overlays)
                mapView.addOverlay(circleOverlay)
                if selectedRoutePolyline != nil{
                    mapView.addOverlay(selectedRoutePolyline!)
                }
                if myPolyline != nil {
                    mapView.addOverlay(myPolyline!)
                }
                //mapView.addOverlay(circleOverlay)
                
            }
            
            return dequeuedView
            
            
        } else {
            // 3
            view.annotation = annotation
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            // Add a detail disclosure button to the callout.
            let rightButton : UIButton = UIButton(type: UIButtonType.DetailDisclosure)
            view.rightCalloutAccessoryView = rightButton
            
            // Add an image to the left callout.
            let imageView : UIImageView = UIImageView(image: UIImage(named: "placeholder.jpg"))
            imageView.frame = CGRectMake(0,0,31,31);
            imageView.layer.cornerRadius = imageView.frame.size.width / 6;
            imageView.clipsToBounds = true;
            view.leftCalloutAccessoryView = imageView;
            for userLocation in visibleUsers{
                if userLocation.user.login == annotation.title!!{
                    if let json : NSDictionary = getCustomDataDictionaryFromUser(userLocation.user){
                        if let isDriver = json["isDriver"] as? Bool{
                            if isDriver {
                                var image : UIImage = UIImage(named: "DriverAnnotation.png")!
                                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/2.5, height : image.size.height/2.5))
                                view.image = image
                                view.centerOffset = CGPointMake(0, -view.image!.size.height / 2);
                                
                            }
                            else if !isDriver{
                                var image : UIImage = UIImage(named: "PassengerAnnotation.png")!
                                image = imageResize(image: image, sizeChange: CGSize(width: image.size.width/2.5, height : image.size.height/2.5))
                                view.image = image
                                view.centerOffset = CGPointMake(0, -view.image!.size.height / 2);
                                
                            }
                        }
                    }
                }
                for carpooler in currentCarpoolers{
                    if carpooler.user.login == userLocation.user.login{
                        //print("")
                        UIView.animateWithDuration(0.5, animations: { () -> Void in
                            view.alpha = 0.5
                            }, completion: { (Bool) -> Void in
                                
                                
                                UIView.animateWithDuration(0.5, animations: { () -> Void in
                                    view.alpha = 1.0
                                    }, completion: { (Bool) -> Void in
                                        
                                })
                                
                                
                        })
                    }
                }

            }
            return view
        }
        //return view
    }
    ///holds the value of the selected annotation's geodata on the map
    var selectedGeoData : QBLGeoData = QBLGeoData()
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        selectedGeoData.user = QBUUser()
        selectedGeoData.user.login = ""
        for element in visibleUsers{
            if let annTitle = view.annotation!.title{
                if element.user.login.lowercaseString == annTitle!.lowercaseString{
                    selectedGeoData = element
                    currentUserLocation = element
                }
            }
            
        }
        if control == view.rightCalloutAccessoryView && !selectedGeoData.user.login.isEmpty {
            self.performSegueWithIdentifier("goToProfileFromMapSegue", sender: self)
        }
    }
    ///method that updates the location of the circle that converys the radius of the search on the map
    func updateCircleLocation(){
        if currentUserLocation != nil{
            self.map.removeOverlay(circleOverlay)
            circleOverlay = MKCircle(centerCoordinate: CLLocationCoordinate2D(latitude: currentUserLocation!.latitude, longitude: currentUserLocation!.longitude), radius: currentRadius * 1000)
            self.map.addOverlay(circleOverlay)
        }
    }
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        _ = mapView.region
        if lockMapToUserLocation{
            map.userTrackingMode = MKUserTrackingMode.FollowWithHeading
        }

//
//        let orientation = UIApplication.sharedApplication().statusBarOrientation
//        if orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown{
//            var edgePoint = mapView.center
//            edgePoint.x = 0.06 * edgePoint.x
//            let edgeCoord : CLLocationCoordinate2D = mapView.convertPoint(edgePoint, toCoordinateFromView: mapView)
//            let edgeLocation = CLLocation(latitude: edgeCoord.latitude , longitude: edgeCoord.longitude)
//            let centerCoord : CLLocationCoordinate2D = mapView.centerCoordinate
//            let centerLocation : CLLocation = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
//            if edgeLocation.distanceFromLocation(centerLocation)<1000000.0{
//                currentRadius = edgeLocation.distanceFromLocation(centerLocation)/1000.0
//            }
//            else
//            {
//                currentRadius = 10
//            }
//        }
//        else{
//            var edgePoint = mapView.center
//            edgePoint.y = 0.06 * edgePoint.y
//            let edgeCoord : CLLocationCoordinate2D = mapView.convertPoint(edgePoint, toCoordinateFromView: mapView)
//            let edgeLocation = CLLocation(latitude: edgeCoord.latitude , longitude: edgeCoord.longitude)
//            let centerCoord : CLLocationCoordinate2D = mapView.centerCoordinate
//            let centerLocation : CLLocation = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
//            if edgeLocation.distanceFromLocation(centerLocation)<1000000.0{
//                currentRadius = edgeLocation.distanceFromLocation(centerLocation)/1000.0
//            }
//            else
//            {
//                currentRadius = 10
//            }
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToProfileFromMapSegue"{
            let profileViewController : UserProfileViewController = segue.destinationViewController as! UserProfileViewController
            profileViewController.selectedUser = selectedGeoData.user
            profileViewController.hasBackButton = true
        }
    }
    
    
}
