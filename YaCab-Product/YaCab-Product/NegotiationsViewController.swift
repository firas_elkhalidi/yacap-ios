//
//  NegotiationsViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/5/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
/**
enum that is used to hold the responses of the user.
- PASSENGER_BARGAIN: indicates that the response is from a passenger and that the passenger wants to bargain the price
- PASSENGER_ACCEPT: indicates that the response is from a passenger and that the passenger has accepted the carpool
- PASSENGER_REJECT: indicates that the response is from a passenger and that the passenger has rejected the carpool
- DRIVER_INTERESTED: indicates that the response is from a driver and that the driver is interested in carpooling with the passenger
- DRIVER_NOTINTERESTED: indicates that the response is from a driver and that the driver is not interested in carpooling with the passenger
- FINISH: indicates that the response is from a passenger and that the passenger has reached the destination of the carpool and is finishing it
*/
enum CarpoolResponses : String {
    case  PASSENGER_BARGAIN = "PASSENGER_BARGAIN", PASSENGER_ACCEPT = "PASSENGER_ACCEPT", PASSENGER_REJECT = "PASSENGER_REJECT", DRIVER_INTERESTED = "DRIVER_INTERESTED", DRIVER_NOTINTERESTED = "DRIVER_NOTINTERESTED", FINISH = "FINISH"
}
/**
enum that is used to hold the states of the carpool.
- FREE: indicates that the carpool has been accepted and that it is now in the free chat state
- ACCEPT_BARGAIN_REJECT: indicates that the carpool is now in the bargaining state where the passenger can set a bargaining price, accept the carpool, or reject it
- INTERESTED_NOTINTERESTED: indicates that the carpool is now in a state where the driver has received an offer or counter offer and the driver can be interested or not interested in the new offer
- CANCELLED: indicates that the carpool is in a cancelled state indicating that either the passenger rejected the carpool or that the driver was not interested
- CARPOOL_FINISHED: indicates that the carpool has finished and that the passenger has reached the destination
- INITIAL: indicates that the carpool is in the initial state of when it was created
*/
enum CarpoolStates : String {
    case FREE = "FREE", ACCEPT_BARGAIN_REJECT = "ACCEPT_BARGAIN_REJECT", INTERESTED_NOTINTERESTED = "INTERESTED_NOTINTERESTED", CANCELLED = "CANCELLED",CARPOOL_FINISHED = "CARPOOL_FINISHED", INITIAL = "INITIAL"
}
/**
enum that indicates the color of the cell's background.
- GREEN: indicates the cell is green
- YELLOW: indicates the cell is yellow
*/
enum CellColorStates : String {
    case GREEN = "GREEN", YELLOW = "YELLOW"
}
class NegotiationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,QBChatDelegate, UIScrollViewDelegate, UITextFieldDelegate ,UserRatingViewControllerDelegate{
    ///holds the current page number of the negotiations messages
    var pageNumber = 1
    ///boolean that indicates whether the table view should get any older messages or not
    var getNewPage = true
    ///instance of a label placed next to the driverPriceTextField
    @IBOutlet weak var driverPriceLabel: UILabel!
    ///boolean that indicates whether the chat should receive any new messages or not
    var receiveMessages = false
    ///instance of the driver price text field that is used to set the price of the driver's offer
    @IBOutlet weak var driverPriceTextField: UITextField!
    ///method that is used to hide all elements on the screen that are below the table view
    func hideAllButtons(){
        interestedButton.hidden = true
        notInterestedButton.hidden = true
        requestCarpoolButton.hidden = true
        rejectButton.hidden = true
        acceptButton.hidden = true
        bargainButton.hidden = true
        bargainPromptLabel.hidden = true
        bargainPriceTextField.hidden = true
        freeChatButton.hidden = true
        finishCarpoolButton.hidden = true
        driverPriceTextField.hidden = true
        driverPriceLabel.hidden = true
        
    }
    ///delegate method from UserRatingViewControllerDelegate that is used to indicate that the user has dismissed the user rating view controller
    func willDismissUserRating() {
        self.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealMapViewController", sender: self.revealViewController().rearViewController)
        
        //self.navigationController?.popViewControllerAnimated(true)
    }
    ///instance of a label that is used to indicate that the user is waiting for the opponent user to reply
    @IBOutlet weak var waitingForOtherUserLabel: UILabel!
    ///instance of the interested button that is only accessible to the driver
    @IBOutlet weak var interestedButton: UIButton!
    ///instance of the not interested button that is only accessible to the passenger
    @IBOutlet weak var notInterestedButton: UIButton!
    ///instance of the request carpool button that is available to both the driver and the passenger
    @IBOutlet weak var requestCarpoolButton: UIButton!
    ///instance of the driver price text field's horizontal height contraint
    @IBOutlet weak var driverPriceTextFieldHorizontalConstraint: NSLayoutConstraint!
    ///instance of the reject button that is only accessible by the passenger
    @IBOutlet weak var rejectButton: UIButton!
    ///instance of the accept button that is only accessible by the passenger
    @IBOutlet weak var acceptButton: UIButton!
    ///instance of the bargain button that is only accessible by the passenger
    @IBOutlet weak var bargainButton: UIButton!
    ///instance of a label that is placed above the bargain price text field
    @IBOutlet weak var bargainPromptLabel: UILabel!
    ///instance of the bargain price text field that is only accessible by the passenger
    @IBOutlet weak var bargainPriceTextField: UITextField!
    ///instance of an image view containing the opponent's profile image
    @IBOutlet weak var opponentImageView: UIImageView!
    ///instance of an image view that seperates between the user's and the opponent's profile picture
    @IBOutlet weak var imageSeperatorImageView: UIImageView!
    /**
    method that detects when the bargain button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func bargainButtonPressed(sender: UIButton) {

        self.view.endEditing(true)
        bargainPriceTextField.resignFirstResponder()
        if bargainPriceTextField.text!.isEmpty{
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }, completion: { (Bool) -> Void in
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                        }, completion: { (Bool) -> Void in
                            
                    })
                    
            })
        }
        else{
            let formatter : NSNumberFormatter = NSNumberFormatter()
            let isValid = formatter.numberFromString(bargainPriceTextField.text!)
            if isValid != nil {
                if Int(bargainPriceTextField.text!)! > (currentCarpool?.fields.valueForKey("currentPrice") as! Int){
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: "You bargained with a price that is larger than the offered price, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
                    let alertOk : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                        self.sendResponse(CarpoolResponses.PASSENGER_BARGAIN.rawValue)
                        self.hideAllButtons()
                    })
                    let alertNo : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
                    alert.addAction(alertOk)
                    alert.addAction(alertNo)
                    self.presentViewController(alert, animated: true, completion: nil)
                    UIView.animateWithDuration(1.5, animations: { () -> Void in
                        self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                        }, completion: { (Bool) -> Void in
                            UIView.animateWithDuration(1.5, animations: { () -> Void in
                                self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                                }, completion: { (Bool) -> Void in
                                    
                            })
                            
                    })
                }
                else{
                    sendResponse(CarpoolResponses.PASSENGER_BARGAIN.rawValue)
                    hideAllButtons()
                }
            }
            else{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.5, animations: { () -> Void in
                            self.bargainPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                            }, completion: { (Bool) -> Void in
                                
                        })
    
                })
                self.bargainPriceTextField.text = ""
            }
        }
        

    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    ///scroll view that has all the elements of the screen inside it
    @IBOutlet weak var scrollView: UIScrollView!
    /**
    method that adds a route to a message.
    - Parameters:
        - message: the message that will carry the route.
        - route: the route that will be embeded in the message.
    - Returns: a QBChatMessage that is the message parameter with the route parameter embeded inside it.
    */
    func modifyMessageWithRoute(message : QBChatMessage,route : QBCOCustomObject)->QBChatMessage{
        message.customParameters["activeRouteID"] = route.ID
        message.customParameters["fromLocation"] = route.fields.valueForKey("sourceLocationName") as? String
        message.customParameters["toLocation"] = route.fields.valueForKey("destinationLocationName") as? String
        return message
    }
    ///method that is used to perform a carpool request to the opponent user. Used by both the passenger and the driver
    func performRequest(){
        hideAllButtons()
        QBRequest.userWithID(opponentUser.ID, successBlock: { (response : QBResponse!, newOpponentUser : QBUUser!) -> Void in
            self.opponentUser = newOpponentUser
            var numberOfSeats: Int?
            if let n = getCustomDataDictionaryFromUser(self.opponentUser)?.valueForKey("numberOfSeats") as? Int{
                numberOfSeats = n
                //print("numberOfSeats:" + numberOfSeats!.description)
                
                
            }
            else{
                numberOfSeats = -1
            }
            getNumberOfCarpoolersForUserWithID(self.opponentUser.ID, completion: { (numberOfCarpools) -> Void in
                if (getCustomDataDictionaryFromUser(self.opponentUser)?.valueForKey("isDriver") as! Bool) && numberOfSeats != -1 && numberOfCarpools >= numberOfSeats{
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.DRIVERFULLVEHICLE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                        let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)
                        })
                        alert.addAction(alertOK)
                        self.presentViewController(alert, animated: true, completion: nil)
                }
                else if !(getCustomDataDictionaryFromUser(self.opponentUser)?.valueForKey("isDriver") as! Bool) && numberOfCarpools > 0{
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERALREADYINCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                        self.navigationController?.popViewControllerAnimated(true)
                    })
                    alert.addAction(alertOK)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else{
                    QBRequest.userWithID(currentUser.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                        currentUser = user
                        self.currentUserDictionary = getCustomDataDictionaryFromUser(currentUser)!
                        if !(getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool){
                            if numberOfCurrentCarpoolers > 0{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message:CarpoolErrorMessages.USERPASSENGERALREADYHASCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                    self.navigationController?.popViewControllerAnimated(true)
                                })
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else if !(getCustomDataDictionaryFromUser(self.opponentUser)!.valueForKey("isDriver") as! Bool) {
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERPASSENGERCARPOOLWITHPASSENGER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                    self.navigationController?.popViewControllerAnimated(true)
                                })
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else{
                                self.performRequestHelper()
                            }
                        }
                        else if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool) {
                            
                            if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else if (((getCustomDataDictionaryFromUser(self.opponentUser)!.valueForKey("activeRouteID") as! String).isEmpty)){
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERHASNOROUTE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                    self.navigationController?.popViewControllerAnimated(true)
                                })
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else if numberOfCurrentCarpoolers > 0{
                                if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message:CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                        self.navigationController?.popViewControllerAnimated(true)
                                    })
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                                else if numberOfCurrentCarpoolers >= Int((getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String)){
                                    //print("")
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERNOTENOUGHSEATS.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                        self.navigationController?.popViewControllerAnimated(true)
                                    })
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                                else if (getCustomDataDictionaryFromUser(self.opponentUser)!.valueForKey("isDriver") as! Bool) {
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                        self.navigationController?.popViewControllerAnimated(true)
                                    })
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                                else{
                                    self.performRequestHelper()
                                    //self.performSegueWithIdentifier("segueToNegotiationsViewController", sender: self)
                                }
                            }
                            else if (getCustomDataDictionaryFromUser(self.opponentUser)!.valueForKey("isDriver") as! Bool) {
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message:CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                    self.navigationController?.popViewControllerAnimated(true)
                                })
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else{
                                self.performRequestHelper()
                                //self.performSegueWithIdentifier("segueToNegotiationsViewController", sender: self)
                            }
                            
                        }
                        
                        }, errorBlock: { (response : QBResponse!) -> Void in
                    })
                }
            })
            }) { (response : QBResponse!) -> Void in
        }
    }
    ///helper method for the performRequest() method
    func performRequestHelper(){
        self.hideAllButtons()
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setObject(self.opponentUser.ID, forKey: "opponentID")
        fields.setObject(self.currentDialog.ID , forKey: "dialogID")
        fields.setObject(true, forKey: "isActive")
        
        fields.setObject("", forKey: "lastMessageNotIsNeg")
        
        var message = self.transitionState(CarpoolStates.INITIAL.rawValue, response: "")
        
        if !(self.currentUserDictionary.valueForKey("isDriver") as! Bool){
            message.customParameters.setValue(CellColorStates.GREEN.rawValue, forKey: "color")
            fields.setObject(CarpoolStates.INTERESTED_NOTINTERESTED.rawValue, forKey: "currentState")
            let customObject : QBCOCustomObject = QBCOCustomObject()
            customObject.className = "Carpool"
            customObject.fields = fields
            QBRequest.objectWithClassName("Route", ID: currentUserDictionary.valueForKey("activeRouteID") as? String, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
                message = self.modifyMessageWithRoute(message,route: route)
                fields.setObject(message.text, forKey: "lastMessageIsNeg")
                QBRequest.createObject(customObject, successBlock: { (response : QBResponse!, customObject : QBCOCustomObject!) -> Void in
                    message.senderID = currentUser.ID
                    self.currentCarpool = customObject
                    self.currentCarpool?.className = "Carpool"
                    self.currentDialog.sendMessage(message)
                    self.waitingForOtherUserLabel.hidden = false
                    //QBChat.instance().sendMessage(message)
//                    self.currentDialog.sendMessage(message, sentBlock: { (error : NSError!) -> Void in
//                        if error != nil{
//                            //print("errorinsending")
//                            //print(error.description)
//                        }
//                        else{
//                            //print("sentSuccessfully")
//                        }
//                    })
                    self.messages.append(message)
                    self.tableView.reloadData()
                    //print("createdcarpool:")
                    //print(self.currentCarpool)
                    getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                        numberOfNegotiations = numberOfNegotiation
                    })
                    getNumberOfCarpoolersForUserWithID(currentUser.ID, completion: { (numberOfCarpools) -> Void in
                        numberOfCurrentCarpoolers = numberOfCarpools
                    })
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        //print("failedcreatedObject:")
                        //print(response.description)
                })
                
                }, errorBlock: { (response : QBResponse!) -> Void in
                    //print("failed create")
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: "To be able to carpool you have to have a Route!", preferredStyle: UIAlertControllerStyle.Alert)
                    let actionOk : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                    alert.addAction(actionOk)
                    self.presentViewController(alert, animated: true, completion: nil)
                    //print(response.description)
            })
            
        }
        else{
            message.customParameters.setValue(CellColorStates.YELLOW.rawValue, forKey: "color")
            fields.setObject(CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue, forKey: "currentState")
            let customObject : QBCOCustomObject = QBCOCustomObject()
            customObject.className = "Carpool"
            fields.setObject(Int(driverPriceTextField.text!)!, forKey: "currentPrice")
            customObject.fields = fields
            fields.setObject(message.text, forKey: "lastMessageIsNeg")
            QBRequest.createObject(customObject, successBlock: { (response : QBResponse!, customObject : QBCOCustomObject!) -> Void in
                message.senderID = currentUser.ID
                self.currentCarpool = customObject
                self.currentCarpool?.className = "Carpool"
                self.currentDialog.sendMessage(message, sentBlock: { (error : NSError!) -> Void in
                    if error != nil{
                        //print("errorinsending")
                        //print(error.description)
                    }
                    else{
                        //print("sentSuccessfully")
                    }
                })
                self.waitingForOtherUserLabel.hidden = false
                //QBChat.instance().sendMessage(message)
                self.messages.append(message)
                self.tableView.reloadData()
                self.hideAllButtons()
                //print("createdcarpool:")
                //print(self.currentCarpool)
                getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                    numberOfNegotiations = numberOfNegotiation
                })
                getNumberOfCarpoolersForUserWithID(currentUser.ID, completion: { (numberOfCarpools) -> Void in
                    numberOfCurrentCarpoolers = numberOfCarpools
                })
                }, errorBlock: { (response : QBResponse!) -> Void in
                    //print("failedcreatedObject:")
                    //print(response.description)
            })
            
        }
    }
    /**
    method that detects when the request carpool button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func requestCarpoolPressed(sender: UIButton) {
        if !(self.currentUserDictionary.valueForKey("isDriver") as! Bool){
            performRequest()
        }
        else{
            if driverPriceTextField.text!.isEmpty{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.driverPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.5, animations: { () -> Void in
                            self.driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                            }, completion: { (Bool) -> Void in
                                
                        })
                        
                })
                
            }
            else{
                let formatter : NSNumberFormatter = NSNumberFormatter()
                let isValid = formatter.numberFromString(driverPriceTextField.text!)
                if isValid != nil {
                    performRequest()
                }
                else{
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.driverPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                        }, completion: { (Bool) -> Void in
                            UIView.animateWithDuration(0.5, animations: { () -> Void in
                                self.driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                                }, completion: { (Bool) -> Void in
                                    
                            })
                            
                    })
                    driverPriceTextField.text = ""
                }
                
            }
        }
        
    }
    /**
    method that detects when the interested button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func interestedButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        if driverPriceTextField.text!.isEmpty{
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.driverPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }, completion: { (Bool) -> Void in
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                        }, completion: { (Bool) -> Void in
                            
                    })
                    
            })
            
        }
        else{
            let formatter : NSNumberFormatter = NSNumberFormatter()
            let isValid = formatter.numberFromString(driverPriceTextField.text!)
            if isValid != nil {
                if let _ = currentCarpool?.fields.valueForKey("currentPrice") as? Double{
                    if Double(driverPriceTextField.text!)! < (currentCarpool?.fields.valueForKey("currentPrice") as! Double){
                        let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Your offer has a price that is smaller than the bargained price, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
                        let alertOk : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                            self.sendResponse(CarpoolResponses.DRIVER_INTERESTED.rawValue)
                            self.hideAllButtons()
                        })
                        let alertNo : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
                        alert.addAction(alertOk)
                        alert.addAction(alertNo)
                        self.presentViewController(alert, animated: true, completion: nil)
                        UIView.animateWithDuration(1.5, animations: { () -> Void in
                            self.driverPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                            }, completion: { (Bool) -> Void in
                                UIView.animateWithDuration(1.5, animations: { () -> Void in
                                    self.driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                                    }, completion: { (Bool) -> Void in
                                        
                                })
                                
                        })
                    }
                    else{
                        sendResponse(CarpoolResponses.DRIVER_INTERESTED.rawValue)
                        hideAllButtons()
                    }
                }
                else{
                    sendResponse(CarpoolResponses.DRIVER_INTERESTED.rawValue)
                    hideAllButtons()
                }
            }
            else{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.driverPriceTextField.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.5, animations: { () -> Void in
                            self.driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
                            }, completion: { (Bool) -> Void in
                                
                        })
                        
                })
                driverPriceTextField.text = ""
            }
        }

    }
    /**
    method that detects when the reject button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func rejectButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        sendResponse(CarpoolResponses.PASSENGER_REJECT.rawValue)
        hideAllButtons()
    }
    /**
    method that detects when the accept button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func acceptButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        sendResponse(CarpoolResponses.PASSENGER_ACCEPT.rawValue)
        //hideAllButtons()
    }
    /**
    method that detects when the not interested button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func notInterestedButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        sendResponse(CarpoolResponses.DRIVER_NOTINTERESTED.rawValue)
        hideAllButtons()
    }
    ///instance of the free chat button that will take the user to the chat view controller
    @IBOutlet weak var freeChatButton: UIButton!
    ///instance of the finish carpool button that will finish the chat
    @IBOutlet weak var finishCarpoolButton: UIButton!
    /**
    method that detects when the finish carpool button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func finishCarpoolButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        let alert : UIAlertController = UIAlertController(title: "Are You Sure?", message: "Pressing yes will complete the carpool and transfer the funds between you and the other user", preferredStyle: UIAlertControllerStyle.Alert)
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action : UIAlertAction) -> Void in
            self.sendResponse(CarpoolResponses.FINISH.rawValue)
            self.hideAllButtons()
        }
        let noAction : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    /**
    method that detects when the free chat button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func freeChatButtonPressed(sender: UIButton) {
        self.view.endEditing(true)
        pushOpponentIsNeg = false
        pushOpponentUser = opponentUser
        
        self.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=ChatList", sender: self.revealViewController().rearViewController)
        //self.performSegueWithIdentifier("goToChatFromVisibleUsersSegue", sender: self)
    }
    ///instance of an image containing the user's profile image
    @IBOutlet weak var userImageView: UIImageView!
    ///holds the opponent user in this negotiation
    var opponentUser : QBUUser = QBUUser()
    ///array of the messages in the table view
    var messages : [QBChatMessage] = []
    ///holds the current dialog of the negotiation
    var currentDialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
    ///holds the current carpool of the negotiation
    var currentCarpool : QBCOCustomObject?
    ///holds the keyboard size
    var keyboardSize : CGSize = CGSize(width: 0, height: 0)
    ///holds the current user's custom data dictionary
    var currentUserDictionary : NSDictionary = NSDictionary()
    ///holds the current keyboard size
    var currentKeyboardSize : CGSize = CGSize(width: 0, height: 0)
    ///boolean that indicates that the bargain price text field was the first responder
    var bargainPriceTextFieldWasFirstResponder : Bool = false
    ///boolean that indicates that the driver price text field was the first responder
    var driverPriceTextFieldWasFirstResponder : Bool = false
    ///date formatter that is used to format the date in the messages
    var formatter : NSDateFormatter = NSDateFormatter()
    ///label that is used to indicate the negotiation's page is loading in new messages
    var placeholder : UILabel = UILabel()
    ///instance of the table view that holds the messages
    @IBOutlet weak var tableView: UITableView!
    ///method that is used to end editing in the view
    func endEditing(){
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        driverPriceTextField.borderStyle = UITextBorderStyle.RoundedRect
        bargainPriceTextField.borderStyle = UITextBorderStyle.RoundedRect

        if pushOpponentUser != nil{
            opponentUser = pushOpponentUser!.copy() as! QBUUser
            pushOpponentUser = nil
        }
        super.viewDidLoad()
        if self.revealViewController() != nil{
            revealViewController().rightRevealToggleAnimated(true)
        }
        QBChat.instance().addDelegate(self)
        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("endEditing"))
        self.scrollView.addGestureRecognizer(tapGestureRecognizer)
        bargainPriceTextField.delegate = self
        scrollView.delegate = self
        let defaultCell = UINib(nibName: "NegotiationsDefaultTableViewCell", bundle: nil)
        tableView.registerNib(defaultCell, forCellReuseIdentifier: "NegotiationsDefaultTableViewCellReuseID")
        let locationCell = UINib(nibName: "NegotiationsLocationTableViewCell", bundle: nil)
        tableView.registerNib(locationCell, forCellReuseIdentifier: "NegotiationsLocationTableViewCellReuseID")
        let priceCell = UINib(nibName: "NegotiationsPriceTableViewCell", bundle: nil)
        tableView.registerNib(priceCell, forCellReuseIdentifier: "NegotiationsPriceTableViewCellReuseID")
        setTheme()
        hideAllButtons()
        tableView.transform = CGAffineTransformMakeScale(1, -1)
        //tableView.backgroundColor = UIColor.blackColor()
        // register new tableviewcell
        //                var nib = UINib(nibName: "NegotiationsTableViewCell", bundle: nil)
        //                tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        
        formatter.dateFormat = "h:mm a, EEE, MMM d"
        formatter.timeZone = NSTimeZone.localTimeZone()
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        //tableView.separatorColor = ApplicationThemeColor().WhiteThemeColor
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"orientationChanged:", name: UIDeviceOrientationDidChangeNotification, object: UIDevice.currentDevice())
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillAppear:", name: UIKeyboardWillShowNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillDisappear:", name: UIKeyboardWillHideNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardDidDisappear:", name: UIKeyboardDidHideNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardDidAppear:", name: UIKeyboardDidShowNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardFrameWillChange:", name: UIKeyboardWillChangeFrameNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardFrameDidChange:", name: UIKeyboardDidChangeFrameNotification, object: nil)
        tableView.dataSource = self
        tableView.delegate = self
        
   
        //QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        let placeholder : UILabel = UILabel()
        placeholder.font = UIFont.italicSystemFontOfSize(14)
        placeholder.numberOfLines = 1; // Use as many lines as needed.
        placeholder.text = "loading"
        placeholder.textAlignment = NSTextAlignment.Center
        placeholder.textColor = UIColor.lightGrayColor()
        placeholder.hidden = false
        self.placeholder.frame = self.tableView.frame;
        self.view.addSubview(placeholder)
        //placeholder.transform = CGAffineTransformMakeScale(1, -1)
        self.placeholder = placeholder
        QBRequest.userWithID(DatabaseHelper().FetchUser().ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            currentUser = user

            self.currentUserDictionary = getCustomDataDictionaryFromUser(currentUser)!
            if (self.currentUserDictionary.valueForKey("isDriver") as! Bool){
                self.waitingForOtherUserLabel.text = "Waiting For Passenger To Respond"
            }
            else{
                self.waitingForOtherUserLabel.text = "Waiting For Driver To Respond"
            }
            self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 6;
            self.opponentImageView.layer.cornerRadius = self.opponentImageView.frame.size.width / 6;
            self.opponentImageView.layer.masksToBounds = true
            self.userImageView.layer.masksToBounds = true
            self.getMessages()
            
            }) { (response : QBResponse!) -> Void in
                self.navigationController?.popViewControllerAnimated(true)
        }
        //userImageView.image = UIImage(named : "DriverAnnotation.png")

    }
    override func viewWillLayoutSubviews() {
        self.placeholder.frame = self.tableView.frame;
    }
    ///method that sets the theme for the view controller
    func setTheme(){
        bargainPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
        driverPriceTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
        interestedButton.layer.cornerRadius = 4
        interestedButton.clipsToBounds = true
        notInterestedButton.layer.cornerRadius = 4
        notInterestedButton.clipsToBounds = true
        requestCarpoolButton.layer.cornerRadius = 4
        requestCarpoolButton.clipsToBounds = true
        rejectButton.layer.cornerRadius = 4
        rejectButton.clipsToBounds = true
        acceptButton.layer.cornerRadius = 4
        acceptButton.clipsToBounds = true
        bargainButton.layer.cornerRadius = 4
        bargainButton.clipsToBounds = true
        freeChatButton.layer.cornerRadius = 4
        freeChatButton.clipsToBounds = true
        finishCarpoolButton.layer.cornerRadius = 4
        finishCarpoolButton.clipsToBounds = true
        if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
        }
        freeChatButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
        self.view.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        self.scrollView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        //print("viewwillTransition")
        if bargainPriceTextField.isFirstResponder(){
            bargainPriceTextFieldWasFirstResponder = true
            self.view.endEditing(true)
        }
        else if driverPriceTextField.isFirstResponder(){
            driverPriceTextFieldWasFirstResponder = true
            self.view.endEditing(true)
        }
    }
    func handleSwipe(sender : UISwipeGestureRecognizer){
        //AGPushNoteView.close()
    }
    func orientationChanged(notification : NSNotification){
        if bargainPriceTextFieldWasFirstResponder{
            self.bargainPriceTextField.becomeFirstResponder()
            bargainPriceTextFieldWasFirstResponder = false
        }
        else if driverPriceTextFieldWasFirstResponder{
            self.driverPriceTextField.becomeFirstResponder()
            driverPriceTextFieldWasFirstResponder = false
        }
        //NSLog("screenSize : " + UIScreen.mainScreen().bounds.height.description + " " + UIScreen.mainScreen().bounds.width.description)
//        if UIScreen.mainScreen().bounds.height > 650{
//            self.scrollView.contentSize.height = UIScreen.mainScreen().bounds.height
//        }
//        else{
//            self.scrollView.contentSize.height = 650
//        }
        //scrollView.contentSize.height = 650
        //tableView.reloadData()
    }
    ///method that is used to get the last 10 negotiations messages in the dialog
    func getMessages(){
//        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
//        boxView.backgroundColor = UIColor.whiteColor()
//        boxView.alpha = 1.0
//        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Conversation"
        self.view.userInteractionEnabled = false
//        boxView.addSubview(activityView)
//        boxView.addSubview(textLabel)
//        self.view.addSubview(boxView)
        QBRequest.dialogsWithSuccessBlock({ (response : QBResponse!,dialogs : [AnyObject]!, set : Set<NSObject>!) -> Void in
            self.currentDialog.name = "error"
            for dialog in dialogs{
                if let d = dialog as? QBChatDialog{
                    for userID in d.occupantIDs{
                        if userID as! UInt == self.opponentUser.ID{
                            self.currentDialog = d
                            self.currentDialog.name = ""
                            let fields : NSMutableDictionary = NSMutableDictionary()
                            fields.setValue(self.currentDialog.ID, forKey: "dialogID")
                            fields.setValue(true, forKey: "isActive")
                            QBRequest.objectsWithClassName("Carpool", extendedRequest: fields, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                self.handleMessageLogic(carpools)
                                
                                let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                                
                                let now : NSDate = NSDate()
                                extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
                                extendedRequest["sort_desc"] = "date_sent"
                                extendedRequest["limit"] = 10;
                                QBRequest.messagesWithDialogID(self.currentDialog.ID, extendedRequest: extendedRequest as [NSObject : AnyObject], forPage: QBResponsePage(limit: 10), successBlock: { (response : QBResponse!, messages : [AnyObject]!,responsePage : QBResponsePage!) -> Void in
                                    for message in messages{
                                        if let mess = message as? QBChatMessage{
                                            //print("messageReceived:")
                                            //print(message)
                                            //print(message.customParameters)
                                            if let isNeg = mess.customParameters["isNeg"] as? String{
                                                //print("isNeg:")
                                                //print(mess.customParameters["isNeg"] as? Bool)
                                                if isNeg == "1" || isNeg == "true" {
                                                    self.messages.append(mess)
                                                }
                                            }
                                            
                                        }
                                    }
                                    self.messages = Array(self.messages.reverse())
                                    self.tableView.reloadData()
                                    self.receiveMessages = true
                                    self.placeholder.hidden = true
                                    //self.boxView.removeFromSuperview()
                                    self.view.userInteractionEnabled = true
                                    }, errorBlock: { (response : QBResponse!) -> Void in
                                        let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                                        //dialog.type = QBChatDialogTypePrivate
                                        let selectedUserID : NSMutableArray = NSMutableArray()
                                        selectedUserID.addObject(self.opponentUser.ID)
                                        dialog.occupantIDs = selectedUserID as [AnyObject]
                                        QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                                            self.currentDialog = newDialog
                                            self.placeholder.hidden = true
                                            self.view.userInteractionEnabled = true
                                            self.receiveMessages = true
                                            }, errorBlock: { (response : QBResponse!) -> Void in
                                                self.placeholder.hidden = true
                                                self.view.userInteractionEnabled = true
                                                self.receiveMessages = true
                                                
                                        })
                                })
                                
                                }) { (response : QBResponse!) -> Void in
                                    QBRequest.messagesWithDialogID(self.currentDialog.ID, successBlock: { (response : QBResponse!, messages : [AnyObject]!) -> Void in
                                        for message in messages{
                                            if let mess = message as? QBChatMessage{
                                                if mess.dialogID == self.currentDialog.ID{
                                                    self.messages.append(mess)
                                                }
                                            }
                                        }
                                        self.tableView.reloadData()
                                        self.receiveMessages = true
                                        self.placeholder.hidden = true
                                        //self.boxView.removeFromSuperview()
                                        self.view.userInteractionEnabled = true
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                                            //dialog.type = QBChatDialogType.Private
                                            let selectedUserID : NSMutableArray = NSMutableArray()
                                            selectedUserID.addObject(self.opponentUser.ID)
                                            dialog.occupantIDs = selectedUserID as [AnyObject]
                                            QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                                                self.currentDialog = newDialog
                                                self.placeholder.hidden = true
                                                //self.boxView.removeFromSuperview()
                                                self.view.userInteractionEnabled = true
                                                self.receiveMessages = true
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    self.placeholder.hidden = true
                                                    //self.boxView.removeFromSuperview()
                                                    self.view.userInteractionEnabled = true
                                            })
                                    })
                            }
                        }
                    }
                }
            }
            if self.currentDialog.name == "error"{
                let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                //dialog.type = QBChatDialogType.Private
                let selectedUserID : NSMutableArray = NSMutableArray()
                selectedUserID.addObject(self.opponentUser.ID)
                dialog.occupantIDs = selectedUserID as [AnyObject]
                QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                    self.currentDialog = newDialog
                    self.placeholder.hidden = true
                    //self.boxView.removeFromSuperview()
                    self.self.handleMessageLogic([])
                    self.view.userInteractionEnabled = true
                    self.receiveMessages = true
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.placeholder.hidden = true
                        //self.boxView.removeFromSuperview()
                        self.view.userInteractionEnabled = true
                })
                
                
            }
            
            }, errorBlock: { (response : QBResponse!) -> Void in
                //print(response)
                let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                //dialog.type = QBChatDialogTypePrivate
                let selectedUserID : NSMutableArray = NSMutableArray()
                selectedUserID.addObject(self.opponentUser.ID)
                dialog.occupantIDs = selectedUserID as [AnyObject]
                QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                    self.currentDialog = newDialog
                    self.placeholder.hidden = true
                    //self.boxView.removeFromSuperview()
                    self.self.handleMessageLogic([])
                    self.view.userInteractionEnabled = true
                    self.receiveMessages = true
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.placeholder.hidden = true
                        //self.boxView.removeFromSuperview()
                        self.view.userInteractionEnabled = true
                })
                
                
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        //NSLog("screenSize : " + UIScreen.mainScreen().bounds.height.description + " " + UIScreen.mainScreen().bounds.width.description)
//        if UIScreen.mainScreen().bounds.height > 650{
//            self.scrollView.contentSize.height = UIScreen.mainScreen().bounds.height
//        }
//        else{
//            self.scrollView.contentSize.height = 650
//        }
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "NegotiationsViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        self.navigationItem.title = opponentUser.login
        //let value = UIInterfaceOrientation.Portrait.rawValue
        // UIDevice.currentDevice().setValue(value, forKey: "orientation")
        currentViewController = self
        QBRequest.downloadFileWithID(UInt(currentUser.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
            self.userImageView.image = UIImage(data: data)
            }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                
            }, errorBlock: { (response : QBResponse!) -> Void in
                self.userImageView.image = UIImage(named: "placeholder.jpg")
        })
        QBRequest.downloadFileWithID(UInt(opponentUser.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
            self.opponentImageView.image = UIImage(data: data)
            }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                
            }, errorBlock: { (response : QBResponse!) -> Void in
                self.userImageView.image = UIImage(named: "placeholder.jpg")
        })
        self.placeholder.frame = self.tableView.frame;
    }
    func chatDidReceiveMessage(message: QBChatMessage!) {
        var append = true
        for mess in messages{
            if let messID = mess.ID{
                if let messageID = message.ID{
                    if messID == messageID{
                        append = false
                    }
                }
            }
        }
        if receiveMessages && append && currentViewController is NegotiationsViewController{
            if (message.senderID == opponentUser.ID ){
                //print("message received from :" + message.senderID.description)
                self.waitingForOtherUserLabel.hidden = true
                handleMessageLogicOnMessageReceived(message)
                if let isNeg = message.customParameters["isNeg"] as? String{
                    if isNeg == "1" || isNeg == "true"{
                        
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue{
                            getCarpoolers()
                            getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                                numberOfNegotiations = numberOfNegotiation
                            })
                        }
                        //print("messageAppended")
                        messages.append(message)
                        self.tableView.reloadData()
                    }
                }
                else if let isNeg = message.customParameters["isNeg"] as? Int{
                    if isNeg == 1 {
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue || message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                            getCarpoolers()
                            getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                                numberOfNegotiations = numberOfNegotiation
                            })

                        }
                        //print("messageAppened")
                        messages.append(message)
                        self.tableView.reloadData()
                    }
                }
            }
        }

    }

    
    func chatDidNotSendMessage(message: QBChatMessage!, error: NSError!) {
        //print("error")
        NSLog(error.debugDescription)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
    }
    /**
    method that is used to return a QBChatMessage that changes the state of the negotiations according to the response (response parameter) and the current state (state parameter) of the current negotiations
    - Parameters:
        - state: holds the current state of the negotiations
        - response: holds the response of the user
    - Returns: a QBChatMessage that will change the state of the current carpool when it is sent
    */
    func transitionState(state : String, response : String) -> QBChatMessage{
        let message : QBChatMessage = QBChatMessage()
        message.text = "blablabla"
        message.recipientID = opponentUser.ID
        message.dialogID = self.currentDialog.ID
        let params : NSMutableDictionary = NSMutableDictionary()
        params["save_to_history"] = true
        params["senderLogin"] = DatabaseHelper().FetchUser().login
        params["isNeg"] = true
        params["itemType"] = "3"
        
        enum CarpoolMessages : String {
            case CARPOOLFINISHED = "Carpool Finished!",CARPOOLACCEPTED = "Carpool Accepted!", PASSENGERBARGAIN = "I think this price would be more suitable",PASSENGERREJECT = "Carpool Denied!",DRIVERINTERESTED = "I'm Interested!\nHere's my price", DRIVERNOTINTERESTED = "Not Interested!", DRIVERREQUEST = "Hello, I can give you a ride for",PASSENGERREQUEST = "Hey, can you pick me up?"
        }
        
        
        if state == CarpoolStates.FREE.rawValue{
            if response == CarpoolResponses.FINISH.rawValue{
                message.text = "Carpool Finished!"
                message.text = CarpoolMessages.CARPOOLFINISHED.rawValue
                params["currentState"] = CarpoolStates.CARPOOL_FINISHED.rawValue
            }
            else
            {
                params["currentState"] = CarpoolStates.FREE.rawValue
            }
        }
        else if state == CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue{
            if response == CarpoolResponses.PASSENGER_ACCEPT.rawValue{
                message.text = "Carpool Accepted!"
                message.text = CarpoolMessages.CARPOOLACCEPTED.rawValue
                params["currentState"] = CarpoolStates.FREE.rawValue
            }
            else if response == CarpoolResponses.PASSENGER_BARGAIN.rawValue{
                params["currentState"] = CarpoolStates.INTERESTED_NOTINTERESTED.rawValue
                params["currentPrice"] = bargainPriceTextField.text
                params["itemType"] = "1"
                message.text = "I think this price would be more suitable"
                message.text = CarpoolMessages.PASSENGERBARGAIN.rawValue
            }
            else if response == CarpoolResponses.PASSENGER_REJECT.rawValue{
                params["currentState"] = CarpoolStates.CANCELLED.rawValue
                message.text = "Carpool Denied!"
                message.text = CarpoolMessages.PASSENGERREJECT.rawValue
            }
        }
        else if state == CarpoolStates.INTERESTED_NOTINTERESTED.rawValue{
            if response == CarpoolResponses.DRIVER_INTERESTED.rawValue{
                params["currentState"] = CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue
                message.text = "I'm Interested!\nHere's my price"
                message.text = CarpoolMessages.DRIVERINTERESTED.rawValue
                params["currentPrice"] = driverPriceTextField.text
                params["itemType"] = "1"
            }
            else if response == CarpoolResponses.DRIVER_NOTINTERESTED.rawValue{
                params["currentState"] = CarpoolStates.CANCELLED.rawValue
                message.text = "Not Interested!"
                message.text = CarpoolMessages.DRIVERNOTINTERESTED.rawValue
            }
        }
        else if state == CarpoolStates.INITIAL.rawValue{
            if currentUserDictionary.valueForKey("isDriver") as! Bool{
                params["currentState"] = CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue
                message.text = "Hey want me to pick you up?"
                message.text = CarpoolMessages.DRIVERREQUEST.rawValue
                params["currentPrice"] = driverPriceTextField.text
                params["itemType"] = "1"
            }
            else if !(currentUserDictionary.valueForKey("isDriver") as! Bool){
                params["currentState"] = CarpoolStates.INTERESTED_NOTINTERESTED.rawValue
                message.text = "Hey can you pick me up?"
                message.text = CarpoolMessages.PASSENGERREQUEST.rawValue
                params["itemType"] = "2"
            }
            
        }
        message.customParameters = params
        
        return message
    }
    ///method that sends the user's response
    func sendResponse(response : String){
        if currentCarpool != nil{
            let message = transitionState(currentCarpool?.fields.valueForKey("currentState") as! String, response: response)
            

            if (currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue) && (response == CarpoolResponses.PASSENGER_BARGAIN.rawValue){
                currentCarpool?.fields.setValue(Int(bargainPriceTextField.text!), forKey: "currentPrice")
            }
            else if (currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.INTERESTED_NOTINTERESTED.rawValue) && (response == CarpoolResponses.DRIVER_INTERESTED.rawValue){
                currentCarpool?.fields.setValue(Int(driverPriceTextField.text!), forKey: "currentPrice")
            }
            
            
            currentCarpool?.fields.setValue(message.customParameters.valueForKey("currentState") as! String, forKey: "currentState")
            if currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue{
                hideAllButtons()
                waitingForOtherUserLabel.hidden = true
                freeChatButton.hidden = false
                if !(currentUserDictionary.valueForKey("isDriver") as! Bool){
                    finishCarpoolButton.hidden = false
                }
            }
            else if (currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CANCELLED.rawValue) || currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                currentCarpool?.fields.setValue(false, forKey: "isActive")
            }

            currentCarpool!.className = "Carpool"
            currentCarpool!.fields.setValue(message.text, forKey: "lastMessageIsNeg")
            QBRequest.updateObject(currentCarpool, successBlock: { (response : QBResponse!, updatedCarpool : QBCOCustomObject!) -> Void in
                self.currentCarpool = updatedCarpool
                self.currentCarpool?.className = "Carpool"
                //print("updatedCarpool:")
                //print(self.currentCarpool)
                
                if let isDriver = self.currentUserDictionary.valueForKey("isDriver") as? Bool{
                    if isDriver{
                        message.customParameters.setValue(CellColorStates.YELLOW.rawValue, forKey: "color")
                    }
                    else{
                        
                        if (updatedCarpool.fields.valueForKey("currentState") as! String) == CarpoolStates.FREE.rawValue{
                            cancelAllOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (success) -> Void in
                                
                            })
                        }
                        message.customParameters.setValue(CellColorStates.GREEN.rawValue, forKey: "color")
                    }
                }
                else if let isDriverString = self.currentUserDictionary.valueForKey("isDriver") as? String{
                    if isDriverString == "1" || isDriverString == "true"{
                        message.customParameters.setValue(CellColorStates.YELLOW.rawValue, forKey: "color")
                    }
                    else{
                        message.customParameters.setValue(CellColorStates.GREEN.rawValue, forKey: "color")
                    }
                }
                self.waitingForOtherUserLabel.hidden = false
                self.currentDialog.sendMessage(message)
                //QBChat.instance().sendMessage(message)
                let newMessage : QBChatMessage = QBChatMessage()
                newMessage.text = message.text
                newMessage.recipientID = self.opponentUser.ID
                newMessage.senderID = DatabaseHelper().FetchUser().ID
                newMessage.dialogID = self.currentDialog.ID
                newMessage.dateSent = NSDate()
                newMessage.customParameters = message.customParameters
                self.messages.append(newMessage)
                self.tableView.reloadData()
                if self.currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue{
                    self.waitingForOtherUserLabel.hidden = true
                    self.freeChatButton.hidden = false
                    if !(self.currentUserDictionary.valueForKey("isDriver") as! Bool){
                        self.finishCarpoolButton.hidden = false
                    }
                    getCarpoolers()
                    getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                        numberOfNegotiations = numberOfNegotiation
                    })
                    
                    
                    if !(getCustomDataDictionaryFromUser(currentUser)?.valueForKey("isDriver") as! Bool){
                        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                        extendedRequest.setObject(self.opponentUser.ID, forKey: "driverID")
                        extendedRequest.setObject(true, forKey: "isActive")
                        QBRequest.objectsWithClassName("CarpoolRecord", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, records : [AnyObject]!, page : QBResponsePage!) -> Void in
                            if records != nil{
                                if records.count != 0{
                                    if let record = records[0] as? QBCOCustomObject{
                                        var passengersIDsTemp = record.fields.valueForKey("passengersIDsTemp") as! [UInt]
                                        var passengersIDs = record.fields.valueForKey("passengersIDs") as! [UInt]
                                        var carpoolIDs = record.fields.valueForKey("carpoolIDs") as! [String]
                                        var carpoolSet = NSSet(array: carpoolIDs)
                                        var tempSet = NSSet(array: passengersIDsTemp)
                                        var set = NSSet(array: passengersIDs)
                                        carpoolSet = carpoolSet.setByAddingObject(self.currentCarpool!.ID)
                                        set = set.setByAddingObject(currentUser.ID)
                                        tempSet = tempSet.setByAddingObject(currentUser.ID)
                                        carpoolIDs = NSArray(array: carpoolSet.allObjects) as! [String]
                                        passengersIDsTemp = NSArray(array: tempSet.allObjects) as! [UInt]
                                        passengersIDs = NSArray(array: set.allObjects) as! [UInt]
                                        record.className = "CarpoolRecord"
                                        record.fields.setObject(passengersIDsTemp, forKey: "passengersIDsTemp")
                                        record.fields.setObject(passengersIDs, forKey: "passengersIDs")
                                        record.fields.setObject(carpoolIDs, forKey: "carpoolIDs")
                                        QBRequest.updateObject(record, successBlock: { (response : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in

                                            }, errorBlock: { (response : QBResponse!) -> Void in
                                                
                                        })
                                    }
                                }
                                else{
                                    let record : QBCOCustomObject = QBCOCustomObject()
                                    record.className = "CarpoolRecord"
                                    record.fields.setObject([currentUser.ID], forKey: "passengersIDsTemp")
                                    record.fields.setObject([currentUser.ID], forKey: "passengersIDs")
                                    record.fields.setObject([self.opponentUser.ID], forKey: "driverID")
                                    record.fields.setObject([self.currentCarpool!.ID], forKey: "carpoolIDs")
                                    record.fields.setObject(true, forKey: "isActive")
                                    QBRequest.createObject(record, successBlock: { (response : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in
                                        
                                        //print("recordCreated")
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            //print("recordCreationFailed")
                                    })
                                }
                            }
                            else{
                                let record : QBCOCustomObject = QBCOCustomObject()
                                record.className = "CarpoolRecord"
                                record.fields.setObject([currentUser.ID], forKey: "passengersIDsTemp")
                                record.fields.setObject([currentUser.ID], forKey: "passengersIDs")
                                record.fields.setObject([self.opponentUser.ID], forKey: "driverID")
                                record.fields.setObject([self.currentCarpool!.ID], forKey: "carpoolIDs")
                                record.fields.setObject(true, forKey: "isActive")
                                QBRequest.createObject(record, successBlock: { (response : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in
                                    
                                    //print("recordCreated")
                                    }, errorBlock: { (response : QBResponse!) -> Void in
                                        //print("recordCreationFailed")
                                })
                            }
                            }, errorBlock: { (response : QBResponse!) -> Void in
                                let record : QBCOCustomObject = QBCOCustomObject()
                                record.className = "CarpoolRecord"
                                record.fields.setObject([currentUser.ID], forKey: "passengersIDsTemp")
                                record.fields.setObject([currentUser.ID], forKey: "passengersIDs")
                                record.fields.setObject([self.opponentUser.ID], forKey: "driverID")
                                record.fields.setObject([self.currentCarpool!.ID], forKey: "carpoolIDs")
                                record.fields.setObject(true, forKey: "isActive")
                                QBRequest.createObject(record, successBlock: { (response : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in
    
                                    //print("recordCreated")
                                    }, errorBlock: { (response : QBResponse!) -> Void in
                                        //print("recordCreationFailed")
                                })
                        })
                    }
                    
                }
                if (self.currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CANCELLED.rawValue) || self.currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                    if self.currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                        self.performSegueWithIdentifier("goToUserRatingSegueFromNegotiationsViewController", sender: self)
                    }
                    self.currentCarpool = nil
                    self.handleMessageLogic([])
                    getCarpoolers()
                    getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                        numberOfNegotiations = numberOfNegotiation
                    })
                    
                }
                }, errorBlock: { (response : QBResponse!) -> Void in
                    //print("errorresponse:")
                    //print(response.error.description)//self.dismissSelf()
            })
        }
        
        
    }
    /**
    method that sets the state of the negotiations page according to the current carpool of the chat's dialog
    - Parameter carpools: array of carpools that contains one carpool that is the current carpool of the chat dialog
    */
    func handleMessageLogic(carpools : [AnyObject]){
        hideAllButtons()
        if let carpoolObjects = carpools as? [QBCOCustomObject]{
            for carpool in carpoolObjects {
                if carpool.fields.valueForKey("isActive") as! Bool {
                    currentCarpool = carpool
                    if currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue {
//                        let exitAlert : UIAlertController = UIAlertController(title: "Alert!", message: "Carpool Accepted", preferredStyle: UIAlertControllerStyle.Alert)
//                        let exitOK : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
//                            //self.navigationController?.popViewControllerAnimated(true)
//                        })
//                        exitAlert.addAction(exitOK)
//                        self.presentViewController(exitAlert, animated: true, completion: nil)
                        freeChatButton.hidden = false
                        if !(currentUserDictionary.valueForKey("isDriver") as! Bool){
                            finishCarpoolButton.hidden = false
                        }
                        
                    }
                    else if currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.ACCEPT_BARGAIN_REJECT.rawValue{
                        if !(currentUserDictionary.valueForKey("isDriver") as! Bool){
                            hideAllButtons()
                            acceptButton.hidden = false
                            bargainButton.hidden = false
                            rejectButton.hidden = false
                            bargainPriceTextField.hidden = false
                            bargainPromptLabel.hidden = false
                        }
                        else{
                            self.waitingForOtherUserLabel.hidden = false
                        }
                    }
                    else if currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.INTERESTED_NOTINTERESTED.rawValue{
                        if currentUserDictionary.valueForKey("isDriver") as! Bool{
                            hideAllButtons()
                            interestedButton.hidden = false
                            driverPriceLabel.hidden = false
                            notInterestedButton.hidden = false
                            //driverPriceTextFieldHorizontalConstraint.constant = -44
                            driverPriceTextField.hidden = false
                        }
                        else{
                            self.waitingForOtherUserLabel.hidden = false
                        }
                    }
                }
                if currentCarpool != nil{
                    if(currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CANCELLED.rawValue || currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue){
                        handleMessageLogic([])
//                        if (currentCarpool?.fields.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue){
//                            let exitAlert : UIAlertController = UIAlertController(title: "Alert!", message: "Carpool was finished", preferredStyle: UIAlertControllerStyle.Alert)
//                            let exitOK : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
//                                //self.navigationController?.popViewControllerAnimated(true)
//                            })
//                            exitAlert.addAction(exitOK)
//                            self.presentViewController(exitAlert, animated: true, completion: nil)
//                        }
//                        else{
//                            let exitAlert : UIAlertController = UIAlertController(title: "Alert!", message: "Carpool was cancelled", preferredStyle: UIAlertControllerStyle.Alert)
//                            let exitOK : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
//                                //self.navigationController?.popViewControllerAnimated(true)
//                            })
//                            exitAlert.addAction(exitOK)
//                            self.presentViewController(exitAlert, animated: true, completion: nil)
//                        }

                        
                    }
                }
            }
        }
        if carpools.count == 0 {
            hideAllButtons()
            requestCarpoolButton.hidden = false
            self.waitingForOtherUserLabel.hidden = true
            if (self.currentUserDictionary.valueForKey("isDriver") as! Bool){
                driverPriceLabel.hidden = false
                //driverPriceTextFieldHorizontalConstraint.constant = 0
                driverPriceTextField.hidden = false
            }
        }
    }
    /**
    method that updates the user's carpool according to the message that is received
    - Parameter message: it is the message receiverd by the dialog
    */
    func handleMessageLogicOnMessageReceived(message : QBChatMessage){
        if currentCarpool != nil{
            if let price = message.customParameters.valueForKey("currentPrice") as? String{
                currentCarpool?.fields.setValue(Int(price), forKey: "currentPrice")
            }
            currentCarpool?.fields.setValue(message.customParameters.valueForKey("currentState") as! String, forKey: "currentState")
            var carpools : [AnyObject] = []
            carpools.append(currentCarpool!)
            handleMessageLogic(carpools)
        }
        else{
            let fields : NSMutableDictionary = NSMutableDictionary()
            fields.setValue(self.currentDialog.ID, forKey: "dialogID")
            QBRequest.objectsWithClassName("Carpool", extendedRequest: fields, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void
                in
                getCarpoolers()
                getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                    numberOfNegotiations = numberOfNegotiation
                })
                self.handleMessageLogic(carpools)
                }) { (response : QBResponse!) -> Void in
                    
            }
        }
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if currentViewController is NegotiationsViewController{
            return messages.count
        }
        else {
            return 0
        }
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let type : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("itemType") as? String{
            if type == "2"{
                return true
            }
        }
        return false
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //        if editingStyle == .Delete {
        //            // Delete the row from the data source
        //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        //        } else if editingStyle == .Insert {
        //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //        }
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        if let type : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("itemType") as? String{
            if type == "2"{
                var actions : [UITableViewRowAction] = []
                let detailsAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Details") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    var myRouteViewController : MyRouteViewController = MyRouteViewController()
                    myRouteViewController = storyboard.instantiateViewControllerWithIdentifier("MyRouteViewController") as! MyRouteViewController
                    myRouteViewController.currentRouteID = self.messages[self.messages.count-1-indexPath.row].customParameters["activeRouteID"] as? String
                    myRouteViewController.presented = true
                    let navController : UINavigationController = UINavigationController()
                    navController.pushViewController(myRouteViewController, animated: false)
                    self.presentViewController(navController, animated: true, completion: { () -> Void in
                        
                    })
                }
                detailsAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                actions.append(detailsAction)
                return actions
            }
        }
        return []
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let itemType : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("itemType") as? String{
            if itemType == "1"{
                let cell = tableView.dequeueReusableCellWithIdentifier("NegotiationsPriceTableViewCellReuseID", forIndexPath: indexPath) as! NegotiationsPriceTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.transform = CGAffineTransformMakeScale(1, -1)
                let time : NSString = formatter.stringFromDate(messages[messages.count-1-indexPath.row].dateSent)
                cell.textView!.text = messages[messages.count-1-indexPath.row].text
                cell.dateLabel.text = time as String
                if let price = messages[messages.count-1-indexPath.row].customParameters.valueForKey("currentPrice") as? String{
                    cell.priceLabel.text = "Price: " + price + " Pesos"
                }
                else{
                    cell.priceLabel.text = ""
                }
                cell.dateLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                cell.textView.backgroundColor = UIColor.clearColor()
                cell.textView.font = UIFont.boldSystemFontOfSize(16.0)
                cell.textView.textAlignment = NSTextAlignment.Center
                cell.textView.textColor = ApplicationThemeColor().GreyTextColor
                cell.rightConstraint.constant = 0
                cell.leftConstraint.constant = 0
                if messages[messages.count-1-indexPath.row].senderID == DatabaseHelper().FetchUser().ID{
                    cell.leftConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Right
                }
                else{
                    cell.rightConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Left
                }
                if let color : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("color") as? String{
                    if color == CellColorStates.GREEN.rawValue{
                        cell.dateLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                        cell.textView.textColor = ApplicationThemeColor().WhiteThemeColor
                        cell.priceLabel.textColor =  ApplicationThemeColor().WhiteThemeColor
                        cell.cellMovingView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    }
                    else if color == CellColorStates.YELLOW.rawValue{
                        cell.dateLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
                        cell.textView.textColor = ApplicationThemeColor().DarkGreyTextColor
                        cell.priceLabel.textColor =  ApplicationThemeColor().DarkGreyTextColor
                        cell.cellMovingView.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }
                }
                
                return cell
                
            }
            else if itemType == "2"{
                let cell = tableView.dequeueReusableCellWithIdentifier("NegotiationsLocationTableViewCellReuseID", forIndexPath: indexPath) as! NegotiationsLocationTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.transform = CGAffineTransformMakeScale(1, -1)
                let time : NSString = formatter.stringFromDate(messages[messages.count-1-indexPath.row].dateSent)
                cell.textView!.text = messages[messages.count-1-indexPath.row].text
                cell.dateLabel.text = time as String
                cell.sourceLocationImageView.image = UIImage(named: "Location.png")
                cell.destinationLocationImageView.image = UIImage(named: "Destination.png")
                if let _ = messages[messages.count-1-indexPath.row].customParameters.valueForKey("toLocation") as? String{
                    cell.sourceLocationNameLabel.text = messages[messages.count-1-indexPath.row].customParameters.valueForKey("toLocation") as? String
                    cell.sourceLocationNameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                }
                if let _ = messages[messages.count-1-indexPath.row].customParameters.valueForKey("fromLocation") as? String{
                    cell.destinationLocationNameLabel.text = messages[messages.count-1-indexPath.row].customParameters.valueForKey("fromLocation") as? String
                    cell.destinationLocationNameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                }
                cell.dateLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                cell.textView.font = UIFont.boldSystemFontOfSize(16.0)
                cell.textView.textAlignment = NSTextAlignment.Center
                cell.sourceLocationImageView.image = cell.sourceLocationImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.destinationLocationImageView.image = cell.destinationLocationImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                cell.sourceLocationImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
                cell.destinationLocationImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
                cell.cellMovingView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                cell.textView.textColor = ApplicationThemeColor().WhiteThemeColor
                cell.textView.backgroundColor = UIColor.clearColor()
                cell.rightConstraint.constant = 0
                cell.leftConstraint.constant = 0
                if messages[messages.count-1-indexPath.row].senderID == DatabaseHelper().FetchUser().ID{
                    cell.leftConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Right
                }
                else{
                    cell.rightConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Left
                    
                }
                return cell
            }
            else if itemType == "3"{
                let cell = tableView.dequeueReusableCellWithIdentifier("NegotiationsDefaultTableViewCellReuseID", forIndexPath: indexPath) as! NegotiationsTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.transform = CGAffineTransformMakeScale(1, -1)
                let time : NSString = formatter.stringFromDate(messages[messages.count-1-indexPath.row].dateSent)
                cell.textView!.text = messages[messages.count-1-indexPath.row].text
                cell.dateLabel.text = time as String
                cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                cell.textView.backgroundColor = UIColor.clearColor()
                cell.textView.font = UIFont.boldSystemFontOfSize(16.0)
                cell.textView.textAlignment = NSTextAlignment.Center
                cell.rightConstraint.constant = 0
                cell.leftConstraint.constant = 0
                if messages[messages.count-1-indexPath.row].senderID == DatabaseHelper().FetchUser().ID{
                    cell.leftConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Right
                }
                else{
                    cell.rightConstraint.constant = UIScreen.mainScreen().bounds.width/4
                    cell.dateLabel.textAlignment = NSTextAlignment.Left
                    
                }
                if let color : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("color") as? String{
                    if color == CellColorStates.GREEN.rawValue{
                        cell.dateLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                        cell.textView.textColor = ApplicationThemeColor().WhiteThemeColor
                        cell.cellMovingView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                        cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                        cell.contentView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                    }
                    else if color == CellColorStates.YELLOW.rawValue{
                        cell.dateLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
                        cell.textView.textColor = ApplicationThemeColor().DarkGreyTextColor
                        cell.cellMovingView.backgroundColor = ApplicationThemeColor().YellowThemeColor
                        cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                        cell.contentView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                    }
                }
                return cell
            }
            
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("NegotiationsDefaultTableViewCellReuseID", forIndexPath: indexPath) as! NegotiationsTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.transform = CGAffineTransformMakeScale(1, -1)
        let time : NSString = formatter.stringFromDate(messages[messages.count-1-indexPath.row].dateSent)
        cell.textView!.text = messages[messages.count-1-indexPath.row].text
        cell.dateLabel.text = time as String
        cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        cell.textView.backgroundColor = UIColor.clearColor()
        cell.textView.font = UIFont.boldSystemFontOfSize(16.0)
        cell.textView.textAlignment = NSTextAlignment.Center
        cell.rightConstraint.constant = 0
        cell.leftConstraint.constant = 0
        if messages[messages.count-1-indexPath.row].senderID == DatabaseHelper().FetchUser().ID{
            cell.leftConstraint.constant = UIScreen.mainScreen().bounds.width/4
            cell.dateLabel.textAlignment = NSTextAlignment.Right
        }
        else{
            cell.rightConstraint.constant = UIScreen.mainScreen().bounds.width/4
            cell.dateLabel.textAlignment = NSTextAlignment.Left
            
        }
        if let color : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("color") as? String{
            if color == CellColorStates.GREEN.rawValue{
                cell.dateLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                cell.textView.textColor = ApplicationThemeColor().WhiteThemeColor
                cell.cellMovingView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                cell.contentView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            }
            else if color == CellColorStates.YELLOW.rawValue{
                cell.dateLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
                cell.textView.textColor = ApplicationThemeColor().DarkGreyTextColor
                cell.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                cell.contentView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                cell.cellMovingView.backgroundColor = ApplicationThemeColor().YellowThemeColor
            }
        }
        return cell
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.view.endEditing(true)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardDidAppear(notification: NSNotification){
        
    }
    func keyboardFrameWillChange(notification : NSNotification){
        //print("keyboardFrameWillChange")
        let userInfo:NSDictionary = notification.userInfo!
        let oldKeyboardHeight : CGSize = userInfo.objectForKey(UIKeyboardFrameBeginUserInfoKey)!.CGRectValue.size
        let newKeyboardHeight : CGSize = userInfo.objectForKey(UIKeyboardFrameEndUserInfoKey)!.CGRectValue.size
        let difference = newKeyboardHeight.height - oldKeyboardHeight.height
        if(difference != 0){
            let oldViewFrame : CGRect = self.view.frame
            self.view.frame = CGRectMake (oldViewFrame.origin.x, oldViewFrame.origin.y, oldViewFrame.width, oldViewFrame.height - difference)
            
        }
        currentKeyboardSize = newKeyboardHeight
        
    }
    ///scrolls the scroll view to the bottom
    func scrollToBottom(){
        let bottomOffset : CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        self.scrollView.setContentOffset(bottomOffset, animated:true);
    }
    func keyboardFrameDidChange(notification : NSNotification){
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("scrollToBottom"), userInfo: nil, repeats: false)
    }
    func keyboardWillAppear(notification: NSNotification){
        
        
        if keyboardSize.height == 0{
            let userInfo:NSDictionary = notification.userInfo!
            keyboardSize = userInfo.objectForKey(UIKeyboardFrameEndUserInfoKey)!.CGRectValue.size
            let oldViewFrame : CGRect = self.view.frame
            self.view.frame = CGRectMake (oldViewFrame.origin.x, oldViewFrame.origin.y, oldViewFrame.width, oldViewFrame.height - keyboardSize.height)
            //print("keyboardsize:" + keyboardSize.height.description)
        }
        
    }
    
    func keyboardWillDisappear(notification: NSNotification){
        let userInfo:NSDictionary = notification.userInfo!
        keyboardSize = userInfo.objectForKey(UIKeyboardFrameEndUserInfoKey)!.CGRectValue.size
        let oldViewFrame : CGRect = self.view.frame
        self.view.frame = CGRectMake (oldViewFrame.origin.x, oldViewFrame.origin.y, oldViewFrame.width, oldViewFrame.height + keyboardSize.height)
        keyboardSize.height = 0
    }
    func keyboardDidDisappear(notification: NSNotification){
        //print("keyboarddiddisappear")
    }
//    
//    func calculateHeightForString(inString:String) -> CGFloat
//    {
//        let messageString = inString
//        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15.0)])
//        let rect:CGRect = attrString!.boundingRectWithSize(CGSizeMake(300.0,CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context:nil )//hear u will get nearer height not the exact value
//        let requredSize:CGRect = rect
//        return requredSize.height  //to include button's in your tableview
//        
//    }
//    
//    func findSizeForText(text : NSString, font : UIFont) -> CGSize
//    {
//        if text.length != 0{
//            let textSize : CGSize = CGSizeMake(260.0, 10000.0)      //Width and height of text area
//            let frame : CGRect = text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.UsesFontLeading, attributes: [NSFontAttributeName : font], context: nil)
//            var size : CGSize = CGSizeMake(frame.size.width, frame.size.height+1);
//            size.width = size.width + 15
//            if size.width>textSize.width{
//                size.width = 260.0
//            }
//            if size.height>textSize.height{
//                size.height = 10000.0
//            }
//            return size
//            
//        }
//        return CGSizeMake(0, 0)
//    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let itemType : String? = messages[messages.count-1-indexPath.row].customParameters.valueForKey("itemType") as? String{
            if itemType == "2"{
                return 144
            }
            else if itemType == "1"{
                return 130
            }
            else if itemType == "3"{
                return 92
            }
        }
        return 92
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == messages.count-1 {
            print(indexPath.row.description + "=" + (messages.count-1).description)
            if getNewPage{
                getNewPage = false
                if currentDialog.ID != nil{
                    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                    
                    let now : NSDate = NSDate()
                    extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
                    extendedRequest["sort_desc"] = "date_sent"
                    extendedRequest["limit"] = 10;
                    QBRequest.messagesWithDialogID(currentDialog.ID, extendedRequest: extendedRequest as [NSObject : AnyObject], forPage: QBResponsePage(limit: 10, skip: pageNumber * 10), successBlock: { (response : QBResponse!, ms : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                        if ms.count != 0{
                            self.getNewPage = true
                            self.pageNumber++
                        }
                        print("got more messages:" + self.pageNumber.description)
                        var newMessages : [QBChatMessage] = []
                        //let currentMessageCount = self.messages.count
                        for message in (ms as! [QBChatMessage]){
                            if let isNeg = message.customParameters["isNeg"] as? String{
                                if isNeg == "1" || isNeg == "true" {
                                    if let _ = self.messages.indexOf(message){
                                        
                                    }
                                    else{
                                        if message.senderID != currentUser.ID{
                                            QBChat.instance().readMessage(message)
                                        }
                                        newMessages.append(message)
                                    }
                                    
                                }
                            }
                        }

                        //newMessages = Array(newMessages.reverse())
                        
                        //tableView.beginUpdates()
                        self.messages = self.messages.reverse()
                        self.messages.appendContentsOf(newMessages)
                        self.messages = self.messages.reverse()
                        self.tableView.reloadData()
                        //tableView.endUpdates()

                        }, errorBlock: { (response : QBResponse!) -> Void in
                            self.getNewPage = true
                    })
                }
            }
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        //print("segue")
        if segue.identifier == "goToChatFromVisibleUsersSegue"{
            //print("chatnavigation")
            let chatViewController : ChatViewController = segue.destinationViewController as! ChatViewController
            chatViewController.opponentUser = opponentUser
        }
        else if segue.identifier == "goToUserRatingSegueFromNegotiationsViewController"{
            if segue.destinationViewController is UINavigationController{
                if (segue.destinationViewController as! UINavigationController).topViewController is UserRatingViewController{
                    (((segue.destinationViewController as! UINavigationController).topViewController) as! UserRatingViewController).opponentUser = self.opponentUser
                    (((segue.destinationViewController as! UINavigationController).topViewController) as! UserRatingViewController).delegate = self
                }
            }
        }
    }
    
    
}
