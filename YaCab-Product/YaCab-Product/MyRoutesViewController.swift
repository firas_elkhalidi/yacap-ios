//
//  MyRoutesViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/31/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class MyRoutesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,CurrentThemeDelegate {
    ///boolean that indicates whether the view should attempt to get a new page of data to be displayed in the tableview
    var getNewPage = true
    ///indicates the current page of data that is in the table view
    var pageNumber = 1
    ///instance of the table view
    @IBOutlet weak var tableView: UITableView!
    ///array of routes that are for the user
    var myRoutes : [QBCOCustomObject] = []
    ///box view that contains a loading indicator
    var boxView : UIView = UIView()
    ///refresh control for the table view containing the routes of the user
    var refreshControl = UIRefreshControl()
    ///routeID of the selected route id in the table view
    var selectedRouteID : String?
    ///create route button that takes the user to the MyRoutesMapViewController
    @IBOutlet weak var createRouteButton: UIButton!
    ///method that sets the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.PassengerTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().GreenThemeColor
            refreshControl.tintColor = ApplicationThemeColor().WhiteThemeColor
            //menuButton.tintColor = ApplicationThemeColor().GreenThemeColor
            createRouteButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            createRouteButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
            //tableView.backgroundColor = ApplicationThemeColor().GreenThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().YellowThemeColor
            refreshControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
            //menuButton.tintColor = ApplicationThemeColor().YellowThemeColor
            createRouteButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            createRouteButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            //tableView.backgroundColor = ApplicationThemeColor().YellowThemeColor
        }
        self.tableView.reloadData()
    }
    ///method that is run when the user refreshes the data in the tableview using the refresh control
    func refresh(){
        getNewPage = true
        pageNumber = 1
        getData()
    }
    ///delegate method that is part of the CurrentThemeDelegate that detects when the theme is changed
    func themeDidChange() {
        setTheme()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        // register new tableviewcell
        let nib = UINib(nibName: "UserLocationTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        setTheme()

        refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)

    }
    ///method that moves the user to the MyRoutesMapSegueViewController
    @IBAction func goToMyRoutesMapSegue(){
        self.performSegueWithIdentifier("goToMyRoutesMapSegue", sender: self)
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRoutesViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        getData()
    }
    ///method that gets the data for the table view
    func getData(){
        getNewPage = true
        pageNumber = 1
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Routes"
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        self.view.userInteractionEnabled = false
        myRoutes = []
        tableView.reloadData()
        
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setObject(DatabaseHelper().FetchUser().ID, forKey: "user_id")
        fields.setObject(false, forKey: "isDeleted")
        fields.setObject(10, forKey: "limit")
        QBRequest.objectsWithClassName("Route", extendedRequest: fields, successBlock: { (response : QBResponse!,routes : [AnyObject]!, responsePage: QBResponsePage!) -> Void in
            if routes != nil{
                self.myRoutes = routes as! [QBCOCustomObject]
            }

            self.tableView.reloadData()
            self.view.userInteractionEnabled = true
            self.boxView.removeFromSuperview()
            self.refreshControl.endRefreshing()
            }, errorBlock: { (response : QBResponse!) -> Void in
                self.tableView.reloadData()
                self.view.userInteractionEnabled = true
                self.boxView.removeFromSuperview()
                self.refreshControl.endRefreshing()
        })
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRouteID = myRoutes[indexPath.row].ID
        self.performSegueWithIdentifier("exitSegueFromMyRoutesViewController", sender: self)
    }
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return myRoutes.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UserLocationTableViewCell
        cell.sourceLocationNameLabel.text = myRoutes[indexPath.row].fields.valueForKey("sourceLocationName") as? String
        cell.destinationLocationNameLabel.text = myRoutes[indexPath.row].fields.valueForKey("destinationLocationName") as? String
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 49
    }
    
    
    // Override to support conditional editing of the table view.
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //    if editingStyle == .Delete {
        //    // Delete the row from the data source
        //    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        //    } else if editingStyle == .Insert {
        //    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //    }
    }
    
    /** 
    method that deletes a route from the table view according to its index in the array.
    - Parameter index: holds the index of the route in the array that is to be deleted
    */
    func deleteRoute(index : Int){
        
        self.view.userInteractionEnabled = false
        let updateObject : QBCOCustomObject = QBCOCustomObject()
        updateObject.ID = myRoutes[index].ID
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setObject(true, forKey: "isDeleted")
        //var customObject : QBCOCustomObject = QBCOCustomObject()
        updateObject.className = "Route"
        updateObject.fields = fields
        
        QBRequest.updateObject(updateObject, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
            QBRequest.userWithID(DatabaseHelper().FetchUser().ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                currentUser = user
                let customUserDictionary : NSDictionary = getCustomDataDictionaryFromUser(user)!
                if self.myRoutes [index].ID == customUserDictionary.valueForKey("activeRouteID") as! String{
                    customUserDictionary.setValue("", forKey: "activeRouteID")
                    let error = NSErrorPointer()
                    do {
                        let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(customUserDictionary, options: NSJSONWritingOptions.PrettyPrinted)
                        let theJSONText = NSString(data: newUserCustomDataJSON,
                            encoding: NSASCIIStringEncoding)
                        user.customData = String(theJSONText!)
                        let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                        userParameters.customData = String(theJSONText!)
                        QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                            //self.tableView.reloadData()
                            self.view.userInteractionEnabled = true
                            //self.getData()
                            
                            self.myRoutes.removeAtIndex(index)
                            self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                            }, errorBlock: { (response : QBResponse!) -> Void in
                                
                        })
                    } catch let error1 as NSError {
                        error.memory = error1
                    } catch {
                        fatalError()
                    }
                    
                }
                else{
                    
                    self.myRoutes.removeAtIndex(index)
                    self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                    //self.tableView.reloadData()
                    self.view.userInteractionEnabled = true
                }
                
                
                }) { (response : QBResponse!) -> Void in
                    
            }
            
            
            

            }) { (response :QBResponse!) -> Void in
                //print("failedDelete")
                self.tableView.reloadData()
                self.view.userInteractionEnabled = true
                self.getData()
        }

    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == myRoutes.count-1 {
            print(indexPath.row.description + "=" + (myRoutes.count-1).description)
            if getNewPage{
                getNewPage = false
                let fields : NSMutableDictionary = NSMutableDictionary()
                fields.setObject(DatabaseHelper().FetchUser().ID, forKey: "user_id")
                fields.setObject(false, forKey: "isDeleted")
                fields.setObject(10, forKey: "limit")
                fields.setObject(10*pageNumber, forKey: "skip")
                QBRequest.objectsWithClassName("Route", extendedRequest: fields, successBlock: { (response :QBResponse!, newRoutes :[AnyObject]!, page : QBResponsePage!) -> Void in
                    if newRoutes != nil{
                        if newRoutes.count != 0{
                            self.getNewPage = true
                            self.pageNumber++
                            self.myRoutes.appendContentsOf(newRoutes as! [QBCOCustomObject])
                            self.refreshControl.endRefreshing()
                            self.tableView.reloadData()
                        }
                    }
                    }) { (response : QBResponse!) -> Void in
                        self.getNewPage = true
                }
                
                
            }
            
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        var actions : [UITableViewRowAction] = []
        let deleteAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
            let alert : UIAlertController = UIAlertController(title: "Delete Route", message: "Are You Sure You Want To Delete This Route?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                self.deleteRoute(indexPath.row)
            })
            let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        deleteAction.backgroundColor = UIColor.redColor()
        
        actions.append(deleteAction)
        return actions
    }
    /**
    method that updates the user route to a new one.
    - Parameter ID: holds the value of the new route id of the user
    */
    func updateUserRouteID(ID : String){
        QBRequest.userWithLogin(DatabaseHelper().FetchUser().login, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            let customUserDictionary = getCustomDataDictionaryFromUser(user)
            customUserDictionary?.setValue(ID, forKey: "activeRouteID")
            let error = NSErrorPointer()
            do {
                let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(customUserDictionary!, options: NSJSONWritingOptions.PrettyPrinted)
                //print("JSON:", terminator: "")
                let theJSONText = NSString(data: newUserCustomDataJSON,
                    encoding: NSASCIIStringEncoding)
                //print(theJSONText)
                user.customData = String(theJSONText!)
                let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                userParameters.customData = String(theJSONText!)
                QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                    self.tableView.reloadData()
                    self.view.userInteractionEnabled = true
                    self.getData()
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        
                })
            } catch let error1 as NSError {
                error.memory = error1
            } catch {
                fatalError()
            }
            }) { (response : QBResponse!) -> Void in
            
        }
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "exitSegueFromMyRoutesViewController"{
            let myRouteViewController : MyRouteViewController = segue.destinationViewController as! MyRouteViewController
            myRouteViewController.currentRouteID = selectedRouteID
            updateUserRouteID(selectedRouteID!)
        }
    }
    

}
