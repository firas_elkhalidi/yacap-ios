//
//  DropDownListCustomTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/4/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class DropDownListCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        cellImageView?.contentMode = UIViewContentMode.ScaleAspectFit
        //self.view.cellImageView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
        //self.contentVerticalAlignment = UIControlContentVerticalAlignment.Fill
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }

}
