//
//  RideRequestsListOrChatListViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/23/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit
///protocol that will allow the RideRequestsViewController to communicate with the RideRequestsListOrChatListViewController
protocol RideRequestsViewControllerDelegate{
    /**
    method that is used to tell the RideRequestsListOrChatListViewController to go to the send opponent's negotiation
    - Parameter opponentUser: the user which negotiation is to be accessed
    */
    func goToThisNeg(opponentUser : QBUUser)
}
///protocol that will allow the ChatsListViewController to communicate with the RideRequestsListOrChatListViewController
protocol ChatsListViewControllerDelegate{
    /**
    method that is used to tell the RideRequestsListOrChatListViewController to go to the send opponent's chat
    - Parameter opponentUser: the user which chat is to be accessed
    */
    func goToThisChat(opponentUser : QBUUser)
}
/**
enum used to indicate which container view to show in the RideRequestsListOrChatListViewController

---
- Requests: it is set to the requests mode
- Chats: it is set to the chats mode
*/
enum TableMode : String{
    case Requests = "RIDEREQUESTS", Chats = "CHATS"
}
class RideRequestsListOrChatListViewController: UIViewController ,CurrentThemeDelegate,RideRequestsViewControllerDelegate,ChatsListViewControllerDelegate{
    
    ///delegate method that is used to tell the RideRequestsListOrChatListViewController to go to the send opponent's chat
    func goToThisChat(opponentUser : QBUUser) {
        selectedUser = opponentUser
        self.performSegueWithIdentifier("goToChatRideRequestsListOrChatListViewController", sender: self)
    }
    ///delegate method that is used to tell the RideRequestsListOrChatListViewController to go to the send opponent's negotiation
    func goToThisNeg(opponentUser : QBUUser) {
        selectedUser = opponentUser
        self.performSegueWithIdentifier("goToNegotiationsRideRequestsListOrChatListViewController", sender: self)
    }
    ///a boolean indicating whether the view controller was entered as part of a push notification for a negotiation
    var isNegPush = false
    ///holds the user of the selected row
    var selectedUser : QBUUser?
    ///progress bar that shows that the chats button is selected
    @IBOutlet weak var chatsProgressBar: UIProgressView!
    ///progress bar that shows that the ride requests button is selected
    @IBOutlet weak var rideRequestsProgressBar: UIProgressView!
    ///instance of the container view containing the RideRequestsViewController
    @IBOutlet weak var rideRequestsContainerView: UIView!
    ///instance of the container view containing the ChatsListViewController
    @IBOutlet weak var chatContainerView: UIView!
    ///hamburger button to open the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    /**
    method that detects when the ride requests button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func rideRequestsButtonPressed(sender: UIButton) {
        initialMode = TableMode.Requests.rawValue
        rideRequestsContainerView.hidden = false
        rideRequestsContainerView.userInteractionEnabled = true
        chatContainerView.userInteractionEnabled = false
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.rideRequestsContainerView.alpha = 1
            self.chatContainerView.alpha = 0
            self.rideRequestsProgressBar.setProgress(1, animated: true)
            self.chatsProgressBar.setProgress(0, animated: true)
            self.setupButtons()
            }) { (Bool) -> Void in
                self.chatContainerView.hidden = true
        }
    }
    ///method that detects when the ride requests button is pressed
    func rideRequestsButtonPressed() {
        initialMode = TableMode.Requests.rawValue
        rideRequestsContainerView.hidden = false
        rideRequestsContainerView.userInteractionEnabled = true
        chatContainerView.userInteractionEnabled = false
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.rideRequestsContainerView.alpha = 1
            self.chatContainerView.alpha = 0
            self.rideRequestsProgressBar.setProgress(1, animated: true)
            self.chatsProgressBar.setProgress(0, animated: true)
            self.setupButtons()
            }) { (Bool) -> Void in
                self.chatContainerView.hidden = true
        }
    }
    ///holds the initial mode of the view controller
    var initialMode : String = ""
    ///delegate method of the CurrentThemeDelegate protocol that indicates that the theme did change
    func themeDidChange() {
        setTheme()
    }
    /**
    method that detects when the chats button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func chatsButtonPressed(sender: UIButton) {
        initialMode = TableMode.Chats.rawValue
        chatContainerView.hidden = false
        chatContainerView.userInteractionEnabled = true
        rideRequestsContainerView.userInteractionEnabled = false
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.rideRequestsContainerView.alpha = 0
            self.chatContainerView.alpha = 1
            self.chatsProgressBar.setProgress(1, animated: true)
            self.rideRequestsProgressBar.setProgress(0, animated: true)
            self.setupButtons()
            }) { (Bool) -> Void in
                self.rideRequestsContainerView.hidden = true
        }
    }
    ///method that detects when the chats button is pressed
    func chatsButtonPressed() {
        initialMode = TableMode.Chats.rawValue
        chatContainerView.hidden = false
        chatContainerView.userInteractionEnabled = true
        rideRequestsContainerView.userInteractionEnabled = false
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.rideRequestsContainerView.alpha = 0
            self.chatContainerView.alpha = 1
            self.chatsProgressBar.setProgress(1, animated: true)
            self.rideRequestsProgressBar.setProgress(0, animated: true)
            self.setupButtons()
            }) { (Bool) -> Void in
                self.rideRequestsContainerView.hidden = true
        }
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "RideRequestsListOrChatListViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    ///instance of the ride requests button
    @IBOutlet weak var rideRequestsButton: UIButton!
    ///instance of the chats button
    @IBOutlet weak var chatsButton: UIButton!
    override func viewDidLoad() {
        currentViewController = self
        super.viewDidLoad()
        if pushOpponentUser != nil{
            if pushOpponentIsNeg{
                self.performSegueWithIdentifier("goToNegotiationsRideRequestsListOrChatListViewController", sender: self)
            }
            else if !pushOpponentIsNeg{
                self.performSegueWithIdentifier("goToChatRideRequestsListOrChatListViewController", sender: self)
            }
        }
        print("initialMode:" + initialMode)
        setTheme()
        currentThemeDelegate  = self
        chatsProgressBar.transform = CGAffineTransformMakeScale(1, 2)
        rideRequestsProgressBar.transform = CGAffineTransformMakeScale(-1, 2)
        rideRequestsProgressBar.progress = 0
        chatsProgressBar.progress = 0
        if initialMode == TableMode.Requests.rawValue{
            rideRequestsContainerView.hidden = false
            rideRequestsContainerView.userInteractionEnabled = true
            chatContainerView.userInteractionEnabled = false
            self.rideRequestsContainerView.alpha = 1
            self.chatContainerView.alpha = 0
            self.chatContainerView.hidden = true
            self.rideRequestsProgressBar.progress = 1
        }
        else{
            chatContainerView.hidden = false
            chatContainerView.userInteractionEnabled = true
            rideRequestsContainerView.userInteractionEnabled = false
            self.rideRequestsContainerView.alpha = 0
            self.chatContainerView.alpha = 1
            self.rideRequestsContainerView.hidden = true
            self.chatsProgressBar.progress = 1
        }
        setupButtons()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
     
        
        // Do any additional setup after loading the view.
    }
    
    ///method that sets up the buttons according to the initial mode of the view controller
    func setupButtons(){
        if currentTheme == Theme.DriverTheme.rawValue{
            if initialMode == TableMode.Chats.rawValue{
                chatsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                rideRequestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            }
            else{
                rideRequestsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                chatsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            }
        }
        else{
            if initialMode == TableMode.Chats.rawValue{
                chatsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                rideRequestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            }
            else{
                rideRequestsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                chatsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            }
        }
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        let childControllers = self.childViewControllers
        for child in childControllers {
            if let requests = child as? RideRequestsViewController{
                requests.delegate = self
                requests.setTheme()
            }
            else if let chats = child as? ChatsListViewController{
                chats.delegate = self
                chats.setTheme()
            }
        }
        chatsButton.backgroundColor = UIColor.clearColor()
        rideRequestsButton.backgroundColor = UIColor.clearColor()

        if currentTheme == Theme.DriverTheme.rawValue{
            menuButton.tintColor = ApplicationThemeColor().YellowThemeColor
            rideRequestsProgressBar.tintColor = ApplicationThemeColor().YellowThemeColor
            chatsProgressBar.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else{
            menuButton.tintColor = ApplicationThemeColor().GreenThemeColor
            chatsProgressBar.tintColor = ApplicationThemeColor().GreenThemeColor
            rideRequestsProgressBar.tintColor = ApplicationThemeColor().GreenThemeColor
        }
        setupButtons()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    self.performSegueWithIdentifier("goToNegotiationsRideRequestsListOrChatListViewController", sender: self)
//}
//else if !pushOpponentIsNeg{
//    self.performSegueWithIdentifier("goToChatRideRequestsListOrChatListViewController", sender: self)
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToNegotiationsRideRequestsListOrChatListViewController"{
            
            let negController = segue.destinationViewController as! NegotiationsViewController
            if selectedUser != nil{
                negController.opponentUser = selectedUser!
            }
        }
        else if segue.identifier == "goToChatRideRequestsListOrChatListViewController"{
            
            let negController = segue.destinationViewController as! ChatViewController
            if selectedUser != nil{
                negController.opponentUser = selectedUser!
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
