//
//  Carpool.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/16/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
class Carpool {
    ///holds the id of the carpool. Same as the one from QuickBlox
    var ID : String = String()
    ///holds the user id of the creator of the object
    var userID : UInt = UInt()
    ///holds the parent ID
    var parentID : UInt = UInt()
    ///holds the opponentid of the user that created the carpool
    var opponentID : UInt = UInt()
    ///holds the current state of the carpool
    var currentState : String = String()
    ///holds that dialog id that is correlated with the carpool
    var dialogID : String = String()
    ///NOTUSED
    var type : String = String()
    ///contains the driver's route id
    var driverRouteID : String = String()
    ///contains the passenger's route id
    var passengerRouteID : String = String()
    ///boolean that indicates whether this carpool is active or not
    var isActive : Bool = Bool()
    init(){
        
    }
    init(ID : String , userID : UInt , parentID : UInt , opponentID : UInt , currentState : String , dialogID : String , type : String , driverRouteID : String , passengerRouteID : String , isActive : Bool){
        self.ID = ID
        self.userID = userID
        self.parentID = parentID
        self.opponentID = opponentID
        self.currentState = currentState
        self.dialogID = dialogID
        self.type = type
        self.driverRouteID = driverRouteID
        self.passengerRouteID = passengerRouteID
        self.isActive = isActive
    }
}