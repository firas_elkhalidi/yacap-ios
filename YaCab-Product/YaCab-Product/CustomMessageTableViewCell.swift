//
//  CustomMessageTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/16/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//


import Foundation
import UIKit

class CustomMessageTableViewCell : UITableViewCell {
    var orangeBubble : UIImage = UIImage(named: "orangeBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15)
    var padding : CGFloat = 20.0
    var aquaBubble : UIImage =  UIImage(named: "aquaBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15)
    var messageTextView: UITextView!
    var backgroundImageView: UIImageView!
    var dateLabel : UILabel!
    var genericButton : UISlider!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.dateLabel = UILabel()
        self.dateLabel.frame = CGRectMake(10, 5, UIScreen.mainScreen().bounds.width-20, 20)
        self.dateLabel.font = UIFont.systemFontOfSize(11)
        self.dateLabel.textColor = UIColor.lightGrayColor()
        self.contentView.addSubview(self.dateLabel)
        
        self.backgroundImageView = UIImageView()
        self.backgroundImageView.frame = CGRectZero
        self.contentView.addSubview(self.backgroundImageView)
        
        self.messageTextView = UITextView()
        self.messageTextView.backgroundColor = UIColor.clearColor()
        self.messageTextView.editable = false
        self.messageTextView.scrollEnabled = false
        self.messageTextView.sizeToFit()
        self.contentView.addSubview(self.messageTextView)
        
        self.genericButton = UISlider()
        self.genericButton.frame = CGRectMake(10, 5, UIScreen.mainScreen().bounds.width - 20, 30)
        //self.genericButton.tintColor = UIColor.blackColor()
        self.contentView.addSubview(genericButton)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(style: UITableViewCellStyle.Default, reuseIdentifier: String())
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
    
    
    
    
    func configureCellWithMessage(message : QBChatMessage, opponentLogin : QBUUser){
        
        var bubbleframe : CGRect = messageTextView.frame;
        bubbleframe.size.width = messageTextView.contentSize.width;
        
        
        self.messageTextView.text = message.text;
        self.messageTextView.font = UIFont.boldSystemFontOfSize(13)
        let font : UIFont = UIFont.boldSystemFontOfSize(13)
        let size : CGSize  = findSizeForText(message.text, font: font)
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "h:mm a, EEE, MMM d"
        formatter.timeZone = NSTimeZone.localTimeZone()
        let time : NSString = formatter.stringFromDate(message.dateSent)
        // Left/Right bubble
        //print("testest")
        //print(DatabaseHelper().FetchUser().ID)
        //print(message.senderID)
        if (DatabaseHelper().FetchUser().ID == message.senderID) {
            //print("aquabubble:" + message.text)
            let screenSize = UIScreen.mainScreen().bounds.width
            //print("thesizeheightis:" + size.height.description)
            self.messageTextView.frame = CGRectMake(screenSize-size.width-padding/2, padding+5, size.width, size.height+padding)
            self.messageTextView.sizeToFit()
            
            self.backgroundImageView.frame = CGRectMake(screenSize-size.width-padding/2, padding+5,
                self.messageTextView.frame.size.width+padding/2, self.messageTextView.frame.size.height+5)
            self.backgroundImageView.image = aquaBubble;
            
            self.dateLabel.textAlignment = NSTextAlignment.Right;
            self.dateLabel.text = DatabaseHelper().FetchUser().login + " at " + (time as String)
            self.dateLabel.frame = CGRectMake(10, 5, UIScreen.mainScreen().bounds.width-20, 20)
            //self.genericButton.setTitle("accept", forState: UIControlState.Normal)
            self.genericButton.frame = CGRectMake(50, 50, UIScreen.mainScreen().bounds.width - 20, 300)
            //self.genericButton.backgroundColor = UIColor.blueColor()
        } else {
            //print("orangebubble")
            self.messageTextView.frame = CGRectMake(padding, padding+5, size.width, size.height + padding)
            self.messageTextView.sizeToFit()
            //print("frame : " )
            //print(self.messageTextView.frame)
            self.backgroundImageView.frame = CGRectMake(padding/2,padding + 5,self.messageTextView.frame.size.width+padding/2, self.messageTextView.frame.size.height+5)
            //print("Changing color" + size.width.description)
            self.backgroundImageView.image = orangeBubble;
            
            self.dateLabel.textAlignment = NSTextAlignment.Left;
            //self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", [[ChatService shared].currentUser login], time];
            self.dateLabel.text = opponentLogin.login + " at " + (time as String)
            //self.genericButton.setTitle("accept", forState: UIControlState.Normal)
            self.genericButton.frame = CGRectMake(50, 50, UIScreen.mainScreen().bounds.width - 20, 300)
            //self.genericButton.backgroundColor = UIColor.blueColor()
        }
    }
    func findHeightForText(text : NSString, font : UIFont) -> CGFloat
    {
        var result : CGFloat = font.pointSize + 4
        if text.length != 0{
            let textSize : CGSize = CGSizeMake(260.0, 10000.0)//Width and height of text area
            let frame : CGRect = text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.TruncatesLastVisibleLine, attributes: [NSFontAttributeName : font], context: nil)
            let size : CGSize = CGSizeMake(frame.size.width, frame.size.height+1);
            //print("Widthforcell:" + frame.size.width.description)
            result = max(size.height, result)//At least one row
            
        }
        return result
    }
    func findSizeForText(text : NSString, font : UIFont) -> CGSize
    {
        if text.length != 0{
            let textSize : CGSize = CGSizeMake(260.0, 10000.0)      //Width and height of text area
            let frame : CGRect = text.boundingRectWithSize(CGSizeMake(260.0, 10000.0), options: NSStringDrawingOptions.TruncatesLastVisibleLine, attributes: [NSFontAttributeName : font], context: nil)
            var size : CGSize = CGSizeMake(frame.size.width, frame.size.height+1);
            size.width = size.width + 15
            //print("heightforcell: " + size.height.description)
            //print("Widthforcell:" + size.width.description)
            if size.width>textSize.width{
                size.width = 260.0
            }
            if size.height>textSize.height{
                size.height = 10000.0
            }
            return size
            
        }
        return CGSizeMake(0, 0)
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
