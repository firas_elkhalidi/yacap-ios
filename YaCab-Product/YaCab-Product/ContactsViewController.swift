//
//  ContactsViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 9/7/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,QBChatDelegate,CurrentThemeDelegate {
    ///holds the selected row's user
    var selectedUser : QBUUser?
    /**
    method that detects when the requests button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func requestsButtonPressed(sender: UIButton) {
        mode = ContactsModes.Requests.rawValue
        setupButtons()
        self.receivedRequests = []
        self.tableView.reloadData()
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setValue(currentUser.ID, forKey: "receiverID")
        
        QBRequest.objectsWithClassName("ContactRequest", extendedRequest: fields, successBlock: { (response : QBResponse!, requests : [AnyObject]!, responsePage :  QBResponsePage!) -> Void in
            //self.mode = ContactsModes.Requests.rawValue
            if requests != nil{
                if requests.count != 0{
                    self.receivedRequests = requests as! [QBCOCustomObject]
                    self.tableView.reloadData()
                }
                else{
                    self.receivedRequests = []
                    self.tableView.reloadData()
                }
            }
            else{
                self.receivedRequests = []
                self.tableView.reloadData()
            }
            }, errorBlock: { (response : QBResponse!) -> Void in
                //self.mode = ContactsModes.Requests.rawValue
                self.receivedRequests = []
                self.tableView.reloadData()
                
        })

    }
    /**
    method that detects when the friends button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func friendsButtonPressed(sender: UIButton) {
        mode = ContactsModes.Contacts.rawValue
        setupButtons()
        tableView.reloadData()
    }
    ///progress view used to indicate that the requests buton is pressed
    @IBOutlet weak var requestsButtonProgressView: UIProgressView!
    ///progress view used to indicate that the friends button is pressed
    @IBOutlet weak var friendsButtonProgressView: UIProgressView!
    ///instance of the requests button that shows the friend requests
    @IBOutlet weak var requestsButton: UIButton!
    ///instance of the friends button that shows the friends
    @IBOutlet weak var friendsButton: UIButton!
    ///search bar that is used to search between the the elements of the table view
    @IBOutlet weak var searchBar: UISearchBar!
    ///hamburger button used to open the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    ///array that holds the received requests of the user
    var receivedRequests : [QBCOCustomObject] = []
    ///color that holds the default iOS blue color
    var defaultBlueColor : UIColor = UIColor()
    /**
    enum that holds the mode of the table view
    - Contacts: indicates that the contacts button is pressed and that the contacts are seen in the table view
    - Requests: indicates that the requests button is pressed and that the friend requests are seen in the table view
    */
    enum ContactsModes : String{
        case Contacts = "CONTACTS", Requests = "REQUESTS"
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        currentThemeDelegate = self
        //setupButtons()
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "ContactsViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    ///method that sets up the buttons according to the current mode
    func setupButtons(){
        if mode == ContactsModes.Contacts.rawValue{
            if currentTheme == Theme.DriverTheme.rawValue{
                
                tableView.alpha = 0
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.tableView.alpha = 1
                    self.friendsButtonProgressView.setProgress(1, animated: true)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(0, animated: true)
                })
            }
            else{
                self.tableView.alpha = 0
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.tableView.alpha = 1
                    self.friendsButtonProgressView.setProgress(1, animated: true)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(0, animated: true)
                })
            }
        }
        else if mode == ContactsModes.Requests.rawValue{
            
            if currentTheme == Theme.DriverTheme.rawValue{
                self.tableView.alpha = 0
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.tableView.alpha = 1
                    self.friendsButtonProgressView.setProgress(0, animated: true)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(1, animated: true)
                })
            }
            else{
                self.tableView.alpha = 0
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.tableView.alpha = 1
                    self.friendsButtonProgressView.setProgress(0, animated: true)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(1, animated: true)
                })
            }
        }
    }
    ///method that sets up the buttons according to the mode on load
    func setupButtonsOnLoad(){
        if mode == ContactsModes.Contacts.rawValue{
            if currentTheme == Theme.DriverTheme.rawValue{
                    self.friendsButtonProgressView.setProgress(1, animated: false)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(0, animated: false)
            }
            else{
                
                    self.friendsButtonProgressView.setProgress(1, animated: false)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(0, animated: false)
                
            }
        }
        else if mode == ContactsModes.Requests.rawValue{
            if currentTheme == Theme.DriverTheme.rawValue{
                
                    self.friendsButtonProgressView.setProgress(0, animated: false)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(1, animated: false)
                
            }
            else{
                
                    self.friendsButtonProgressView.setProgress(0, animated: false)
                    self.friendsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
                    self.requestsButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
                    self.requestsButtonProgressView.setProgress(1, animated: false)
                
            }
        }
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        requestsButton.backgroundColor = UIColor.clearColor()
        friendsButton.backgroundColor = UIColor.clearColor()
        if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            friendsButtonProgressView.progressTintColor = ApplicationThemeColor().YellowThemeColor
            requestsButtonProgressView.progressTintColor = ApplicationThemeColor().YellowThemeColor
            //self.modeSegmentedControl.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            friendsButtonProgressView.progressTintColor = ApplicationThemeColor().GreenThemeColor
            requestsButtonProgressView.progressTintColor = ApplicationThemeColor().GreenThemeColor
            //self.modeSegmentedControl.tintColor = ApplicationThemeColor().GreenThemeColor
        }
        setupButtonsOnLoad()
    }
    func chatDidReceiveContactItemActivity(userID: UInt, isOnline: Bool, status: String!) {
        if mode == ContactsModes.Contacts.rawValue{
            tableView.reloadData()
        }
    }
    func chatDidReceiveContactAddRequestFromUser(userID: UInt) {
        if mode == ContactsModes.Requests.rawValue{
            let fields : NSMutableDictionary = NSMutableDictionary()
            fields.setValue(currentUser.ID, forKey: "receiverID")
            mode = ContactsModes.Requests.rawValue
            QBRequest.objectsWithClassName("ContactRequest", extendedRequest: fields, successBlock: { (response : QBResponse!, requests : [AnyObject]!, responsePage :  QBResponsePage!) -> Void in
                self.receivedRequests = requests as! [QBCOCustomObject]
                self.tableView.reloadData()
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
    }
    ///delegate method of the CurrentThemeDelegate protocol that indicates that the theme has changed
    func themeDidChange() {
        setTheme()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mode == ContactsModes.Contacts.rawValue{
            if QBChat.instance() != nil{
                if QBChat.instance().contactList != nil{
                    if QBChat.instance().contactList.contacts != nil{
                        return QBChat.instance().contactList.contacts.count
                    }
                    else{
                        return 0
                    }
                }
                else{
                    return 0
                }
            }
            else{
                return 0
            }
            
        }
        else{
            return receivedRequests.count
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 72.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! VisibleUsersTableViewCell
        cell.onlineOfflineIndicatorView.hidden = true
        cell.userProfileImageView.image = UIImage(named: "placeholder")
        var currentCellContactListItem : QBContactListItem?
        if mode == ContactsModes.Contacts.rawValue{
            currentCellContactListItem = QBChat.instance().contactList.contacts[indexPath.row] as? QBContactListItem
        }
        if currentCellContactListItem != nil{
            if _userCache!.objectForKey((currentCellContactListItem?.userID.description)!) != nil{
                let user = _userCache!.objectForKey((currentCellContactListItem?.userID.description)!) as! QBUUser
                cell.usernameLabel.text = user.login
                if currentCellContactListItem!.online{
                    cell.userCurrentLocationLabel.text = "Online"
                    cell.onlineOfflineIndicatorView.backgroundColor = UIColor.greenColor()
                }
                else {
                    cell.userCurrentLocationLabel.text = "Offline"
                    cell.onlineOfflineIndicatorView.backgroundColor = UIColor.redColor()
                }
                if _imageCache!.objectForKey(user.ID.description) != nil{
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            }
            
            
            QBRequest.userWithID(currentCellContactListItem!.userID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                _userCache!.setObject(user, forKey: user.ID.description)
                cell.usernameLabel.text = user.login
                if currentCellContactListItem!.online{
                    cell.userCurrentLocationLabel.text = "Online"
                }
                else {
                    cell.userCurrentLocationLabel.text = "Offline"
                }
                if _imageCache!.objectForKey(user.ID.description) != nil{
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
                }, errorBlock: { (reponse : QBResponse!) -> Void in
                    
            })
        }
        else{
            if receivedRequests.count != 0{
                cell.userCurrentLocationLabel.text = ""
                if _userCache!.objectForKey(receivedRequests[indexPath.row].userID.description) != nil{
                    let user = _userCache!.objectForKey(receivedRequests[indexPath.row].userID.description) as! QBUUser
                    cell.usernameLabel.text = user.login
                    if _imageCache!.objectForKey(user.ID.description) != nil{
                        cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                    }
                    QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                        _imageCache!.setObject(data, forKey: user.ID.description)
                        cell.userProfileImageView.image = UIImage(data: data)
                        }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                            
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                    })
                }
                
                QBRequest.userWithID(receivedRequests[indexPath.row].userID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                    _userCache!.setObject(user, forKey: user.ID.description)
                    cell.usernameLabel.text = user.login
                    if _imageCache!.objectForKey(user.ID.description) != nil{
                        cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                    }
                    QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                        _imageCache!.setObject(data, forKey: user.ID.description)
                        cell.userProfileImageView.image = UIImage(data: data)
                        }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                            
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                    })
                    }, errorBlock: { (reponse : QBResponse!) -> Void in
                        
                })
            }

            
        }
        return cell
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if mode == ContactsModes.Requests.rawValue && receivedRequests.count > 0{
            return true
        }
        else if mode == ContactsModes.Contacts.rawValue{
            return true
        }
        return false
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        if mode == ContactsModes.Requests.rawValue && receivedRequests.count != 0{
            var actions : [UITableViewRowAction] = []
            let acceptAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Accept") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
                let objectID : String = self.receivedRequests[indexPath.row].ID
                QBChat.instance().confirmAddContactRequest(self.receivedRequests[indexPath.row].userID)
                QBRequest.deleteObjectWithID(objectID , className: "ContactRequest", successBlock: { (response : QBResponse!) -> Void in
                    self.receivedRequests.removeAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
                        numberOfFriendRequests = numberOfrequests
                    })
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        //print(response)
                        //print("Didntdeleterequest:" + objectID)
                })

            }
            acceptAction.backgroundColor = defaultBlueColor
            let rejectAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Reject") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
                
                let objectID : String = self.receivedRequests[indexPath.row].ID
                QBChat.instance().rejectAddContactRequest(self.receivedRequests[indexPath.row].userID, sentBlock: { (error : NSError!) -> Void in
                    if error != nil{
                        //print("contactError:" + error.debugDescription)
                    }
                    else{
                        

                    }
                })
                QBRequest.deleteObjectWithID(objectID , className: "ContactRequest", successBlock: { (response : QBResponse!) -> Void in
                    self.receivedRequests.removeAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
                        numberOfFriendRequests = numberOfrequests
                    })
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        
                })
                
                
            }
            rejectAction.backgroundColor = UIColor.redColor()
            actions.append(rejectAction)
            actions.append(acceptAction)
            
            return actions
        }
        else if mode == ContactsModes.Contacts.rawValue{
            var actions : [UITableViewRowAction] = []
            let unfriendAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Remove Friend") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
                let currentCellContactListItem = QBChat.instance().contactList.contacts[indexPath.row] as? QBContactListItem
                let objectID : UInt = (currentCellContactListItem?.userID)!
                let alert : UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove friend?", preferredStyle: UIAlertControllerStyle.Alert)
                let actionYes : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                    QBChat.instance().removeUserFromContactList(objectID, sentBlock: { (error : NSError!) -> Void in
                        if error == nil{
                            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                        }
                        else{
                            //print("Could not remove Friend")
                        }
                    })
                })
                let actionNo : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
                alert.addAction(actionYes)
                alert.addAction(actionNo)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            unfriendAction.backgroundColor = UIColor.redColor()
            actions.append(unfriendAction)
            return actions
        }

        
        return []
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if mode == ContactsModes.Contacts.rawValue{
            let userID = (QBChat.instance().contactList.contacts[indexPath.row] as! QBContactListItem).userID
            selectedUser = QBUUser()
            selectedUser!.ID = userID
            performSegueWithIdentifier("segueToUserProfileViewControllerFromContactsViewController", sender: self)
        }
        else{
            selectedUser = QBUUser()
            selectedUser!.ID = receivedRequests[indexPath.row].userID
            performSegueWithIdentifier("segueToUserProfileViewControllerFromContactsViewController", sender: self)
        }
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //        if editingStyle == .Delete {
        //            // Delete the row from the data source
        //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        //        } else if editingStyle == .Insert {
        //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //        }
    }
    ///holds the mode of the table view
    var mode : String?
    ///refreshes the data that is present in the table view
    func refreshData(){
        if mode == ContactsModes.Contacts.rawValue{
            mode = ContactsModes.Requests.rawValue
            self.receivedRequests = []
            self.tableView.reloadData()
        }

        //performSelectorInBackground(Selector("switchMode"), withObject: nil)
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("switchMode"), userInfo: nil, repeats: false)
        //performSelectorOnMainThread(Selector("switchMode"), withObject: nil, waitUntilDone: true)
    }
    ///instance of the table view that holds
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        requestsButtonProgressView.transform = CGAffineTransformMakeScale(1, 2)
        friendsButtonProgressView.transform = CGAffineTransformMakeScale(-1, 2)
        requestsButtonProgressView.progress = 0
        friendsButtonProgressView.progress = 0
        QBChat.instance().addDelegate(self)
        defaultBlueColor = self.view.tintColor
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        mode = ContactsModes.Contacts.rawValue
        //modeSegmentedControl.selectedSegmentIndex = 0
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        // register new tableviewcell
        let nib = UINib(nibName: "VisibleUsersTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        tableView.dataSource = self
        tableView.delegate = self
        //
        setTheme()
        // Do any additional setup after loading the view.
    }
    func chatContactListDidChange(contactList: QBContactList!) {
        if mode == ContactsModes.Requests.rawValue{
            let fields : NSMutableDictionary = NSMutableDictionary()
            fields.setValue(currentUser.ID, forKey: "receiverID")
            QBRequest.objectsWithClassName("ContactRequest", extendedRequest: fields, successBlock: { (response : QBResponse!, requests : [AnyObject]!, responsePage :  QBResponsePage!) -> Void in
                self.receivedRequests = requests as! [QBCOCustomObject]
                self.tableView.reloadData()
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        else{
            tableView.reloadData()
        }
        //print("contactListChanged")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueToUserProfileViewControllerFromContactsViewController"{
            if let controller = segue.destinationViewController as? UserProfileViewController{
                controller.selectedUser = selectedUser
                controller.hasBackButton = true
            }
        }
    }
    

}
