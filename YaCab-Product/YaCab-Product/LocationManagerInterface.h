//
//  LocationManagerInterface.h
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/15/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface CLLocationManager () <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *lastTimestamp;

@end
