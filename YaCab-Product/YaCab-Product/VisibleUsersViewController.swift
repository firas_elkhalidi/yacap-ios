//
//  VisibleUsersViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/31/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
/**
enum that contains the error messages of the ViewController.

---

- DRIVERFULLVEHICLE: the driver has a full vehicle.
- PASSENGERALREADYINCARPOOL: the passenger is already in a carpool.
- USERPASSENGERALREADYHASCARPOOL: the user already has a carpool as a passenger.
- USERPASSENGERCARPOOLWITHPASSENGER: the user is a passenger trying to carpool with a passenger.
- USERDRIVERPROFILEINCOMPLETE: the user has an incomplete profile as a driver.
- PASSENGERHASNOROUTE: the passenger does not have a route.
- USERDRIVERNOTENOUGHSEATS: the user's car doesn't have enough free seats.
- USERDRIVERCARPOOLWITHDRIVER: the user is a driver trying to carpool with a driver.
*/
enum CarpoolErrorMessages : String {
    case  DRIVERFULLVEHICLE = "The driver you are trying to contact has a full car. Please try again later.", PASSENGERALREADYINCARPOOL = "The passenger you are trying to reach is already in a carpool. We hope you meet again!", USERPASSENGERALREADYHASCARPOOL = "You can't carpool with more than one driver as a passenger!", USERPASSENGERCARPOOLWITHPASSENGER = "You can't carpool with a passenger if you are a passenger!",USERDRIVERPROFILEINCOMPLETE = "Please complete your profile to be able to carpool with passengers!",PASSENGERHASNOROUTE = "This passenger does not have a route!", USERDRIVERNOTENOUGHSEATS = "Your car is full! You can't carpool with more passengers!",USERDRIVERCARPOOLWITHDRIVER = "You can't carpool with a driver if you are a driver!"
}
class VisibleUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FiltersViewControllerDelegate,UISearchBarDelegate {
    ///holds the current page that is loaded in the tableView.
    var pageNumber : UInt = 1
    ///has an instance of the FiltersTableViewController in the application.
    var filtersTableViewController : FiltersTableViewController?
    ///used to hold the visible users temporarily while the user is using the searchBar.
    var tempArrayVisibleUsers : [QBLGeoData] = []
    ///used to hold the carpoolers temporarily while the user is using the searchBar.
    var tempArrayVisibleCarpoolers : [QBLGeoData] = []
    ///used to detect if the text is changing for the first time in the searchBar. Used to backup the tableView items in tempArrayVisibleUsers and tempArrayVisibleCarpoolers.
    var searchBarFirstTime = true
    ///used to present the filtersTableViewController on top of the ViewController.
    @IBOutlet weak var searchFiltersButton: UIButton!
    ///used to search in the current list of users in the tableView.
    @IBOutlet weak var searchBar: UISearchBar!
    ///label on the top right of the view.
    @IBOutlet weak var seatsAvailableLabel: UILabel!
    ///label on the top left of the view.
    @IBOutlet weak var usersLabel: UILabel!
    ///method that detects when the searchFiltersButton is pressed.
    @IBAction func searchFiltersButtonPressed(sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let filtersNavigationController = storyboard.instantiateViewControllerWithIdentifier("FiltersNavigationController") as! FiltersNavigationController
        filtersTableViewController = filtersNavigationController.topViewController as? FiltersTableViewController
        filtersTableViewController!.smokerFilterLocal = smokerFilter
        filtersTableViewController!.genderFilterLocal = genderFilter
        filtersTableViewController!.acFilterLocal = ACFilter
        filtersTableViewController?.delegate = self
        self.presentViewController(filtersNavigationController, animated: true) { () -> Void in
            
        }
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if !(searchText.isEmpty){
            if searchBarFirstTime{
                tempArrayVisibleCarpoolers = visibleCarpoolers
                tempArrayVisibleUsers = visibleUsers
                visibleCarpoolers = []
                visibleUsers = []
                searchBarFirstTime = false
                let backcolor = self.view.backgroundColor
                self.view.backgroundColor = UIColor.whiteColor()
                self.usersLabel.text = "SEARCH RESULTS"
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.tableView.alpha = 0
                    
                    
                    }, completion: { (Bool) -> Void in
                        for userData in self.tempArrayVisibleUsers{
                            if userData.user.login.lowercaseString.containsString(searchText.lowercaseString){
                                self.visibleUsers.append(userData)
                                //tableView.reloadData()
                            }
                        }
                        for userData in self.tempArrayVisibleCarpoolers{
                            if userData.user.login.lowercaseString.containsString(searchText.lowercaseString){
                                self.visibleUsers.append(userData)
                                //tableView.reloadData()
                            }
                        }
                        self.tableView.reloadData()
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.tableView.alpha = 1
                            },completion: { (Bool) -> Void in
                                self.view.backgroundColor = backcolor
                        })
                        
                })
                
            }
            else{
                visibleCarpoolers = []
                visibleUsers = []
                for userData in self.tempArrayVisibleUsers{
                    if userData.user.login.lowercaseString.containsString(searchText.lowercaseString){
                        self.visibleUsers.append(userData)
                    }
                }
                for userData in self.tempArrayVisibleCarpoolers{
                    if userData.user.login.lowercaseString.containsString(searchText.lowercaseString){
                        self.visibleUsers.append(userData)
                    }
                }
                self.tableView.reloadData()
            }

        }
        else{
            let backcolor = self.view.backgroundColor
            self.view.backgroundColor = UIColor.whiteColor()
            self.usersLabel.text = "USERS"
            self.searchBarFirstTime = true
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.tableView.alpha = 0
                self.visibleCarpoolers = self.tempArrayVisibleCarpoolers
                self.visibleUsers = self.tempArrayVisibleUsers
                }, completion: { (Bool) -> Void in
                    self.tableView.reloadData()
                    UIView.animateWithDuration(0.1, animations: { () -> Void in
                        self.tableView.alpha = 1
                        },completion: { (Bool) -> Void in
                            //self.tableView.reloadData()
                            self.view.backgroundColor = backcolor
                    })
                    
            })
            
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    ///delegate method that updates the values of the filters after they have been updated in the filtersTableViewController
    func didChangeFilters() {
        ACFilter = filtersTableViewController!.acFilterLocal!
        genderFilter = filtersTableViewController!.genderFilterLocal!
        smokerFilter = filtersTableViewController!.smokerFilterLocal!
        currentRadius = tableViewFilterRadius
        filtersTableViewController = nil
        self.getData()
    }
    ///instance of the tableView that holds the users
    @IBOutlet weak var tableView: UITableView!
    ///label that is showed above the tableView when it is loading
    var placeholder : UILabel = UILabel()
    ///holds the array of visible users on the map
    var visibleUsers : [QBLGeoData] = []
    ///holds the index path of the row selected on the tableView
    var selectedUserRow : NSIndexPath = NSIndexPath()
    ///holds the array of unfiltered users
    var availableUsers : [UserGeoData] = []
    ///holds the array of the user's carpoolers
    var visibleCarpoolers : [QBLGeoData] = []
    ///refresh controller set for the tableView
    var refreshControl = UIRefreshControl()
    /**
    method that is called by tapGesture that is set on the tableView.
    - Parameter sender: holds the sender of the gesture
    */
    func tapped(sender : UITapGestureRecognizer){
        self.view.endEditing(true)
        if tableView.gestureRecognizers != nil{
            if tableView.gestureRecognizers?.count > 0{
                for gesture in tableView.gestureRecognizers!{
                    if gesture is UITapGestureRecognizer{
                        tableView.removeGestureRecognizer(gesture)
                    }
                    
                }
                
            }
        }
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapped:"))
        tableView.addGestureRecognizer(tapGesture)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        setTheme()
        if self.revealViewController() != nil{
            revealViewController().rightRevealToggleAnimated(true)
        }
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        // register new tableviewcell
        let nib = UINib(nibName: "VisibleUsersTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        self.refreshControl.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        tableView.contentInset = UIEdgeInsetsZero;
        let placeholder : UILabel = UILabel()
        placeholder.font = UIFont.italicSystemFontOfSize(14)
        placeholder.numberOfLines = 1; // Use as many lines as needed.
        placeholder.text = "loading"
        placeholder.textAlignment = NSTextAlignment.Center
        placeholder.textColor = UIColor.lightGrayColor()
        
        placeholder.hidden = false
        self.tableView.addSubview(placeholder)
        self.placeholder = placeholder
        self.placeholder.frame = self.tableView.frame;
        getData()

        
    }
    ///method that sets the theme of the ViewController.
    func setTheme(){
        self.title = "Users"
        if currentTheme == Theme.PassengerTheme.rawValue{
            
            self.view.backgroundColor = ApplicationThemeColor().GreenThemeColor
            usersLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            seatsAvailableLabel.hidden = false
            seatsAvailableLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            searchFiltersButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
            searchFiltersButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
            self.refreshControl.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.refreshControl.tintColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            self.view.backgroundColor = ApplicationThemeColor().YellowThemeColor
            usersLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            seatsAvailableLabel.hidden = true
            //seatsAvailableLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            searchFiltersButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            searchFiltersButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
            self.refreshControl.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.refreshControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
        }
        self.navigationController?.navigationBar.translucent = false;
        self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().WhiteThemeColor
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        self.placeholder.frame = self.tableView.frame;
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        self.placeholder.frame = self.tableView.frame;
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "VisibleUsersViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    ///used to sort QBLGeoData according their distance from the user's location
    func sorterForLocations(this:QBLGeoData, that:QBLGeoData) -> Bool {
        if currentUserLocation != nil{
          return currentUserLocation!.location().distanceFromLocation(this.location()) < currentUserLocation!.location().distanceFromLocation(that.location())
        }
        return false
    }
    ///method used recursively to get the currently visible users on the map
    func getDataRecursive(){
        availableUsers = []
        if currentUserLocation != nil{
            let userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            userFilter.lastOnly = true
            let responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: pageNumber, perPage: 100)
            pageNumber++
            print(pageNumber)
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in
                
                var userIDs : [UInt] = []
                print("currentCarpoolers:")
                print(currentCarpoolers)
                for carpooler in currentCarpoolers{
                    if !carpooler.isVisible{
                        userIDs.append(carpooler.user.ID)
                    }
                }
                let filter : QBLGeoDataFilter = QBLGeoDataFilter()
                filter.userIDs = userIDs
                QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1, perPage: 100), successBlock: { (response : QBResponse!, geodata :[AnyObject]!, responsePage : QBGeneralResponsePage!) -> Void in
                    //print("numberofnonvisiblecarpoolers:" + geodata.count.description)
                    var insertedUserIDs : [UInt] = []
                    for g in geodata{
                        if let userGeoData = g as? QBLGeoData{
                            //self.visibleCarpoolers.append(userGeoData)
                            insertedUserIDs.append(userGeoData.user.ID)
                        }
                    }
                    
                    for geoData in userLocations{
                        if let element = geoData as? QBLGeoData{
                            if !insertedUserIDs.contains(element.user.ID){
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                    }
                    
                    //self.visibleUsers = []
                    self.filterIntoVisibleUsers()
                    self.visibleUsers.sortInPlace(self.sorterForLocations)
                    self.tableView.reloadData()
                    self.placeholder.hidden = true
                    self.refreshControl.endRefreshing()
                    //self.boxView.removeFromSuperview()
                    self.view.userInteractionEnabled = true
                    if userLocations.count == 100{
                        self.getDataRecursive()
                    }
                    }) { (response : QBResponse!) -> Void in
                        for geoData in userLocations{
                            if let element = geoData as? QBLGeoData{
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                        
                        
                        
                        //self.visibleUsers = []
                        //self.visibleCarpoolers = []
                        self.filterIntoVisibleUsers()
                        self.visibleUsers.sortInPlace(self.sorterForLocations)
                        self.tableView.reloadData()
                        self.placeholder.hidden = true
                        self.refreshControl.endRefreshing()
                        //self.boxView.removeFromSuperview()
                        self.view.userInteractionEnabled = true
                        if userLocations.count == 100{
                            self.getDataRecursive()
                        }
                }
                
                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    self.placeholder.hidden = true
                    //self.boxView.removeFromSuperview()
                    self.refreshControl.endRefreshing()
                    //
            }
        }
    }
    ///method used to get the currently visible users on the map
    func getData(){
        placeholder.hidden = false
        availableUsers = []
        visibleUsers = []
        visibleCarpoolers = []
        pageNumber = 1
        self.tableView.reloadData()
        if currentUserLocation != nil{
            let userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            userFilter.lastOnly = true
            let responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: pageNumber, perPage: 100)
            pageNumber++
            //userFilter.sortAsc = true
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in
                print("currentCarpoolers:")
                print(currentCarpoolers)
                var userIDs : [UInt] = []
                for carpooler in currentCarpoolers{
                    if !carpooler.isVisible{
                        userIDs.append(carpooler.user.ID)
                    }
                }
                let filter : QBLGeoDataFilter = QBLGeoDataFilter()
                filter.userIDs = userIDs
                filter.lastOnly = true
                QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1, perPage: 100), successBlock: { (response : QBResponse!, geodata :[AnyObject]!, responsePage : QBGeneralResponsePage!) -> Void in
                    //print("numberofnonvisiblecarpoolers:" + geodata.count.description)
                    var insertedUserIDs : [UInt] = []
                    for g in geodata{
                        if let userGeoData = g as? QBLGeoData{
                            self.visibleCarpoolers.append(userGeoData)
                            insertedUserIDs.append(userGeoData.user.ID)
                        }
                    }
                    
                    for geoData in userLocations{
                        if let element = geoData as? QBLGeoData{
                            if !insertedUserIDs.contains(element.user.ID){
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }
                    }
                    
                    //self.visibleUsers = []
                    self.filterIntoVisibleUsers()
                    self.visibleUsers.sortInPlace(self.sorterForLocations)
                    self.visibleCarpoolers.sortInPlace(self.sorterForLocations)
                    self.tableView.reloadData()
                    self.placeholder.hidden = true
                    self.refreshControl.endRefreshing()
                    //self.boxView.removeFromSuperview()
                    self.view.userInteractionEnabled = true
                    if userLocations.count == 100{
                        self.getDataRecursive()
                    }
                    }) { (response : QBResponse!) -> Void in
                        for geoData in userLocations{
                            if let element = geoData as? QBLGeoData{
                                self.availableUsers.append(UserGeoData(geoData: element))
                            }
                        }

                        
                        
                        self.visibleUsers = []
                        self.visibleCarpoolers = []
                        self.filterIntoVisibleUsers()
                        self.visibleUsers.sortInPlace(self.sorterForLocations)
                        self.tableView.reloadData()
                        self.placeholder.hidden = true
                        self.refreshControl.endRefreshing()
                        //self.boxView.removeFromSuperview()
                        self.view.userInteractionEnabled = true
                        if userLocations.count == 100{
                            self.getDataRecursive()
                        }
                }
                
                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    //self.boxView.removeFromSuperview()
                    self.placeholder.hidden = true
                    self.refreshControl.endRefreshing()
                    //
            }
        }
    }
    ///method used to filter the availableUsers array in to the visibleUsers array.
    func filterIntoVisibleUsers(){
        for unfilteredUserLocation in self.availableUsers{
            
            if(unfilteredUserLocation.geoData.user.ID == DatabaseHelper().FetchUser().ID){
                //self.visibleUsers.append(unfilteredUserLocation.geoData)
            }
            else{
                var skipFilter = false
                for carpooler in currentCarpoolers{
                    if carpooler.user.login == unfilteredUserLocation.geoData.user.login{
                        self.visibleUsers.append(unfilteredUserLocation.geoData)
                        skipFilter = true
                    }
                }
                if !skipFilter{
                    
                    if let isSmokerBool : Bool = unfilteredUserLocation.userJSONDictionary["isSmoker"] as? Bool{
                        var isSmokerString : String = String()
                        if isSmokerBool{
                            isSmokerString = SmokerStates.IsSmoker.rawValue
                        }
                        else{
                            isSmokerString = SmokerStates.NotSmoker.rawValue
                        }
                        if isSmokerString == smokerFilter || smokerFilter
                            == SmokerStates.DontCareSmoker.rawValue{
                                if let hasAC : Bool = unfilteredUserLocation.userJSONDictionary["hasAC"] as? Bool{
                                    var hasACString : String = String()
                                    if hasAC{
                                        hasACString = ACStates.AC.rawValue
                                    }
                                    else {
                                        hasACString = ACStates.NoAC.rawValue
                                    }
                                    if hasACString == ACFilter || ACFilter == ACStates.DontCareAC.rawValue || !(unfilteredUserLocation.userJSONDictionary["isDriver"] as! Bool){
                                        if let gender = unfilteredUserLocation.userJSONDictionary["genderType"] as? String{
                                            if gender == genderFilter || genderFilter == GenderStates.DontCareGender.rawValue{
                                                self.visibleUsers.append(unfilteredUserLocation.geoData)
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
                
            }
            
        }
    }

    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        if visibleCarpoolers.count == 0{
            return 1
        }
        else{
            return 2
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if visibleCarpoolers.count != 0 {
            if section == 1{
                return visibleUsers.count
            }
            else {
                return visibleCarpoolers.count
            }
        }
        return visibleUsers.count
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        var actions : [UITableViewRowAction] = []
        let profileAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Profile") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
            self.selectedUserRow = indexPath
            self.performSegueWithIdentifier("goToProfileFromVisibleUsersSegue", sender: self)
        }
        profileAction.backgroundColor = UIColor.purpleColor()

        let chatAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Negotiate") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
            self.view.userInteractionEnabled = false
            self.selectedUserRow = indexPath
            var selectedUser: QBUUser = QBUUser()
            if indexPath.section == 0 && self.visibleCarpoolers.count != 0{
                selectedUser = self.visibleCarpoolers[indexPath.row].user
            }
            else if indexPath.section == 0 && self.visibleCarpoolers.count == 0{
                selectedUser = self.visibleUsers[indexPath.row].user
            }
            else if indexPath.section == 1 && self.visibleCarpoolers.count != 0{
                selectedUser = self.visibleUsers[indexPath.row].user
            }
            var numberOfSeats: Int?
            if let n = getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("numberOfSeats") as? Int{
                    numberOfSeats = n
            }
            else{
                numberOfSeats = -1
            }
            getNumberOfCarpoolersForUserWithID(selectedUser.ID, completion: { (numberOfCarpools) -> Void in
                if (getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("isDriver") as! Bool) && numberOfSeats != -1 && numberOfCarpools >= numberOfSeats{
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.DRIVERFULLVEHICLE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction) -> Void in
                        //self.navigationController?.popViewControllerAnimated(true)
                    })
                    alert.addAction(alertOK)
                    self.presentViewController(alert, animated: true, completion: {
                        self.view.userInteractionEnabled = true
                    })
                }
                else if !(getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("isDriver") as! Bool) && numberOfCarpools > 0{
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERALREADYINCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                    alert.addAction(alertOK)
                    
                    self.presentViewController(alert, animated: true, completion: {
                        self.view.userInteractionEnabled = true
                    })
                }
                else{
                    QBRequest.userWithID(currentUser.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                        currentUser = user
                        if !(getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool){
                            if numberOfCurrentCarpoolers > 0{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERPASSENGERALREADYHASCARPOOL.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: {
                                    self.view.userInteractionEnabled = true
                                })

                            }
                            else if !(getCustomDataDictionaryFromUser(selectedUser)!.valueForKey("isDriver") as! Bool) {
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERPASSENGERCARPOOLWITHPASSENGER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: {
                                    self.view.userInteractionEnabled = true
                                })

                            }
                            else{
                                self.view.userInteractionEnabled = true
                                self.performSegueWithIdentifier("segueToNegotiationsViewController", sender: self)
                            }
                        }
                        else if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("isDriver") as! Bool) {
                            if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: {
                                    self.view.userInteractionEnabled = true
                                })
                                

                                
                            }
                            
                            else if (((getCustomDataDictionaryFromUser(selectedUser)!.valueForKey("activeRouteID") as! String).isEmpty)){
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.PASSENGERHASNOROUTE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:  { (alert : UIAlertAction!) -> Void in
                                    //self.navigationController?.popViewControllerAnimated(true)
                                })
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: {
                                    self.view.userInteractionEnabled = true
                                })
                            }
                                
                            else if numberOfCurrentCarpoolers > 0{
                                if (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty{
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERPROFILEINCOMPLETE.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: {
                                        self.view.userInteractionEnabled = true
                                    })
                                }
                                else if numberOfCurrentCarpoolers >= Int((getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String)){
                                    //print("")
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERNOTENOUGHSEATS.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: {
                                        self.view.userInteractionEnabled = true
                                    })

                                    
                                }
                                else if (getCustomDataDictionaryFromUser(selectedUser)!.valueForKey("isDriver") as! Bool) {
                                    let alert : UIAlertController = UIAlertController(title: "Alert!", message:CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                    let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                    alert.addAction(alertOK)
                                    self.presentViewController(alert, animated: true, completion: {
                                        self.view.userInteractionEnabled = true
                                    })
                                }
                                else{
                                    self.view.userInteractionEnabled = true
                                    self.performSegueWithIdentifier("segueToNegotiationsViewController", sender: self)
                                }
                            }
                            else if (getCustomDataDictionaryFromUser(selectedUser)!.valueForKey("isDriver") as! Bool) {
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: CarpoolErrorMessages.USERDRIVERCARPOOLWITHDRIVER.rawValue, preferredStyle: UIAlertControllerStyle.Alert)
                                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(alertOK)
                                self.presentViewController(alert, animated: true, completion: {
                                    self.view.userInteractionEnabled = true
                                })
                            }
                            else{
                                self.view.userInteractionEnabled = true
                                self.performSegueWithIdentifier("segueToNegotiationsViewController", sender: self)
                            }
                            
                        }
                        
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            self.view.userInteractionEnabled = true
                    })
                }
            })


        }
        if indexPath.section == 0 && self.visibleCarpoolers.count != 0{
            let selectedUser = self.visibleCarpoolers[indexPath.row].user
                if getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("isDriver") as! Bool{
                    chatAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }
                else{
                    chatAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                }
        }
        else if indexPath.section == 0 && self.visibleCarpoolers.count == 0{
            
            let selectedUser = self.visibleUsers[indexPath.row].user
                if getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("isDriver") as! Bool{
                    chatAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }
                else{
                    chatAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                }
        }
        else if indexPath.section == 1 && self.visibleCarpoolers.count != 0{
             let selectedUser = self.visibleUsers[indexPath.row].user
                if getCustomDataDictionaryFromUser(selectedUser)?.valueForKey("isDriver") as! Bool{
                    chatAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }
                else{
                    chatAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    profileAction.backgroundColor = ApplicationThemeColor().GreenThemeColor
                }
            
            
        }
        //chatAction.backgroundColor = UIColor.orangeColor()
        //actions.append(profileAction)
        actions.append(chatAction)
        return actions
    }
    /**
    method used to animate the color of a VisibleUsersTableViewCell according to the sent color and duration.
    - Parameters:
        - cell: the cell that will be animated.
        - color: the new background color of the animated cell.
        - textColor: the new text color of the animated cell.
        - duration: the duration of the animation.
    */
    func animateCell(cell : VisibleUsersTableViewCell, color : UIColor,textColor : UIColor, duration : NSTimeInterval){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            cell.backgroundColor = color
            cell.userCurrentLocationLabel.textColor = textColor
            cell.usernameLabel.textColor = textColor
        }) { (Bool) -> Void in
            
        }
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if visibleCarpoolers.count != 0{
            if section == 0{
                return "Current Carpoolers"
            }
            else if section == 1{
                return "Visible Users"
            }
        }
        return "Visible Users"
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedUserRow = indexPath
        self.performSegueWithIdentifier("goToProfileFromVisibleUsersSegue", sender: self)
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! VisibleUsersTableViewCell
        cell.userProfileImageView.image = UIImage(named : "placeholder.jpg")!
        cell.userCurrentLocationLabel.text = ""
        cell.numberOfAvailableSeatsLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
        cell.numberOfAvailableSeatsLabel.text = ""
        cell.userStatusCapImageView.image = nil
        if indexPath.section == 0 && visibleCarpoolers.count == 0{
            let selectedUser = visibleUsers[indexPath.row]
                cell.usernameLabel.text = (selectedUser.user as QBUUser).login
                //cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                var selectedUserMapItem : MKMapItem = MKMapItem()
                let geocoder : CLGeocoder = CLGeocoder()
            
            if _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) != nil{
                cell.userCurrentLocationLabel.text = _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) as? String
            }
            
                geocoder.reverseGeocodeLocation(selectedUser.location(), completionHandler: { (placemarks : [CLPlacemark]?, error : NSError?) -> Void in
                    if placemarks?.count > 0 {
                        if let placemark: CLPlacemark = placemarks![0]{
                            selectedUserMapItem =  MKMapItem(placemark: MKPlacemark(placemark: placemark))
                            let locationString = "from " + selectedUserMapItem.name!
                            let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                            extendedRequest.setObject(selectedUser.user.ID, forKey: "user_id")
                            extendedRequest.setObject(true, forKey: "isActive")
                            QBRequest.objectsWithClassName("Route", extendedRequest: extendedRequest, successBlock: { (response :QBResponse!, routes : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                if routes != nil{
                                    if routes.count > 0{
                                        if let route = routes[0] as? QBCOCustomObject{
                                            cell.userCurrentLocationLabel.text = locationString + " to " + (route.fields.valueForKey("destinationLocationName") as! String)
                                            _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                        }
                                    }
                                    else{
                                        cell.userCurrentLocationLabel.text = locationString
                                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                    }
                                }
                                else{
                                    cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                }
                                }, errorBlock: { (response : QBResponse!) -> Void in
                             cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                            })
                        }
                    }
                    else{
                        cell.userCurrentLocationLabel.text = "Location Unknown"
                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                    }
                })

                if (getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("isDriver") as! Bool)
                {
//                    cell.backgroundColor = ApplicationThemeColor().YellowThemeColor
//                    cell.userCurrentLocationLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
//                    cell.usernameLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
                    cell.userStatusCapImageView.image = UIImage(named: "driverCap")
                    if let numberOfSeatsInCar = getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("numberOfSeats"){
                        getNumberOfCarpoolersForUserWithID(selectedUser.user.ID, completion: { (numberOfCarpools) -> Void in
                            if let _ = numberOfSeatsInCar as? String{
                                let availableSeats = Int(numberOfSeatsInCar as! String)! - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                            else{
                                let availableSeats = (numberOfSeatsInCar as! Int) - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                        })
                    }

                }
                else{
                    cell.userStatusCapImageView.image = UIImage(named: "passengerCap")
                    //cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    //cell.userCurrentLocationLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                    //cell.usernameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                }
            if _imageCache!.objectForKey(selectedUser.user.ID.description) != nil {
                cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(selectedUser.user.ID.description) as! NSData)
            }
                QBRequest.downloadFileWithID(UInt(selectedUser.user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: selectedUser.user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })

            
            
        }
        else if indexPath.section == 0 && visibleCarpoolers.count != 0{
            
            let selectedUser = visibleCarpoolers[indexPath.row]
                cell.usernameLabel.text = selectedUser.user.login
                //cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                var selectedUserMapItem : MKMapItem = MKMapItem()
                let geocoder : CLGeocoder = CLGeocoder()
            
            
            if _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) != nil{
                cell.userCurrentLocationLabel.text = _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) as? String
            }
            
                geocoder.reverseGeocodeLocation(selectedUser.location(), completionHandler: { (placemarks : [CLPlacemark]?, error : NSError?) -> Void in
                    if placemarks?.count > 0 {
                        if let placemark: CLPlacemark = placemarks![0]{
                            selectedUserMapItem =  MKMapItem(placemark: MKPlacemark(placemark: placemark))
                            let locationString = "from " + selectedUserMapItem.name!
                            let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                            extendedRequest.setObject(selectedUser.user.ID, forKey: "user_id")
                            extendedRequest.setObject(true, forKey: "isActive")
                            QBRequest.objectsWithClassName("Route", extendedRequest: extendedRequest, successBlock: { (response :QBResponse!, routes : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                if routes != nil{
                                    if routes.count > 0{
                                        if let route = routes[0] as? QBCOCustomObject{
                                            cell.userCurrentLocationLabel.text = cell.userCurrentLocationLabel.text! + " to " + (route.fields.valueForKey("destinationLocationName") as! String)
                                            _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                        }
                                    }
                                    else{
                                        cell.userCurrentLocationLabel.text = locationString
                                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                    }
                                }
                                else{
                                    cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                }
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                            })
                        }
                    }
                    else{
                        cell.userCurrentLocationLabel.text = "Location Unknown"
                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                    }
                })
                
                if (getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("isDriver") as! Bool)
                {
                    cell.userStatusCapImageView.image = UIImage(named: "driverCap")
                    if let numberOfSeatsInCar = getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("numberOfSeats"){
                        getNumberOfCarpoolersForUserWithID(selectedUser.user.ID, completion: { (numberOfCarpools) -> Void in
                            if let _ = numberOfSeatsInCar as? String{
                                let availableSeats = Int(numberOfSeatsInCar as! String)! - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                            else{
                                let availableSeats = (numberOfSeatsInCar as! Int) - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                        })
                    }
                }
                else{
                    cell.userStatusCapImageView.image = UIImage(named: "passengerCap")
//                    cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
//                    cell.userCurrentLocationLabel.textColor = ApplicationThemeColor().WhiteThemeColor
//                    cell.usernameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                }
            if _imageCache!.objectForKey(selectedUser.user.ID.description) != nil {
                cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(selectedUser.user.ID.description) as! NSData)
            }
                QBRequest.downloadFileWithID(UInt(selectedUser.user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: selectedUser.user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            
        }
        else if indexPath.section == 1 && currentCarpoolers.count != 0 {
            
            let selectedUser = visibleUsers[indexPath.row]
                cell.usernameLabel.text = selectedUser.user.login
                //cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                var selectedUserMapItem : MKMapItem = MKMapItem()
                let geocoder : CLGeocoder = CLGeocoder()
            
            
            if _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) != nil{
                cell.userCurrentLocationLabel.text = _userGeocodedLocationStringCache!.objectForKey(selectedUser.user.ID.description) as? String
            }
                geocoder.reverseGeocodeLocation(selectedUser.location(), completionHandler: { (placemarks : [CLPlacemark]?, error : NSError?) -> Void in
                    if placemarks?.count > 0 {
                        if let placemark: CLPlacemark = placemarks![0]{
                            selectedUserMapItem =  MKMapItem(placemark: MKPlacemark(placemark: placemark))
                            let locationString = "from " + selectedUserMapItem.name!
                            let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                            extendedRequest.setObject(selectedUser.user.ID, forKey: "user_id")
                            extendedRequest.setObject(true, forKey: "isActive")
                            QBRequest.objectsWithClassName("Route", extendedRequest: extendedRequest, successBlock: { (response :QBResponse!, routes : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                if routes != nil{
                                    if routes.count > 0{
                                        if let route = routes[0] as? QBCOCustomObject{
                                            cell.userCurrentLocationLabel.text = cell.userCurrentLocationLabel.text! + " to " + (route.fields.valueForKey("destinationLocationName") as! String)
                                            _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                        }
                                    }
                                    else{
                                        cell.userCurrentLocationLabel.text = locationString
                                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                    }
                                }
                                else{
                                    cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                                }
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    cell.userCurrentLocationLabel.text = locationString
                                    _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                            })
                        }
                    }
                    else{
                        cell.userCurrentLocationLabel.text = "Location Unknown"
                        _userGeocodedLocationStringCache!.setObject(cell.userCurrentLocationLabel.text!, forKey: selectedUser.user.ID.description)
                    }
                    
                    
                })
                if (getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("isDriver") as! Bool)
                {
                    cell.userStatusCapImageView.image = UIImage(named: "driverCap")
//                    cell.backgroundColor = ApplicationThemeColor().YellowThemeColor
//                    cell.userCurrentLocationLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
//                    cell.usernameLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
                    if let numberOfSeatsInCar = getCustomDataDictionaryFromUser(selectedUser.user)?.valueForKey("numberOfSeats"){
                        getNumberOfCarpoolersForUserWithID(selectedUser.user.ID, completion: { (numberOfCarpools) -> Void in
                            if let _ = numberOfSeatsInCar as? String{
                                let availableSeats = Int(numberOfSeatsInCar as! String)! - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                            else{
                                let availableSeats = (numberOfSeatsInCar as! Int) - numberOfCarpools
                                cell.numberOfAvailableSeatsLabel.text = availableSeats.description
                            }
                        })
                    }
                }
                else{
                    cell.userStatusCapImageView.image = UIImage(named: "passengerCap")
                    //cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    //cell.userCurrentLocationLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                    //cell.usernameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
                }
            if _imageCache!.objectForKey(selectedUser.user.ID.description) != nil {
                cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(selectedUser.user.ID.description) as! NSData)
            }
                QBRequest.downloadFileWithID(UInt(selectedUser.user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: selectedUser.user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
        }

        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 72.0
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //        if editingStyle == .Delete {
        //            // Delete the row from the data source
        //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        //        } else if editingStyle == .Insert {
        //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //        }
    }
    
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToChatFromVisibleUsersSegue"{
            //print("chatnavigation")
            let navigationController = segue.destinationViewController as! ChatViewNavigationViewController
            let chatViewController : ChatViewController = navigationController.topViewController as! ChatViewController
            if selectedUserRow.section == 0 && visibleCarpoolers.count == 0{
            chatViewController.opponentUser = visibleUsers[selectedUserRow.row].user
            }
            else if selectedUserRow.section == 1 && currentCarpoolers.count != 0{
                chatViewController.opponentUser = visibleUsers[selectedUserRow.row].user
            }
            else if selectedUserRow.section == 0 && currentCarpoolers.count != 0{
                chatViewController.opponentUser = visibleCarpoolers[selectedUserRow.row].user
            }
        }
            
        else if segue.identifier == "segueToNegotiationsViewController"{
            let negotiationsViewController : NegotiationsViewController = segue.destinationViewController as! NegotiationsViewController
            if selectedUserRow.section == 0 && visibleCarpoolers.count == 0{
                negotiationsViewController.opponentUser = visibleUsers[selectedUserRow.row].user
            }
            else if selectedUserRow.section == 1 && currentCarpoolers.count != 0{
                negotiationsViewController.opponentUser = visibleUsers[selectedUserRow.row].user
            }
            else if selectedUserRow.section == 0 && currentCarpoolers.count != 0{
                negotiationsViewController.opponentUser = visibleCarpoolers[selectedUserRow.row].user
            }
        }
        else if segue.identifier == "goToProfileFromVisibleUsersSegue"{
            let profileViewController : UserProfileViewController = segue.destinationViewController as! UserProfileViewController
            if selectedUserRow.section == 0 && visibleCarpoolers.count == 0{
                profileViewController.selectedUser = visibleUsers[selectedUserRow.row].user
                profileViewController.hasBackButton = true
            }
            else if selectedUserRow.section == 1 && currentCarpoolers.count != 0{
                profileViewController.selectedUser = visibleUsers[selectedUserRow.row].user
                profileViewController.hasBackButton = true
            }
            else if selectedUserRow.section == 0 && currentCarpoolers.count != 0{
                profileViewController.selectedUser = visibleCarpoolers[selectedUserRow.row].user
                profileViewController.hasBackButton = true
            }
            
        }

    }
    
}
