//
//  VisibleUsersTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khalidi on 6/10/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class VisibleUsersTableViewCell: UITableViewCell {

    @IBOutlet weak var userProfileImageView: UIImageView!
    
    @IBOutlet weak var userCurrentLocationLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!

    @IBOutlet weak var onlineOfflineIndicatorView: UIView!
    @IBOutlet weak var userStatusCapImageView: UIImageView!
    @IBOutlet weak var numberOfAvailableSeatsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width / 6;
        userProfileImageView.clipsToBounds = true;
        userProfileImageView.userInteractionEnabled = true
        
        onlineOfflineIndicatorView.layer.cornerRadius = onlineOfflineIndicatorView.frame.size.width/2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
