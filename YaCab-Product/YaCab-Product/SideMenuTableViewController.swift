//
//  SideMenuTableViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/31/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

/**
method that cancels all the ongoing negotiations of the user with userID as his user id.
- Parameters:
    - userID: holds the user id of the user id of the user
*/
func cancelAllOngoingNegotiationsForUserWithUserID(userID : UInt, completion : (success : Bool) -> Void){
    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
    extendedRequest.setValue(userID, forKey: "user_id[or]")
    extendedRequest.setValue(userID, forKey: "opponentID[or]")
    extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState[ne]")
    extendedRequest.setValue(true, forKey: "isActive")
    
    QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
        var dialogIDs : [String] = []
        for carpool : QBCOCustomObject in carpools as! [QBCOCustomObject]{
            carpool.fields.setValue(CarpoolStates.CANCELLED.rawValue,forKey : "currentState")
            carpool.fields.setValue(false , forKey : "isActive")
            dialogIDs.append(carpool.fields.valueForKey("dialogID") as! String)
        }
        let message : QBChatMessage = QBChatMessage()
        message.text = "blablabla"
        let params : NSMutableDictionary = NSMutableDictionary()
        params["save_to_history"] = true
        params["senderLogin"] = DatabaseHelper().FetchUser().login
        params["isNeg"] = true
        params["itemType"] = "3"
        params["currentState"] = CarpoolStates.CANCELLED.rawValue
        params["color"] = CellColorStates.GREEN.rawValue
        if (getCustomDataDictionaryFromUser(currentUser)?.valueForKey("isDriver") as! Bool){
            params["color"] = CellColorStates.YELLOW.rawValue
        }
        
        message.text = "Carpool Denied!"
        if (getCustomDataDictionaryFromUser(currentUser)?.valueForKey("isDriver") as! Bool){
            message.text = "Not Interested!"
        }
        message.customParameters = params
        
        QBRequest.dialogsWithSuccessBlock({ (response : QBResponse!, ds: [AnyObject]!, set : Set<NSObject>!) -> Void in
            if let dialogs = ds as? [QBChatDialog]{
                for dialog in dialogs{
                    if dialogIDs.contains(dialog.ID) {
                        dialog.sendMessage(message)
                    }
                }
            }
            }, errorBlock: { (response : QBResponse!) -> Void in
                
        })
        QBRequest.updateObjects(carpools, className: "Carpool", successBlock: { (response : QBResponse!, _: [AnyObject]!, _: [AnyObject]!) -> Void in
            completion(success: true)
            }, errorBlock: { (response : QBResponse!) -> Void in
                
        })
        }) { (response : QBResponse!) -> Void in
            completion(success: false)
    }
}

class SideMenuTableViewController: UITableViewController, QBChatDelegate,CurrentNumberOfCarpoolersDelegate,CurrentNumberOfNegotiationsDelegate,CurrentNumberOfContactRequestsDelegate {
    ///label that shows the number of current carpools for the user(chats)
    @IBOutlet weak var numberOfCarpoolsLabel: UILabel!
    ///label that shows the number of current ride requests for the user(ride requests)
    @IBOutlet weak var numberOfRideRequestsLabel: UILabel!
    ///image view of the logo of the application
    @IBOutlet weak var imageView: UIImageView!
    ///image view of the map cell
    @IBOutlet weak var mapImageView: UIImageView!
    ///label that is on the map cell
    @IBOutlet weak var mapLabel: UILabel!
    ///label that is on the profile cell
    @IBOutlet weak var profileLabel: UILabel!
    ///image view of the profile cell
    @IBOutlet weak var profileImageView: UIImageView!
    ///image view of the my route cell
    @IBOutlet weak var myRouteImageView: UIImageView!
    ///label that is on the my route cell
    @IBOutlet weak var myRouteLabel: UILabel!
    ///image view of the settings cell
    @IBOutlet weak var settingsImageView: UIImageView!
    ///label that is on the settings cell
    @IBOutlet weak var settingsLabel: UILabel!
    ///label that is on the logout cell
    @IBOutlet weak var logoutLabel: UILabel!
    ///image view of the favorites cell
    @IBOutlet weak var favoritesImageView: UIImageView!
    ///label that is on the favorites cell
    @IBOutlet weak var favoritesLabel: UILabel!
    ///image view of the friends cell
    @IBOutlet weak var amigosImageView: UIImageView!
    ///label that is on the friends cell
    @IBOutlet weak var amigosLabel: UILabel!
    ///image view of the ride requests cell
    @IBOutlet weak var rideRequestsImageView: UIImageView!
    ///label that is on the ride requests cell
    @IBOutlet weak var rideRequestsLabel: UILabel!
    ///image view of the chats cell
    @IBOutlet weak var chatImageView: UIImageView!
    ///label that is on the chats cell
    @IBOutlet weak var chatLabel: UILabel!
    ///segmented control that indicates the state of the user (passenger or driver)
    @IBOutlet weak var passengerORdriverSegmentedControl: UISegmentedControl!
    ///seperator below the map cell
    @IBOutlet weak var mapSeparator: UIView!
    ///seperator below the profile cell
    @IBOutlet weak var profileSeparator: UIView!
    ///seperator below the chats cell
    @IBOutlet weak var chatSeparator: UIView!
    ///seperator below the ride requests cell
    @IBOutlet weak var rideRequestsSeparator: UIView!
    ///seperator below the friends cell
    @IBOutlet weak var amigosSeparator: UIView!
    ///seperator below the favorites cell
    @IBOutlet weak var favoritesSeparator: UIView!
    ///seperator below the my route cell
    @IBOutlet weak var myRouteSeparator: UIView!
    ///label that is on the about us cell
    @IBOutlet weak var aboutUsLabel: UILabel!
    ///seperator below the about us cell
    @IBOutlet weak var aboutUsSeparator: UIView!
    ///indicates the currently selected row of table view
    var selectedRow = 1
    ///delegate method of the CurrentNumberOfNegotiationsDelegate that indicates that the current number of negotiations of the user has changed
    func currentNumberOfNegotiationsDidChange() {
        //print("currentNumberOfNegotiationsDidChange")
        if numberOfNegotiations != 0{
            numberOfRideRequestsLabel.text = numberOfNegotiations.description
        }
        else{
             numberOfRideRequestsLabel.text = ""
        }
        if currentNumberOfNegotiationsInRideRequestsDelegate != nil{
            currentNumberOfNegotiationsInRideRequestsDelegate?.currentNumberOfNegotiationsDidChange()
        }
    }
    ///delegate method of the CurrentNumberOfCarpoolersDelegate that indicates that the current number of carpoolers of the user has changed
    func currentNumberOfCarpoolersDidChange() {
        //print("currentNumberOfCarpoolersDidChange")
        if numberOfCurrentCarpoolers != 0{
            numberOfCarpoolsLabel.text = numberOfCurrentCarpoolers.description
        }
        else{
            numberOfCarpoolsLabel.text = ""
        }
    }
    ///delegate method of the CurrentNumberOfContactRequestsDelegate that indicates that the current number of carpoolers of the user has changed
    func currentNumberOfContactRequestsDidChange() {
        if numberOfFriendRequests != 0{
            numberOfFriendRequestsLabel.text = numberOfFriendRequests.description
        }
        else {
            numberOfFriendRequestsLabel.text = ""
        }
        
    }
    override func viewDidLoad() {
        globalRevealViewController = self.revealViewController()
        super.viewDidLoad()
        numberOfRideRequestsLabel.text = ""
        numberOfCarpoolsLabel.text = ""
        if numberOfNegotiations != 0{
            numberOfRideRequestsLabel.text = numberOfNegotiations.description
        }
        if numberOfCurrentCarpoolers != 0{
            numberOfCarpoolsLabel.text = numberOfCurrentCarpoolers.description
        }
        if numberOfFriendRequests != 0{
            numberOfFriendRequestsLabel.text = numberOfFriendRequests.description
        }
        else {
            numberOfFriendRequestsLabel.text = ""
        }
        QBChat.instance().addDelegate(self)
        currentNumberOfCarpoolersDelegate = self
        currentNumberOfNegotiationsDelegate = self
        currentNumberOfContactRequestsDelegate = self
        imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        mapImageView.image = mapImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        profileImageView.image = profileImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        myRouteImageView.image = myRouteImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        settingsImageView.image = settingsImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        //pesosImageView.image = pesosImageView.image?.imageWithRenderingMode( UIImageRenderingMode.AlwaysTemplate)
        
        favoritesImageView.image = favoritesImageView.image?.imageWithRenderingMode( UIImageRenderingMode.AlwaysTemplate)
        amigosImageView.image = amigosImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        rideRequestsImageView.image = rideRequestsImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        chatImageView.image = chatImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        setTheme()
        ApplicationIconCell.userInteractionEnabled = false
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    ///cell that holds the logo of the application
    @IBOutlet weak var ApplicationIconCell: UITableViewCell!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        if numberOfNegotiations != 0{
            numberOfRideRequestsLabel.text = numberOfNegotiations.description
        }
        else{
            numberOfRideRequestsLabel.text = ""
        }
        if numberOfCurrentCarpoolers != 0{
            numberOfCarpoolsLabel.text = numberOfCurrentCarpoolers.description
        }
        else{
            numberOfCarpoolsLabel.text = ""
        }
        if numberOfFriendRequests != 0{
            numberOfFriendRequestsLabel.text = numberOfFriendRequests.description
        }
        else {
            numberOfFriendRequestsLabel.text = ""
        }
        self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedRow, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "SideMenuTableViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    func chatDidReceiveContactAddRequestFromUser(userID: UInt) {
        getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
            numberOfFriendRequests = numberOfrequests
        })
    }
    ///label that holds the number of friend requests of the user on the friends cell
    @IBOutlet weak var numberOfFriendRequestsLabel: UILabel!
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row == 19{
            logout()
            tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedRow, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
        }
        else if indexPath.row == 1{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? MapViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealMapViewController", sender: self)
                }
                selectedRow = 1
            }
        }
        else if indexPath.row == 3{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? UserProfileViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealUserProfileViewController", sender: self)
                }
                selectedRow = 3
            }
        }
        else if indexPath.row == 5{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? MyRouteViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealMyRouteViewController", sender: self)
                }
                selectedRow = 5
            }
        }
        else if indexPath.row == 7{
            
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? MyRatingsViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealMyRatingsViewController", sender: self)
                }
                selectedRow = 7
            }
            
        }
        else if indexPath.row == 9{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? ContactsViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealContactsViewController", sender: self)
                }
                selectedRow = 9
            }
        }
        else if indexPath.row == 11{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let controller = rightViewController.topViewController as? RideRequestsListOrChatListViewController{
                    if controller.initialMode == TableMode.Requests.rawValue{
                        self.revealViewController().revealToggleAnimated(true)
                    }
                    else{
                        controller.performSelector(Selector("rideRequestsButtonPressed"))
                        self.revealViewController().revealToggleAnimated(true)
                    }
                    
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=RideRequests", sender: self)
                }
                selectedRow = 11
            }
        }
        else if indexPath.row == 13{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let controller = rightViewController.topViewController as? RideRequestsListOrChatListViewController{
                    if controller.initialMode == TableMode.Chats.rawValue{
                        self.revealViewController().revealToggleAnimated(true)
                    }
                    else{
                        controller.performSelector(Selector("chatsButtonPressed"))
                        self.revealViewController().revealToggleAnimated(true)
                    }
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=ChatList", sender: self)
                }
                selectedRow = 13
            }
        }
        else if indexPath.row == 15{
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? SettingsViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealSettingsViewController", sender: self)
                }
                selectedRow = 15
            }
        }
        else if indexPath.row == 17 {
            if let rightViewController = self.revealViewController().frontViewController as? UINavigationController{
                if let _ = rightViewController.topViewController as? AboutUsViewController{
                    self.revealViewController().revealToggleAnimated(true)
                }
                else{
                    performSegueWithIdentifier("sideMenuRevealAboutUsViewController", sender: self)
                }
                selectedRow = 17
            }
        }
        
        self.view.userInteractionEnabled = false
        NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("enableUserInteraction"), userInfo: nil, repeats: false)
    }
    ///method used to enable the user interaction with the view
    func enableUserInteraction(){
        self.view.userInteractionEnabled = true
    }
//    func storeButtonPressed() {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        let iapViewController : InAppPurchasesViewController = storyboard.instantiateViewControllerWithIdentifier("InAppPurchasesViewController") as! InAppPurchasesViewController
//        //iapViewController.delegate = self
//        self.presentViewController(iapViewController, animated: true, completion: nil)
//    }
//    func didBuy(collectionIndex: Int) {
//        //print("bought")
//    }
    ///method that logs the user out of the application and then sends the user to the initial view controller
    func logout(){
        let alert : UIAlertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
        let actionYes : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            currentUser = QBUUser()
            DatabaseHelper().deleteCredentials()
            QBSession.currentSession().endSession()
            if QBChat.instance().isLoggedIn()
            {
                QBChat.instance().logout()
            }
            currentViewController = nil
            currentCarpoolers = []
            let uniqueID : String = UIDevice.currentDevice().identifierForVendor!.description
            QBRequest.unregisterSubscriptionForUniqueDeviceIdentifier(uniqueID, successBlock: { (response : QBResponse!) -> Void in
                UIApplication.sharedApplication().unregisterForRemoteNotifications()
                }) { (error : QBError!) -> Void in
                    UIApplication.sharedApplication().unregisterForRemoteNotifications()
            }
            let appdel = UIApplication.sharedApplication().delegate as! AppDelegate
            appdel.deactivateLocationServicesUpdates()
            globalRevealViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })

            //appdel.resetAppToFirstViewController()
            
        }
        let actionNo : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    override func viewWillDisappear(animated: Bool) {
        
    }
    ///method that sets the theme of the view
    func setTheme(){
        if currentTheme == Theme.PassengerTheme.rawValue{
            
            passengerORdriverSegmentedControl.tintColor = ApplicationThemeColor().WhiteThemeColor
            passengerORdriverSegmentedControl.selectedSegmentIndex = 0
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            self.tableView.separatorColor = ApplicationThemeColor().WhiteThemeColor
            self.tableView.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.tableView.reloadData()
            imageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            mapImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            profileImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            settingsImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            myRouteImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            //pesosImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            favoritesImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            amigosImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            rideRequestsImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            chatImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            mapLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            profileLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            myRouteLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            settingsLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            //pesosLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            favoritesLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            amigosLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            rideRequestsLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            logoutLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            chatLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            aboutUsLabel.textColor = ApplicationThemeColor().WhiteThemeColor
            
            mapSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            profileSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            amigosSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            favoritesSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            //pesosSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            //logoutSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            myRouteSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            chatSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            rideRequestsSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
            aboutUsSeparator.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            self.tableView.separatorColor = ApplicationThemeColor().DarkGreyTextColor
            self.tableView.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.tableView.reloadData()

            passengerORdriverSegmentedControl.selectedSegmentIndex = 1
            passengerORdriverSegmentedControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
            imageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            mapImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            profileImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            settingsImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            myRouteImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            //pesosImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            favoritesImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            amigosImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            rideRequestsImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            chatImageView.tintColor = ApplicationThemeColor().DarkGreyTextColor
            mapLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            profileLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            myRouteLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            settingsLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            //pesosLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            favoritesLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            amigosLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            rideRequestsLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            logoutLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            chatLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            aboutUsLabel.textColor = ApplicationThemeColor().DarkGreyTextColor
            
            mapSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            profileSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            amigosSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            favoritesSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            //pesosSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            //logoutSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            myRouteSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            chatSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            rideRequestsSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            aboutUsSeparator.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
            
        }
    }
    /**
    method that changes the user status segmented control has changed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func userStatusDidChange(sender: UISegmentedControl) {
        changeStateButtonPressed()
    }
    ///instance of the cell that holds the application logo
    @IBOutlet weak var headerCell: UITableViewCell!
    ///method that is run initially to set the state of the user
    func initialRefreshStateButton(){
        QBRequest.userWithID(currentUser.ID, successBlock: { (response : QBResponse!, selectedUser : QBUUser!) -> Void in
            if selectedUser.customData != nil{
                let str : String = selectedUser.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                    if let isDriver = json["isDriver"] as? Bool{
                        if !isDriver{
                            currentTheme = Theme.PassengerTheme.rawValue
                            self.setTheme()
                        }
                        else if isDriver{
                            currentTheme = Theme.DriverTheme.rawValue
                            self.setTheme()
                        }
                    }
                }
            }
            }) { (response : QBResponse!) -> Void in
                
        }
    }
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if currentTheme == Theme.DriverTheme.rawValue{
            cell.backgroundColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
        }
        
    }
    ///method that changes the state of the user from passenger to driver
    func changeStateButtonPressed() {
        
        getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
            if ((getCustomDataDictionaryFromUser(currentUser)!.valueForKey("numberOfSeats") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carRegistrationNumber") as! String).isEmpty || (getCustomDataDictionaryFromUser(currentUser)!.valueForKey("carModel") as! String).isEmpty) && !(getCustomDataDictionaryFromUser(currentUser)?.valueForKey("isDriver") as! Bool)
            {
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Please complete your profile to be able to carpool with passengers!", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (alert : UIAlertAction) -> Void in
                    self.passengerORdriverSegmentedControl.selectedSegmentIndex = 0
                })
                alert.addAction(alertOK)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            ////println("numberofNegotations: " + numberOfNegotiations.description)
            else if numberOfCurrentCarpoolers > 0{
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: "You can't switch between passenger and driver if you have active carpools", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOK: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                alert.addAction(alertOK)
                self.presentViewController(alert, animated: true, completion: nil)
                self.refreshStateButton(currentUser)
            }
                
            else if numberOfNegotiations == 0{
                QBRequest.userWithID(currentUser.ID, successBlock: { (response : QBResponse!, selectedUser : QBUUser!) -> Void in
                    if selectedUser.customData != nil{
                        let str : String = selectedUser.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                        let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                        if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                            if let isDriver = json["isDriver"] as? Bool{
                                
                                json.setValue(!isDriver, forKey: "isDriver")
                                
                                do {
                                    let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(json, options: NSJSONWritingOptions.PrettyPrinted)
                                    let theJSONText = NSString(data: newUserCustomDataJSON,
                                        encoding: NSASCIIStringEncoding)
                                    selectedUser.customData = String(theJSONText!)
                                    let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                                    userParameters.customData = String(theJSONText!)
                                    QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                        currentUser = user
                                        self.refreshStateButton(currentUser)
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                    })
                                } catch let error as NSError {
                                    NSErrorPointer().memory = error
                                } catch {
                                    fatalError()
                                }
                                
                            }
                        }
                    }
                    }) { (response : QBResponse!) -> Void in
                        
                }
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: "You have " + numberOfNegotiations.description + " active requests and switching your user status will cancel them all. Do you want to proceed?", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOK: UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alert : UIAlertAction) -> Void in
                    cancelAllOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (success) -> Void in
                        if success{
                            getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiatio) -> Void in
                                numberOfNegotiations = numberOfNegotiatio
                                self.changeStateButtonPressed()
                            })
                            
                        }
                    })
                })
                let alertNo : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {(alert : UIAlertAction) -> Void in
                    self.refreshStateButton(currentUser)
                })

                alert.addAction(alertOK)
                alert.addAction(alertNo)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })


    }
    /**
    method that refreshes the state of the user sent.
    - Parameter selectedUser: the user which state is to be refreshed
    */
    func refreshStateButton(selectedUser : QBUUser){
            if selectedUser.customData != nil{
                let str : String = selectedUser.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
                    if let isDriver = json["isDriver"] as? Bool{
                        if !isDriver{
                            currentTheme = Theme.PassengerTheme.rawValue
                            self.setTheme()
                        }
                        else if isDriver{
                            currentTheme = Theme.DriverTheme.rawValue
                            self.setTheme()
                        }
                    }
                }
            }
    }
    

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        tableView.reloadData()
        print("preparingforsegue")
        if segue.identifier == "sideMenuRevealRideRequestsListOrChatListViewControllerMode=ChatList"{
            let nav = segue.destinationViewController as! UINavigationController
            let chatListViewController = nav.topViewController as!RideRequestsListOrChatListViewController
            chatListViewController.initialMode = TableMode.Chats.rawValue
            
        }
        else if segue.identifier == "sideMenuRevealRideRequestsListOrChatListViewControllerMode=RideRequests"{
            let nav = segue.destinationViewController as! UINavigationController
            let rideRequestsViewController = nav.topViewController as!RideRequestsListOrChatListViewController
            rideRequestsViewController.initialMode = TableMode.Requests.rawValue
            
        }
    }
    

}
