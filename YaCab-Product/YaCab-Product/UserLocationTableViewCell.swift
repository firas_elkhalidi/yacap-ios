//
//  UserLocationTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/25/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class UserLocationTableViewCell: UITableViewCell {
    @IBOutlet weak var destinationLocationImageView: UIImageView!
    @IBOutlet weak var sourceLocationImageView: UIImageView!
    @IBOutlet weak var destinationLocationNameLabel: UILabel!
    @IBOutlet weak var sourceLocationNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        destinationLocationImageView.image = UIImage(named:"Destination")
        sourceLocationImageView.image = UIImage(named:"Location")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
