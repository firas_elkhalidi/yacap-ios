//
//  TermsAndConditionsViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 11/20/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit
///protocol that is used by the view controllers that present the terms and conditions view controller
protocol TermsAndConditionsDelegate{
    ///delegate method that indicates that the user has accepted the terms and conditions
    func userDidAcceptTermsAndConditions();
}
class TermsAndConditionsViewController: UIViewController {
    ///delegate of the TermsAndConditionsViewController conforms to the TermsAndConditionsDelegate protocol
    var delegate : TermsAndConditionsDelegate?
    ///boolean that indicates that this view controller was presented from the sign up page
    var presentedFromSignUp = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Terms And Conditions"
        if presentedFromSignUp{
            let acceptButton : UIBarButtonItem = UIBarButtonItem(title: "Accept", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("acceptTerms"))
            self.navigationItem.rightBarButtonItem = acceptButton
            let declineButton : UIBarButtonItem = UIBarButtonItem(title: "Decline", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("declineTerms"))
            self.navigationItem.leftBarButtonItem = declineButton
        }
        else{
            let okButton : UIBarButtonItem = UIBarButtonItem(title: "Ok", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("declineTerms"))
            self.navigationItem.leftBarButtonItem = okButton
        }
        self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "TermsAndConditionsViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    ///method that accepts the terms and dismisses the view controller
    func acceptTerms(){
        if delegate != nil{
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                delegate?.userDidAcceptTermsAndConditions()
            })
        }
        else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    ///method that declines the terms and dismisses the view controller
    func declineTerms(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
