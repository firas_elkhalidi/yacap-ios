//
//  FiltersTableViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/21/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
/**
protocol that is used to inform the delegate that the filters did change
*/
protocol FiltersViewControllerDelegate{
    ///method that indicates that the filters did change
    func didChangeFilters()
}
///holds the value of the current table view filter radius
var tableViewFilterRadius : Double = 0
class FiltersTableViewController: UITableViewController, CurrentThemeDelegate {
    ///holds the theme color
    var themeColor : UIColor = ApplicationThemeColor().GreenThemeColor
    ///holds the delegate that will conform to the FiltersViewControllerDelegate protocol
    var delegate : FiltersViewControllerDelegate?
    ///holds the local value of the smoker filter
    var smokerFilterLocal : String?
    ///holds the local value of the air conditioning filter
    var acFilterLocal : String?
    ///holds the local value of the gender filter
    var genderFilterLocal : String?
    ///delegate method of the CurrentThemeDelegate that indicates that the theme did change
    func themeDidChange() {
        setTheme()
    }
    
    override func viewDidAppear(animated: Bool) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "FiltersTableViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // register new tableviewcell
        let nib = UINib(nibName: "FiltersRadiusTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifierRadius")
        //
        //currentThemeDelegate = self
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.sectionHeaderHeight = 60
        self.tableView.sectionFooterHeight = 0
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        let cancelButton : UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("dismissSelf"))
        let doneButton : UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("doneButtonPressed"))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        
        self.navigationController?.navigationBar.translucent = false
        
        setTheme()
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 51
        }
        else{
            return 44
        }
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().DarkGreyTextColor
            self.navigationController?.navigationBar.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().DarkGreyTextColor]
            self.navigationController?.title = "SEARCH FILTERS"
            tableView.tintColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().WhiteThemeColor
            self.navigationController?.navigationBar.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().WhiteThemeColor]
            self.navigationController?.title = "SEARCH FILTERS"
            tableView.tintColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().GreenThemeColor
        }
    }
    ///method that indicates that the done button was pressed
    func doneButtonPressed(){
        delegate?.didChangeFilters()
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    ///method that dismisses the filters table view controller
    func dismissSelf(){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        var numOfFilters = 0
        if smokerFilterLocal != nil{
            numOfFilters++
        }
        if acFilterLocal != nil {
            numOfFilters++
        }
        if genderFilterLocal != nil{
            numOfFilters++
        }
        return numOfFilters + 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if section == 3{
            return 1
        }
        return 3
    }
//
//    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0{
//            return "GENDER PREFERENCE"
//        }
//        else if section == 1{
//            return "SMOKING PREFERENE"
//        }
//        else if section == 2{
//            return "AIR CONDITIONING"
//        }
//        return ""
//    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenSize = UIScreen.mainScreen().bounds
        let view : UIView = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: screenSize.width, height: 60)))
        let separator : UIView = UIView(frame: CGRect(origin: CGPoint(x: 10, y: 59), size: CGSize(width: screenSize.width - 10, height: 1)))
        separator.backgroundColor = ApplicationThemeColor().LightGreyTextColor
        view.addSubview(separator)
        let label : UILabel = UILabel(frame: CGRect(origin: CGPoint(x: 10, y: 30), size: CGSize(width: screenSize.width, height: 21)))
        label.textColor = ApplicationThemeColor().LightGreyTextColor
        if section == 0{
            label.text = "GENDER PREFERENCE"
        }
        else if section == 1{
            label.text = "SMOKING PREFERENCE"
        }
        else if section == 2{
            label.text = "AIR CONDITIONING"
        }
        else if section == 3 {
            label.text = "FILTER RADIUS"
        }
        view.addSubview(label)
        label.font = UIFont.boldSystemFontOfSize(14.0)
        return view
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section != 3{
            let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.Default
            cell.accessoryType = UITableViewCellAccessoryType.None
            if indexPath.section == 0{
                if indexPath.row == 0{
                    if genderFilterLocal == GenderStates.Male.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Male"
                }
                else if indexPath.row == 1{
                    if genderFilterLocal == GenderStates.Female.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Female"
                    
                }
                else if indexPath.row == 2{
                    if genderFilterLocal == GenderStates.DontCareGender.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Doesn't Matter"
                    
                }
            }
            else if indexPath.section == 1{
                if indexPath.row == 0{
                    if smokerFilterLocal == SmokerStates.IsSmoker.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Smoking"
                    
                }
                else if indexPath.row == 1{
                    if smokerFilterLocal == SmokerStates.NotSmoker.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Non-Smoking"
                    
                }
                else if indexPath.row == 2{
                    if smokerFilterLocal == SmokerStates.DontCareSmoker.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Doesn't Matter"
                    
                }
            }
            else if indexPath.section == 2{
                if indexPath.row == 0{
                    if acFilterLocal == ACStates.AC.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "A/C"
                    
                }
                else if indexPath.row == 1{
                    if acFilterLocal == ACStates.NoAC.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "No AC"
                    
                }
                else if indexPath.row == 2{
                    if acFilterLocal == ACStates.DontCareAC.rawValue{
                        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                    }
                    cell.textLabel?.text = "Doesn't Matter"
                }
            }
            
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifierRadius", forIndexPath: indexPath) as! FiltersRadiusTableViewCell
            if currentTheme == Theme.PassengerTheme.rawValue{
                cell.slider.tintColor = ApplicationThemeColor().GreenThemeColor
            }
            else{
                cell.slider.tintColor = ApplicationThemeColor().YellowThemeColor
            }
            return cell
        }
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0{
            if indexPath.row == 0{
                genderFilterLocal = GenderStates.Male.rawValue
            }
            else if indexPath.row == 1{
                genderFilterLocal = GenderStates.Female.rawValue
            }
            else if indexPath.row == 2{
                genderFilterLocal = GenderStates.DontCareGender.rawValue
            }
        }
        else if indexPath.section == 1{
            if indexPath.row == 0{
                smokerFilterLocal = SmokerStates.IsSmoker.rawValue
            }
            else if indexPath.row == 1{
                smokerFilterLocal = SmokerStates.NotSmoker.rawValue
            }
            else if indexPath.row == 2{
                smokerFilterLocal = SmokerStates.DontCareSmoker.rawValue
            }
        }
        else if indexPath.section == 2{
            if indexPath.row == 0{
                acFilterLocal = ACStates.AC.rawValue
            }
            else if indexPath.row == 1{
                acFilterLocal = ACStates.NoAC.rawValue
            }
            else if indexPath.row == 2{
                acFilterLocal = ACStates.DontCareAC.rawValue
            }
        }
        tableView.reloadData()
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
