//
//  RideRequestsViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/24/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
///extension to the date that is used to compare dates that is used to sort the chats and negotiations according to the date of the last message received
extension NSDate
{
    /**
    comparator method that is used to compare if a date is greater than another
    - Parameter dateToCompare: the date in question
    - Returns: a boolean indicating whether it is true or not
    */
    func isGreaterThanDate(dateToCompare : NSDate) -> Bool
    {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    /**
    comparator method that is used to compare if a date is less than another
    - Parameter dateToCompare: the date in question
    - Returns: a boolean indicating whether it is true or not
    */
    func isLessThanDate(dateToCompare : NSDate) -> Bool
    {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    
//    func isEqualToDate(dateToCompare : NSDate) -> Bool
//    {
//        //Declare Variables
//        var isEqualTo = false
//        
//        //Compare Values
//        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame
//        {
//            isEqualTo = true
//        }
//        
//        //Return Result
//        return isEqualTo
//    }
//    
    
    /**
    method that is used to add days to a date
    - Parameter daysToAdd: the days that are to be added to the date
    - Returns: the new date after adding the days
    */
    func addDays(daysToAdd : Int) -> NSDate
    {
        let secondsInDays : NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded : NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    /**
    method that is used to add hours to a date
    - Parameter hoursToAdd: the hours that are to be added to the date
    - Returns: the new date after adding the hours
    */
    func addHours(hoursToAdd : Int) -> NSDate
    {
        let secondsInHours : NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded : NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
class RideRequestsViewController: UIViewController, CurrentThemeDelegate, UITableViewDelegate, UITableViewDataSource, QBChatDelegate,CurrentNumberOfNegotiationsDelegate{
    ///label that is used to indicate that the list of requests is loading
    var placeholder : UILabel = UILabel()
    ///delegate the conforms to the RideRequestsViewControllerDelegate protocol
    var delegate : RideRequestsViewControllerDelegate?
    ///delegate method of the CurrentNumberOfNegotiationsDelegate that indicates that the number of negotiations did change
    func currentNumberOfNegotiationsDidChange() {
        //print("currentNumberofnegschanged")
        getData()
    }
    ///holds the array of current negotiations of the user
    var requests : [QBCOCustomObject] = []
    ///holds the currently selected row user
    var selectedRowUser : QBUUser?
    ///refresh control that is placed above the ride requests table view
    var refreshControl = UIRefreshControl()
    ///instance of the table view that holds the ride requests
    @IBOutlet weak var tableView: UITableView!
    ///hamburger button that opens the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placeholder.hidden = requests.count > 0
        return requests.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! RideRequestsTableViewCell
        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
        
        let now : NSDate = NSDate()
        extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
        extendedRequest["sort_desc"] = "date_sent"
        extendedRequest["limit"] = 50;

        if requests[indexPath.row].userID == DatabaseHelper().FetchUser().ID{
            if _userCache!.objectForKey((requests[indexPath.row].fields.valueForKey("opponentID") as! UInt).description) != nil{
                let user = _userCache!.objectForKey((requests[indexPath.row].fields.valueForKey("opponentID") as! UInt).description) as! QBUUser
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            }
            QBRequest.userWithID(requests[indexPath.row].fields.valueForKey("opponentID") as! UInt, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                _userCache!.setObject(user, forKey: user.ID.description)
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
                
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        else {
            if _userCache!.objectForKey(requests[indexPath.row].userID.description) != nil{
                let user = _userCache!.objectForKey(requests[indexPath.row].userID.description) as! QBUUser
                cell.usernameLabel.text = user.login
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
            }
            QBRequest.userWithID(requests[indexPath.row].userID, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                cell.usernameLabel.text = user.login
                _userCache!.setObject(user, forKey: user.ID.description)
                if _imageCache!.objectForKey(user.ID.description) != nil {
                    cell.userProfileImageView.image = UIImage(data: _imageCache!.objectForKey(user.ID.description) as! NSData)
                }
                QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                    _imageCache!.setObject(data, forKey: user.ID.description)
                    cell.userProfileImageView.image = UIImage(data: data)
                    }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                        
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
                })
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        cell.lastMessageLabel.text = requests[indexPath.row].fields.valueForKey("lastMessageIsNeg") as? String
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 71
    }
    
    func chatDidReceiveMessage(message: QBChatMessage!) {
        if let isNeg = message.customParameters["isNeg"] as? String{
            //print(message.customParameters["isNeg"] as? Bool)
            if isNeg == "1" || isNeg == "true" {
                var refreshPage = true
                for var i = 0; i < requests.count; i++
                {
                    if requests[i].userID == DatabaseHelper().FetchUser().ID{
                        if (requests[i].fields.valueForKey("opponentID") as? UInt) == message.senderID{
                            requests[i].fields.setValue(message.text, forKey: "lastMessageIsNeg")
                            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! RideRequestsTableViewCell
                            cell.lastMessageLabel.text = message.text
                            requests[i].updatedAt = NSDate()
                            requests.sortInPlace(sorterForRequests)
                            tableView.moveRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                            refreshPage = false
                        }
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue{
                            requests.removeAtIndex(0)
                            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                        }
                    }
                    else{
                        if (requests[i].userID) == message.senderID{
                            requests[i].fields.setValue(message.text, forKey: "lastMessageIsNeg")
                            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! RideRequestsTableViewCell
                            cell.lastMessageLabel.text = message.text
                            requests[i].updatedAt = NSDate()
                            requests.sortInPlace(sorterForRequests)
                            tableView.moveRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0), toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                            refreshPage = false
                        }
                        if (message.customParameters.valueForKey("currentState") as! String == CarpoolStates.FREE.rawValue) || (message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CANCELLED.rawValue){
                            requests.removeAtIndex(0)
                            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
                        }
                    }
                }
                if refreshPage{
                    getData()
                }
            }
        }
    }
    override func viewWillDisappear(animated: Bool) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        currentNumberOfNegotiationsInRideRequestsDelegate = self
        refreshControl.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        QBChat.instance().addDelegate(self)
        // register new tableviewcell
        let nib = UINib(nibName: "RideRequestsTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //

        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.delegate = self
        tableView.dataSource = self
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        setTheme()
        let placeholder : UILabel = UILabel()
        placeholder.font = UIFont.italicSystemFontOfSize(14)
        placeholder.numberOfLines = 1; // Use as many lines as needed.
        placeholder.text = "loading"
        placeholder.textAlignment = NSTextAlignment.Center
        placeholder.textColor = UIColor.lightGrayColor()
        
        placeholder.hidden = true
        self.tableView.addSubview(placeholder)
        
        self.placeholder = placeholder
        //self.placeholder.backgroundColor = UIColor.blackColor()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.placeholder.frame = self.tableView.frame;
    }
    override func viewDidAppear(animated: Bool) {
        //currentViewController = self
        self.view.userInteractionEnabled = false
        getData()
        self.placeholder.frame = self.tableView.frame;
    }
    ///delegate method of the CurrentThemeDelegate that is used to indicate that the theme has changed
    func themeDidChange() {
        setTheme()
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        self.navigationController?.navigationBar.translucent = false
        self.title = "Ride Requests"
        if currentTheme == Theme.DriverTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().YellowThemeColor
            refreshControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
            menuButton.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            refreshControl.backgroundColor = ApplicationThemeColor().GreenThemeColor
            refreshControl.tintColor = ApplicationThemeColor().WhiteThemeColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
            menuButton.tintColor = ApplicationThemeColor().GreenThemeColor
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if requests[indexPath.row].userID == DatabaseHelper().FetchUser().ID{
            self.view.userInteractionEnabled = false
                QBRequest.userWithID(requests[indexPath.row].fields.valueForKey("opponentID") as! UInt, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                    if currentViewController is RideRequestsListOrChatListViewController{
                        self.selectedRowUser = user
                        self.view.userInteractionEnabled = true
                        if self.delegate != nil{
                            self.delegate?.goToThisNeg(self.selectedRowUser!)
                        }
                        
                        //self.performSegueWithIdentifier("segueToNegotiationsViewControllerFromRideRequestsViewController", sender: self)
                    }
                }, errorBlock: { (response : QBResponse!) -> Void in
                
            })
        }
        else {
            self.view.userInteractionEnabled = false
            QBRequest.userWithID(requests[indexPath.row].userID, successBlock: { (response: QBResponse!, user : QBUUser!) -> Void in
                
                if currentViewController is RideRequestsListOrChatListViewController{
                    self.selectedRowUser = user
                    self.view.userInteractionEnabled = true
                    if self.delegate != nil{
                        self.delegate?.goToThisNeg(self.selectedRowUser!)
                    }//self.performSegueWithIdentifier("segueToNegotiationsViewControllerFromRideRequestsViewController", sender: self)
                }

                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
        
    }
    /**
    sorter method that sorts the requests according to the last message received
    - Parameters:
        - this: holds the first request object
        - that: holds the second request object
    - Returns: a boolean that indicates whether (this) is a greater date than (that)
    */
    func sorterForRequests(this:QBCOCustomObject, that:QBCOCustomObject) -> Bool {
        return this.updatedAt.isGreaterThanDate(that.updatedAt)
    }
    ///method that is used to get the requests and place them in the table view
    func getData(){
        placeholder.text = "loading"
        requests = []
        tableView.reloadData()
        
        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
        extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "user_id[or]")
        extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "opponentID[or]")
        extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState[ne]")
        extendedRequest.setValue(true, forKey: "isActive")
        //var responsePage : QBResponsePage = QBResponsePage(limit: 20)
        QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
            for carpool in carpools{
                var append = true
                for request in self.requests{
                    if carpool.ID == request.ID{
                        append = false
                    }
                }
                if append {
                    if let request = carpool as? QBCOCustomObject{
                        self.requests.append(request)
                    }
                }
                
            }
            self.requests.sortInPlace(self.sorterForRequests)
            if numberOfNegotiations != self.requests.count{
                numberOfNegotiations = self.requests.count
            }
            self.tableView.reloadData()
            self.placeholder.text = "You have no ongoing requests"
            self.refreshControl.endRefreshing()
            self.view.userInteractionEnabled = true
            }) { (response : QBResponse!) -> Void in
                self.view.userInteractionEnabled = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueToNegotiationsViewControllerFromRideRequestsViewController"{
            let negotiationsViewController : NegotiationsViewController = segue.destinationViewController as! NegotiationsViewController
            if selectedRowUser != nil{
                negotiationsViewController.opponentUser = selectedRowUser!
            }
            
        }
    }
    

}
