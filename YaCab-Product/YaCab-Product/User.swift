//
//  User.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
///class that is used to store a user
class User {
    ///holds the user's id. Same as the one from QuickBlox
    internal var id : UInt
    ///holds the user's username. Same as the one from QuickBlox
    internal var username : String
    ///holds the user's password. Same as the one from QuickBlox
    internal var password : String
    init (){
        id = UInt()
        password = String()
        username = String()
    }
    init (id: UInt , username : String , password : String){
        self.id = id
        self.username = username
        self.password = password
    }

}