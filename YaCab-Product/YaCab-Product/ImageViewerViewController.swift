//
//  ImageViewerViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/16/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class ImageViewerViewController: UIViewController,UIScrollViewDelegate {
    ///UIImage that is set in the ImageViewerViewController.
    var image = UIImage()
    ///UIScrollView that is used to scroll and zoom.
    @IBOutlet weak var scrollView: UIScrollView!
    ///UIImageView that is set in the scrollView variable.
    @IBOutlet weak var imageView: UIImageView!
    ///method that is called by the tapGestureRecognizer. Dismisses the ViewController.
    func tap(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidAppear(animated: Bool) {
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "ImageViewerViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as! [NSObject : AnyObject])
    }
    override func viewDidLoad() {
        /// UITapGestureRecognizer thats detects a tap on the scrollView variable.
        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tap"))
        self.scrollView.addGestureRecognizer(tapGestureRecognizer)
        super.viewDidLoad()
        imageView.image = image
        scrollView.delegate = self
        //scrollView.zoomScale = 1.0
        //scrollView.z
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        // Do any additional setup after loading the view.
    }
    ///method that sets the theme of the ViewController.
    func setTheme(){
//        if currentTheme == Theme.DriverTheme.rawValue{
//                    doneButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
//        }else{
//                    doneButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
//        }

        
    }
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
