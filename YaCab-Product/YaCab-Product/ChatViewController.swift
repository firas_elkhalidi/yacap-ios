//
//  ChatViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khalidi on 6/15/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,QBChatDelegate, UITextViewDelegate, UserRatingViewControllerDelegate {
    //text
    //var boxView = UIView()
    ///holds the page number of the chat messages.
    var pageNumber = 1
    ///boolean that indicates whether the table view should get new chat message pages or not
    var getNewPage = true
    ///instance of the send button that is used to send the message in the message text view
    @IBOutlet weak var sendButton: UIButton!
    ///holds the height of the current tableview cell being loaded
    var heightForCurrentUITableViewCell : CGFloat = CGFloat()
    ///holds the opponent in the user's chat
    var opponentUser : QBUUser = QBUUser()
    ///instance of the message text view that is used to hold the new message that the user wants to send
    @IBOutlet weak var messageTextField: TNResizableTextView!
    ///array of the current messages in the table view
    var messages : [QBChatMessage] = []
    ///holds the current dialog of the chat
    var currentDialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
    ///holds the current carpool of the chat
    var currentCarpool : QBCOCustomObject?
    ///holds the dictionary of the custom data of the current user
    var currentUserDictionary : NSDictionary = NSDictionary()
    ///boolean indicating whether the chat should be receiving messages currently or not
    var receiveMessages = false
    ///boolean indicating whether the opponent user is typing in the chat or not
    var isTyping = false
    ///label that is shown initially when the view controller is loading in the messages of the dialog
    var placeholder : UILabel = UILabel()
    ///instance of the table view that holds the messages of the dialog
    @IBOutlet weak var tableView: UITableView!
    ///method that is used to set the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            messageTextField.layer.borderColor = ApplicationThemeColor().YellowThemeColor.CGColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            sendButton.setTitleColor(ApplicationThemeColor().YellowThemeColor, forState: UIControlState.Normal)
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor]
        }
        else if currentTheme == Theme.PassengerTheme.rawValue{
            messageTextField.layer.borderColor = ApplicationThemeColor().GreenThemeColor.CGColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            sendButton.setTitleColor(ApplicationThemeColor().GreenThemeColor, forState: UIControlState.Normal)
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor]
        }
    }
    ///holds the old size of the message text view
    var messageTextViewSizeOld : CGRect?
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        messageTextViewSizeOld = textView.frame
        //QBChat.instance().sendUserIsTypingToUserWithID(opponentUser.ID)
        currentDialog.sendUserIsTyping()
        //print("userIsTyping")
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        currentDialog.sendUserStoppedTyping()
        //QBChat.instance().sendUserStopTypingToUserWithID(opponentUser.ID)
    }
    func chatDidReceiveUserIsTypingFromUserWithID(userID: UInt) {
        if userID == opponentUser.ID && self.navigationItem.title != nil && !isTyping{
            isTyping = true
            self.navigationItem.title = self.navigationItem.title! + " is typing"
        }
    }
    func chatDidReceiveUserStopTypingFromUserWithID(userID: UInt) {
        if userID == opponentUser.ID && isTyping && self.navigationItem.title != nil{
            self.navigationItem.title = self.navigationItem.title!.substringWithRange(Range<String.Index>(start: self.navigationItem.title!.startIndex, end: self.navigationItem.title!.endIndex.advancedBy(-10)))
            isTyping = false
        }
    }
    ///delegate method from UserRatingViewControllerDelegate that is used to indicate that the user has dismissed the user rating view controller
    func willDismissUserRating() {
            self.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealMapViewController", sender: self.revealViewController().rearViewController)
        
       // self.dismissSelf()
    }
    func textViewDidChange(textView: UITextView) {
        messageTextField.updateConstraints()
        if messageTextViewSizeOld != nil{
            let heightDifference = messageTextField.frame.height - messageTextViewSizeOld!.height
            tableView.frame = CGRect(origin: tableView.frame.origin, size: CGSize(width: tableView.frame.width, height: tableView.frame.height - heightDifference))
        }
        scrollView.scrollToActiveTextField()
        scrollView.scrollRectToVisible(messageTextField.frame, animated: true)
        //self.tableView.updateConstraints()
    }
    override func viewDidLoad() {
        if self.revealViewController() != nil{
            revealViewController().rightRevealToggleAnimated(true)
        }
        messageTextField.scrollEnabled = true
        messageTextField.showsHorizontalScrollIndicator = false
        messageTextField.showsVerticalScrollIndicator = true
        messageTextField.layer.cornerRadius = 5
        messageTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        messageTextField.layer.borderWidth = 1
        messageTextField.sizeToFit()
        messageTextField.layoutIfNeeded()
        messageTextField.delegate = self
        if pushOpponentUser != nil{
            opponentUser = pushOpponentUser!.copy() as! QBUUser
            pushOpponentUser = nil
        }
        super.viewDidLoad()
        tableView.transform = CGAffineTransformMakeScale(1, -1)
        tableView.registerClass(ChatMessageTableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        tableView.dataSource = self
        tableView.delegate = self

        QBChat.instance().addDelegate(self)
        QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        let placeholder : UILabel = UILabel()
        placeholder.font = UIFont.italicSystemFontOfSize(14)
        placeholder.numberOfLines = 1; // Use as many lines as needed.
        placeholder.text = "loading"
        placeholder.textAlignment = NSTextAlignment.Center
        placeholder.textColor = UIColor.lightGrayColor()
        placeholder.hidden = false
        self.placeholder.frame = self.tableView.frame;
        self.view.addSubview(placeholder)
        //placeholder.transform = CGAffineTransformMakeScale(1, -1)
        self.placeholder = placeholder
        
        QBRequest.userWithID(DatabaseHelper().FetchUser().ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            currentUser = user
            self.currentUserDictionary = getCustomDataDictionaryFromUser(currentUser)!
            self.getMessages()
            }) { (response : QBResponse!) -> Void in
            
        }

        
        
        setTheme()
        
    }
    override func viewWillLayoutSubviews() {
        self.placeholder.frame = self.tableView.frame;
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        //print("viewwillTransition")
//        if messageTextField.isFirstResponder(){
//            wasFirstResponder = true
//            self.view.endEditing(true)
//        }
        tableView.reloadData()
    }
    func handleSwipe(sender : UISwipeGestureRecognizer){
        //AGPushNoteView.close()
        
    }
    func orientationChanged(notification : NSNotification){
//        if wasFirstResponder{
//            self.messageTextField.becomeFirstResponder()
//            wasFirstResponder = false
//        }
        //tableView.reloadData()
    }
    ///method that is used to fetch the last 10 messages in the dialog
    func getMessages(){
//        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
//        boxView.backgroundColor = UIColor.whiteColor()
//        boxView.alpha = 1.0
//        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Conversation"
        self.view.userInteractionEnabled = false
        //boxView.addSubview(activityView)
        //boxView.addSubview(textLabel)
        //self.view.addSubview(boxView)
        
        QBRequest.dialogsWithSuccessBlock({ (response : QBResponse!,dialogs : [AnyObject]!, set : Set<NSObject>!) -> Void in
            self.currentDialog.name = "error"
            for dialog in dialogs{
                if let d = dialog as? QBChatDialog{
                    for userID in d.occupantIDs{
                        if userID as! UInt == self.opponentUser.ID{
                            self.currentDialog = d
                            let fields : NSMutableDictionary = NSMutableDictionary()
                            fields.setValue(self.currentDialog.ID, forKey: "dialogID")
                            fields.setValue(true, forKey: "isActive")
                            QBRequest.objectsWithClassName("Carpool", extendedRequest: fields, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                if carpools.count > 0{
                                    self.currentCarpool = carpools[0] as? QBCOCustomObject
                                    if !(self.currentUserDictionary.valueForKey("isDriver") as! Bool){
                                        let finishCarpoolButton : UIBarButtonItem = UIBarButtonItem(title: "Finish Carpool", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("finishCarpool"))
                                        self.navigationItem.rightBarButtonItem = finishCarpoolButton
                                        
                                    }
                                    
                                    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                                    
                                    let now : NSDate = NSDate()
                                    extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
                                    extendedRequest["sort_desc"] = "date_sent"
                                    extendedRequest["limit"] = 10;
                                    QBRequest.messagesWithDialogID(self.currentDialog.ID, extendedRequest: extendedRequest as [NSObject : AnyObject], forPage: QBResponsePage(limit: 10), successBlock: { (response : QBResponse!, messages : [AnyObject]!,responsePage : QBResponsePage!) -> Void in
                                        for message in messages{
                                            if let mess = message as? QBChatMessage{
                                                //print("messageReceived:")
                                                //print(message)
                                                //print(message.customParameters)
                                                if let isNeg = mess.customParameters["isNeg"] as? String{
                                                    //print("isNeg:")
                                                    //print(mess.customParameters["isNeg"] as? Bool)
                                                    if isNeg == "0" || isNeg == "false" {
                                                        if mess.senderID != currentUser.ID{
                                                            QBChat.instance().readMessage(mess)
                                                        }
                                                        self.messages.append(mess)
                                                    }
                                                }
                                                
                                            }
                                        }
                                        self.messages = Array(self.messages.reverse())
                                        self.tableView.reloadData()
                                        self.receiveMessages = true
                                        self.placeholder.hidden = true
                                        //self.boxView.removeFromSuperview()
                                        self.view.userInteractionEnabled = true
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                                            //dialog.type = QBChatDialogTypePrivate
                                            let selectedUserID : NSMutableArray = NSMutableArray()
                                            selectedUserID.addObject(self.opponentUser.ID)
                                            dialog.occupantIDs = selectedUserID as [AnyObject]
                                            QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                                                self.currentDialog = newDialog
                                                self.placeholder.hidden = true
                                                //self.boxView.removeFromSuperview()
                                                self.view.userInteractionEnabled = true
                                                self.receiveMessages = true
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    self.placeholder.hidden = true
                                                    //self.boxView.removeFromSuperview()
                                                    self.view.userInteractionEnabled = true
                                                    
                                            })
                                    })
                                }
                                else{
                                    self.dismissSelf()
                                }
                                }) { (response : QBResponse!) -> Void in
                                    QBRequest.messagesWithDialogID(self.currentDialog.ID, successBlock: { (response : QBResponse!, messages : [AnyObject]!) -> Void in
                                        for message in messages{
                                            if let mess = message as? QBChatMessage{
                                                if mess.dialogID == self.currentDialog.ID{
                                                    self.messages.append(mess)
                                                }
                                            }
                                        }
                                        self.tableView.reloadData()
                                        self.receiveMessages = true
                                        self.placeholder.hidden = true
                                        //self.boxView.removeFromSuperview()
                                        self.view.userInteractionEnabled = true
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            let dialog : QBChatDialog = QBChatDialog(dialogID: "", type: QBChatDialogType.Private)
                                            let selectedUserID : NSMutableArray = NSMutableArray()
                                            selectedUserID.addObject(self.opponentUser.ID)
                                            dialog.occupantIDs = selectedUserID as [AnyObject]
                                            QBRequest.createDialog(dialog, successBlock: { (response : QBResponse!, newDialog : QBChatDialog!) -> Void in
                                                self.currentDialog = newDialog
                                                self.placeholder.hidden = true
                                                //self.boxView.removeFromSuperview()
                                                self.view.userInteractionEnabled = true
                                                self.receiveMessages = true
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    self.placeholder.hidden = true
                                                    //self.boxView.removeFromSuperview()
                                                    self.view.userInteractionEnabled = true
                                            })
                                    })
                            }
                        }
                    }
                    
                }
            }
            
            }, errorBlock: { (response : QBResponse!) -> Void in
                self.placeholder.hidden = true
                //self.boxView.removeFromSuperview()
                self.view.userInteractionEnabled = true
                
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.navigationItem.title = opponentUser.login
        
        //let value = UIInterfaceOrientation.Portrait.rawValue
        // UIDevice.currentDevice().setValue(value, forKey: "orientation")
        currentViewController = self
        self.placeholder.frame = self.tableView.frame;
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "ChatViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
     
    }
    ///method that is used to finish the carpool related to the current chat dialog
    func finishCarpool(){
        let alert : UIAlertController = UIAlertController(title: "Are You Sure?", message: "Pressing yes will complete the carpool and transfer the funds between you and the other user", preferredStyle: UIAlertControllerStyle.Alert)
        let yesAction : UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (alert : UIAlertAction) -> Void in
        
            if self.currentCarpool != nil{
                self.currentCarpool?.fields.setValue(CarpoolStates.CARPOOL_FINISHED.rawValue, forKey: "currentState")
                self.currentCarpool?.fields.setValue(false, forKey: "isActive")
                self.currentCarpool!.className = "Carpool"
                QBRequest.updateObject(self.currentCarpool, successBlock: { (response : QBResponse!, updatedCarpool : QBCOCustomObject!) -> Void in
                    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                    extendedRequest.setObject(self.opponentUser.ID, forKey: "driverID")
                    extendedRequest.setObject(true, forKey: "isActive")
                    QBRequest.objectsWithClassName("CarpoolRecord", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, records : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                        if records != nil{
                            if records.count != 0{
                                if let record = records[0] as? QBCOCustomObject{
                                    var passengersIDsTemp = record.fields.valueForKey("passengersIDsTemp") as! [UInt]
                                    //var passengersIDs = record.fields.valueForKey("passengersIDs") as! [UInt]
                                    let tempSet = NSMutableSet(array: passengersIDsTemp)
                                    //let set = NSMutableSet(array: passengersIDs)
                                    //set.removeObject(currentUser.ID)
                                    tempSet.removeObject(currentUser.ID)
                                    passengersIDsTemp = NSArray(array: tempSet.allObjects) as! [UInt]
                                    //passengersIDs = NSArray(array: set.allObjects) as! [UInt]
                                    if passengersIDsTemp.count == 0{
                                        record.fields.setObject(false, forKey: "isActive")
                                        record.fields.setObject(NSDate(), forKey: "finishedAt")
                                    }
                                    record.className = "CarpoolRecord"
                                    record.fields.setObject(passengersIDsTemp, forKey: "passengersIDsTemp")
                                    //record.fields.setObject(passengersIDs, forKey: "passengersIDs")
                                    QBRequest.updateObject(record, successBlock: { (response : QBResponse!, newUpdatedRecord : QBCOCustomObject!) -> Void in
                                        
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            
                                    })
                                }
                            }
                        }
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            
                    })
                    let message : QBChatMessage = QBChatMessage()
                    message.text = "blablabla"
                    var recipientID : UInt = 0
                    if updatedCarpool.userID == currentUser.ID{
                        recipientID = updatedCarpool.fields.valueForKey("opponentID") as! UInt
                        message.recipientID = recipientID
                    }
                    else{
                        recipientID = updatedCarpool.userID
                        message.recipientID = recipientID
                    }
                    let params : NSMutableDictionary = NSMutableDictionary()
                    params["save_to_history"] = true
                    params["senderLogin"] = DatabaseHelper().FetchUser().login
                    params["isNeg"] = true
                    params["itemType"] = "3"
                    params["currentState"] = CarpoolStates.CARPOOL_FINISHED.rawValue
                    params["color"] = CellColorStates.GREEN.rawValue
                    if (getCustomDataDictionaryFromUser(currentUser)?.valueForKey("isDriver") as! Bool){
                        params["color"] = CellColorStates.YELLOW.rawValue
                    }
                    
                    message.text = "Carpool Finished!"
                    message.customParameters = params
                    self.currentDialog.sendMessage(message)
                    getCarpoolers()
                    
                    
                    self.currentCarpool = updatedCarpool
                    self.currentCarpool?.className = "Carpool"
                    self.performSegueWithIdentifier("goToUserRatingSegue", sender: self)
                    //self.dismissSelf()
                    
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.dismissSelf()
                })
            }
        }

        alert.addAction(yesAction)
        let noAction : UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(noAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    ///method that is used to clean up and move the navigation to the chat list in the side menu
    func dismissSelf(){
        opponentUser = QBUUser()
        messages = []
        tableView.reloadData()
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
        self.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealMapViewController=ChatList", sender: self.revealViewController().rearViewController)
        
        
    }
    func chatDidReceiveMessage(message: QBChatMessage!) {
        var append = true
        for mess in messages{
            if let messID = mess.ID{
                if let messageID = message.ID{
                    if messID == messageID{
                        append = false
                    }
                }
            }
        }
        if receiveMessages && append && currentViewController is ChatViewController{
            if message.senderID == opponentUser.ID{
                if let isNeg : Bool = message.customParameters.valueForKey("isNeg") as? Bool{
                    if !isNeg{
                        
                        messages.append(message)
                        if message.markable{
                            if message.senderID != currentUser.ID{
                                QBChat.instance().readMessage(message)
                            }
                        }
                        
                        self.tableView.reloadData()
                    }
                    else{
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                            getCarpoolers()
                            self.dismissSelf()
                        }
                    }
                }
                else if let isNeg : Bool = message.customParameters.valueForKey("isNeg") as? Bool{
                    if !isNeg{
                        messages.append(message)
                        if message.markable{
                            if message.senderID != currentUser.ID{
                                QBChat.instance().readMessage(message)
                            }
                        }
                        self.tableView.reloadData()
                    }
                    else{
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                            getCarpoolers()
                            self.dismissSelf()
                        }
                    }
                }
                else if let isNeg : String = message.customParameters.valueForKey("isNeg") as? String{
                    if isNeg == "0" || isNeg == "false"{
                        messages.append(message)
                        if message.markable{
                            if message.markable{
                                if message.senderID != currentUser.ID{
                                    QBChat.instance().readMessage(message)
                                }
                            }
                        }
                        self.tableView.reloadData()
                        
                    }
                    else{
                        if message.customParameters.valueForKey("currentState") as! String == CarpoolStates.CARPOOL_FINISHED.rawValue{
                            getCarpoolers()
                            self.dismissSelf()
                        }
                    }
                }
            }
        }
    }
    /**
    method that is used to send a contact request to the user that has the userID as an id
    - Parameter userID: holds the id of the user that is to receive the contact request
    */
    func sendContactRequest(userID : UInt){
        //doIt = true
        let fields : NSMutableDictionary = NSMutableDictionary()
        fields.setObject(userID, forKey: "receiverID")
        let customObject : QBCOCustomObject = QBCOCustomObject()
        customObject.className = "ContactRequest"
        customObject.fields = fields
        let permissions : QBCOPermissions = QBCOPermissions()
        permissions.deleteAccess = QBCOPermissionsAccessOpen
        permissions.updateAccess = QBCOPermissionsAccessOpen
        customObject.permissions = permissions
        QBRequest.createObject(customObject, successBlock: { (response : QBResponse!, customObject : QBCOCustomObject!) -> Void in
            QBChat.instance().addUserToContactListRequest(opponentUser.ID, sentBlock: { (error : NSError!) -> Void in
                self.dismissSelf()
            })
            }, errorBlock: { (response : QBResponse!) -> Void in
                //print("failedcreatedObject:")
                //print(response.description)
        })
        
    }


    func chatDidNotSendMessage(message: QBChatMessage!, error: NSError!) {
        var k = 0
        for mess in messages{
            if mess.ID == message.ID{
                messages.removeAtIndex(messages.count-1-k)
                self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: messages.count-1-k, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
            k++
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
    }
    ///scroll view that has all the other views inside it
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    /**
    method that detects when the send button is pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func sendButtonPressed(sender: UIButton) {
        messageTextField.text = messageTextField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if !messageTextField.text.isEmpty{
            currentDialog.sendUserStoppedTyping()
            //QBChat.instance().sendUserStopTypingToUserWithID(opponentUser.ID)
            let message : QBChatMessage = QBChatMessage.markableMessage()
            //let message : QBChatMessage = QBChatMessage()
            message.text = messageTextField.text
            messageTextField.text = ""
            messageTextField.updateConstraints()
            message.recipientID = opponentUser.ID
            let params : NSMutableDictionary = NSMutableDictionary()
            message.markable = true
            //params["isMarkable"] = true
            params["save_to_history"] = true
            params["senderLogin"] = DatabaseHelper().FetchUser().login
            params["currentState"] = CarpoolStates.FREE.rawValue
            params["isNeg"] = false
            if let isDriver = self.currentUserDictionary.valueForKey("isDriver") as? Bool{
                if isDriver{
                    params.setValue(CellColorStates.YELLOW.rawValue, forKey: "color")
                }
                else{
                    params.setValue(CellColorStates.GREEN.rawValue, forKey: "color")
                }
            }
            else if let isDriverString = self.currentUserDictionary.valueForKey("isDriver") as? String{
                if isDriverString == "1" || isDriverString == "true"{
                    params.setValue(CellColorStates.YELLOW.rawValue, forKey: "color")
                }
                else{
                    params.setValue(CellColorStates.GREEN.rawValue, forKey: "color")
                }
            }
            message.customParameters = params
            currentCarpool?.className = "Carpool"
            
            currentCarpool!.fields.setValue(message.text, forKey: "lastMessageNotIsNeg")
            //print("currentCarpool:" + message.text)
            
            //print(currentCarpool)
            QBRequest.updateObject(currentCarpool, successBlock: { (response : QBResponse!, newCarpool : QBCOCustomObject!) -> Void in
                self.currentCarpool = newCarpool
                //print("carpoolUpdated")
                }, errorBlock: { (response : QBResponse!) -> Void in
                
            })
            currentDialog.sendMessage(message)
            
            self.messages.append(message)
            self.tableView.reloadData()
        }
        else{
            messageTextField.text = ""
            messageTextField.updateConstraints()
        }
    }
    func chatDidNotSendMessage(message: QBChatMessage!, toDialogID dialogID: String!, error: NSError!) {
            let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Message was not sent please check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
            
            UIView.animateWithDuration(1.5, animations: { () -> Void in
                self.presentViewController(alert, animated: true, completion: { () -> Void in
                    UIView.animateWithDuration(1.5, animations: { () -> Void in
                        alert.dismissViewControllerAnimated(true, completion: { () -> Void in
                            
                        })
                    })
                })
            })
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentViewController is ChatViewController{
            return messages.count
        }
        else {
            return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as? ChatMessageTableViewCell{
            //cell = ChatMessageTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "reuseIdentifier")
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.configureCellWithMessage(messages[messages.count-1-indexPath.row], opponentLogin: opponentUser)
            heightForCurrentUITableViewCell = cell.messageTextView.frame.height
            self.tableView.addConstraint(NSLayoutConstraint(item: cell.messageTextView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: cell, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))
            
            cell.transform = CGAffineTransformMakeScale(1, -1)
            //print(messages[messages.count-1-indexPath.row].customParameters["color"] as? String)
            if messages[messages.count-1-indexPath.row].customParameters["color"] != nil{
                
                
                if let colorState: AnyObject = messages[messages.count-1-indexPath.row].customParameters["color"]{
                    if colorState as! String == CellColorStates.YELLOW.rawValue{
                        cell.backgroundImageView?.image = cell.backgroundImageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                        cell.backgroundImageView?.tintColor = ApplicationThemeColor().YellowThemeColor
                        cell.messageTextView.textColor = ApplicationThemeColor().DarkGreyTextColor
                    }
                    else if colorState as! String == CellColorStates.GREEN.rawValue{
                        cell.backgroundImageView?.image = cell.backgroundImageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                        cell.backgroundImageView?.tintColor = ApplicationThemeColor().GreenThemeColor
                        cell.messageTextView.textColor = ApplicationThemeColor().WhiteThemeColor
                    }
                }
            }
            return cell
        }
        
        return ChatMessageTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "reuseIdentifier")
    }
    func chatDidReadMessageWithID(messageID: String!) {
        var k = 0
        for message in messages{
            if message.ID == messageID{
                if messages.count >= 1{
                    message.read = true
                    tableView.reloadData()
                    if let cell : ChatMessageTableViewCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: messages.count-1-k, inSection: 0)) as? ChatMessageTableViewCell{
                        cell.configureCellWithMessage(message, opponentLogin: opponentUser)
                    }
                    
                }

            }
            k++
        }
    }
    func chatDidDeliverMessageWithID(messageID: String!) {
        var k = 0
        for message in messages{
            if message.ID == messageID{
                if messages.count >= 1{
                    tableView.reloadData()
                    message.deliveredIDs = [opponentUser.ID]
                    if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: messages.count-1-k, inSection: 0)) as? ChatMessageTableViewCell{
                        cell.configureCellWithMessage(message, opponentLogin: opponentUser)
                    }
                    
                }
            }
            k++
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.view.endEditing(true)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        //+30
        return heightForCurrentUITableViewCell + 30
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == messages.count-1 {
            print(indexPath.row.description + "=" + (messages.count-1).description)
            if getNewPage{
                getNewPage = false
                if currentDialog.ID != nil{
                    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                    
                    let now : NSDate = NSDate()
                    extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
                    extendedRequest["sort_desc"] = "date_sent"
                    extendedRequest["limit"] = 10;
                    QBRequest.messagesWithDialogID(currentDialog.ID, extendedRequest: extendedRequest as [NSObject : AnyObject], forPage: QBResponsePage(limit: 10, skip: pageNumber * 10), successBlock: { (response : QBResponse!, ms : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                        //print("got more messages")
                        if ms.count != 0{
                            self.getNewPage = true
                            self.pageNumber++
                        }
                        var newMessages : [QBChatMessage] = []
                        //let currentMessageCount = self.messages.count
                        for message in (ms as! [QBChatMessage]){
                            if let isNeg = message.customParameters["isNeg"] as? String{
                                if isNeg == "0" || isNeg == "false" {
                                    if let _ = self.messages.indexOf(message){
                                        
                                    }
                                    else{
                                        if message.senderID != currentUser.ID{
                                            QBChat.instance().readMessage(message)
                                        }
                                        newMessages.append(message)
                                    }
                                    
                                }
                            }
                        }
                        //newMessages = Array(newMessages.reverse())
                        
                        //tableView.beginUpdates()
                        self.messages = self.messages.reverse()
                        self.messages.appendContentsOf(newMessages)
                        self.messages = self.messages.reverse()
                        self.tableView.reloadData()
                        //tableView.endUpdates()
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            self.getNewPage = true
                    })
                }
            }
        }
    }
    

    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
        if segue.identifier == "goToUserRatingSegue"{
            if segue.destinationViewController is UINavigationController{
                if (segue.destinationViewController as! UINavigationController).topViewController is UserRatingViewController{
                    (((segue.destinationViewController as! UINavigationController).topViewController) as! UserRatingViewController).opponentUser = self.opponentUser
                    (((segue.destinationViewController as! UINavigationController).topViewController) as! UserRatingViewController).delegate = self
                    
                }
            }
        }
    // Pass the selected object to the new view controller.
    }
    
    
}
