//
//  EditProfileViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 6/23/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate, UITextFieldDelegate {
    ///scroll view that holds all the elements in the screen
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    ///allows the user to change the gender of the account
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    ///the label for the user's organization
    @IBOutlet weak var organizationLabel: UILabel!
    ///the label for the user's number of seats in the car
    @IBOutlet weak var numberOfSeatsLabel: UILabel!
    ///the label for the registration number of the car
    @IBOutlet weak var carRegistrationNumberLabel: UILabel!
    ///the label for the model of the car
    @IBOutlet weak var carModelLabel: UILabel!
    ///the label that is placed next to the genderSegmentedControl
    @IBOutlet weak var genderLabel: UILabel!
    ///the label indicating whether the user has air conditioning or not in the car
    @IBOutlet weak var acLabel: UILabel!
    ///the label indicating whether the user is a smoker or not
    @IBOutlet weak var smokerLabel: UILabel!
    ///the label that hold's the user's address
    @IBOutlet weak var addressLabel: UILabel!
    ///boolean that indicates whether the user changed the profile picture or not
    var didChangeProfilePicture : Bool = false
    ///boolean that indicates whether the user changed the car's picture or not
    var didChangeCarPicture : Bool = false
    /**
    enum holding the modes of the image picker controller that is used to pick either the car's photo or the profile's photo.
    
    ---
    - Car: indicates that the mode of the image picker is for the car.
    - Profile: indicates that the mode of the image picker is for the profile.
    */
    enum ImagePickerMode : String{
        case Car = "CAR", Profile = "PROFILE"
    }
    ///image view holding the user's profile image
    @IBOutlet weak var profileImageView: UIImageView!
    ///the user that is to be edited
    var selectedUser : QBUUser = QBUUser()
    ///the custom data that is for the user is saved in this dictionary
    var selectedUserCustomDataDictionary : NSDictionary = NSDictionary()
    ///the data for the organizations that are found in the autocomplete textfield is found in this array
    var dataForOrganizations = [MLPAutoCompletionObject]()
    ///this is an autocomplete textfield holding an array of organizations that are received from the server
    @IBOutlet weak var organizationTextField: MLPAutoCompleteTextField!
    ///this a progress bar that indicates the process of saving the newly edited profile
    @IBOutlet weak var progressBar: UIProgressView!
    ///method that is used to get the organizations and place them in the autocomplete textfield
    func getDataForOrganizations(){
        QBRequest.objectsWithClassName("Organization", successBlock: { (response :QBResponse!, organizations : [AnyObject]!) -> Void in
            for organization in organizations as! [QBCOCustomObject]{
                self.dataForOrganizations.append(AutoCompleteCustomObject(organization: organization))
            }
            }) { (response : QBResponse!) -> Void in
                
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == numberOfSeatsTextField{
            let formatter : NSNumberFormatter = NSNumberFormatter()
            let isValid = formatter.numberFromString(numberOfSeatsTextField.text!)
            if isValid == nil{
                textField.text = ""
            }
        }
    }
    /**
    method used to detect when the smoking segmented control's value is changed.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func smokingSegmentedControlChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1{
            selectedUserCustomDataDictionary.setValue(false, forKey: "isSmoker")
        }
        else {
            selectedUserCustomDataDictionary.setValue(true, forKey: "isSmoker")
        }
    }
    /**
    method used to detect when the air conditioning segmented control's value is changed.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func acSegmentedControlChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1{
            selectedUserCustomDataDictionary.setValue(false, forKey: "hasAC")
        }
        else {
            selectedUserCustomDataDictionary.setValue(true, forKey: "hasAC")
        }
    }
    ///holds the mode of the image picker
    var imagePickerMode : String = ""
    ///holds the image of the car of the user
    @IBOutlet weak var carImageView: UIImageView!
    ///holds the text of the address of the user
    @IBOutlet weak var addressTextField: FormTextField!
    ///holds the text of the car model of the user
    @IBOutlet weak var carModelTextField: FormTextField!
    ///holds the text of the car registration number of the user
    @IBOutlet weak var carRegistrationNumberTextField: FormTextField!
    ///holds the text of the number of seats of the user
    @IBOutlet weak var numberOfSeatsTextField: FormTextField!
    ///segmented control for the user to choose whether the car has air conditioning or not
    @IBOutlet weak var acSegmentedControl: UISegmentedControl!
    ///segmented control for the user to choose smoking is allowed or not
    @IBOutlet weak var smokingSegmentedControl: UISegmentedControl!
    ///image picker controller that is used by the user to choose the iages of either the car or the profile
    var imagePickerController = UIImagePickerController()
    /**
    method that is used to update the user's dictionary and add a car image id.
    - Parameter carImageBlobID: it is the id of the image of the car on the quickblox server
    */
    func updateDictionary(carImageBlobID : UInt){
        if carImageBlobID != 0{
            if let carBlobID = selectedUserCustomDataDictionary.valueForKey("carBlobID") as? UInt{
                QBRequest.deleteBlobWithID(carBlobID, successBlock: { (response : QBResponse!) -> Void in
                    
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        
                })
            }
            selectedUserCustomDataDictionary.setValue(carImageBlobID, forKey: "carBlobID")
        }
        selectedUserCustomDataDictionary.setValue(addressTextField.text, forKey: "address")
        selectedUserCustomDataDictionary.setValue(carModelTextField.text, forKey: "carModel")
        selectedUserCustomDataDictionary.setValue(carRegistrationNumberTextField.text, forKey: "carRegistrationNumber")
        selectedUserCustomDataDictionary.setValue(organizationTextField.text, forKey: "organization")
        selectedUserCustomDataDictionary.setValue(numberOfSeatsTextField.text, forKey: "numberOfSeats")
        if acSegmentedControl.selectedSegmentIndex == 0{
            selectedUserCustomDataDictionary.setValue(true, forKey: "hasAC")
        }
        else {
            selectedUserCustomDataDictionary.setValue(false, forKey: "hasAC")
        }
        if smokingSegmentedControl.selectedSegmentIndex == 0{
            selectedUserCustomDataDictionary.setValue(true, forKey: "isSmoker")
        }
        else {
            selectedUserCustomDataDictionary.setValue(false, forKey: "isSmoker")
        }
        
        let error = NSErrorPointer()
        do {
            let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(selectedUserCustomDataDictionary, options: NSJSONWritingOptions.PrettyPrinted)
            let theJSONText = NSString(data: newUserCustomDataJSON,
                encoding: NSASCIIStringEncoding)
            selectedUser.customData = theJSONText as! String
        } catch let error1 as NSError {
            error.memory = error1
        }
    }
    ///method that sets the values of the changeable items in the view according to values of the user's dictionary
    func updateFields(){
        addressTextField.text = selectedUserCustomDataDictionary.valueForKey("address") as? String
        carModelTextField.text = selectedUserCustomDataDictionary.valueForKey("carModel") as? String
        carRegistrationNumberTextField.text = selectedUserCustomDataDictionary.valueForKey("carRegistrationNumber") as? String
        numberOfSeatsTextField.text = selectedUserCustomDataDictionary.valueForKey("numberOfSeats") as? String
        organizationTextField.text = selectedUserCustomDataDictionary.valueForKey("organization") as? String
        if let hasAC = selectedUserCustomDataDictionary.valueForKey("hasAC") as? Bool{
            if hasAC{
                acSegmentedControl.selectedSegmentIndex = 0
            }
            else {
                acSegmentedControl.selectedSegmentIndex = 1
            }
        }
        if let isSmoker = selectedUserCustomDataDictionary.valueForKey("isSmoker") as? Bool{
            if isSmoker{
                smokingSegmentedControl.selectedSegmentIndex = 0
            }
            else {
                smokingSegmentedControl.selectedSegmentIndex = 1
            }
        }
        if let hasAC = selectedUserCustomDataDictionary.valueForKey("hasAC") as? String{
            if hasAC == "1" || hasAC == "true"{
                acSegmentedControl.selectedSegmentIndex = 0
            }
            else {
                acSegmentedControl.selectedSegmentIndex = 1
            }
        }
        if let isSmoker = selectedUserCustomDataDictionary.valueForKey("isSmoker") as? String{
            if isSmoker == "1" || isSmoker == "true"{
                smokingSegmentedControl.selectedSegmentIndex = 0
            }
            else {
                smokingSegmentedControl.selectedSegmentIndex = 1
            }
        }
        if let gender = selectedUserCustomDataDictionary.valueForKey("genderType") as? String{
            if gender == "m"{
                genderSegmentedControl.selectedSegmentIndex = 0
            }
            else{
                genderSegmentedControl.selectedSegmentIndex = 1
            }
        }
        
        
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, possibleCompletionsForString string: String!) -> [AnyObject]! {
        return dataForOrganizations
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, didSelectAutoCompleteString selectedString: String!, withAutoCompleteObject selectedObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) {
        textField.text = selectedString
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, shouldConfigureCell cell: UITableViewCell!, withAutoCompleteString autocompleteString: String!, withAttributedString boldedString: NSAttributedString!, forAutoCompleteObject autocompleteObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        cell.textLabel?.text = autocompleteString
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
        return true
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, willShowAutoCompleteTableView autoCompleteTableView: UITableView!) {
        ////print("showing autocomplete")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.backgroundColor = ApplicationThemeColor().GreyThemeColor
        numberOfSeatsTextField.delegate = self
        organizationTextField.sortAutoCompleteSuggestionsByClosestMatch = true
        organizationTextField.autoCompleteDataSource = self
        organizationTextField.autoCompleteDelegate = self
        getDataForOrganizations()
        profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 6;
        profileImageView.clipsToBounds = true;
        profileImageView.userInteractionEnabled = true
        profileImageView.contentMode = UIViewContentMode.ScaleAspectFill
        carImageView.layer.cornerRadius = self.carImageView.frame.size.width / 6;
        carImageView.clipsToBounds = true;
        carImageView.contentMode = UIViewContentMode.ScaleAspectFill
        carImageView.userInteractionEnabled = true
        setTheme()
        getProfileImage()

    }
    ///method that is used to get the user's profile image
    func getProfileImage(){
        let profileImageTapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("profilePicturePressed"))
        
        
        QBRequest.userWithID(selectedUser.ID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            self.selectedUser = user
            self.selectedUserCustomDataDictionary = getCustomDataDictionaryFromUser(self.selectedUser)!
            self.updateFields()
            self.progressBar.hidden = false
            QBRequest.downloadFileWithID(UInt(self.selectedUser.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                self.profileImageView.image = UIImage(data: data)
                self.profileImageView.addGestureRecognizer(profileImageTapGestureRecognizer)
                //self.progressBar.hidden = true
                //self.progressBar.setProgress(0, animated: true)
                //let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("done"))
                
                //self.navigationItem.rightBarButtonItem = doneButton
                self.getCarImage()
                }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        self.progressBar.progress = status.percentOfCompletion * 0.5
                    })
                    NSLog("PercentageComplete:" + status.percentOfCompletion.description);
                }, errorBlock: { (response : QBResponse!) -> Void in
                    self.profileImageView.addGestureRecognizer(profileImageTapGestureRecognizer)
                    //self.progressBar.hidden = true
                    //self.progressBar.setProgress(0, animated: true)
                    //let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("done"))
                    self.getCarImage()
                    //self.navigationItem.rightBarButtonItem = doneButton
            })
            
            
            
            
            }) { (response : QBResponse!) -> Void in
                
        }
    }
    ///method that is used to get the user's car image
    func getCarImage(){
        let carImageTapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("carPicturePressed"))
        var blobID : UInt = 0
        if let _ = (getCustomDataDictionaryFromUser(self.selectedUser)?.valueForKey("carBlobID")) as? UInt{
            blobID = (getCustomDataDictionaryFromUser(self.selectedUser)?.valueForKey("carBlobID")) as! UInt
        }
        QBRequest.downloadFileWithID(blobID, successBlock: { (response : QBResponse!, data : NSData!) -> Void in
            self.carImageView.image = UIImage(data: data)
            self.carImageView.addGestureRecognizer(carImageTapGestureRecognizer)
            self.progressBar.hidden = true
            self.progressBar.setProgress(0, animated: true)
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("done"))
            
            self.navigationItem.rightBarButtonItem = doneButton
            }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.progressBar.progress = 0.5 + status.percentOfCompletion * 0.5
                })
                NSLog("PercentageComplete:" + status.percentOfCompletion.description);
            }, errorBlock: { (response : QBResponse!) -> Void in
                self.carImageView.addGestureRecognizer(carImageTapGestureRecognizer)
                self.progressBar.hidden = true
                self.progressBar.setProgress(0, animated: true)
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("done"))
                
                self.navigationItem.rightBarButtonItem = doneButton
        })
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        genderLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        smokerLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        acLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        addressLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        organizationLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        numberOfSeatsLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        carRegistrationNumberLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        carModelLabel.textColor = ApplicationThemeColor().LightGreyTextColor
        addressLabel.text = addressLabel.text?.uppercaseString
        organizationLabel.text = organizationLabel.text?.uppercaseString
        numberOfSeatsLabel.text = numberOfSeatsLabel.text?.uppercaseString
        carRegistrationNumberLabel.text = carRegistrationNumberLabel.text?.uppercaseString
        carModelLabel.text = carModelLabel.text?.uppercaseString
        if currentTheme == Theme.PassengerTheme.rawValue{
            
            genderLabel.textColor = ApplicationThemeColor().GreenThemeColor
            smokerLabel.textColor = ApplicationThemeColor().GreenThemeColor
            acLabel.textColor = ApplicationThemeColor().GreenThemeColor
            addressLabel.textColor = ApplicationThemeColor().GreenThemeColor
            organizationLabel.textColor = ApplicationThemeColor().GreenThemeColor
            numberOfSeatsLabel.textColor = ApplicationThemeColor().GreenThemeColor
            carRegistrationNumberLabel.textColor = ApplicationThemeColor().GreenThemeColor
            carModelLabel.textColor = ApplicationThemeColor().GreenThemeColor
            
            
            progressBar.tintColor = ApplicationThemeColor().GreenThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            genderSegmentedControl.tintColor = ApplicationThemeColor().GreenThemeColor
            smokingSegmentedControl.tintColor = ApplicationThemeColor().GreenThemeColor
            acSegmentedControl.tintColor = ApplicationThemeColor().GreenThemeColor
            carModelTextField.attributedPlaceholder = NSAttributedString(string: "Car Model", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
            carRegistrationNumberTextField.attributedPlaceholder = NSAttributedString(string: "Car Registration Number", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
            numberOfSeatsTextField.attributedPlaceholder = NSAttributedString(string: "Number Of Seats", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
            addressTextField.attributedPlaceholder = NSAttributedString(string: "Address", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
            organizationTextField.attributedPlaceholder = NSAttributedString(string: "Organization", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
            
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            
            genderLabel.textColor = ApplicationThemeColor().YellowThemeColor
            smokerLabel.textColor = ApplicationThemeColor().YellowThemeColor
            acLabel.textColor = ApplicationThemeColor().YellowThemeColor
            addressLabel.textColor = ApplicationThemeColor().YellowThemeColor
            organizationLabel.textColor = ApplicationThemeColor().YellowThemeColor
            numberOfSeatsLabel.textColor = ApplicationThemeColor().YellowThemeColor
            carRegistrationNumberLabel.textColor = ApplicationThemeColor().YellowThemeColor
            carModelLabel.textColor = ApplicationThemeColor().YellowThemeColor
            
            
            progressBar.tintColor = ApplicationThemeColor().YellowThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            genderSegmentedControl.tintColor = ApplicationThemeColor().YellowThemeColor
            smokingSegmentedControl.tintColor = ApplicationThemeColor().YellowThemeColor
            acSegmentedControl.tintColor = ApplicationThemeColor().YellowThemeColor
            carModelTextField.attributedPlaceholder = NSAttributedString(string: "Car Model", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor])
            carRegistrationNumberTextField.attributedPlaceholder = NSAttributedString(string: "Car Registration Number", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor])
            numberOfSeatsTextField.attributedPlaceholder = NSAttributedString(string: "Number Of Seats", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor])
            addressTextField.attributedPlaceholder = NSAttributedString(string: "Address", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor])
            organizationTextField.attributedPlaceholder = NSAttributedString(string: "Organization", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().YellowThemeColor])
        }
    }
    /**
    method that is used to detect when the genderSegmentedControl value has changed.
    - Parameter sender: holds thr sender of the method.
    */
    @IBAction func genderSegmentedControlChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1{
            selectedUserCustomDataDictionary.setValue("f", forKey: "genderType")
        }
        else {
            selectedUserCustomDataDictionary.setValue("m", forKey: "genderType")
        }
    }
    ///method called by the tap gesture recognizer placed on the profile picture
    func profilePicturePressed() {
        //print("here")
    
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            imagePickerController.delegate = self
            self.imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePickerController.allowsEditing = true
            //imagePickerController.
            imagePickerMode = ImagePickerMode.Profile.rawValue
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
    }
    ///method called by the tap gesture recognizer placed on the car picture
    func carPicturePressed(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            imagePickerController.delegate = self
            self.imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePickerController.allowsEditing = true
            imagePickerMode = ImagePickerMode.Car.rawValue
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        if imagePickerMode == ImagePickerMode.Profile.rawValue{
            profileImageView.image = image
            var compression : CGFloat = 0.9
            let maxCompression : CGFloat = 0.1
            let maxFileSize : Int = 250*1024;
            
            var imageData : NSData = UIImageJPEGRepresentation(image, compression)!
            while (imageData.length > maxFileSize && compression > maxCompression)
            {
                compression -= 0.1;
                imageData = UIImageJPEGRepresentation(image, compression)!;
            }
            
            //profileImageView.image = UIImage(data: imageData)
            picker.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.didChangeProfilePicture = true
            })
        }
        else{
            carImageView.image = image
            var compression : CGFloat = 0.9
            let maxCompression : CGFloat = 0.1
            let maxFileSize : Int = 250*1024;
            
            var imageData : NSData = UIImageJPEGRepresentation(image, compression)!
            while (imageData.length > maxFileSize && compression > maxCompression)
            {
                compression -= 0.1;
                imageData = UIImageJPEGRepresentation(image, compression)!;
            }
            
            
            picker.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.didChangeCarPicture = true
            })
        }

        
    }
    ///method that returns an Int from an UInt
    func returnSignedInt(unsignedInteger : UInt) -> Int{
        return Int(unsignedInteger)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "EditProfileViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as? [NSObject : AnyObject])
    }
    ///method called when the done button is pressed.
    func done(){
        let formatter : NSNumberFormatter = NSNumberFormatter()
        let isValid = formatter.numberFromString(numberOfSeatsTextField.text!)
        if isValid != nil{
            if let seats = Int(numberOfSeatsTextField.text!){
                if seats >= 0{
                    getNumberOfOngoingNegotiationsForUserWithUserID(selectedUser.ID) { (numberOfNegotiation) -> Void in
                        numberOfNegotiations = numberOfNegotiation
                        getNumberOfCarpoolersForUserWithID(self.selectedUser.ID) { (numberOfCarpools) -> Void in
                            numberOfCurrentCarpoolers = numberOfCarpools
                            
                            if numberOfNegotiations == 0 && numberOfCurrentCarpoolers == 0{
                                self.view.userInteractionEnabled = false
                                var boxView : UIView = UIView()
                                boxView = UIView(frame: CGRect(x: self.view.frame.midX - 90, y: self.view.frame.midY - 25, width: 180, height: 50))
                                boxView.backgroundColor = UIColor.whiteColor()
                                boxView.alpha = 1.0
                                boxView.layer.cornerRadius = 10
                                
                                //Here the spinnier is initialized
                                let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
                                activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                                activityView.startAnimating()
                                let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
                                textLabel.textColor = UIColor.grayColor()
                                textLabel.text = "Saving Profile"
                                self.view.userInteractionEnabled = false
                                boxView.addSubview(activityView)
                                boxView.addSubview(textLabel)
                                self.view.addSubview(boxView)
                                
                                //                    if let image = self.profileImageView.image{
                                let profileImage = self.profileImageView.image
                                var compression : CGFloat = 0.9
                                let maxCompression : CGFloat = 0.1
                                let maxFileSize : Int = 250*1024;
                                var imageData : NSData = UIImageJPEGRepresentation(profileImage!, compression)!
                                while (imageData.length > maxFileSize && compression > maxCompression)
                                {
                                    compression -= 0.1;
                                    imageData = UIImageJPEGRepresentation(profileImage!, compression)!;
                                }
                                self.progressBar.progress = 0
                                let statusBlock : QBRequestStatusUpdateBlock = { (request : QBRequest!, status : QBRequestStatus!) in
                                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                                        self.progressBar.progress = 0.5 * status.percentOfCompletion
                                    })
                                    NSLog(status.percentOfCompletion.description);
                                }
                                self.progressBar.hidden = false
                                if self.didChangeProfilePicture && self.didChangeCarPicture{
                                    QBRequest.TUploadFile(imageData, fileName: "myAvatar", contentType: "image/jpeg", isPublic: true,successBlock:{ (response : QBResponse!, blob : QBCBlob!) -> Void in
                                        
                                        if let blobID = UInt(self.selectedUser.blobID) as? UInt{
                                            QBRequest.deleteBlobWithID(blobID, successBlock: { (response : QBResponse!) -> Void in
                                                
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    
                                            })
                                        }
                                        let carImage = self.carImageView.image
                                        var compression : CGFloat = 0.9
                                        let maxCompression : CGFloat = 0.1
                                        let maxFileSize : Int = 250*1024;
                                        var carImageData : NSData = UIImageJPEGRepresentation(carImage!, compression)!
                                        while (carImageData.length > maxFileSize && compression > maxCompression)
                                        {
                                            compression -= 0.1;
                                            carImageData = UIImageJPEGRepresentation(carImage!, compression)!;
                                        }
                                        QBRequest.TUploadFile(carImageData, fileName: "myCar", contentType: "image/jpeg", isPublic: true, successBlock: { (response : QBResponse!, carBlob : QBCBlob!) -> Void in
                                            self.selectedUser.blobID = self.returnSignedInt(blob.ID)
                                            self.updateDictionary(carBlob.ID)
                                            
                                            let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                                            userParameters.blobID = self.returnSignedInt(blob.ID)
                                            userParameters.customData = self.selectedUser.customData
                                            
                                            QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                                boxView.removeFromSuperview()
                                                self.view.userInteractionEnabled = true
                                                self.selectedUser = user
                                                currentUser = user
                                                UIView.animateWithDuration(0.3, animations: { () -> Void in
                                                    self.progressBar.setProgress(1, animated: true)
                                                })
                                                self.progressBar.hidden = true
                                                self.navigationController?.popViewControllerAnimated(true)
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    self.view.userInteractionEnabled = true
                                            })
                                            }, statusBlock: { (request :QBRequest!, status : QBRequestStatus!) -> Void in
                                                self.progressBar.progress = 0.5 + 0.5 * status.percentOfCompletion
                                            }, errorBlock: { (response : QBResponse!) -> Void in
                                                
                                        })
                                        
                                        
                                        },statusBlock: statusBlock) { (request : QBResponse!) -> Void in
                                            self.view.userInteractionEnabled = true
                                    }
                                }
                                else if self.didChangeProfilePicture && !self.didChangeCarPicture{
                                    QBRequest.TUploadFile(imageData, fileName: "myAvatar", contentType: "image/jpeg", isPublic: true,successBlock:{ (response : QBResponse!, blob : QBCBlob!) -> Void in
                                        if let blobID = UInt(self.selectedUser.blobID) as? UInt{
                                            QBRequest.deleteBlobWithID(blobID, successBlock: { (response : QBResponse!) -> Void in
                                                
                                                }, errorBlock: { (response : QBResponse!) -> Void in
                                                    
                                            })
                                        }
                                        self.selectedUser.blobID = self.returnSignedInt(blob.ID)
                                        
                                        self.updateDictionary(0)
                                        
                                        let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                                    
                                        userParameters.blobID = self.returnSignedInt(blob.ID)
                                        userParameters.customData = self.selectedUser.customData
                                        QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                            boxView.removeFromSuperview()
                                            self.view.userInteractionEnabled = true
                                            self.selectedUser = user
                                            currentUser = user
                                            UIView.animateWithDuration(0.3, animations: { () -> Void in
                                                self.progressBar.setProgress(1, animated: true)
                                            })
                                            self.progressBar.hidden = true
                                            self.navigationController?.popViewControllerAnimated(true)
                                            }, errorBlock: { (response : QBResponse!) -> Void in
                                                self.view.userInteractionEnabled = true
                                        })
                                        
                                        
                                        },statusBlock: { (request :QBRequest!, status : QBRequestStatus!) -> Void in
                                            self.progressBar.progress = status.percentOfCompletion
                                        }) { (request : QBResponse!) -> Void in
                                            self.view.userInteractionEnabled = true
                                    }
                                }
                                else if !self.didChangeProfilePicture && self.didChangeCarPicture{
                                    let carImage = self.carImageView.image
                                    var compression : CGFloat = 0.9
                                    let maxCompression : CGFloat = 0.1
                                    let maxFileSize : Int = 250*1024;
                                    var carImageData : NSData = UIImageJPEGRepresentation(carImage!, compression)!
                                    while (carImageData.length > maxFileSize && compression > maxCompression)
                                    {
                                        compression -= 0.1;
                                        carImageData = UIImageJPEGRepresentation(carImage!, compression)!;
                                    }
                                    QBRequest.TUploadFile(carImageData, fileName: "myCar", contentType: "image/jpeg", isPublic: true, successBlock: { (response : QBResponse!, carBlob : QBCBlob!) -> Void in
                                        //                           self.selectedUser.blobID = self.returnSignedInt(blob.ID)
                                        self.updateDictionary(carBlob.ID)
                                        
                                        let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                                        //userParameters.blobID = self.returnSignedInt(blob.ID)
                                        userParameters.customData = self.selectedUser.customData
                                        
                                        QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                            boxView.removeFromSuperview()
                                            self.view.userInteractionEnabled = true
                                            self.selectedUser = user
                                            currentUser = user
                                            UIView.animateWithDuration(0.3, animations: { () -> Void in
                                                self.progressBar.setProgress(1, animated: true)
                                            })
                                            self.progressBar.hidden = true
                                            self.navigationController?.popViewControllerAnimated(true)
                                            }, errorBlock: { (response : QBResponse!) -> Void in
                                                self.view.userInteractionEnabled = true
                                        })
                                        }, statusBlock: { (request :QBRequest!, status : QBRequestStatus!) -> Void in
                                            self.progressBar.progress = status.percentOfCompletion
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            
                                    })
                                }
                                else if !self.didChangeCarPicture && !self.didChangeProfilePicture{
                                    self.updateDictionary(0)
                                    let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
                                    userParameters.customData = self.selectedUser.customData
                                    self.progressBar.progress = 0
                                    self.progressBar.hidden = false
                                    QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                        boxView.removeFromSuperview()
                                        self.view.userInteractionEnabled = true
                                        self.selectedUser = user
                                        currentUser = user
                                        UIView.animateWithDuration(0.3, animations: { () -> Void in
                                            self.progressBar.setProgress(1, animated: true)
                                        })
                                        self.progressBar.hidden = true
                                        self.navigationController?.popViewControllerAnimated(true)
                                        }, errorBlock: { (response : QBResponse!) -> Void in
                                            self.view.userInteractionEnabled = true
                                    })
                                }
                                
                                
                            }
                            else{
                                let alert : UIAlertController = UIAlertController(title: "Alert!", message: "You are not allowed to edit your car info because you have " + numberOfCurrentCarpoolers.description + " ongoing carpools", preferredStyle: UIAlertControllerStyle.Alert)
                                let ok : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(ok)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                        }
                    }
 
                }
                else{
                    let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Please Check That The Number Of Seats Is A Valid Number", preferredStyle: UIAlertControllerStyle.Alert)
                    let ok : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else{
                let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Please Check That The Number Of Seats Is A Valid Number", preferredStyle: UIAlertControllerStyle.Alert)
                let ok : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Alert!", message: "Please Check That The Number Of Seats Is A Valid Number", preferredStyle: UIAlertControllerStyle.Alert)
            let ok : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
