//
//  CustomDataManagerFunctions.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 6/30/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
class CustomDataManagerFunctions {
    func downloadImageWithURL (url : NSURL,   completion: (succeeded : Bool , image : UIImage) ->Void)
    {
        let request : NSMutableURLRequest  = NSMutableURLRequest(URL: url)
        //print(url)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
            {
                (response, data, error) in
                if let _ : UIImage  = UIImage(data: data!) {
                    if (error == nil && data != nil)
                    {
                        let image : UIImage  = UIImage(data: data!)!
                        completion(succeeded: true,image: image)
                    }
                    else{
                        completion(succeeded: false,image: UIImage());
                    }
                }
                else{
                    completion(succeeded: false,image: UIImage());
                }
        }
    }
}