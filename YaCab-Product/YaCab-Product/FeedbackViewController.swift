//
//  FeedbackViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 11/23/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let cancelButton : UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("cancelButtonPressed"))
        let doneButton : UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("doneButtonPressed"))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        setTheme()
        textView.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "FeedbackViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    func cancelButtonPressed(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func setTheme(){
        if currentTheme == Theme.DriverTheme.rawValue{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
        }
        else{
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
        }
    }
    func doneButtonPressed(){
        if textView.text.isEmpty{
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                if currentTheme == Theme.DriverTheme.rawValue{
                    self.textView.backgroundColor = ApplicationThemeColor().YellowThemeColor
                }
                else{
                    self.textView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                }
                }, completion: { (Bool) -> Void in
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.textView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                        }, completion: { (Bool) -> Void in
                            
                    })
                    
            })
        }
        else{
            let feedBackText = textView.text
            let customObject : QBCOCustomObject = QBCOCustomObject()
            customObject.className = "Feedback"
            customObject.fields.setValue(feedBackText, forKey: "feedback")
            QBRequest.createObject(customObject, successBlock: { (response : QBResponse!, newCustomObject : QBCOCustomObject!) -> Void in
                let alert : UIAlertController = UIAlertController(title: "", message: "Thanks For Your Feedback!", preferredStyle: UIAlertControllerStyle.Alert)
                self.presentViewController(alert, animated: true, completion: {() -> Void in
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        
                        }, completion: { (Bool) -> Void in
                            alert.dismissViewControllerAnimated(true, completion: nil)
                            self.cancelButtonPressed()
                    })
                })

                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
