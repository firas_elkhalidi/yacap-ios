//
//  MyRatingViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/9/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class MyRatingsViewController: UIViewController, CurrentThemeDelegate, UITableViewDelegate, UITableViewDataSource {
    ///array that holds all the ratings of the user
    var ratings : [QBCOCustomObject] = []
    ///refresh control that is placed on the table views to allow the user to refresh and find the new ratings
    var refreshControl : UIRefreshControl = UIRefreshControl()
    ///holds the current page number of the ratings in the table view
    var pageNumber = 1
    ///a boolean that indicates whether the table view should get a new page or not
    var getNewPage = true
    ///instance of the table view that holds the ratings
    @IBOutlet weak var tableView: UITableView!
    ///hamburger button that is used to open the side menu
    @IBOutlet weak var menuButton: UIBarButtonItem!
    ///delegate method of the CurrentThemeDelegate protocol that is used to indicate that the theme has changed
    func themeDidChange() {
        setTheme()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratings.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! MyRatingsTableViewCell
        cell.starRating.rating = (ratings[indexPath.row].fields.objectForKey("rating") as? Double)!
        cell.commentTextView.text = ratings[indexPath.row].fields.objectForKey("comment") as? String
        if _userCache!.objectForKey(ratings[indexPath.row].userID.description) != nil{
            let user = _userCache!.objectForKey(ratings[indexPath.row].userID.description) as! QBUUser
            cell.nameLabel.text = user.login
            if _imageCache!.objectForKey(ratings[indexPath.row].userID.description) != nil{
                cell.userImageView.image = UIImage(data: _imageCache!.objectForKey(ratings[indexPath.row].userID.description) as! NSData)
            }
            QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                _imageCache!.setObject(data, forKey: user.ID.description)
                cell.userImageView.image = UIImage(data: data)
                }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                    
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
        }

        
        
        QBRequest.userWithID(ratings[indexPath.row].userID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            
            _userCache!.setObject(user, forKey: user.ID.description)
            if _imageCache!.objectForKey(self.ratings[indexPath.row].userID.description) != nil{
                cell.userImageView.image = UIImage(data: _imageCache!.objectForKey(self.ratings[indexPath.row].userID.description) as! NSData)
            }
            cell.nameLabel.text = user.login
            
            QBRequest.downloadFileWithID(UInt(user.blobID), successBlock: { (response : QBResponse!, data : NSData!) -> Void in
                _imageCache!.setObject(data, forKey: user.ID.description)
                cell.userImageView.image = UIImage(data: data)
                }, statusBlock: { (request : QBRequest!, status : QBRequestStatus!) -> Void in
                    
                }, errorBlock: { (response : QBResponse!) -> Void in
                    
            })
            }) { (response :QBResponse!) -> Void in
                
        }
        return cell
    }
    ///method that is used to set the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.PassengerTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            self.refreshControl.backgroundColor = ApplicationThemeColor().GreenThemeColor
            self.refreshControl.tintColor = ApplicationThemeColor().WhiteThemeColor
            
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            self.refreshControl.backgroundColor = ApplicationThemeColor().YellowThemeColor
            self.refreshControl.tintColor = ApplicationThemeColor().DarkGreyTextColor
        }

    }
    ///method that is used to get the ratings of the users
    func getData(){
        let extendedRequest : NSMutableDictionary = NSMutableDictionary()
        extendedRequest.setObject(currentUser.ID, forKey: "ratedUserID")
        extendedRequest.setObject(10, forKey: "limit")
        QBRequest.objectsWithClassName("UserRating", extendedRequest: extendedRequest, successBlock: { (response :QBResponse!, ratings :[AnyObject]!, page : QBResponsePage!) -> Void in
            
            if ratings != nil{
                //print(ratings)
                self.ratings = ratings as! [QBCOCustomObject]
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
            else{
                self.ratings = []
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
            }) { (response : QBResponse!) -> Void in
                
        }
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        currentThemeDelegate = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRatingsViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    func refresh(){
        pageNumber = 1
        getNewPage = true
        getData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false 
        // register new tableviewcell
        let nib = UINib(nibName: "MyRatingsTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        
        self.refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        tableView.contentInset = UIEdgeInsetsZero;
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if !QBChat.instance().isLoggedIn(){
            QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        }
        setTheme()
        tableView.tableFooterView = UIView(frame: CGRectZero)
        getData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == ratings.count-1 {
            print(indexPath.row.description + "=" + (ratings.count-1).description)
            if getNewPage{
                getNewPage = false
                let extendedRequest : NSMutableDictionary = NSMutableDictionary()
                extendedRequest.setObject(currentUser.ID, forKey: "ratedUserID")
                extendedRequest.setObject(10, forKey: "limit")
                extendedRequest.setObject(pageNumber*10, forKey: "skip")
                QBRequest.objectsWithClassName("UserRating", extendedRequest: extendedRequest, successBlock: { (response :QBResponse!, newRatings :[AnyObject]!, page : QBResponsePage!) -> Void in
                    if newRatings != nil{
                        if newRatings.count != 0{
                            self.getNewPage = true
                            self.pageNumber++
                            self.ratings.appendContentsOf(newRatings as! [QBCOCustomObject])
                            self.refreshControl.endRefreshing()
                            self.tableView.reloadData()
                        }
                    }
                    }) { (response : QBResponse!) -> Void in
                        self.getNewPage = true
                }

                
            }
            
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
