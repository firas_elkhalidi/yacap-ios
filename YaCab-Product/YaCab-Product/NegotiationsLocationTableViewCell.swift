
//
//  NegotiationsLocationTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/19/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class NegotiationsLocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellMovingView: UIView!
    @IBOutlet weak var destinationLocationImageView: UIImageView!
    @IBOutlet weak var sourceLocationImageView: UIImageView!
    @IBOutlet weak var destinationLocationNameLabel: UILabel!
    @IBOutlet weak var sourceLocationNameLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
