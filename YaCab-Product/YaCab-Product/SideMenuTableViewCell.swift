//
//  SideMenuTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/21/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        if highlighted{
            if currentTheme == Theme.PassengerTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().GreenThemeColor
                            if label?.text != "Logout"{
                                label?.frame = CGRect(x: label!.frame.origin.x + 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
                            imageView?.frame = CGRect(x: imageView!.frame.origin.x + 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                        
                        
                    }
                    
                    }, completion: { (Bool) -> Void in
                        
                })
            }
            else if currentTheme == Theme.DriverTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().YellowThemeColor
                            if label?.text != "Logout"{
                                label?.frame = CGRect(x: label!.frame.origin.x + 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                            
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
                            
                            imageView?.frame = CGRect(x: imageView!.frame.origin.x + 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    }, completion: { (Bool) -> Void in
                        
                })
                
            }
            
        }
        else{
            if currentTheme == Theme.PassengerTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    //self.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            //label?.textColor = ApplicationThemeColor().WhiteThemeColor
                            if label?.text != "Logout"{
                                label?.frame = CGRect(x: label!.frame.origin.x - 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                                
                            }
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            //imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
                            imageView?.frame = CGRect(x: imageView!.frame.origin.x - 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    }, completion: { (Bool) -> Void in
                        
                })
            }
            else if currentTheme == Theme.DriverTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            //label?.textColor = ApplicationThemeColor().DarkGreyTextColor
                            if label?.text != "Logout"{
                                label?.frame = CGRect(x: label!.frame.origin.x - 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                            
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            //imageView?.tintColor = ApplicationThemeColor().DarkGreyTextColor
                            imageView?.frame = CGRect(x: imageView!.frame.origin.x - 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    //self.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }, completion: { (Bool) -> Void in
                        
                })
                
            }
            
        }
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            if currentTheme == Theme.PassengerTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().GreenThemeColor
                            if label?.text != "Logout"{
                                //label?.frame = CGRect(x: label!.frame.origin.x + 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().GreenThemeColor
                            //imageView?.frame = CGRect(x: imageView!.frame.origin.x + 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                        
                        
                    }
                    
                    }, completion: { (Bool) -> Void in
                        
                })
            }
            else if currentTheme == Theme.DriverTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.backgroundColor = ApplicationThemeColor().DarkGreyTextColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().YellowThemeColor
                            if label?.text != "Logout"{
                                //label?.frame = CGRect(x: label!.frame.origin.x + 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                            
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().YellowThemeColor
                            
                            //imageView?.frame = CGRect(x: imageView!.frame.origin.x + 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    }, completion: { (Bool) -> Void in
                        
                })
                
            }
            
        }
        else{
            if currentTheme == Theme.PassengerTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().WhiteThemeColor
                            if label?.text != "Logout"{
                                //label?.frame = CGRect(x: label!.frame.origin.x - 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                                
                            }
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().WhiteThemeColor
                            //imageView?.frame = CGRect(x: imageView!.frame.origin.x - 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    }, completion: { (Bool) -> Void in
                        
                })
            }
            else if currentTheme == Theme.DriverTheme.rawValue{
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    for view in self.contentView.subviews{
                        if let label : UILabel? = view as? UILabel{
                            label?.textColor = ApplicationThemeColor().DarkGreyTextColor
                            if label?.text != "Logout"{
                                //label?.frame = CGRect(x: label!.frame.origin.x - 20, y: label!.frame.origin.y, width: label!.frame.width, height: label!.frame.height)
                            }
                            
                        }
                        if let imageView : UIImageView? = view as? UIImageView{
                            imageView?.tintColor = ApplicationThemeColor().DarkGreyTextColor
                            //imageView?.frame = CGRect(x: imageView!.frame.origin.x - 20, y: imageView!.frame.origin.y, width: imageView!.frame.width, height: imageView!.frame.height)
                            
                        }
                    }
                    self.backgroundColor = ApplicationThemeColor().YellowThemeColor
                    }, completion: { (Bool) -> Void in
                        
                })
                
            }
            
        }
        // Configure the view for the selected state
    }
}
