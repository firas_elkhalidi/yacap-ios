//
//  NegotiationsTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class NegotiationsTableViewCell: UITableViewCell {
    @IBOutlet weak var cellMovingView: UIView!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
