//
//  CustomAnnotation.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/12/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
class MapPin: MKAnnotationView {
    class var reuseIdentifier:String {
        return "mapPin"
    }
    
    var calloutView:MapPinCallout?
    private var hitOutside:Bool = true
    //var userGeoData : QBLGeoData?
    var preventDeselection:Bool {
        return !hitOutside
    }
    
    
    convenience init(annotation:MKAnnotation!) {
        self.init(annotation: annotation, reuseIdentifier: MapPin.reuseIdentifier)
        
        canShowCallout = false;
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        let calloutViewAdded = calloutView?.superview != nil
        
        if (selected || !selected && hitOutside) {
            super.setSelected(selected, animated: animated)
        }
        
        self.superview?.bringSubviewToFront(self)
        
        if (calloutView == nil) {
            calloutView = MapPinCallout()
        }
        
        if (self.selected && !calloutViewAdded) {
            addSubview(calloutView!)
        }
        
        if (!self.selected) {
            calloutView?.removeFromSuperview()
        }
    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        var hitView = super.hitTest(point, withEvent: event)
        
        if let callout = calloutView {
            if (hitView == nil && self.selected) {
                hitView = callout.hitTest(point, withEvent: event)
            }
        }
        
        hitOutside = hitView == nil

        return hitView;
    }
}


class MapPinCallout: UIView {
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let viewPoint = superview?.convertPoint(point, toView: self) ?? point
        let view = super.hitTest(viewPoint, withEvent: event)
        return view
    }
    
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        return CGRectContainsPoint(bounds, point)
    }
}