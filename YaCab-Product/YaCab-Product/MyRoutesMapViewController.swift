//
//  MyRoutesMapViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/3/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AddressBookUI
/**
method that zooms to the polyline in the map. Can be animated or sudden.
- Parameters:
    - map: it is the instance of the map sent
    - polyline: it is the polyline that we wish to zoom to
    - animated: boolean that sets whether it is animated or not
*/
func zoomToPolyLine(map : MKMapView, polyline : MKPolyline, animated : Bool){
    map.setVisibleMapRect(polyline.boundingMapRect, edgePadding: UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30), animated: animated)
}
class MyRoutesMapViewController: UIViewController, UITextFieldDelegate, ChooseLocationDelegate, MKMapViewDelegate{
    
    ///declaration of the map.
    @IBOutlet weak var map: MKMapView!
    ///from UITextField.
    @IBOutlet weak var fromTextField: UITextField!
    ///to UITextField.
    @IBOutlet weak var toTextField: UITextField!
    ///method that sets the theme of the ViewController.
    func setTheme(){
        fromTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
        
        toTextField.backgroundColor = ApplicationThemeColor().LightGreytextFieldBackgroundColor
        
        if currentTheme == Theme.PassengerTheme.rawValue{
            fromTextField.attributedPlaceholder = NSAttributedString(string:"Start Location",
                attributes:[NSForegroundColorAttributeName: ApplicationThemeColor().GreenThemeColor])
            toTextField.attributedPlaceholder = NSAttributedString(string:"Destination",
                attributes:[NSForegroundColorAttributeName: ApplicationThemeColor().GreenThemeColor])
            backgroundView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().WhiteThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            fromTextField.textColor = ApplicationThemeColor().GreenThemeColor
            toTextField.textColor = ApplicationThemeColor().GreenThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            fromTextField.attributedPlaceholder = NSAttributedString(string:"Start Location",
                attributes:[NSForegroundColorAttributeName: ApplicationThemeColor().YellowThemeColor])
            toTextField.attributedPlaceholder = NSAttributedString(string:"Destination",
                attributes:[NSForegroundColorAttributeName: ApplicationThemeColor().YellowThemeColor])
            backgroundView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
            self.navigationController?.navigationBar.barTintColor = ApplicationThemeColor().WhiteThemeColor
            self.navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            //self.navigationItem.rightBarButtonItem?.tintColor = ApplicationThemeColor().GreenThemeColor
            
            fromTextField.textColor = ApplicationThemeColor().YellowThemeColor
            toTextField.textColor = ApplicationThemeColor().YellowThemeColor
        }
        
    }
    /// Placemark used to store the from location.
    var fromPlacemark : MKPlacemark?
    /// Placemark used to store the to location.
    var toPlacemark : MKPlacemark?
    /// MKPolylineRenderer used to render the line on the map.
    var polylineRenderer : MKPolylineRenderer = MKPolylineRenderer()
    /**
    method that detects when the doneButton is pressed.
    - Parameter sender: holds the sender of the function.
    */
    @IBAction func doneButtonPressed(sender: UIBarButtonItem) {
        if map.annotations.count < 2{
            let alert : UIAlertController = UIAlertController(title: "Oops!", message: "Please Set All Fields", preferredStyle: UIAlertControllerStyle.Alert)
            let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            let alert : UIAlertController = UIAlertController(title: "Almost Done", message: "Please add a name to your new route (can be empty)", preferredStyle: UIAlertControllerStyle.Alert)
            var textFieldName : UITextField = UITextField()
            alert.addTextFieldWithConfigurationHandler({ (textField : UITextField) -> Void in
                textField.placeholder = "name"
                textFieldName = textField
            })
            let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                let route : Route = Route()
                route.isDeleted = false
                route.sourceLocationName = self.fromTextField.text!
                route.destinationLocationName = self.toTextField.text!
                route.sourceLocation = self.fromPlacemark!.location!
                route.destinationLocation = self.toPlacemark!.location!
                let newRoute : QBCOCustomObject = QBCOCustomObject()
                let fields : NSMutableDictionary = NSMutableDictionary()
                fields.setObject(route.isDeleted   , forKey: "isDeleted")
                fields.setObject(route.sourceLocationName, forKey: "sourceLocationName")
                fields.setObject(route.destinationLocationName, forKey: "destinationLocationName")
                fields.setObject(route.destinationLocation.coordinate.latitude, forKey: "destinationLocationLatitude")
                fields.setObject(route.destinationLocation.coordinate.longitude, forKey: "destinationLocationLongitude")
                fields.setObject(route.sourceLocation.coordinate.latitude, forKey: "sourceLocationLatitude")
                fields.setObject(route.sourceLocation.coordinate.longitude, forKey: "sourceLocationLongitude")
                if textFieldName.text != nil{
                    fields.setObject(textFieldName.text!, forKey: "name")
                }
                else{
                    fields.setObject("", forKey: "name")
                }
                
                newRoute.fields = fields
                newRoute.className = "Route"
                self.view.userInteractionEnabled = false
                QBRequest.createObject(newRoute, successBlock: { (response : QBResponse!, route : QBCOCustomObject!) -> Void in
                    self.view.userInteractionEnabled = true
                    self.navigationController?.popViewControllerAnimated(true)
                    
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        self.view.userInteractionEnabled = true
                })

            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action : UIAlertAction) -> Void in
                
            })
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    /**
    method that detects when the getMyLocationFromButton is pressed.
    - Parameter sender: holds the sender of the function.
    */
    @IBAction func getMyLocationFromButtonPressed(sender: UIButton) {
        if currentUserLocation != nil{
            let reverseGeoCoder : CLGeocoder = CLGeocoder()
            reverseGeoCoder.reverseGeocodeLocation(currentUserLocation!.location(), completionHandler: { (placemarks : [CLPlacemark]?,error : NSError?) -> Void in
                if placemarks != nil{
                    if placemarks!.count>0{//
                            if self.fromPlacemark != nil{
                                for annotation in self.map.annotations{
                                    if annotation.title! == "Source"
                                    {
                                        self.map.removeAnnotation(annotation as! MKPointAnnotation)
                                    }
                                }
                            }
//
//                            
                            self.fromPlacemark = MKPlacemark(placemark: placemarks![0])
                            if let lines : AnyObject = self.fromPlacemark!.addressDictionary?["FormattedAddressLines"]{
                                let addressString : NSString = lines.componentsJoinedByString(", ")
                                self.fromTextField.text = addressString as String
                            }
                            else{
                                self.fromTextField.text = ABCreateStringWithAddressDictionary(self.fromPlacemark!.addressDictionary!, true)
                            }
                            let annotation : MKPointAnnotation = MKPointAnnotation()
                            annotation.title = "Source"
                            annotation.coordinate = self.fromPlacemark!.coordinate
                            self.map.addAnnotation(annotation)
                            self.map.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)), animated: true)
                    }
                }
                
            })
        }
    }
    /**
    method that detects when the getMyLocationToButton is pressed.
    - Parameter sender: holds the sender of the function.
    */
    @IBAction func getMyLocationToButtonPressed(sender: UIButton) {
        if currentUserLocation != nil{
            let reverseGeoCoder : CLGeocoder = CLGeocoder()
            reverseGeoCoder.reverseGeocodeLocation(currentUserLocation!.location(), completionHandler: { (placemarks : [CLPlacemark]?,error : NSError?) -> Void in
                if placemarks != nil{
                    if placemarks!.count>0{
//                            
                            if self.toPlacemark != nil{
                                for annotation in self.map.annotations{
                                    if annotation.title! == "Destination"
                                    {
                                        self.map.removeAnnotation(annotation as! MKPointAnnotation)
                                    }
                                }
                            }
//
//                            
                            self.toPlacemark = MKPlacemark(placemark: placemarks![0])
                            if let lines : AnyObject = self.toPlacemark!.addressDictionary?["FormattedAddressLines"]{
                                let addressString : NSString = lines.componentsJoinedByString(", ")
                                self.toTextField.text = addressString as String
                            }
                            else{
                                self.toTextField.text = ABCreateStringWithAddressDictionary(self.toPlacemark!.addressDictionary!, true)
                            }
                            let annotation : MKPointAnnotation = MKPointAnnotation()
                            annotation.title = "Destination"
                            annotation.coordinate = self.toPlacemark!.coordinate
                            self.map.addAnnotation(annotation)
                            self.map.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)), animated: true)
                    }
                }
            })
        }
    }
    /**
    delegate method that transfers the from placemark from the ChooseLocationViewController.
    - Parameter placemark: holds the value returned from the ChooseLocationViewController.
    */
    func returnToFrom(placemark : MKPlacemark) {
        if fromPlacemark != nil{
            for annotation in map.annotations{
                if annotation.title! == "Source"
                {
                    map.removeAnnotation(annotation as! MKPointAnnotation)
                }
            }
        }
        fromPlacemark = placemark
        
        if let lines : AnyObject = fromPlacemark!.addressDictionary?["FormattedAddressLines"]{
            let addressString : NSString = lines.componentsJoinedByString(", ")
            fromTextField.text = addressString as String
        }
        else{
            fromTextField.text = ABCreateStringWithAddressDictionary(self.fromPlacemark!.addressDictionary!, true)
        }
        fromTextField.text = placemark.name
        
        let annotation : MKPointAnnotation = MKPointAnnotation()
        annotation.title = "Source"
        annotation.coordinate = self.fromPlacemark!.coordinate
        self.map.addAnnotation(annotation)
        self.map.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)), animated: true)
        
    }
    /**
    delegate method that transfers the to placemark from the ChooseLocationViewController.
    - Parameter placemark: holds the value returned from the ChooseLocationViewController.
    */
    func returnToTo(placemark : MKPlacemark) {
        if toPlacemark != nil{
            for annotation in map.annotations{
                if annotation.title! == "Destination"
                {
                    map.removeAnnotation(annotation as! MKPointAnnotation)
                }
            }
        }
        toPlacemark = placemark
        
        if let lines : AnyObject = toPlacemark!.addressDictionary?["FormattedAddressLines"]{
            let addressString : NSString = lines.componentsJoinedByString(", ")
            toTextField.text = addressString as String
        }
        else{
            toTextField.text = ABCreateStringWithAddressDictionary(self.toPlacemark!.addressDictionary!, true)
        }
        toTextField.text = placemark.name
        let annotation : MKPointAnnotation = MKPointAnnotation()
        annotation.title = "Destination"
        annotation.coordinate = self.toPlacemark!.coordinate
        self.map.addAnnotation(annotation)
        self.map.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)), animated: true)
        drawRoute()
    }
    
    /**
    method that refreshes the value of the placemark after the dragging of the annotation is done.
    - Parameter viewForAnnotation: holds the value of the annotation view of the placemark.
    */
    func refreshPlacemarkAfterDrag(viewForAnnotation : MKAnnotationView){
        let reverseGeoCoder : CLGeocoder = CLGeocoder()
        if viewForAnnotation.annotation!.title!! == "Source"{
            reverseGeoCoder.reverseGeocodeLocation(CLLocation(latitude: viewForAnnotation.annotation!.coordinate.latitude,longitude: viewForAnnotation.annotation!.coordinate.longitude), completionHandler: { (placemarks : [CLPlacemark]?,error : NSError?) -> Void in
                if placemarks != nil{
                    if placemarks!.count>0{
                        self.fromPlacemark = MKPlacemark(placemark: placemarks![0])
                        if let lines : AnyObject = self.fromPlacemark!.addressDictionary?[ "FormattedAddressLines"]{
                            let addressString : NSString = lines.componentsJoinedByString(", ")
                            self.fromTextField.text = addressString as String
                        }
                        else{
                            self.fromTextField.text = ABCreateStringWithAddressDictionary(self.fromPlacemark!.addressDictionary!, true)
                        }
                        self.drawRoute()
                    }
                }

            })
            
        }
        else if viewForAnnotation.annotation!.title!! == "Destination"{
            reverseGeoCoder.reverseGeocodeLocation(CLLocation(latitude: viewForAnnotation.annotation!.coordinate.latitude,longitude: viewForAnnotation.annotation!.coordinate.longitude), completionHandler: { (placemarks : [CLPlacemark]?,error : NSError?) -> Void in
                if placemarks!.count>0{
                        self.toPlacemark = MKPlacemark(placemark: placemarks![0])
                        if let lines : AnyObject = self.toPlacemark!.addressDictionary?["FormattedAddressLines"]{
                            let addressString : NSString = lines.componentsJoinedByString(", ")
                            self.toTextField.text = addressString as String
                        }
                        else{
                            self.toTextField.text = ABCreateStringWithAddressDictionary(self.toPlacemark!.addressDictionary!, true)
                        }
                        self.drawRoute()
                }
            })
            
            
        }
        
    }
    
    /**
    method that detects when the fromButton is pressed. Takes the user to the ChooseLocationViewController
    - Parameter sender: holds the sender of the function
    */
    @IBAction func fromButtonPressed(sender: UIButton) {
        var chooseLocationNavigationViewController : ChooseLocationNavigationViewController = ChooseLocationNavigationViewController()
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        chooseLocationNavigationViewController = storyboard.instantiateViewControllerWithIdentifier("ChooseLocationNavigationViewController") as! ChooseLocationNavigationViewController
        let chooseLocationViewController : ChooseLocationViewController = chooseLocationNavigationViewController.topViewController as! ChooseLocationViewController
        chooseLocationViewController.mode = "from"
        chooseLocationViewController.delegate = self
        self.presentViewController(chooseLocationNavigationViewController, animated: true, completion: nil)
        
    }
    /**
    method that detects when the fromButton is pressed. Takes the user to the ChooseLocationViewController
    - Parameter sender: holds the sender of the function
    */
    @IBAction func toButtonPressed(sender: UIButton) {
        var chooseLocationNavigationViewController : ChooseLocationNavigationViewController = ChooseLocationNavigationViewController()
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        chooseLocationNavigationViewController = storyboard.instantiateViewControllerWithIdentifier("ChooseLocationNavigationViewController") as! ChooseLocationNavigationViewController
        let chooseLocationViewController : ChooseLocationViewController = chooseLocationNavigationViewController.topViewController as! ChooseLocationViewController
        chooseLocationViewController.mode = "to"
        chooseLocationViewController.delegate = self
        self.presentViewController(chooseLocationNavigationViewController, animated: true, completion: nil)
    }
    @IBOutlet weak var backgroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil{
            revealViewController().rightRevealToggleAnimated(true)
        }
        self.title = "Add Route"
        setTheme()
        map.delegate = self
        fromTextField.userInteractionEnabled = false
        toTextField.userInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRoutesMapViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mapView(mapView: MKMapView, didAddOverlayViews overlayViews: [AnyObject]) {
        
    }
    /**
    This is a method that draws the MKPolyline in the ViewController.
    */
    func drawRouteHelper(){
        var points: [CLLocationCoordinate2D]
        points = [self.fromPlacemark!.coordinate, self.toPlacemark!.coordinate,CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)]
        let geodesic = MKGeodesicPolyline(coordinates: &points[0], count: 2)
        self.map.addOverlay(geodesic, level: MKOverlayLevel.AboveLabels)
        let overlays : [MKOverlay] = self.map.overlays
        //self.map.removeOverlays(overlays)
        self.map.addOverlays(overlays, level: MKOverlayLevel.AboveLabels)
    }
    /**
    This is a method that draws gets the route using a Google API call.
    */
    func drawRoute(){
        if map.annotations.count == 2{
            //print("numberOfOverlaysPresent = ")
            //if map.overlays != []{
                if map.overlays.count > 0{
                    map.removeOverlays(map.overlays)
                }
            //}
            
            
            let urlString : String = "http://maps.google.com/maps/api/directions/json?origin=" + self.fromPlacemark!.coordinate.latitude.description + "," + self.fromPlacemark!.coordinate.longitude.description + "&destination=" + self.toPlacemark!.coordinate.latitude.description + "," + self.toPlacemark!.coordinate.longitude.description + "&sensor=false"
            GoogleParser().returnJSONDictionary(urlString, completion: { (dictionary, status) -> Void in
                if status{
                    if let routes: AnyObject = dictionary["routes"]{
                        if routes.count > 0{
                            if let polylineDictionary : NSDictionary = routes[0]["overview_polyline"] as? NSDictionary{
                                if let polylineString : String = polylineDictionary["points"] as? String{
                                    let polyline : MKPolyline = MKPolyline(encodedString: polylineString)
                                    //print("polyline: ")
                                    //print(polyline.coordinate.latitude.description + " " + polyline.coordinate.longitude.description)
                                    self.map.addOverlay(polyline, level: MKOverlayLevel.AboveLabels)
                                    //self.map.showAnnotations(self.map.annotations, animated: true)
                                    zoomToPolyLine(self.map,polyline: polyline, animated: true)
                                }
                            }
                        }
                    }
                    
                    
                }
                else if !status{
                    //print("routeDictionaryNotFound!")
                }
            })
            
            
        }
        
    }

    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if oldState == MKAnnotationViewDragState.Dragging && newState == MKAnnotationViewDragState.Ending{
            refreshPlacemarkAfterDrag(view)
        }
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        var view: MKPinAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
            as? MKPinAnnotationView { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView
                view.canShowCallout = true
                view.draggable = true
                view.annotation = annotation
                if view.annotation!.title! == "Destination"{
                    view.pinColor = MKPinAnnotationColor.Green
                }
                
        } else {
            // 3
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.draggable = true
            view.annotation = annotation
            if view.annotation!.title! == "Destination"{
                view.pinColor = MKPinAnnotationColor.Green
            }
            
        }
        return view
        
        
    }
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 4
            
            return polylineRenderer
        }
        
        
        return MKOverlayRenderer()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }
    
    
}
