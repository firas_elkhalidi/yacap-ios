//
//  MyRatingsTableViewCell.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 10/9/15.
//  Copyright © 2015 WhyApp. All rights reserved.
//

import UIKit

class MyRatingsTableViewCell: UITableViewCell {

    @IBOutlet weak var commentTextView: UITextView!
    //@IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet weak var starRating: CosmosView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 6;
        starRating.updateOnTouch = false
        starRating.rating = 0
        starRating.settings.fillMode = StarFillMode.Half
        userImageView.clipsToBounds = true;
        userImageView.userInteractionEnabled = true
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
