//
//  SignUpViewController.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
@IBDesignable

class FormTextField: UITextField {
    ///variable of type CGFloat that holds the value of the inset in the text field
    @IBInspectable var inset: CGFloat = 0
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, inset, inset)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return textRectForBounds(bounds)
    }
    
}
class SignUpViewController: UIViewController,MLPAutoCompleteTextFieldDataSource,MLPAutoCompleteTextFieldDelegate,UIGestureRecognizerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, TermsAndConditionsDelegate, UIPopoverControllerDelegate {
    ///instance of the image view that is found in the background of the view
    @IBOutlet weak var backgroundImageView: UIImageView!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var fullNameLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var emailLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var usernameLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var organizationLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var passwordLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var genderLabel: UILabel!
    ///label that is placed above its corresponding text field
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    ///activity indicator that indicates loading when the user is signing up
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    ///image picker that allows the user to choose an image for the profile on registration of the account
    var imagePickerController : UIImagePickerController = UIImagePickerController()
    ///boolean indicator that indicates whether an image was set
    var didUpdateImage = false
    ///image view that contains the user's image
    @IBOutlet weak var userImageView: UIImageView!
    ///data that is found for the organizations autocomplete text field.
    var dataForOrganizations = [MLPAutoCompletionObject]()
    ///text field that holds the organization of the user
    @IBOutlet weak var organizationTextField: MLPAutoCompleteTextField!
    ///segmented control that is used to indicate the gender of the user
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    ///method that gets the data used for the autocomplete organizations text field
    func getDataForOrganizations(){
        QBRequest.objectsWithClassName("Organization", successBlock: { (response :QBResponse!, organizations : [AnyObject]!) -> Void in
            for organization in organizations as! [QBCOCustomObject]{
                self.dataForOrganizations.append(AutoCompleteCustomObject(organization: organization))
            }
            }) { (response : QBResponse!) -> Void in
                
        }
    }
//    override func shouldAutorotate() -> Bool {
//        return false
//    }
//    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
//        return UIInterfaceOrientation.Portrait
//    }
    
    /**
    method used to detect that the editing did end in the date of birth days text field.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func dobDaysTextFieldEditingDidEnd(sender: UITextField) {
        if !(dobDaysTextField.text!.isEmpty){
            if let _ = Int(dobDaysTextField.text!){
                if Int(dobDaysTextField.text!)! < 1 || Int(dobDaysTextField.text!)! > 31{
                    dobDaysTextField.text = ""
                }
            }
            else{
                dobDaysTextField.text = ""
            }
        }
    }

    /**
    method used to detect that the editing did end in the date of birth months text field.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func dobMonthsTextFieldEditingDidEnd(sender: UITextField) {
        if !(dobMonthsTextField.text!.isEmpty){
            if let _ = Int(dobMonthsTextField.text!){
                if Int(dobMonthsTextField.text!)! < 1 || Int(dobMonthsTextField.text!)! > 12{
                    dobMonthsTextField.text = ""
                }
            }
            else{
                dobMonthsTextField.text = ""
            }
        }
    }
    /**
    method used to detect that the editing did end in the date of birth years text field.
    - Parameter sender: holds the sender of the method
    */
    @IBAction func dobYearsTextFieldEditingDidEnd(sender: UITextField) {
        let now : NSDate = NSDate()
        let cal : NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var year : Int = Int()
        year = cal.component(NSCalendarUnit.Year, fromDate: now)
        if !(dobYearsTextField.text!.isEmpty){
            if let _ = Int(dobYearsTextField.text!){
                if Int(dobYearsTextField.text!)! < 1900 || Int(dobYearsTextField.text!)! > year{
                    dobYearsTextField.text = ""
                }
            }
            else{
                dobYearsTextField.text = ""
            }
        }

    }
    ///TPKeyboardAvoidingScrollView that is in the background of all the views
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    /**
    method that validates that the date string inputted is at least 18 or more.
    - Parameter date: it is the date in question.
    */
    func is18YearsOld(date : String) -> Bool{
        let format : NSDateFormatter = NSDateFormatter()
        format.dateStyle = NSDateFormatterStyle.ShortStyle
        format.dateFormat = "dd/mm/yyyy"
        if let validateDOB : NSDate  = format.dateFromString(date){
            let now : NSDate = NSDate()
            let ageComponents : NSDateComponents = NSCalendar.currentCalendar().components( NSCalendarUnit.Year, fromDate: validateDOB, toDate: now, options: [])
            let age = ageComponents.year
            return (age >= 18)
        }
        return false

    }
    /**
    method that validates that the date string is a real date.
    - Parameter date: it is the date in question.
    */
    func isValidateDOB(dateOfBirth : NSString) -> Bool
    {
        let format : NSDateFormatter = NSDateFormatter()
        format.dateStyle = NSDateFormatterStyle.ShortStyle
        format.dateFormat = "yyyy-mm-dd"
        //format.locale = NSLocale(localeIdentifier: "GMT")
        print("dateString:" + (dateOfBirth as String))
        if let _ : NSDate  = format.dateFromString(dateOfBirth as String){
            return true;
        }
        else{
            return false;
        }
        
    }
    ///date of birth years text field
    @IBOutlet weak var dobYearsTextField: UITextField!
    ///date of birth months text field
    @IBOutlet weak var dobMonthsTextField: UITextField!
    ///date of birth days text field
    @IBOutlet weak var dobDaysTextField: UITextField!
    ///user username text field
    @IBOutlet weak var usernameTextField: UITextField!
    ///user password text field
    @IBOutlet weak var passwordTextField: UITextField!
    ///text field that is used to confirm the password of the user
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    ///user email text field
    @IBOutlet weak var emailTextField: UITextField!
    ///user full name text field
    @IBOutlet weak var fullNameTextField: UITextField!
    ///delegate method in the TermsAndConditionsDelegate and it is used to indicate that the user accepted the terms and conditions
    func userDidAcceptTermsAndConditions() {
        var alert : UIAlertController = UIAlertController()
        var action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        if fullNameTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Full Name Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.fullNameTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if emailTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Email Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.emailTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if usernameTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Username Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.usernameTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
            
        else if passwordTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Password Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.passwordTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if passwordTextField.text!.characters.count < 8{
            alert = UIAlertController(title: "Oops", message: "Password must be more than 7 characters", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.passwordTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if passwordTextField.text!.characters.count > 40{
            alert = UIAlertController(title: "Oops", message: "Password can not be more than 40 characters", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.passwordTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if passwordTextField.text != confirmPasswordTextField.text{
            alert = UIAlertController(title: "Oops", message: "Passwords Don't Match", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.passwordTextField.text = ""
                self.confirmPasswordTextField.text = ""
                self.passwordTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if genderSegmentedControl.selectedSegmentIndex == UISegmentedControlNoSegment{
            alert = UIAlertController(title: "Oops", message: "Please Select A Gender", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if dobDaysTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Days Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.dobDaysTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if dobMonthsTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Months Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.dobMonthsTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if dobYearsTextField.text!.isEmpty{
            alert = UIAlertController(title: "Oops", message: "Years Field Empty", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.dobYearsTextField.becomeFirstResponder()
            })
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if !isValidateDOB(dobYearsTextField.text! + "-" + dobMonthsTextField.text! + "-" + dobDaysTextField.text!){
            alert = UIAlertController(title: "Oops", message: "The DOB you entered is not valid", preferredStyle: UIAlertControllerStyle.Alert)
            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.dobDaysTextField.becomeFirstResponder()
            })
            self.dobDaysTextField.text = ""
            self.dobMonthsTextField.text = ""
            self.dobYearsTextField.text = ""
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if !is18YearsOld(dobDaysTextField.text! + "/" + dobMonthsTextField.text! + "/" + dobYearsTextField.text!){
            alert = UIAlertController(title: "Oops", message: "Aren't a little too young for carpooling?(18+)", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            self.view.userInteractionEnabled = false
            let newUser : QBUUser = QBUUser()
            newUser.login = usernameTextField.text
            newUser.password = passwordTextField.text
            newUser.fullName = fullNameTextField.text
            newUser.email = emailTextField.text
            
            var gender : String = ""
            if genderSegmentedControl.selectedSegmentIndex == 0{
                gender = "m"
            }
            else{
                gender = "f"
            }
            let dateOfBirthString = dobDaysTextField.text! + "/" + dobMonthsTextField.text! + "/" + dobYearsTextField.text!
            
            let format : NSDateFormatter = NSDateFormatter()
            format.dateStyle = NSDateFormatterStyle.ShortStyle
            format.dateFormat = "dd/mm/yyyy"
            let organization : String = organizationTextField.text!
            let newUserCustomData : NSDictionary = ["genderType" : gender, "dateOfBirth" : dateOfBirthString ,"address" : "", "isSmoker" : false, "isDriver" : false,"status" : "visible","carRegistrationNumber" : "", "carModel" : "", "carColor" : "", "numberOfSeats" : "", "activeRouteID" : "","hasAC" : false, "carBlobID" : "" , "organization" : organization]
            let error = NSErrorPointer()
            do {
                let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(newUserCustomData, options: NSJSONWritingOptions.PrettyPrinted)
                //print("JSON:", terminator: "")
                let theJSONText = NSString(data: newUserCustomDataJSON,
                    encoding: NSASCIIStringEncoding)
                //print(theJSONText)
                newUser.customData = String(theJSONText!)
            } catch let error1 as NSError {
                error.memory = error1
            }
            self.activityIndicator.startAnimating()
            
            //self.view.addSubview(boxView)
            QBRequest.signUp(newUser, successBlock: {(response : QBResponse!, user : QBUUser!) -> Void in
                
                
                if self.didUpdateImage{
                    
                    let profileImage = self.userImageView.image
                    var compression : CGFloat = 0.9
                    let maxCompression : CGFloat = 0.1
                    let maxFileSize : Int = 250*1024;
                    var imageData : NSData = UIImageJPEGRepresentation(profileImage!, compression)!
                    while (imageData.length > maxFileSize && compression > maxCompression)
                    {
                        compression -= 0.1;
                        imageData = UIImageJPEGRepresentation(profileImage!, compression)!;
                    }
                    let statusBlock : QBRequestStatusUpdateBlock = { (request : QBRequest!, status : QBRequestStatus!) in
                        NSLog(status.percentOfCompletion.description);
                    }
                    QBRequest.logInWithUserEmail(self.emailTextField.text, password: self.passwordTextField.text, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                        QBRequest.TUploadFile(imageData, fileName: "myAvatar", contentType: "image/jpeg", isPublic: true, successBlock: { (response : QBResponse!, blob : QBCBlob!) -> Void in
                            let parameters : QBUpdateUserParameters = QBUpdateUserParameters()
                            parameters.blobID = Int(blob.ID)
                            QBRequest.updateCurrentUser(parameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                //self.boxView.removeFromSuperview()
                                self.activityIndicator.stopAnimating()
                                self.view.userInteractionEnabled = true
                                NSLog(response.description)
                                alert = UIAlertController(title: "Success", message: "Sign Up Successful!", preferredStyle: UIAlertControllerStyle.Alert)
                                action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                })
                                alert.addAction(action)
                                self.presentViewController(alert, animated: true, completion: nil)
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    //self.boxView.removeFromSuperview()
                                    self.activityIndicator.stopAnimating()
                                    self.view.userInteractionEnabled = true
                                    NSLog(response.description)
                                    alert = UIAlertController(title: "Success", message: "Sign Up Successful!", preferredStyle: UIAlertControllerStyle.Alert)
                                    action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                                        self.dismissViewControllerAnimated(true, completion: nil)
                                    })
                                    alert.addAction(action)
                                    self.presentViewController(alert, animated: true, completion: nil)
                            })
                            }, statusBlock: statusBlock, errorBlock: { (response : QBResponse!) -> Void in
                                
                        })
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            //self.boxView.removeFromSuperview()
                            self.activityIndicator.stopAnimating()
                            self.view.userInteractionEnabled = true
                            NSLog(response.description)
                            alert = UIAlertController(title: "Success", message: "Sign Up Successful!", preferredStyle: UIAlertControllerStyle.Alert)
                            action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                                self.dismissViewControllerAnimated(true, completion: nil)
                            })
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                    })
                    
                }
                else{
                    //self.boxView.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    self.view.userInteractionEnabled = true
                    NSLog(response.description)
                    alert = UIAlertController(title: "Success", message: "Sign Up Successful!", preferredStyle: UIAlertControllerStyle.Alert)
                    action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
                
                
                
                }, errorBlock: {(response : QBResponse!) -> Void in
                    //self.boxView.removeFromSuperview()
                    self.activityIndicator.stopAnimating()
                    self.view.userInteractionEnabled = true
                    NSLog(response.error.description)
                    if response.error.reasons.description.rangeOfString("has already been taken") != nil && response.error.reasons.description.rangeOfString("email") != nil{
                        alert = UIAlertController(title: "Oops", message: "The email you are trying to use already exists", preferredStyle: UIAlertControllerStyle.Alert)
                        action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                            self.emailTextField.becomeFirstResponder()
                        })
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                        //self.emailTextField.becomeFirstResponder()
                    }
                    else if response.error.reasons.description.rangeOfString("should look like an email address") != nil && response.error.reasons.description.rangeOfString("email") != nil{
                        alert = UIAlertController(title: "Oops", message: "This is not an email address", preferredStyle: UIAlertControllerStyle.Alert)
                        action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                            self.emailTextField.becomeFirstResponder()
                        })
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                        //self.emailTextField.becomeFirstResponder()
                    }
                    else if response.error.reasons.description.rangeOfString("has already been taken") != nil && response.error.reasons.description.rangeOfString("login") != nil{
                        alert = UIAlertController(title: "Oops", message: "The username you are trying to use already exists", preferredStyle: UIAlertControllerStyle.Alert)
                        action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                            self.usernameTextField.becomeFirstResponder()
                        })
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                        //self.usernameTextField.becomeFirstResponder()
                    }
                    else{
                        alert = UIAlertController(title: "Oops", message: "Something Went Wrong, Please Check That You Are Connected!", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            })
            
        }
    }
    /**
    method that detects that the sign up button is pressed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func signupButtonPressed(sender: UIButton) {
        let navigationController : UINavigationController = UINavigationController()
        let termsAndConditionsViewController : TermsAndConditionsViewController = storyboard?.instantiateViewControllerWithIdentifier("TermsAndConditionsViewController") as! TermsAndConditionsViewController
        termsAndConditionsViewController.delegate = self
        navigationController.pushViewController(termsAndConditionsViewController, animated: false)
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad{
            navigationController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        }
        self.presentViewController(navigationController, animated: true, completion: nil)

    }
    /**
    method that detects that the cancel button is pressed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        
        fullNameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        emailLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        usernameLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        passwordLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        confirmPasswordLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        dateOfBirthLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        genderLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        organizationLabel.textColor = ApplicationThemeColor().WhiteThemeColor
        fullNameLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        emailLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        usernameLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        passwordLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        confirmPasswordLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        dateOfBirthLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        genderLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        organizationLabel.backgroundColor = ApplicationThemeColor().GreenThemeColor
        fullNameLabel.layer.cornerRadius = 8;
        emailLabel.layer.cornerRadius = 8;
        usernameLabel.layer.cornerRadius = 8;
        passwordLabel.layer.cornerRadius = 8;
        confirmPasswordLabel.layer.cornerRadius = 8;
        dateOfBirthLabel.layer.cornerRadius = 8;
        genderLabel.layer.cornerRadius = 8;
        organizationLabel.layer.cornerRadius = 8;
        fullNameLabel.layer.masksToBounds = true;
        emailLabel.layer.masksToBounds = true;
        usernameLabel.layer.masksToBounds = true;
        passwordLabel.layer.masksToBounds = true;
        confirmPasswordLabel.layer.masksToBounds = true;
        dateOfBirthLabel.layer.masksToBounds = true;
        genderLabel.layer.masksToBounds = true;
        organizationLabel.layer.masksToBounds = true;
        
        
        let allAlphaForLabels : CGFloat = 0.6
        fullNameLabel.alpha = allAlphaForLabels
        emailLabel.alpha = allAlphaForLabels
        usernameLabel.alpha = allAlphaForLabels
        passwordLabel.alpha = allAlphaForLabels
        confirmPasswordLabel.alpha = allAlphaForLabels
        dateOfBirthLabel.alpha = allAlphaForLabels
        genderLabel.alpha = allAlphaForLabels
        organizationLabel.alpha = allAlphaForLabels
        
        let allAlphaForTextFields : CGFloat = 0.85
        
        fullNameTextField.alpha = allAlphaForTextFields
        emailTextField.alpha = allAlphaForTextFields
        usernameTextField.alpha = allAlphaForTextFields
        passwordTextField.alpha = allAlphaForTextFields
        confirmPasswordTextField.alpha = allAlphaForTextFields
        dobDaysTextField.alpha = allAlphaForTextFields
        dobMonthsTextField.alpha = allAlphaForTextFields
        dobYearsTextField.alpha = allAlphaForTextFields
        
        fullNameTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        emailTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        usernameTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        passwordTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        confirmPasswordTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        dobDaysTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        dobMonthsTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        dobYearsTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        genderSegmentedControl.tintColor = ApplicationThemeColor().GreenThemeColor
        genderSegmentedControl.alpha = 0.8
        genderSegmentedControl.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        genderSegmentedControl.layer.cornerRadius = 4
        genderSegmentedControl.layer.masksToBounds = true
        fullNameTextField.textColor = ApplicationThemeColor().GreenThemeColor
        emailTextField.textColor = ApplicationThemeColor().GreenThemeColor
        usernameTextField.textColor = ApplicationThemeColor().GreenThemeColor
        passwordTextField.textColor = ApplicationThemeColor().GreenThemeColor
        confirmPasswordTextField.textColor = ApplicationThemeColor().GreenThemeColor
        dobDaysTextField.textColor = ApplicationThemeColor().GreenThemeColor
        dobMonthsTextField.textColor = ApplicationThemeColor().GreenThemeColor
        dobYearsTextField.textColor = ApplicationThemeColor().GreenThemeColor
        
        organizationTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        organizationTextField.tintColor = ApplicationThemeColor().GreenThemeColor
        organizationTextField.attributedPlaceholder = NSAttributedString(string: "Organization", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        //organizationTextField.borderStyle = UITextBorderStyle.
        fullNameTextField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        fullNameTextField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        confirmPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        dobDaysTextField.attributedPlaceholder = NSAttributedString(string: "dd", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        dobMonthsTextField.attributedPlaceholder = NSAttributedString(string: "mm", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        dobYearsTextField.attributedPlaceholder = NSAttributedString(string: "yyyy", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        signUpButton.setTitle("SIGN UP", forState: UIControlState.Normal)
        cancelButton.setTitle("CANCEL", forState: UIControlState.Normal)
        signUpButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        cancelButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        signUpButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
        cancelButton.setTitleColor(ApplicationThemeColor().WhiteThemeColor, forState: UIControlState.Normal)
        self.view.backgroundColor = ApplicationThemeColor().GreyThemeColor
        
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, possibleCompletionsForString string: String!) -> [AnyObject]! {
        return dataForOrganizations
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, didSelectAutoCompleteString selectedString: String!, withAutoCompleteObject selectedObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) {
        textField.text = selectedString
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, shouldConfigureCell cell: UITableViewCell!, withAutoCompleteString autocompleteString: String!, withAttributedString boldedString: NSAttributedString!, forAutoCompleteObject autocompleteObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        cell.textLabel?.text = autocompleteString
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.backgroundColor = ApplicationThemeColor().GreenThemeColor
        return true
    }
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, willShowAutoCompleteTableView autoCompleteTableView: UITableView!) {
        ////print("showing autocomplete")
    }
    ///instance of the sign up button
    @IBOutlet weak var signUpButton: UIButton!
    ///instance of the cancel button
    @IBOutlet weak var cancelButton: UIButton!
    override func viewDidLoad() {
        activityIndicator.stopAnimating()
        //wactivityIndicator.color = UIColor.g()
        //activityIndicator.backgroundColor = ApplicationThemeColor().GreenThemeColor
        //backgroundImageView.image = backgroundImage
        userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 6;
        userImageView.clipsToBounds = true;
        scrollView.canCancelContentTouches = false
        scrollView.backgroundColor = UIColor.clearColor()
        userImageView.userInteractionEnabled = true
        userImageView.image = UIImage(named: "placeholder")
        let tapGestureRecognizer :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped:")
        tapGestureRecognizer.delegate = self
        userImageView.addGestureRecognizer(tapGestureRecognizer)
        super.viewDidLoad()
        organizationTextField.sortAutoCompleteSuggestionsByClosestMatch = true
        organizationTextField.autoCompleteDataSource = self
        organizationTextField.autoCompleteDelegate = self
        genderSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        setTheme()
        getDataForOrganizations()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        //currentViewController = self
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRouteViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as![NSObject : AnyObject])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
    method that is called when the image view of the user is tapped.
    - Parameter gesture: holds the instance of the gesture.
    */
    func imageTapped(gesture : UITapGestureRecognizer){
        print("pressed")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            imagePickerController.delegate = self
            self.imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePickerController.allowsEditing = true
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        didUpdateImage = true
        userImageView.image = image
        imagePickerController.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
