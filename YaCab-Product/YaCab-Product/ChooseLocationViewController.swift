//
//  ChooseLocationViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/8/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
import AddressBookUI
import CoreLocation
import MapKit
protocol ChooseLocationDelegate{
    func returnToTo(placemark : MKPlacemark)
    func returnToFrom(placemark : MKPlacemark)
}
class ChooseLocationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,UISearchBarDelegate{
    ///cancel button that dismisses the view controller
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    ///holds the delegate of the ChooseLocationViewController
    var delegate : ChooseLocationDelegate? = nil
    ///instance of the table view present in the view controller
    @IBOutlet weak var tableView: UITableView!
    ///array of the queried locations from the search bar
    var returnedQueryLocations : [MKMapItem] = []
    ///instance of the search bar that is at the top of the screen
    @IBOutlet weak var searchBar: UISearchBar!
    ///string that indicates whether this view controller was accessed from the from or to text fields
    var mode : String = String()
    /**
    method that detects if the cancel button was pressed.
    - Parameter sender: holds the sender of the method.
    */
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        super.viewDidLoad()
        setTheme()
        // Do any additional setup after loading the view.
    }
    ///method that sets the theme of the view controller
    func setTheme(){
        if currentTheme == Theme.PassengerTheme.rawValue{
            cancelButton.tintColor = ApplicationThemeColor().GreenThemeColor
        }
        else{
            cancelButton.tintColor = ApplicationThemeColor().YellowThemeColor
        }
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        //print("autocompleteQuery:" + self.searchBar.text!)
        
        self.returnedQueryLocations = []
        self.tableView.reloadData()
        let queryString : String = searchText

        let request : MKLocalSearchRequest = MKLocalSearchRequest()
        request.naturalLanguageQuery = queryString
        if self.localSearch != nil{
            self.localSearch!.cancel()
            self.localSearch = nil
        }
        
        self.localSearch = MKLocalSearch(request: request)
        if currentUserLocation != nil{
            var region = MKCoordinateRegion()
            region.center = CLLocationCoordinate2D(latitude: currentUserLocation!.latitude , longitude: currentUserLocation!.longitude)
            request.region = region
        }
        self.localSearch = MKLocalSearch(request: request)
        if !queryString.isEmpty{

            self.localSearch!.startWithCompletionHandler { (response : MKLocalSearchResponse?, error : NSError?) -> Void in
                if error == nil{
                    if response != nil{
                        //print("responseForSearch:")
                        //print(response!)
                        if response!.mapItems.count != 0{
                            self.returnedQueryLocations = response!.mapItems
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
        else {
            self.returnedQueryLocations = []
            self.tableView.reloadData()
        }

    }
    ///variable that is used to search using the search bar text as a query
    var localSearch : MKLocalSearch?
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        returnedQueryLocations = []
        self.tableView.reloadData()
        let queryString : String = self.searchBar.text!
        let request : MKLocalSearchRequest = MKLocalSearchRequest()
        request.naturalLanguageQuery = queryString
        if self.localSearch != nil{
            self.localSearch!.cancel()
            self.localSearch = nil
        }
        self.localSearch = MKLocalSearch(request: request)
        if currentUserLocation != nil{
            var region = MKCoordinateRegion()
            region.center = CLLocationCoordinate2D(latitude: currentUserLocation!.latitude , longitude: currentUserLocation!.longitude)
            request.region = region
        }
        self.localSearch!.startWithCompletionHandler { (response : MKLocalSearchResponse?, error : NSError?) -> Void in
            
            if error == nil{
                if response != nil{
                    //print("responseForSearch:")
                    //print(response!)
                    if response!.mapItems.count != 0{
                        self.returnedQueryLocations = response!.mapItems
                    }
                }
                self.tableView.reloadData()
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if mode == "to"{
            //print(returnedQueryLocations[indexPath.row])
            delegate?.returnToTo(returnedQueryLocations[indexPath.row].placemark)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else if mode == "from"{
            delegate?.returnToFrom(returnedQueryLocations[indexPath.row].placemark)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnedQueryLocations.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) 
        cell.textLabel?.text = returnedQueryLocations[indexPath.row].name
        let formattedAddressLines : String = "FormattedAddressLines"
        if let lines : AnyObject = returnedQueryLocations[indexPath.row].placemark.addressDictionary?[formattedAddressLines]{
            let addressString : NSString = lines.componentsJoinedByString(", ")
            cell.detailTextLabel!.text = addressString as String
        }
        return cell
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "ChooseLocationViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
