//
//  ChatMessageTableViewCell.swift
//  YaCab-Product
//
//  Created by Mohammed Al Khalidi on 6/16/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
import UIKit
func colorImage(image: UIImage, color: UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(image.size, false, UIScreen.mainScreen().scale)
    let context: CGContextRef = UIGraphicsGetCurrentContext()!
    CGContextTranslateCTM(context, 0, image.size.height)
    CGContextScaleCTM(context, 1.0, -1.0)
    let rect: CGRect = CGRectMake(0, 0, image.size.width, image.size.height)
    CGContextSetBlendMode(context,  CGBlendMode.Normal)
    CGContextDrawImage(context, rect, image.CGImage)
    CGContextSetBlendMode(context, CGBlendMode.SourceIn)
    color.setFill()
    CGContextFillRect(context, rect)
    let coloredImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return coloredImage
}

class ChatMessageTableViewCell: UITableViewCell {
    var orangeBubble : UIImage = UIImage(named: "orangeBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15)
    var padding : CGFloat = 20.0
    var aquaBubble : UIImage =  UIImage(named: "aquaBubble")!.stretchableImageWithLeftCapWidth(24, topCapHeight: 15)
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var dateLabel : UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.dateLabel = UILabel()
        self.dateLabel.frame = CGRectMake(10, 5, UIScreen.mainScreen().bounds.width-20, 20)
        self.dateLabel.font = UIFont.systemFontOfSize(11)
        self.dateLabel.textColor = UIColor.lightGrayColor()
        self.contentView.addSubview(self.dateLabel)
        
        self.backgroundImageView = UIImageView()
        self.backgroundImageView.frame = CGRectZero
        self.contentView.addSubview(self.backgroundImageView)
        
        self.messageTextView = UITextView()
        self.messageTextView.backgroundColor = UIColor.clearColor()
        self.messageTextView.editable = false
        self.messageTextView.scrollEnabled = false
        self.messageTextView.sizeToFit()
        self.contentView.addSubview(self.messageTextView)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(style: UITableViewCellStyle.Default, reuseIdentifier: String())
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


    
    
    
    
    func configureCellWithMessage(message : QBChatMessage, opponentLogin : QBUUser){
        var bubbleframe : CGRect = messageTextView.frame;
        bubbleframe.size.width = messageTextView.contentSize.width;
        

        self.messageTextView.text = message.text;
        self.messageTextView.font = UIFont.boldSystemFontOfSize(13)
        let font : UIFont = UIFont.boldSystemFontOfSize(13)
        let size : CGSize  = findSizeForText(message.text, font: font)
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "h:mm a, EEE, MMM d"
        formatter.timeZone = NSTimeZone.localTimeZone()
        let time : NSString = formatter.stringFromDate(message.dateSent)
        // Left/Right bubble
        //print("testest")
        //print(DatabaseHelper().FetchUser().ID)
        //print(message.senderID)
        if (DatabaseHelper().FetchUser().ID == message.senderID) {
            //print("aquabubble:" + message.text)
            //let screenSize = UIScreen.mainScreen().bounds.width
            //print("thesizeheightis:" + size.height.description)
            self.messageTextView.frame = CGRectMake(self.frame.width-size.width-padding/2 + 2, padding+5, size.width, size.height+padding)

            self.messageTextView.sizeToFit()
            self.backgroundImageView.frame = CGRectMake(self.frame.width-size.width-padding/2, padding+5,
            self.messageTextView.frame.size.width+padding/2, self.messageTextView.frame.size.height+5)
            self.backgroundImageView.image = aquaBubble;
            //self.backgroundImageView.image = self.backgroundImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            //self.backgroundImageView.tintColor = ApplicationThemeColor().GreenThemeColor
            self.dateLabel.textAlignment = NSTextAlignment.Right;
            self.dateLabel.text = DatabaseHelper().FetchUser().login + " at " + (time as String)
            
//            
//            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//            attachment.image = [UIImage imageNamed:@"MyIcon.png"];
//            
//            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
//            
//            NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@"My label text"];
//            [myString appendAttributedString:attachmentString];
//            
//            myLabel.attributedText = myString;
            
            let attachement : NSTextAttachment = NSTextAttachment()
            let myString = NSMutableAttributedString(string: self.dateLabel.text! + " ")
           
            
            if message.deliveredIDs != nil{
                if (message.deliveredIDs.count != 0) || message.read{
                    if message.read{
                        let doubleCheckMark : UIImage = UIImage(named: "messageDelivered")!
                        attachement.image = colorImage(doubleCheckMark, color: ApplicationThemeColor().GreenThemeColor)
                        let attachementString : NSAttributedString = NSAttributedString(attachment: attachement)
                        myString.appendAttributedString(attachementString)
                        self.dateLabel.attributedText = myString
                    }
                    else {
                        let doubleCheckMark : UIImage = UIImage(named: "messageDelivered")!
                        attachement.image = colorImage(doubleCheckMark, color: ApplicationThemeColor().LightGreyTextColor)
                        let attachementString : NSAttributedString = NSAttributedString(attachment: attachement)
                        myString.appendAttributedString(attachementString)
                        self.dateLabel.attributedText = myString
                    }
                }
            }

            else{
                let checkMark : UIImage = UIImage(named: "messageSent")!
                attachement.image = colorImage(checkMark, color: ApplicationThemeColor().LightGreyTextColor)
                let attachementString : NSAttributedString = NSAttributedString(attachment: attachement)
                myString.appendAttributedString(attachementString)
                self.dateLabel.attributedText = myString
            }

            self.dateLabel.frame = CGRectMake(10, 5, UIScreen.mainScreen().bounds.width-20, 20)
            
        } else {
            //print("orangebubble")
            self.messageTextView.frame = CGRectMake(padding/2 + 3, padding+5, size.width, size.height + padding)
            self.messageTextView.sizeToFit()
            //print("frame : " )
            //print(self.messageTextView.frame)
            self.backgroundImageView.frame = CGRectMake(padding/2 - 5,padding + 5,self.messageTextView.frame.size.width+padding/2, self.messageTextView.frame.size.height+5)
            //print("Changing color" + size.width.description)
            self.backgroundImageView.image = orangeBubble;
            
            self.dateLabel.textAlignment = NSTextAlignment.Left;
            //self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", [[ChatService shared].currentUser login], time];
            self.dateLabel.text = opponentLogin.login + " at " + (time as String)
            
        }
        
        if let colorState: AnyObject = message.customParameters["color"]{
            if colorState as! String == CellColorStates.YELLOW.rawValue{
                self.backgroundImageView?.image = self.backgroundImageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                self.backgroundImageView?.tintColor = ApplicationThemeColor().YellowThemeColor
                self.messageTextView.textColor = ApplicationThemeColor().DarkGreyTextColor
            }
            else if colorState as! String == CellColorStates.GREEN.rawValue{
                self.backgroundImageView?.image = self.backgroundImageView?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                self.backgroundImageView?.tintColor = ApplicationThemeColor().GreenThemeColor
                self.messageTextView.textColor = ApplicationThemeColor().WhiteThemeColor
            }
        }
        
    }
    func findHeightForText(text : NSString, font : UIFont) -> CGFloat
    {
        var result : CGFloat = font.pointSize + 4
        if text.length != 0{
            let textSize : CGSize = CGSizeMake(260.0, 10000.0)//Width and height of text area
            let frame : CGRect = text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.TruncatesLastVisibleLine, attributes: [NSFontAttributeName : font], context: nil)
            let size : CGSize = CGSizeMake(frame.size.width, frame.size.height+1);
            //print("Widthforcell:" + frame.size.width.description)
            result = max(size.height, result)//At least one row
            
        }
        return result
    }
    func findSizeForText(text : NSString, font : UIFont) -> CGSize
    {
        if text.length != 0{
            let textSize : CGSize = CGSizeMake(260.0, 10000.0)      //Width and height of text area
            let frame : CGRect = text.boundingRectWithSize(CGSizeMake(260.0, 10000.0), options: NSStringDrawingOptions.TruncatesLastVisibleLine, attributes: [NSFontAttributeName : font], context: nil)
            var size : CGSize = CGSizeMake(frame.size.width, frame.size.height+1);
            size.width = size.width + 15
            //print("heightforcell: " + size.height.description)
            //print("Widthforcell:" + size.width.description)
            if size.width>textSize.width{
                size.width = 260.0
            }
            if size.height>textSize.height{
                size.height = 10000.0
            }
            return size

        }
        return CGSizeMake(0, 0)
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
