//
//  ApplicationThemeColor.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/30/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
///class that contains the colors of the application's theme
class ApplicationThemeColor {
    var LightGreytextFieldBackgroundColor = UIColor(red: 221/255.0, green: 225/255.0, blue: 225.0/255.0, alpha: 1.0)
    var GreenThemeColor = UIColor(red: 100/255, green: 146/255, blue: 141/255, alpha: 1.0)
    var WhiteThemeColor = UIColor.whiteColor()
    var YellowThemeColor = UIColor(red: 234/255, green: 198/255, blue: 77/255, alpha: 1.0)
    var GreyThemeColor = UIColor(red: 233/255, green: 234/255, blue: 235/255, alpha: 1.0)
    var GreyTextColor = UIColor(red: 150/255, green: 150/255, blue: 150/255, alpha: 1)
    var LightGreyTextColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
    var DarkGreyTextColor = UIColor(red: 67/255, green: 67/255, blue: 67/255, alpha: 1.0)
}