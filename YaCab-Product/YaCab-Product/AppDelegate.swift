//
//  AppDelegate.swift
//  YaCab-Product
//
//  Created by Mohammed Al Khalidi on 6/7/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import ContactsUI
//var storeController : InAppPurchasesViewController = InAppPurchasesViewController()
///contains the opponent user of a push notification or an in app notification
var pushOpponentUser : QBUUser?
///contains whether the push notification or in app notification is a negotiation message or just a free chat message
var pushOpponentIsNeg : Bool = false
///contains the dictionary that comes in the push notification when its received
var pushOpponentUserInfo : [NSObject : AnyObject]?
///boolean to indicate that the user still hasn't logged in
var waitForCurrentViewController = false
///contains the revealcontroller of the application. It allows us to show the in-app notifications
var globalRevealViewController : SWRevealViewController?
/**
method that calculates the number of contact requests the user has.
- Parameters:
    - userID: contains the userID of the user that we want to fetch the number of contact requests for.
    - completion: (numberOfRequests: Int): contains the number of friend requests of the user.
*/
func getNumberOfContactRequestsForUserWithID(userID : UInt, completion : (numberOfrequests : Int) -> Void){
    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
    extendedRequest.setValue(userID, forKey: "receiverID")

    QBRequest.objectsWithClassName("ContactRequest", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, requests : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
        completion(numberOfrequests: requests.count)
        }) { (response : QBResponse!) -> Void in
            completion(numberOfrequests: 0)
    }
}
/**
method that calculates the number of current carpoolers the user has.
- Parameters:
    - userID: contains the userID of the user that we want to fetch the number of carpoolers for.
    - completion: (numberOfCarpools: Int): contains the number of carpools of the user.
*/
func getNumberOfCarpoolersForUserWithID(userID : UInt, completion : (numberOfCarpools : Int) -> Void){
    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
    extendedRequest.setValue(userID, forKey: "user_id[or]")
    extendedRequest.setValue(userID, forKey: "opponentID[or]")
    extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState")
    extendedRequest.setValue(true, forKey: "isActive")
    QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
        completion(numberOfCarpools: carpools.count)
        }) { (response : QBResponse!) -> Void in
        completion(numberOfCarpools: 0)
    }
}
/**
method that calculates the number of negotiations the user has.
- Parameters:
    - userID: contains the userID of the user that we want to fetch the number of negotiations for.
    - completion: (numberOfNegotiations: Int): contains the number of negotiations of the user.
*/
func getNumberOfOngoingNegotiationsForUserWithUserID(userID : UInt, completion : (numberOfNegotiations : Int) -> Void){
    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
    extendedRequest.setValue(userID, forKey: "user_id[or]")
    extendedRequest.setValue(userID, forKey: "opponentID[or]")
    extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState[ne]")
    extendedRequest.setValue(true, forKey: "isActive")
    QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
        completion(numberOfNegotiations: carpools.count)
        }) { (response : QBResponse!) -> Void in
            completion(numberOfNegotiations: 0)
    }
}
/**
method that gets the carpoolers of a user as objects
*/
func getCarpoolers(){
    currentCarpoolers = []
    let extendedRequest : NSMutableDictionary = NSMutableDictionary()
    extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "user_id[or]")
    extendedRequest.setValue(DatabaseHelper().FetchUser().ID, forKey: "opponentID[or]")
    extendedRequest.setValue(CarpoolStates.FREE.rawValue, forKey: "currentState")
    extendedRequest.setValue(true, forKey: "isActive")
    QBRequest.objectsWithClassName("Carpool", extendedRequest: extendedRequest, successBlock: { (response : QBResponse!, carpools : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
        numberOfCurrentCarpoolers = carpools.count
        for carp in carpools{
            if let carpool = carp as? QBCOCustomObject{
                var userID : UInt = carpool.fields.valueForKey("opponentID") as! UInt
                if DatabaseHelper().FetchUser().ID == userID{
                    userID = carpool.userID
                }
                QBRequest.userWithID(userID, successBlock: { (response : QBResponse!, carpooler : QBUUser!) -> Void in
                    let isVisible = false
                    currentCarpoolers.append(Carpooler(user: carpooler , isVisible: isVisible))
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        
                })
            }
        }
        }) { (response : QBResponse!) -> Void in
    }
}
///delegate for the current number of carpoolers. For the side menu
var currentNumberOfCarpoolersDelegate : CurrentNumberOfCarpoolersDelegate?
///delegate for the current number of negotiations. For the side menu
var currentNumberOfNegotiationsDelegate : CurrentNumberOfNegotiationsDelegate?
///delegate for the current number of negotiations. For the Ride Requests list
var currentNumberOfNegotiationsInRideRequestsDelegate : CurrentNumberOfNegotiationsDelegate?
///delegate for the current number of carpoolers. For the Chats List
var currentNumberOfCarpoolersInChatListDelegate : CurrentNumberOfCarpoolersDelegate?
///delegate for the current number of contact requests. For the ContactsViewController
var currentNumberOfContactRequestsDelegate : CurrentNumberOfContactRequestsDelegate?
///array holding the current carpoolers of the user if the user has any.
var currentCarpoolers : [Carpooler] = []
///holds of the number of current carpoolers
var numberOfCurrentCarpoolers : Int = 0{
willSet{
    
}
didSet{


    if currentNumberOfCarpoolersDelegate != nil{
        currentNumberOfCarpoolersDelegate?.currentNumberOfCarpoolersDidChange()
    }
    if currentNumberOfCarpoolersInChatListDelegate != nil{
        currentNumberOfCarpoolersInChatListDelegate?.currentNumberOfCarpoolersDidChange()
    }
}
}
///holds the number of negotiations
var numberOfNegotiations : Int = 0{
didSet{

    if currentNumberOfNegotiationsDelegate != nil{
        currentNumberOfNegotiationsDelegate?.currentNumberOfNegotiationsDidChange()
    }
    if currentNumberOfNegotiationsInRideRequestsDelegate != nil{
        currentNumberOfNegotiationsInRideRequestsDelegate?.currentNumberOfNegotiationsDidChange()
    }
}
}
///holds the number of friend requests
var numberOfFriendRequests : Int = 0{
didSet{
    if currentNumberOfContactRequestsDelegate != nil{
        currentNumberOfContactRequestsDelegate?.currentNumberOfContactRequestsDidChange()
    }
}
}
///protocol that is used to inform the delegate that the current number of carpoolers has changed
protocol CurrentNumberOfCarpoolersDelegate{
    func currentNumberOfCarpoolersDidChange()
}
///protocol that is used to inform the delegate that the current number of negotiations has changed
protocol CurrentNumberOfNegotiationsDelegate{
    func currentNumberOfNegotiationsDidChange()
}
///protocol that is used to inform the delegate that the current number of friend requests has changed
protocol CurrentNumberOfContactRequestsDelegate{
    func currentNumberOfContactRequestsDidChange()
}
/**
enum that has the themes of the application

---
- PassengerTheme: has the identifier for the passenger theme.
- DriverTheme: has the identifier for the driver theme.
*/
enum Theme : String {
    case PassengerTheme = "PassengerTheme", DriverTheme = "DriverTheme"
}
///protocol that is used to inform the delegate that the current theme of the application has changed
protocol CurrentThemeDelegate{
    func themeDidChange()
}
/**
method that is used to extract the custom data of the user and return it as a dictionary
- Parameters:
    - user: It is the user that we want to extract the custom data from
- Returns: the custom data of a user as NSDictionary OR nil in case it was not found
*/
func getCustomDataDictionaryFromUser(user : QBUUser) -> NSDictionary?{
    if user.customData != nil{
        let str : String = user.customData.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let data : NSData = str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        if let json: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary {
            if let _ = json.valueForKey("activeRouteID"){
                
            }
            else{
                json.setValue("", forKey: "activeRouteID")
            }
            if let numberOfSeats = json.valueForKey("numberOfSeats") as? Int{
                json.setValue(numberOfSeats.description,forKey: "numberOfSeats")
            }
            return json
        }
        
    }
    return nil
}
///contains the currentViewController in the application
var currentViewController : UIViewController?
///contains the current user's location
var currentUserLocation : QBLGeoData?
///contains the current user's heading
var currentUserHeading : CLHeading?
///timer for the location update of the currentUserLocation and currentUserHeading variables
var locationTimer = NSTimer()
///contains the current theme delegate. Will be informed of the change in the current theme
var currentThemeDelegate : CurrentThemeDelegate? = nil
///contains the current theme of the application
var currentTheme: String = Theme.PassengerTheme.rawValue{
willSet
{
    //print("newThemeWillBeSet")
    
}
didSet
{
    //NSLocationWhenInUseUsageDescription
    //print("newThemeWasSet")
    
    currentThemeDelegate?.themeDidChange()
    
}
}
///persistant cache for the images that the user downloads
var _imageCache : ESCache?
///persistant cache for the users that the user downloads
var _userCache : ESCache?
///persistant cache for the geocoded locations strings that the user downloads
var _userGeocodedLocationStringCache : ESCache?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, QBChatDelegate {
    ///location tracker that is used in the background of the application to continiously get the location of the user
    var backgroundLocationTracker : LocationTracker = LocationTracker()
    var window: UIWindow?
    //var upSwipe : UISwipeGestureRecognizer = UISwipeGestureRecognizer()
    var locationTimerCount = 4
    ///method that resets the application to the login screen, used after the user logs out
    func resetAppToFirstViewController(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.stopUpdatingLocation()
        backgroundLocationTracker.stopLocationTracking()
        window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InitialViewController") as! InitialViewController
        
    }
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let uniqueID : String = UIDevice.currentDevice().identifierForVendor!.description
        QBRequest.registerSubscriptionForDeviceToken(deviceToken, uniqueDeviceIdentifier: uniqueID, successBlock: { (respose : QBResponse!, ob :[AnyObject]!) -> Void in
            }) { (error : QBError!) -> Void in
                
        }
        
    }
    func chatDidReceiveContactAddRequestFromUser(userID: UInt) {
        getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
            numberOfFriendRequests = numberOfrequests
        })
    }
    
    
    
    
    
    ///method used to handle receiving push notifications
    func handlePushNotificationFromBackground(userInfo: [NSObject : AnyObject]){
        pushOpponentUserInfo = userInfo
        QBRequest.userWithID(userInfo["user_id"] as! UInt, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
            pushOpponentUser = user
            QBRequest.dialogsWithSuccessBlock({ (response : QBResponse!, dialogs : [AnyObject]!,set : Set<NSObject>!) -> Void in
                if pushOpponentUser != nil{
                    for dialog in dialogs{
                        if let d = dialog as? QBChatDialog{
                            for userID in d.occupantIDs{
                                if (userID as! UInt) == pushOpponentUser!.ID{
                                    
                                    var extendedRequest : [NSObject : AnyObject] = [NSObject : AnyObject]()
                                    
                                    let now : NSDate = NSDate()
                                    extendedRequest["date_sent[lte]"] = now.timeIntervalSince1970
                                    extendedRequest["sort_desc"] = "date_sent"
                                    extendedRequest["limit"] = 1;
                                    
                                    QBRequest.messagesWithDialogID(d.ID, extendedRequest: extendedRequest as [NSObject : AnyObject], forPage: QBResponsePage(limit: 1), successBlock: { (response : QBResponse!, messages : [AnyObject]!, responsePage : QBResponsePage!) -> Void in
                                        if messages != nil{
                                            if messages.count>0{
                                                if let message = messages[0] as? QBChatMessage{
                                                    if currentViewController != nil{
                                                        
                                                        if let isNeg = message.customParameters.valueForKey("isNeg") as? String{
                                                            if (isNeg == "0" || isNeg == "false"){
                                                                pushOpponentIsNeg = false
                                                                if currentViewController! is ChatViewController{
                                                                    if currentViewController?.presentedViewController != nil{
                                                                        currentViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: { () -> Void in
                                                                            
                                                                        })
                                                                    }
                                                                    if !((currentViewController as! ChatViewController).opponentUser.ID == pushOpponentUser!.ID){
                                                                        currentViewController?.navigationController?.popViewControllerAnimated(true)
                                                                    }
                                                                    
                                                                }
                                                                else{
                                                                    if currentViewController?.presentedViewController != nil{
                                                                        currentViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: { () -> Void in
                                                                            
                                                                        })
                                                                    }
                                                                    //currentViewController!.revealViewController().revealToggle(self)
                                                                    currentViewController!.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=ChatList", sender: currentViewController!.revealViewController().rearViewController)
                                                                    
                                                                }                                                               
                                                            }
                                                            else{
                                                                pushOpponentIsNeg = true
                                                                if currentViewController! is NegotiationsViewController{
                                                                    if !((currentViewController as! NegotiationsViewController).opponentUser.ID == pushOpponentUser!.ID){
                                                                        if currentViewController?.presentedViewController != nil{
                                                                            currentViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: { () -> Void in
                                                                                
                                                                            })
                                                                        }
                                                                        currentViewController!.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=RideRequests", sender: currentViewController!.revealViewController().rearViewController)
                                                                    }
                                                                }
                                                                else{
                                                                    //print("type not known")
                                                                    if currentViewController?.presentedViewController != nil{
                                                                        currentViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: { () -> Void in
                                                                            
                                                                        })
                                                                    }
                                                                    
                                                                    //currentViewController!.revealViewController().revealToggle(self)
                                                                    currentViewController!.revealViewController().rearViewController.performSegueWithIdentifier("sideMenuRevealRideRequestsListOrChatListViewControllerMode=RideRequests", sender: currentViewController!.revealViewController().rearViewController)
                                                                }
                                                                
                                                            }
                                                        }
                                                    }
                                                    else if currentViewController == nil{
                                                        waitForCurrentViewController = true
                                                    }
                                                }
                                            
                                            }
                                        }
                                        
                                        }, errorBlock: { (reponse : QBResponse!) -> Void in
                                            pushOpponentUser = nil
                                            pushOpponentUserInfo = nil
                                    })
                                }
                            }
                        }
                    }
                }
                }, errorBlock: { (response : QBResponse!) -> Void in
                    pushOpponentUser = nil
                    pushOpponentUserInfo = nil
            })
            }, errorBlock: { (response : QBResponse!) -> Void in
                pushOpponentUser = nil
                pushOpponentUserInfo = nil
        })
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        //print("remote received1")
        if application.applicationState == UIApplicationState.Active{

            
        }
        else if application.applicationState == UIApplicationState.Background || application.applicationState == UIApplicationState.Inactive {

            handlePushNotificationFromBackground(userInfo)
        }
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    }
    
    /**
    helper method for the sendNotification method
    - Parameters:
        - userInfo: has the user info of the sender of the notification
        - message: has the message that was sent by the opponent user to the current user
        - hasSenderLogin: boolean that indicates if the opponent user has a senderLogin
    */
    func sendNotificationHelper(userInfo : [NSObject : AnyObject], message : QBChatMessage, hasSenderLogin : Bool){
        if globalRevealViewController != nil{
            if hasSenderLogin{
                //TSMessage.showNotificationWithTitle("lol", type: TSMessageNotificationType.Success)
                if currentTheme == Theme.PassengerTheme.rawValue{
                    TSMessage.showNotificationInViewController(globalRevealViewController, title: message.customParameters["senderLogin"] as! String, subtitle: message.text, image: colorImage(UIImage(named: "Chat")!, color: UIColor.whiteColor()), type: TSMessageNotificationType.Success, duration: 8, callback: { () -> Void in
                        TSMessage.dismissActiveNotification()
                        self.handlePushNotificationFromBackground(userInfo as [NSObject : AnyObject])
                        }, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.Top, canBeDismissedByUser: true)
                }
                else{
                    TSMessage.showNotificationInViewController(globalRevealViewController, title: message.customParameters["senderLogin"] as! String, subtitle: message.text, image: colorImage(UIImage(named: "Chat")!, color: ApplicationThemeColor().DarkGreyTextColor), type: TSMessageNotificationType.Warning, duration: 8, callback: { () -> Void in
                        TSMessage.dismissActiveNotification()
                        self.handlePushNotificationFromBackground(userInfo as [NSObject : AnyObject])
                        }, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.Top, canBeDismissedByUser: true)
                }
            }
            else{
                if currentTheme == Theme.PassengerTheme.rawValue{
                    TSMessage.showNotificationInViewController(globalRevealViewController, title: message.senderID.description , subtitle: message.text, image: colorImage(UIImage(named: "Chat")!, color: UIColor.whiteColor()), type: TSMessageNotificationType.Success, duration: 8, callback: { () -> Void in
                        TSMessage.dismissActiveNotification()
                        self.handlePushNotificationFromBackground(userInfo as [NSObject : AnyObject])
                        }, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.Top, canBeDismissedByUser: true)
                }
                else{
                    TSMessage.showNotificationInViewController(globalRevealViewController, title: message.senderID.description, subtitle: "Hello! I would like to carpool with you", image: colorImage(UIImage(named: "Chat")!, color: ApplicationThemeColor().DarkGreyTextColor), type: TSMessageNotificationType.Warning, duration: 8, callback: { () -> Void in
                        TSMessage.dismissActiveNotification()
                        self.handlePushNotificationFromBackground(userInfo as [NSObject : AnyObject])
                        }, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.Top, canBeDismissedByUser: true)
                }
            }

        }

    }
    /**
    method used to send an in-app notification for the user.
    - Parameter message: it is the message that is to be displayed in the in-app notification
    */
    func sendNotification(message : QBChatMessage){
        var userInfo : [NSObject : AnyObject] = [NSObject : AnyObject]()
        userInfo["user_id"] = message.senderID
        if currentViewController != nil && !(currentViewController is ChatsListViewController) {
            if let senderLogin : String =  message.customParameters["senderLogin"] as? String{
                if !senderLogin.isEmpty{
                    sendNotificationHelper(userInfo, message: message, hasSenderLogin: true)
                }
                else{

                    sendNotificationHelper(userInfo, message: message, hasSenderLogin: false)
                }
                
            }
            else{
                sendNotificationHelper(userInfo, message: message, hasSenderLogin: false)
            }
        }
    }
    func chatDidLogin() {
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    }
    func chatDidReceiveMessage(message: QBChatMessage!) {
        if let isNeg = message.customParameters.valueForKey("isNeg") as? String{
            if isNeg == "1" || isNeg == "true"{
                if let currentState = message.customParameters.valueForKey("currentState") as? String{
                    if currentState == CarpoolStates.FREE.rawValue || currentState == CarpoolStates.CARPOOL_FINISHED.rawValue || currentState == CarpoolStates.CANCELLED.rawValue{
                        
                        getCarpoolers()
                        getNumberOfCarpoolersForUserWithID(currentUser.ID, completion: { (numberOfCarpools) -> Void in
                            numberOfCurrentCarpoolers = numberOfCarpools
                        })
                        getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                            numberOfNegotiations = numberOfNegotiation
                        })
                        if currentState == CarpoolStates.CANCELLED.rawValue{
                            QBRequest.userWithID(message.senderID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                let alert : UIAlertController = UIAlertController(title: "Alert", message: user.login + " has cancelled your carpool request. You can try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                                let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(action)
                                globalRevealViewController?.presentViewController(alert, animated: true, completion: nil)
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    
                            })
                        }
                        else if currentState == CarpoolStates.FREE.rawValue{
                            QBRequest.userWithID(message.senderID, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                                let alert : UIAlertController = UIAlertController(title: "Alert", message: user.login + " has accepted your carpool request.", preferredStyle: UIAlertControllerStyle.Alert)
                                let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(action)
                                globalRevealViewController?.presentViewController(alert, animated: true, completion: nil)
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    
                            })
                        }
                    }
                    else{
                        getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                            numberOfNegotiations = numberOfNegotiation
                        })
                    }
                }
            }
        }
        if currentViewController is ChatViewController || currentViewController is NegotiationsViewController{
            if currentViewController is ChatViewController{
                if message.senderID != (currentViewController as! ChatViewController).opponentUser.ID{
                    sendNotification(message)
                }
                else if message.senderID == (currentViewController as! ChatViewController).opponentUser.ID{
                    if let isNeg = message.customParameters.valueForKey("isNeg") as? String{
                        if isNeg == "1" || isNeg == "true"{
                            sendNotification(message)
                        }
                    }
                }
            }
            else if currentViewController is NegotiationsViewController{
                if message.senderID != (currentViewController as! NegotiationsViewController).opponentUser.ID{
                    sendNotification(message)
                }
                else if message.senderID == (currentViewController as! NegotiationsViewController).opponentUser.ID{
                    if let isNeg = message.customParameters.valueForKey("isNeg") as? String{
                        if isNeg == "0" || isNeg == "false"{
                            sendNotification(message)
                        }
                    }
                }
            }
        }
        else{
            sendNotification(message)
        }
    }
    ///method that initiates location services updates
    func initiateLocationServicesUpdates(){
        locationManager.requestAlwaysAuthorization()
        locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
    }
    ///method that deactivates location service updates
    func deactivateLocationServicesUpdates(){
        locationTimer.invalidate()
        locationManager.stopUpdatingLocation()
        locationManager.stopUpdatingHeading()
    }
    ///holds the location manager that will find the location of the user
    var locationManager = CLLocationManager()
    /**
    method that is used to send the location of the user to the server
    - Parameter userLocations: contains the old locations of the user that are to be deleted
    */
    func sendLocation(userLocations : [AnyObject]){
        //print("numberofuserlocations: " + userLocations.count.description)
        if userLocations.count == 0{
            locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
        }
        else if userLocations.count > 1 {
            for location in userLocations {
                if let loc = location as? QBLGeoData{
                    QBRequest.deleteGeoDataWithID(loc.ID, successBlock: { (response : QBResponse!) -> Void in
                        QBRequest.deleteGeoDataWithRemainingDays(1, successBlock: { (response : QBResponse!) -> Void in
                            
                            }, errorBlock: { (response : QBResponse!) -> Void in
                                
                        })
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            QBRequest.deleteGeoDataWithRemainingDays(1, successBlock: { (response : QBResponse!) -> Void in
                                
                                }, errorBlock: { (response : QBResponse!) -> Void in
                                    
                            })
                    })
                    
                }
            }

            
            QBRequest.createGeoData(currentUserLocation, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                //print("savedlocationtoserver")
                locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
                }) { (response : QBResponse!) -> Void in
                locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
            }
        }
        else if userLocations.count == 1{
            if let loc = userLocations[0] as? QBLGeoData{
                loc.latitude = currentUserLocation!.latitude
                loc.longitude = currentUserLocation!.longitude
                loc.status = NSDate().description
                QBRequest.updateGeoData(loc, successBlock: { (response: QBResponse!, geodata: QBLGeoData!) -> Void in
                    //print("LocationUpdated")
                    locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
                })
            }
        }
        else{
            QBRequest.createGeoData(currentUserLocation, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                //print("savedlocationtoserver")
                locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
                }) { (response : QBResponse!) -> Void in
                    locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("locationSenderManager"), userInfo: nil, repeats: false)
            }
        }
    }
    /**
    method that handles the location of the user before it is sent
    */
    func locationSenderManager(){
        if currentUserLocation != nil{
            let filter : QBLGeoDataFilter = QBLGeoDataFilter()
            filter.userID = DatabaseHelper().FetchUser().ID
            filter.lastOnly = false
            QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1,perPage: 100), successBlock: { (response : QBResponse!, userLocations : [AnyObject]!, responsePage: QBGeneralResponsePage!) -> Void in
                self.sendLocation(userLocations)
                }, errorBlock: { (response : QBResponse!) -> Void in
                    QBRequest.createGeoData(currentUserLocation, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                        }) { (response : QBResponse!) -> Void in
                            
                    }
            })
        }
        else{
            self.sendLocation([])
        }
    }
    /**
    method that sends the location to the server in the background
    - Parameters:
        - userLocations: contains the user's old locations
        - locationToBeSaved: contains the new location of the user that is to be sent to the server
    */
    func sendLocationOBJC(userLocations : [AnyObject], locationToBeSaved : QBLGeoData){
        if currentUser.login != nil{
            //print("numberofuserlocations: " + userLocations.count.description)
            if userLocations.count > 1 {
                for location in userLocations {
                    if let loc = location as? QBLGeoData{
                        QBRequest.deleteGeoDataWithID(loc.ID, successBlock: { (response : QBResponse!) -> Void in
                            }, errorBlock: { (response : QBResponse!) -> Void in
                        })
                        
                    }
                }
                QBRequest.deleteGeoDataWithRemainingDays(1, successBlock: { (response : QBResponse!) -> Void in
                    
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        
                })
                
                QBRequest.createGeoData(locationToBeSaved, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                    //print("savedlocationtoserver")
                    }) { (response : QBResponse!) -> Void in
                        
                }
            }
            else if userLocations.count == 1{
                if let loc = userLocations[0] as? QBLGeoData{
                    loc.latitude = locationToBeSaved.latitude
                    loc.longitude = locationToBeSaved.longitude
                    loc.status = NSDate().description
                    QBRequest.updateGeoData(loc, successBlock: { (response: QBResponse!, geodata: QBLGeoData!) -> Void in
                        //print("LocationUpdated")
                        }, errorBlock: { (response : QBResponse!) -> Void in
                            
                    })
                }
            }
            else{
                QBRequest.createGeoData(locationToBeSaved, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                    //print("savedlocationtoserver")
                    }) { (response : QBResponse!) -> Void in
                        
                }
            }
        }

    }
    /**
    method that handles the location of the user before it is sent
    */
    func locationSenderManagerOBJC(geodata : QBLGeoData){
        if currentUser.login == nil{
            let filter : QBLGeoDataFilter = QBLGeoDataFilter()
            filter.userID = DatabaseHelper().FetchUser().ID
            filter.lastOnly = false
            QBRequest.geoDataWithFilter(filter, page: QBGeneralResponsePage(currentPage: 1,perPage: 100), successBlock: { (response : QBResponse!, userLocations : [AnyObject]!, responsePage: QBGeneralResponsePage!) -> Void in
                //print("sucessResponse:" + response.description)
                self.sendLocationOBJC(userLocations, locationToBeSaved: geodata)
                }, errorBlock: { (response : QBResponse!) -> Void in
                    //print("errorResponse: " + response.description)
                    QBRequest.createGeoData(currentUserLocation, successBlock: { (response : QBResponse!, geoData : QBLGeoData!) -> Void in
                        //print("savedlocationtoserver")
                        }) { (response : QBResponse!) -> Void in
                            
                    }
            })
        }
    }
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        currentUserLocation = QBLGeoData()
        currentUserLocation!.longitude = newLocation.coordinate.longitude
        currentUserLocation!.latitude = newLocation.coordinate.latitude
    }
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        currentUserHeading = newHeading
    }
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        
        // Configure tracker from GoogleService-Info.plist.
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        gai.logger.logLevel = GAILogLevel.Verbose  // remove before app release
        
        
        
        QBSettings.setLogLevel(QBLogLevel.Nothing)
        do {try _imageCache = ESCache(name: "imageCache")
            var maxSize : UInt = 50
            _imageCache?.setMaximumNumberOfItemsInCache(&maxSize)
        }
        catch{
            //print(error)
        }
        do {try _userCache = ESCache(name: "userCache")}
        catch{
            //print(error)
        }
        do {try _userGeocodedLocationStringCache = ESCache(name: "_userGeocodedLocationStringCache")}
        catch{
            //print(error)
        }

        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        QBApplication.sharedApplication().applicationId = 24144
        QBConnection.registerServiceKey("hXwxY2eLpeD9MBV")
        QBConnection.registerServiceSecret("xLfyHzUNz6LaWbM")
        QBSettings.setAccountKey("Gc2HxW9c3qjj3K5GvHeQ")

        QBChat.instance().addDelegate(self)
        QBChat.instance().autoReconnectEnabled = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //locationManager.distanceFilter = 0.00000001
        locationManager.pausesLocationUpdatesAutomatically = false
        
        QBRequest.deleteGeoDataWithRemainingDays(1, successBlock: { (response : QBResponse!) -> Void in
            NSLog(response.description)
            }) { (response : QBResponse!) -> Void in
                
        }
        
        //AGPushNoteView.setDelegateForPushNote(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"orientationChanged:", name: UIDeviceOrientationDidChangeNotification, object: UIDevice.currentDevice())
        // Override point for customization after application launch.
        return true
    }
    func orientationChanged(notification : NSNotification){
        //AGPushNoteView.close()
    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //if currentUser
        if currentUser.login == nil{
            backgroundLocationTracker.userID = 0
            backgroundLocationTracker.stopLocationTracking()
            //backgroundLocationTracker.
            deactivateLocationServicesUpdates()
        }
        else if currentUser.login != nil{
            backgroundLocationTracker.userID = currentUser.ID
            backgroundLocationTracker.startLocationTracking()
            deactivateLocationServicesUpdates()
            QBChat.instance().logout()
            //QBChat.instance().sendPresence()
        }
        
    }
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
    }
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        if currentUser.login != nil{
            
            if openedSettings{
                openedSettings = false
                registerRemoteNotification()
                locationManager.requestAlwaysAuthorization()
            }
            
            
            let user : QBUUser = QBUUser()
            user.password = DatabaseHelper().FetchUser().password
            user.ID = currentUser.ID
            user.login = currentUser.login
            QBChat.instance().loginWithUser(user)
            QBChat.instance().streamManagementEnabled = true
            initiateLocationServicesUpdates()
            backgroundLocationTracker.stopLocationTracking()
            getCarpoolers()
            getNumberOfOngoingNegotiationsForUserWithUserID(user.ID, completion: { (numberOfNegotiation) -> Void in
                numberOfNegotiations = numberOfNegotiation
            })
            getNumberOfContactRequestsForUserWithID(user.ID, completion: { (numberOfrequests) -> Void in
                numberOfFriendRequests = numberOfrequests
            })
            let sessionExpiratioDate : NSDate = QBSession.currentSession().sessionExpirationDate
            let currentDate : NSDate = NSDate()
            let interval : NSTimeInterval = currentDate.timeIntervalSinceDate(sessionExpiratioDate)
            if(interval > 0){
                // recreate session here
                QBRequest.logInWithUserLogin(DatabaseHelper().FetchUser().login, password: DatabaseHelper().FetchUser().password, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                    currentUser = user
                    let user : QBUUser = QBUUser()
                    user.password = DatabaseHelper().FetchUser().password
                    user.ID = currentUser.ID
                    user.login = currentUser.login
                    QBChat.instance().loginWithUser(user)
                    }, errorBlock: { (response : QBResponse!) -> Void in
                    
                })
            }
        }
        
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        QBChat.instance().logout()
        QBSession().endSession()
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mycompany.YaCab_Product" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("YaCab_Product", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("YaCab_Product.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
    
}

