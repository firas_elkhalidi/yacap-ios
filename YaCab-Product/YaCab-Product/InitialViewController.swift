//
//  InitialViewController.swift
//  YaCab-Product
//
//  Created by Mohammed Al Khalidi on 6/11/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//
///method that registers the user to remote notifications
func registerRemoteNotification(){
    let application : UIApplication = UIApplication.sharedApplication()
    if application.respondsToSelector("registerUserNotificationSettings:"){
        let settings : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: ([UIUserNotificationType.Badge, UIUserNotificationType.Sound, UIUserNotificationType.Alert]), categories: nil)
        application.registerUserNotificationSettings(settings)
    }
}
/**
method that applies a gaussian filter to an image with a certain radius
- Parameters:
    - image: the image which will have the filter applied to it.
    - radius: radius of the gaussian filter effect.
*/
func imageWithGaussianFilter(image : UIImage, radius : NSNumber) -> UIImage{
    let context: CIContext = CIContext(options: nil)
    let inputImage: CIImage = CIImage(image: image)!
    let filter: CIFilter = CIFilter(name: "CIGaussianBlur")!
    filter.setValue(inputImage, forKey: kCIInputImageKey)
    filter.setValue(radius, forKey: "inputRadius")
    let result: CIImage = filter.valueForKey(kCIOutputImageKey)! as! CIImage
    let cgImage: CGImageRef = context.createCGImage(result, fromRect: inputImage.extent)
    return UIImage(CGImage: cgImage)
}
import UIKit
///current user that is signed in to the application
var currentUser : QBUUser = QBUUser()
/**
method that fixes the custom data of the user.
- Parameter user: the user that is holding the custom data that is to be fixed.
*/
func userCustomDataFixer(user : QBUUser){
    var updateUser : Bool = false
    let userCustomData : NSDictionary = getCustomDataDictionaryFromUser(user)!
    if let _: AnyObject = userCustomData.valueForKey("hasAC")
    {
        
    }
    else{
        userCustomData.setValue(false, forKey: "hasAC")
        updateUser = true
    }
    if let _ = userCustomData.valueForKey("activeRouteID"){
        
    }
    else{
        userCustomData.setValue("", forKey: "activeRouteID")
        updateUser = true
    }
    if let _ : AnyObject = userCustomData.valueForKey("carBlobID"){
        
    }
    else{
        userCustomData.setValue("", forKey: "carBlobID")
    }
    if let _ : AnyObject = userCustomData.valueForKey("organization"){
        
    }
    else{
        userCustomData.setValue("", forKey: "organization")
    }
    if updateUser{
        let error = NSErrorPointer()
        do {
            let newUserCustomDataJSON = try NSJSONSerialization.dataWithJSONObject(userCustomData, options: NSJSONWritingOptions.PrettyPrinted)
            let theJSONText = NSString(data: newUserCustomDataJSON,
                encoding: NSASCIIStringEncoding)
            user.customData = String(theJSONText!)
            let userParameters : QBUpdateUserParameters = QBUpdateUserParameters()
            userParameters.customData = String(theJSONText!)
            QBRequest.updateCurrentUser(userParameters, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                }, errorBlock: { (response : QBResponse!) -> Void in
            })

        } catch let error1 as NSError {
            error.memory = error1
        }
    }
}
class InitialViewController: UIViewController,QBChatDelegate {
    //var boxView : UIView = UIView()
    
//    override func shouldAutorotate() -> Bool {
//        return false
//    }
//    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
//        return UIInterfaceOrientation.Portrait
//    }
    
    ///activity indicator to indicate that the user is signing in
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    ///background image view that is found in the initial view controller
    @IBOutlet weak var splashScreenImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.


        //var resultImage: CIImage = gaussianBlurFilter["outputImage"]
        //splashScreenImageView.image = imageWithGaussianFilter(splashScreenImageView.image!, radius: 10)
        splashScreenImageView.image = UIImage(named : "splashScreenBlurred")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    ///method that indicates that the user has logged in to the chat block in QuickBlox
    func chatDidLogin() {
        let extendedRequest = NSMutableDictionary()
        extendedRequest.setObject("iOS", forKey: "platform")
        QBRequest.objectsWithClassName("Version", extendedRequest:extendedRequest, successBlock: { (response :QBResponse!, versions : [AnyObject]!, page : QBResponsePage!) -> Void in
            if versions != nil{
                if versions.count != 0{
                    print(versions)
                    if let currentVersion = versions[0] as? QBCOCustomObject{
                        if let currentVersionString = currentVersion.fields.valueForKey("version") as? String{
                            let versionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
                            //let buildNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
                            let fullVersionString = versionNumber// + "(" + buildNumber + ")"
                            NSLog("Application Version:" + fullVersionString)
                            if fullVersionString.substringToIndex(fullVersionString.rangeOfString(".")!.first!) == currentVersionString.substringToIndex(currentVersionString.rangeOfString(".")!.first!){
                                getCarpoolers()
                                getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                                    numberOfNegotiations = numberOfNegotiation
                                })
                                getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
                                    numberOfFriendRequests = numberOfrequests
                                })
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                
                                appDelegate.initiateLocationServicesUpdates()
                                //self.boxView.removeFromSuperview()
                                //print("Logged in")
                                QBChat.instance().removeDelegate(self)
                                if pushOpponentUserInfo != nil{
                                    let appdel = UIApplication.sharedApplication().delegate as! AppDelegate
                                    appdel.handlePushNotificationFromBackground(pushOpponentUserInfo!)
                                }
                                //QBChat.instance().removeAllDelegates()
                                self.performSegueWithIdentifier("alreadySignedInSegue", sender: self)
                            }
                            else{
                                let alert : UIAlertController = UIAlertController(title: "Oops", message: "Please update the application to the newest version", preferredStyle: UIAlertControllerStyle.Alert)
                                let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(action)
                                self.presentViewController(alert, animated: true, completion: nil)
                                self.activityIndicator.stopAnimating()
                                self.splashScreenImageView.image = UIImage(named:"splashScreen")
                            }
                        }
                    }
                }
            }
            }) { (response : QBResponse!) -> Void in
                print("failed to get version")
        }
    }
    func chatDidFailWithError(code: Int) {
        //print("fail")
    }
    /**
    method that logins the user to the chat block in QuickBlox.
    - Parameters:
        - userID: holds the user's ID.
        - password: hold the user's password.
    */
    func chatLogin(userID : UInt, password : String){
        registerRemoteNotification()
        QBChat.instance().addDelegate(self)
        QBChat.instance().loginWithUser(currentUser)
        QBChat.instance().streamManagementEnabled = true
    }
    func pushNoteDidAppear() {
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyRouteViewController")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as![NSObject : AnyObject])
        if DatabaseHelper().FetchUser().password.isEmpty{
                self.performSegueWithIdentifier("notSignedInSegue", sender: self)
            }
            else{
                
                let userLogin : NSString  = DatabaseHelper().FetchUser().login
                currentUser.login = userLogin as String
                let userPassword : NSString = DatabaseHelper().FetchUser().password
                currentUser.password = userPassword as String

                let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
                activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                activityView.startAnimating()
                

                self.view.userInteractionEnabled = false

                let sessionParameters : QBSessionParameters = QBSessionParameters()
                sessionParameters.userLogin = userLogin as String
                sessionParameters.userPassword = userPassword as String
                QBRequest.logInWithUserLogin(userLogin as String, password: userPassword as String, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                    userCustomDataFixer(user)
                    currentUser = user
                    currentUser.password = DatabaseHelper().FetchUser().password
                    if let _ = getCustomDataDictionaryFromUser(user) {
                        if let data = getCustomDataDictionaryFromUser(user) {
                            if let isDriver = data.valueForKey("isDriver") as? Bool{
                                if isDriver{
                                    currentTheme = Theme.DriverTheme.rawValue
                                }
                                else{
                                    currentTheme = Theme.PassengerTheme.rawValue
                                }
                            }
                            else if let isDriverString = data.valueForKey("isDriver") as? String{
                                if isDriverString == "1" || isDriverString == "true"{
                                    currentTheme = Theme.DriverTheme.rawValue
                                }
                                else{
                                    currentTheme = Theme.PassengerTheme.rawValue
                                }
                            }
                        }
                    }
                    self.chatLogin(currentUser.ID , password: userPassword as String)
                    }, errorBlock: { (response : QBResponse!) -> Void in
                        //self.boxView.removeFromSuperview()
                        NSLog("error: %@", response.error);
                        self.view.userInteractionEnabled = true
                        var alert : UIAlertController = UIAlertController()
                        let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                        if let _ = response.error.reasons{
                            if  response.error.reasons.description.rangeOfString("Unauthorized") != nil{
                                DatabaseHelper().deleteCredentials()
                                self.performSegueWithIdentifier("notSignedInSegue", sender: self)
                            }
                        }
                        else{
                            alert = UIAlertController(title: "Oops", message: "Please Check Your Connectivity!", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                })
                }
        
        
    }

    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
