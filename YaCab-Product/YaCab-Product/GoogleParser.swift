//
//  GoogleParser.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 7/10/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import Foundation
class GoogleParser {

    func returnJSONDictionary(stringURL : String, completion : (dictionary : NSDictionary,status : Bool)->Void){
        let urlRequest : NSURLRequest = NSURLRequest(URL: NSURL(string: stringURL)!)
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) { (reponse : NSURLResponse?, data : NSData?, error : NSError?) -> Void in
            if error == nil{
                if let dict: NSDictionary = ((try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary){
                    if let _ = dict["routes"]{
                        completion(dictionary: dict,status : true)
                    }
                    else{
                        completion(dictionary: NSDictionary(), status : false)
                    }
                    
                }

            }
            else{
                NSLog(error.debugDescription)
                completion(dictionary: NSDictionary(), status : false)
            }
        }
    }
}