//
//  InAppPurchasesViewController.swift
//  YaCab-Product
//
//  Created by Firas Al Khatib Al Khalidi on 8/11/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//
import UIKit
import StoreKit
protocol InAppPurchasesViewControllerDelegate {
    
    func didBuy(collectionIndex: Int)
    
}


class InAppPurchasesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKProductsRequestDelegate, SKPaymentTransactionObserver,CurrentThemeDelegate {
    @IBOutlet weak var tblProducts: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    func themeDidChange() {
        setTheme()
    }
    func setTheme(){
        if currentTheme == Theme.PassengerTheme.rawValue{
            menuButton.tintColor = ApplicationThemeColor().GreenThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            menuButton.tintColor = ApplicationThemeColor().YellowThemeColor
        }
    }
    override func viewWillDisappear(animated: Bool) {
        tblProducts.reloadData()
        tblProducts.delegate = nil
        tblProducts.dataSource = nil
    }
    
    var delegate: InAppPurchasesViewControllerDelegate?
    
    var productIDs: Array<String!> = []
    
    var productsArray: Array<SKProduct!> = []
    
    var selectedProductIndex: Int!
    
    var transactionInProgress = false
    
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "InAppPurchasesViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    override func viewDidLoad() {
        //storeController = self
        currentViewController = self
        super.viewDidLoad()
        self.tblProducts.tableFooterView = UIView(frame: CGRectZero)
        self.view.userInteractionEnabled = false
        // Do any additional setup after loading the view.
        currentThemeDelegate = self
        tblProducts.delegate = self
        tblProducts.dataSource = self
        setTheme()
        if self.revealViewController() != nil && menuButton != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        QBRequest.objectsWithClassName("Product" , successBlock: { (response: QBResponse!, products: [AnyObject]!) -> Void in
            for product in products{
                if let prod = product as? QBCOCustomObject{
                    self.productIDs.append((prod.fields.valueForKey("productID") as! Int).description)
                }
            }
            self.requestProductInfo()
            SKPaymentQueue.defaultQueue().addTransactionObserver(self)
            }) { (response: QBResponse!) -> Void in
            
        }
        // Replace the product IDs with your own values if needed.

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: IBAction method implementation
    
    @IBAction func dismiss(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: UITableView method implementation
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentViewController is InAppPurchasesViewController{
            return productsArray.count
        }
        else {
            return 0
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("idCellProduct", forIndexPath: indexPath) 
        
        let product = productsArray[indexPath.row]
        cell.textLabel?.text = product.localizedTitle
        cell.detailTextLabel?.text = product.localizedDescription
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedProductIndex = indexPath.row
        self.view.userInteractionEnabled = false
        showActions()
        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
    }
    
    
    // MARK: Custom method implementation
    
    func requestProductInfo() {
            if SKPaymentQueue.canMakePayments() {
                _ = NSSet(array: productIDs)
                //var productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as Set<NSObject>)
                
                //productRequest.delegate = self
                //productRequest.start()
            }
            else {
                //print("Cannot perform In App Purchases.")
            }
            self.view.userInteractionEnabled = true
    }
    
    
    func showActions() {
        if transactionInProgress {
            return
        }
        
        let actionSheetController = UIAlertController(title: "IAPDemo", message: "What do you want to do?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let buyAction = UIAlertAction(title: "Buy", style: UIAlertActionStyle.Default) { (action) -> Void in
            let payment = SKPayment(product: self.productsArray[self.selectedProductIndex] as SKProduct)
            SKPaymentQueue.defaultQueue().addPayment(payment)
            self.transactionInProgress = true
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in
            
        }
        
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        
        presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: SKProductsRequestDelegate method implementation
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        if currentViewController is InAppPurchasesViewController{
            if response.products.count != 0 {
                for product in response.products {
                    productsArray.append(product )
                }
                
                tblProducts.reloadData()
            }
            else {
                //print("There are no products.")
            }
            
            if response.invalidProductIdentifiers.count != 0 {
                //print(response.invalidProductIdentifiers.description)
            }
            self.view.userInteractionEnabled = true
        }
    }
    
    
    // MARK: SKPaymentTransactionObserver method implementation
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        if currentViewController is InAppPurchasesViewController{
            for transaction in transactions {
                switch transaction.transactionState {
                case SKPaymentTransactionState.Purchased:
                    //print("Transaction completed successfully.")
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                    transactionInProgress = false
                    delegate!.didBuy(selectedProductIndex)
                    
                    
                case SKPaymentTransactionState.Failed:
                    //print("Transaction Failed");
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                    transactionInProgress = false
                    
                default:
                    print(transaction.transactionState.rawValue)
                }
            }
            self.view.userInteractionEnabled = true
        }
    }
}
