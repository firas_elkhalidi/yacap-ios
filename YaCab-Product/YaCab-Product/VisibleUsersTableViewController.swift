//
//  VisibleUsersTableViewController.swift
//  YaCab-Product
//
//  Created by Mohammed Al Khalidi on 6/9/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
import MapKit
class VisibleUsersTableViewController: UITableViewController {
    var visibleUsers : [QBLGeoData] = []
    var selectedUserRow : Int = -1
    var boxView : UIView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        // register new tableviewcell
        var nib = UINib(nibName: "VisibleUsersTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "reuseIdentifier")
        //
        
        if !QBChat.instance().isLoggedIn(){
            QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //QBChat.instance().addDelegate(self)
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor.yellowColor()
        self.refreshControl?.tintColor = UIColor.blackColor()
        self.refreshControl?.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        print("Visibleusers")
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        var textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.grayColor()
        textLabel.text = "Getting Users"
        self.view.userInteractionEnabled = false
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.tableView.addSubview(boxView)
        getData()
    }
    
    // MARK: - Table view data source
    
    
    func getData(){
        if currentUserLocation != nil {
            print("refreshing data")
            self.view.userInteractionEnabled = false
            visibleUsers = []
            tableView.reloadData()
            var userFilter : QBLGeoDataFilter = QBLGeoDataFilter()
            userFilter.radius = CGFloat(currentRadius)
            userFilter.currentPosition = currentUserLocation!.location().coordinate
            //userFilter.minCreatedAt = NSDate(timeInterval: -10, sinceDate: NSDate())
            //userFilter.maxCreatedAt = NSDate()
            userFilter.lastOnly = true
            var responsePage : QBGeneralResponsePage = QBGeneralResponsePage(currentPage: 1, perPage: 70)
            
            QBRequest.geoDataWithFilter(userFilter, page: responsePage, successBlock: { (response : QBResponse!, userLocations : [AnyObject]!,responsePage : QBGeneralResponsePage!) -> Void in
                for geoData in userLocations{
                    if let element = geoData as? QBLGeoData{
                        //if element.userID != DatabaseHelper().FetchUser().ID{
                        self.visibleUsers.append(element)
                        //}
                    }
                }
                self.tableView.reloadData()
                
                self.refreshControl?.endRefreshing()
                self.boxView.removeFromSuperview()
                self.view.userInteractionEnabled = true
                
                }) { (response : QBResponse!) -> Void in
                    NSLog(response.error.description)
                    self.boxView.removeFromSuperview()
                    self.refreshControl?.endRefreshing()
                    self.view.userInteractionEnabled = true
            }
        }
    }
    
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return visibleUsers.count
    }
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        var actions : [UITableViewRowAction] = []
        var profileAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Profile") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
            self.selectedUserRow = indexPath.row
            self.performSegueWithIdentifier("goToProfileFromVisibleUsersSegue", sender: self)
        }
        profileAction.backgroundColor = UIColor.purpleColor()
        var chatAction : UITableViewRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Chat") { ( action : UITableViewRowAction,indexPath : NSIndexPath) -> Void in
            self.selectedUserRow = indexPath.row
            self.performSegueWithIdentifier("goToChatFromVisibleUsersSegue", sender: self)
        }
        chatAction.backgroundColor = UIColor.orangeColor()
        actions.append(profileAction)
        actions.append(chatAction)
        return actions
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! VisibleUsersTableViewCell
        let selectedUser = visibleUsers[indexPath.row]
        cell.usernameLabel.text = selectedUser.user.login
        //cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        var selectedUserMapItem : MKMapItem = MKMapItem()
        let geocoder : CLGeocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(selectedUser.location(), completionHandler: {
            (placemarks:[AnyObject]?, error:NSError?) -> Void in
            if placemarks?.count > 0 {
                if let placemark: CLPlacemark = placemarks![0] as? CLPlacemark {
                    selectedUserMapItem =  MKMapItem(placemark: MKPlacemark(placemark: placemark))
                    cell.userCurrentLocationLabel.text = selectedUserMapItem.name
                }
            }
        })
        var statusBlock : QBRequestStatusUpdateBlock = { (request : QBRequest!, status : QBRequestStatus!) in
            NSLog("PercentageComplete:" + status.percentOfCompletion.description);
        }
        QBRequest.TDownloadFileWithBlobID(UInt(selectedUser.user.blobID), successBlock: { (response : QBResponse!,data : NSData!) -> Void in
            
            cell.userProfileImageView.image = UIImage(data: data)
            }, statusBlock: statusBlock) { (response : QBResponse!) -> Void in
                cell.userProfileImageView.image = UIImage(named: "placeholder.jpg")
        }
        
        
        return cell
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 72.0
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //        if editingStyle == .Delete {
        //            // Delete the row from the data source
        //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        //        } else if editingStyle == .Insert {
        //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //        }
    }
    
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToChatFromVisibleUsersSegue"{
            print("chatnavigation")
            var navigationController = segue.destinationViewController as! ChatViewNavigationViewController
            var chatViewController : ChatViewController = navigationController.topViewController as! ChatViewController
            chatViewController.opponentUser = visibleUsers[selectedUserRow].user
            
            
        }
        else if let profileViewController : ProfileViewController  = segue.destinationViewController as? ProfileViewController{
            profileViewController.selectedUser = visibleUsers[selectedUserRow].user
        }
    }
    
    
}
