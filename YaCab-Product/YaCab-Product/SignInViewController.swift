//
//  SignInViewController.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController,QBChatDelegate {
    ///instance of the image view set next to the password textfield(lock)
    @IBOutlet weak var passwordTextFieldImageView: UIImageView!
    ///instance of the image view set next to the user email textfield(mail)
    @IBOutlet weak var userTextFieldImageView: UIImageView!
    ///instance of the view holding the userTextFieldImageView
    @IBOutlet weak var userTextFieldExtraView: UIView!
    ///instance of the view holding the passwordTextFieldImageView
    @IBOutlet weak var passwordTextFieldExtraView: UIView!
    /**
    method that logins the user to the chat block in QuickBlox.
    - Parameters:
        - userID: holds the user's ID.
        - password: hold the user's password.
    */
    func chatLogin(userID : UInt, password : String){
        QBChat.instance().addDelegate(self)
        QBChat.instance().loginWithUser(currentUser)
        QBChat.instance().streamManagementEnabled = true
    }
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    ///method that is run when the user is logged in to the chat block in QuickBlox.
    func chatDidLogin() {
        let extendedRequest = NSMutableDictionary()
        extendedRequest.setObject("iOS", forKey: "platform")
        QBRequest.objectsWithClassName("Version", extendedRequest:extendedRequest, successBlock: { (response :QBResponse!, versions : [AnyObject]!, page : QBResponsePage!) -> Void in
            if versions != nil{
                if versions.count != 0{
                    print(versions)
                    if let currentVersion = versions[0] as? QBCOCustomObject{
                        if let currentVersionString = currentVersion.fields.valueForKey("version") as? String{
                            let versionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
                            //let buildNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
                            let fullVersionString = versionNumber// + "(" + buildNumber + ")"
                            NSLog("Application Version:" + fullVersionString.substringToIndex((fullVersionString.rangeOfString(".")?.first!)!))
                            if fullVersionString.substringToIndex(fullVersionString.rangeOfString(".")!.first!) == currentVersionString.substringToIndex(currentVersionString.rangeOfString(".")!.first!){
                                getCarpoolers()
                                getNumberOfOngoingNegotiationsForUserWithUserID(currentUser.ID, completion: { (numberOfNegotiation) -> Void in
                                    numberOfNegotiations = numberOfNegotiation
                                })
                                getNumberOfContactRequestsForUserWithID(currentUser.ID, completion: { (numberOfrequests) -> Void in
                                    numberOfFriendRequests = numberOfrequests
                                })
                                self.activityIndicator.stopAnimating()
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                appDelegate.initiateLocationServicesUpdates()
                                self.view.userInteractionEnabled = true
                                self.performSegueWithIdentifier("segueToSlideMenuTableViewController", sender: self)
                            }
                            else{
                                let alert : UIAlertController = UIAlertController(title: "Oops", message: "Please update the application to the newest version", preferredStyle: UIAlertControllerStyle.Alert)
                                let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
                                alert.addAction(action)
                                self.presentViewController(alert, animated: true, completion: nil)
                                self.activityIndicator.stopAnimating()
                                self.view.userInteractionEnabled = true
                            }
                        }
                    }
                }
            }
            }) { (response : QBResponse!) -> Void in
                print("failed to get version")
        }
        
        
        

        
        
    }
    ///instance of the forgot my password button that will present the user with the ForgotMyPasswordViewController
    @IBOutlet weak var forgotPasswordButton: UIButton!
    ///method that ends the editing in the view controller
    func endEditing(){
        self.view.endEditing(true)
    }
    ///instance of the background image view of the sign in screen
    @IBOutlet weak var backgroundImageView: UIImageView!
    ///instance of the scrollView that is in the view
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    ///method that sets the theme of the view controller
    func setTheme(){
        self.view.backgroundColor = ApplicationThemeColor().GreyThemeColor
        self.scrollView.backgroundColor = UIColor.clearColor()
        forgotPasswordButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        forgotPasswordButton.setTitle("FORGOT PASSWORD?", forState: UIControlState.Normal)
        signInButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        //signUpButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        signInButton.setAttributedTitle(NSAttributedString(string: "LOGIN", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().WhiteThemeColor]), forState: UIControlState.Normal)
        signUpButton.setTitle("NEW HERE? SIGN UP", forState: UIControlState.Normal)
        signUpButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        //signUpButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        //forgotPasswordButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        //signUpButton.setAttributedTitle(NSAttributedString(string: "NEW HERE? SIGN UP", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().WhiteThemeColor]), forState: UIControlState.Normal)
        usernameTextField.textColor = ApplicationThemeColor().GreenThemeColor
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        passwordTextField.textColor = ApplicationThemeColor().GreenThemeColor
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
        usernameTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        passwordTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
        userTextFieldImageView.image = userTextFieldImageView.image?.imageWithRenderingMode( UIImageRenderingMode.AlwaysTemplate)
        userTextFieldImageView.tintColor = ApplicationThemeColor().GreenThemeColor
        passwordTextFieldImageView.image = passwordTextFieldImageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        passwordTextFieldImageView.tintColor = ApplicationThemeColor().GreenThemeColor
        userTextFieldImageView.backgroundColor = UIColor.clearColor()
        passwordTextFieldImageView.backgroundColor = UIColor.clearColor()
        
        
    }
    ///instance of the sign in button
    @IBOutlet weak var signInButton: UIButton!
    ///instance of the sign up button
    @IBOutlet weak var signUpButton: UIButton!
    ///instance of the username text field that is used to hold the email of the user
    @IBOutlet weak var usernameTextField: UITextField!
    ///instance of the username text field that is used to hold the password of the user
    @IBOutlet weak var passwordTextField: UITextField!
    ///method that is used to register the user to remote notifications
    func registerRemoteNotification(){
        let application : UIApplication = UIApplication.sharedApplication()
        if application.respondsToSelector("registerUserNotificationSettings:"){
            let settings : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: ([UIUserNotificationType.Badge, UIUserNotificationType.Sound, UIUserNotificationType.Alert]), categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    /**
    method that detects that the sign in button was pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func signinButtonPressed(sender: UIButton) {
        var alert : UIAlertController = UIAlertController()
        let action : UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        if usernameTextField.text!.isEmpty || passwordTextField.text!.isEmpty{
            let arrowImageView : UIImageView = UIImageView(image: UIImage(named: "downArrow")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate))
            arrowImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
            arrowImageView.frame = CGRect(x:(self.view.frame.width/2) - 15, y: 0.0, width: 15.0, height: 15.0)
            self.scrollView.addSubview(arrowImageView)
            if (usernameTextField.text!.isEmpty){
                self.activityIndicator.stopAnimating()
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    arrowImageView.frame = CGRect(origin: CGPoint(x: arrowImageView.frame.origin.x+5, y: self.usernameTextField.frame.origin.y-10), size: arrowImageView.frame.size)
                    self.usernameTextField.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    self.userTextFieldImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
                    self.userTextFieldExtraView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    self.usernameTextField.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().WhiteThemeColor])
                    
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.3, animations: { () -> Void in
                            arrowImageView.alpha = 0
                            self.usernameTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                            self.userTextFieldImageView.tintColor = ApplicationThemeColor().GreenThemeColor
                            self.userTextFieldExtraView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                            self.usernameTextField.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
                            
                            }, completion :{(Bool) -> Void in
                            arrowImageView.removeFromSuperview()
                        })
                })
                self.view.userInteractionEnabled = true
            }
            if passwordTextField.text!.isEmpty{
                let arrowImageView : UIImageView = UIImageView(image: UIImage(named: "downArrow")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate))
                arrowImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
                arrowImageView.frame = CGRect(x:(self.view.frame.width/2) - 15, y: 0.0, width: 15.0, height: 15.0)
                self.scrollView.addSubview(arrowImageView)
                self.activityIndicator.stopAnimating()
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    arrowImageView.frame = CGRect(origin: CGPoint(x: arrowImageView.frame.origin.x+5, y: self.passwordTextField.frame.origin.y-12), size: arrowImageView.frame.size)
                    self.passwordTextField.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    self.passwordTextFieldImageView.tintColor = ApplicationThemeColor().WhiteThemeColor
                    self.passwordTextFieldExtraView.backgroundColor = ApplicationThemeColor().GreenThemeColor
                    self.passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().WhiteThemeColor])
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.3, animations: { () -> Void in
                            arrowImageView.alpha = 0
                            self.passwordTextField.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                            self.passwordTextFieldImageView.tintColor = ApplicationThemeColor().GreenThemeColor
                            self.passwordTextFieldExtraView.backgroundColor = ApplicationThemeColor().WhiteThemeColor
                            self.passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : ApplicationThemeColor().GreenThemeColor])
                            }, completion :{(Bool) -> Void in
                                arrowImageView.removeFromSuperview()
                        })
                })
                self.view.userInteractionEnabled = true
            }
        }
        else{
            QBRequest.logInWithUserEmail(usernameTextField.text, password: passwordTextField.text, successBlock: { (response : QBResponse!, user : QBUUser!) -> Void in
                userCustomDataFixer(user)
                currentUser = user
                currentUser.password = self.passwordTextField.text
                let databaseHelper : DatabaseHelper = DatabaseHelper()
                databaseHelper.SaveCredentials(currentUser.ID,login : currentUser.login,password: currentUser.password)
                if let data = getCustomDataDictionaryFromUser(user) {
                    if let isDriver = data.valueForKey("isDriver") as? Bool{
                        if isDriver{
                            currentTheme = Theme.DriverTheme.rawValue
                        }
                        else{
                            currentTheme = Theme.PassengerTheme.rawValue
                        }
                    }
                    else if let isDriverString = data.valueForKey("isDriver") as? String{
                        if isDriverString == "1" || isDriverString == "true"{
                            currentTheme = Theme.DriverTheme.rawValue
                        }
                        else{
                            currentTheme = Theme.PassengerTheme.rawValue
                        }
                    }
                }
                self.registerRemoteNotification()
                self.chatLogin(currentUser.ID, password: currentUser.password)
                
                }) { (response : QBResponse!) -> Void in
                    self.activityIndicator.stopAnimating()
                    NSLog("error: %@", response.error);
                    self.view.userInteractionEnabled = true
                    if let _ = response.error.reasons{
                        print(response.error.reasons.description)
                        if  response.error.reasons.description.rangeOfString("Unauthorized") != nil{
                            alert = UIAlertController(title: "Oops", message: "The email/password combination you have entered in incorrect.", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(action)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        alert = UIAlertController(title: "Oops", message: "Error occurred. Please check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            }
        }
    }
    /**
    method that detects that the sign up button was pressed
    - Parameter sender: holds the sender of the method
    */
    @IBAction func signupButtonPressed(sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var signupViewController : SignUpViewController = SignUpViewController()
        signupViewController = storyboard.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        //signupViewController.backgroundImage = self.backgroundImageView.image!
        self.presentViewController(signupViewController, animated: true, completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        setTheme()
        scrollView.backgroundColor = UIColor.clearColor()
        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("endEditing"))
        self.scrollView.addGestureRecognizer(tapGestureRecognizer)
        QBChat.instance().addDelegate(self)
        self.scrollView.contentSize.height = UIScreen.mainScreen().bounds.height - 20
        self.heightConstraint.constant = self.scrollView.contentSize.height - 20
        // Do any additional setup after loading the view.
    }
    ///holds an instance of the activity indicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidAppear(animated: Bool) {
        //currentViewController = self
        //self.scrollView.contentSize.height = 300
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "SignInViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
