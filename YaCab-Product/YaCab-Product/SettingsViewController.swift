//
//  SettingsViewController.swift
//  YaCab
//
//  Created by Mohammed Al Khalidi on 6/6/15.
//  Copyright (c) 2015 WhyApp. All rights reserved.
//

import UIKit
var openedSettings : Bool = false
class SettingsViewController: UIViewController,CurrentThemeDelegate {
    ///instance of the settingsButton
    @IBOutlet weak var settingsButton: UIButton!
    ///instance of the aboutButton
    @IBOutlet weak var aboutButton: UIButton!
    ///method that detects when the settingsButton is pressed
    @IBAction func settingsButtonPressed(sender: UIButton) {
//        if UIApplication.sharedApplication().isRegisteredForRemoteNotifications(){
//            registerRemoteNotification()
//        }
//        else{
//            unregisterUserFromPushNotifications()
//        }
        if let _ = UIApplicationOpenSettingsURLString as? String {
            if !(UIApplicationOpenSettingsURLString.isEmpty){
                let appSettings : NSURL = NSURL(string: UIApplicationOpenSettingsURLString)!
                openedSettings = true
                UIApplication.sharedApplication().openURL(appSettings)
            }
        }
    }
    
//    func unregisterUserFromPushNotifications(){
//        if UIApplication.sharedApplication().isRegisteredForRemoteNotifications(){
//            let uniqueID : String = UIDevice.currentDevice().identifierForVendor!.description
//            QBRequest.unregisterSubscriptionForUniqueDeviceIdentifier(uniqueID, successBlock: { (response : QBResponse!) -> Void in
//                UIApplication.sharedApplication().unregisterForRemoteNotifications()
//                self.settingsButton.setTitle("Register To Notifications", forState: UIControlState.Normal)
//                
//                }) { (error : QBError!) -> Void in
//                    
//            }
//        }
//
//    }
    ///hamburger button on the top left
    @IBOutlet var menuButton: UIBarButtonItem!
    ///method that detects when
    @IBAction func aboutButtonPressed(sender: UIButton) {
    }
    ///delegate method that tells the view controller to update its theme
    func themeDidChange() {
        setTheme()
    }
    ///method that sets the theme of the view
    func setTheme(){
        settingsButton.setTitle("Application Settings", forState: UIControlState.Normal)
        if currentTheme == Theme.PassengerTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().GreenThemeColor
            settingsButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            settingsButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
            aboutButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            aboutButton.backgroundColor = ApplicationThemeColor().GreenThemeColor
        }
        else if currentTheme == Theme.DriverTheme.rawValue{
            navigationController?.navigationBar.tintColor = ApplicationThemeColor().YellowThemeColor
            settingsButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            settingsButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
            aboutButton.setTitleColor(ApplicationThemeColor().DarkGreyTextColor, forState: UIControlState.Normal)
            aboutButton.backgroundColor = ApplicationThemeColor().YellowThemeColor
        }
    }
    override func viewDidAppear(animated: Bool) {
        currentViewController = self
        currentThemeDelegate = self
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "SettingsViewController")
        
        var builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIApplication.sharedApplication().isRegisteredForRemoteNotifications(){
            settingsButton.setTitle("Unregister From Notifications", forState: UIControlState.Normal)
        }
        else{
            settingsButton.setTitle("Register to Notifications", forState: UIControlState.Normal)
        }
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if !QBChat.instance().isLoggedIn(){
            QBChat.instance().loginWithUser(DatabaseHelper().FetchUser())
        }
        setTheme()
        // Do any additional setup after loading the view.
        
        
        
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
